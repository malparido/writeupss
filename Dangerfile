# Version 1.1.1

# Get mr basic information
mr_info = gitlab.mr_json
sltn_commit = gitlab.mr_title.include? 'sltn(sys)'

# MR under 300 deltas if commit is not solution
if git.lines_of_code > 300 && !sltn_commit
  failure 'MR must be under or equal to 300 deltas'
end

# Only one commit per MR
if git.commits.length > 1
  failure "This MR adds #{git.commits.length} commits.
    Only 1 commit per MR is allowed. Please use git squash"
end

# Check if the last pipeline was successful before opening MR
pipe = gitlab.api.merge_request_pipelines(mr_info['project_id'], mr_info['iid'])
if pipe[1].nil? || pipe[1].to_h['status'] != 'success'
  failure 'A MR must only be created after a successful CI run in your branch'
end

# Check if a solution MR modifies files outside systems folder
other_modified_files = git.modified_files.select { |i| i[%r{^(?!systems\/)}] }
if sltn_commit && !other_modified_files.empty?
  warn('This solution MR is modifying files outside the systems folder')
end
