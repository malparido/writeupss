[//]: # (Version 1.0)

# Writeups by [Fluid Attacks](https://fluidattacks.com/)

In this repo you will find an extensive collection
of documented vulnerabilities and hacks
made by [Fluid Attacks](https://fluidattacks.com/)'
hackers and trainees to several vulnerable applications,
especially designed to train people on
[Pen-Testing](https://en.wikipedia.org/wiki/Penetration_test).

## Getting Started

### Trainees

Hey there! We hope you have fun while hacking some
[ToE](https://en.wikipedia.org/wiki/Common_Criteria)'s! 😄

Also, please make sure you understand all the
[rules](https://fluidattacks.com/web/en/careers/technical-challenges/)
before you start posting new solutions to the repository.

Let the hacking begin! 😎

### Guests

Feel free to roam around the repository and ask any questions. 😄

Make sure to read our [License](#license) section
before using any data from this project.

## Structure

In this section we will be giving a brief description
of the most relevant files and folders
that can be found in the repository

* **systems:** Folder containing the hacking solutions.
  For more information about its structure,
  please visit this
  [link](https://fluidattacks.com/web/en/careers/technical-challenges/#structure)
* **build.nix:** This script allows to locally execute the
  [CI](#continuous-integration-tools).
  For more information please read this:
  [builds](https://fluidattacks.com/web/en/careers/technical-challenges/#builds)
* **nanoc/nanoc-local.sh:** This script allows to locally execute
  a docker container that hosts the nanoc site
  for debugging and testing purposes.
  For more information, please read this:
  [Nanoc local build](https://gitlab.com/fluidattacks/writeups/wikis/Nanoc-Local-Build)
* **templates:** Folder containing templates
  that must be followed in order to post new hacking solutions

## ToE's List

The vulnerable applications currently included are:

* [Bodgeit](https://github.com/psiinon/bodgeit)
* [OWASP Bricks](https://www.owasp.org/index.php/OWASP_Bricks)
* [bWAPP](http://www.itsecgames.com/)
* [Damn Vulnerable NodeJS Application (DVNA)](https://github.com/appsecco/dvna)
* [Damn Vulnerable Web Application (DVWA)](http://www.dvwa.co.uk/)
* [Firing Range](https://github.com/google/firing-range)
* [Gruyere](https://google-gruyere.appspot.com/)
* [OWASP Hackademic Challenges Project](https://www.owasp.org/index.php/OWASP_Hackademic_Challenges_Project)
* [Hackazon](https://github.com/rapid7/hackazon/wiki)
* [OWASP Juice Shop Project](https://www.owasp.org/index.php/OWASP_Juice_Shop_Project)
* [LAMPSecurity Training](https://sourceforge.net/projects/lampsecurity/)
* [Metasploitable 2](https://metasploit.help.rapid7.com/docs/metasploitable-2)
* [Mutillidae](http://www.irongeek.com/i.php?page=security/mutillidae-deliberately-vulnerable-php-owasp-top-10)
* [WackoPicko Vulnerable Website](https://github.com/adamdoupe/WackoPicko)
* [OWASP WebGoat Project](https://www.owasp.org/index.php/Category:OWASP_WebGoat_Project)
* [XSS Game](https://xss-game.appspot.com/)
* [Xtreme Vulnerable Web Application (XVWA)](https://github.com/s4n7h0/xvwa)

## Continuous Integration Tools

In [Fluid Attacks](https://fluidattacks.com/)
we strongly believe in the
[DevOps](https://theagileadmin.com/what-is-devops) philosophy
and are devoted to work with fast pace methodologies, such as
[Continuous Integration](https://www.thoughtworks.com/continuous-integration) (CI)

Some of the most important third-party tools
for this project's CI are:

* [Docker](https://www.docker.com/) - For our CI Containers
* [PyParsing](https://github.com/pyparsing/pyparsing) - Commit Messages parser
* [Pre-Commit](https://pre-commit.com/) - Multi-language linters
* [SCons](https://scons.org/) - Software Builder (Make on steroids)
* [Bats](https://github.com/sstephenson/bats) - Framework for unit testing

## License

This project is licensed under the:

**Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International Public License**.

Please follow this [link](https://gitlab.com/fluidattacks/writeups/blob/master/LICENSE.txt)
to know more about what you can (and can't) do
with the data stored in this project.
