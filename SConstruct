# -*- coding: utf-8 -*-

import os
import sys
import itertools
sys.path.append('./ci-scripts')
import file_search
import others_builder
import gherkin_builder
import folders_builder

# Guarantee scons version
EnsureSConsVersion(2, 3)

# Build default construction environment
env = Environment()

# Set external variables
env['ENV']['PATH'] = os.environ['PATH']
env['ENV']['HOME'] = os.environ['HOME']

# Set signature
env.Decider('MD5-timestamp')

# Dissable python warnings
env['ENV']['PYTHONWARNINGS'] = 'ignore'

# File change cache location
env.SConsignFile('scons-build/decider')
env.CacheDir('scons-build/cache')

# Find all required sources
dirs = ["systems/"]
gherkin_sources = file_search.find_sources('feature', 0, dirs)
others_sources = file_search.find_sources('lst', 1, dirs)
folder_sources = file_search.find_folders(dirs)

# Prep OTHERS targets
others_targets = []
for target in others_sources:
    others_targets.append("scons-build/" + target)
# Prep Gherkin targets
gherkin_targets = []
for target in gherkin_sources:
    gherkin_targets.append("scons-build/" + target)
# Prep Folder targets
folder_targets = []
for target in folder_sources:
    dir_path = os.path.dirname(os.path.realpath(target[0]))
    folder_targets.append("scons-build/" + dir_path + "/folder.txt")

# Builder - OTHERS
bothers_builder = Builder(action = others_builder.build_others)
env.Append(BUILDERS = {'Bothers' : bothers_builder})
for sr, tg in itertools.izip(others_sources, others_targets):
    bothers_run = env.Bothers(target = tg, source = sr)
env.Alias('others', others_targets)

# Builder - Gherkin
bgherkin_builder = Builder(action = gherkin_builder.build_gherkin)
env.Append(BUILDERS = {'Bgherkin' : bgherkin_builder})
for sr, tg in itertools.izip(gherkin_sources, gherkin_targets):
    gherkin_run = env.Bgherkin(target = tg, source = sr)
env.Alias('gherkin', gherkin_targets)

# Builder - Folders
bfolders_builder = Builder(action = folders_builder.build_folders)
env.Append(BUILDERS = {'Bfolders' : bfolders_builder})
for sr, tg in itertools.izip(folder_sources, folder_targets):
    bfolders_run = env.Bfolders(target = tg, source = sr)
env.Alias('folders', folder_targets)

# Enable explicit builds only
Default(None)
