#!/usr/bin/env nix-shell
#!nix-shell -i bash -p docker gitlab-runner

if [ -z ${DOCKER_USER+x} ]; then echo "DOCKER_USER not set"; exit 1; fi
if [ -z ${DOCKER_PASS+x} ]; then echo "DOCKER_PASS not set"; exit 1; fi

gitlab-runner exec docker test
