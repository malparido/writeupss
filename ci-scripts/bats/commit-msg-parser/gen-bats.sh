#!/bin/bash
bats=parse-commit.bats
script=../../parse_commit_msg.py
echo -e "#!/usr/bin/env bats\n" > $bats
file=testcases.lst
while read -r line
do
 if [[ $line = "[" ]]; then
   write=1
 elif [[ $line = "]" ]]; then
   write=0
 else
   if [[ $write = 1 ]]; then
     echo "$line" >> "$msg"
   else
     low=${line,,}
     msg=${low//[^[:alnum:]]/-}
     echo "@test \"$line\" {" >> $bats
     echo "run $script $msg" >> $bats
     if [[ $line = "OK"* ]]; then ret=0; else ret=1; fi
     echo -e "  [ \"\$status\" -eq $ret ]" >> $bats
     echo -e "  echo \$output\n}\n" >> $bats
     echo -n "" > "$msg"
   fi
 fi
done <"$file"
