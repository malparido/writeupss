#!/usr/bin/env bash

# shellcheck disable=SC2038

ERRORS=0

function error {
    echo -e "\\e[1;31mERRORS: ${1}\\e[0m" >&2
    ERRORS=1
}

# Whitelist check of valid paths
if find systems \
  | pcregrep -v '.*OTHERS|LINK|DATA|SPEC|TODO|LANG.*' \
  | pcregrep --color=always '[^a-z0-9\.\-\/]+'; then
  error "Character not allowed in path (only a-z, 0-9, y .-/)."
else
  echo "Path characters OK, continue checks..."
fi

# Check that there are no tabs anywhere
TABS=$(pcregrep --color -Inrl --exclude-dir={.git,builds,node_modules} \
  --exclude=.mailmap '\t' .)
if [[ -n $TABS ]]
then
    error "The following files contain tabs. Change them to spaces:"
    echo "$TABS"
else
  echo "Soft tabs check OK, continue checks..."
fi

# Find directories and files with name > 35 chars
if find systems | sed 's_^.*/__g' | pcregrep '.{36,}'; then
  error "All files and directories must be renamed to less than 35 characters."
else
  echo "Short filename check OK, continue checks..."
fi

# Validate Pykwalify toe data.yaml files with schema.yaml
if ! find systems/ -mindepth 1 -maxdepth 1 -type d | \
   xargs -I % pykwalify -d %/data.yaml -s schema.yaml > /dev/null; then
   error "Pykwalify checks failed."
else
   echo "Pykwalify checks OK, continue checks..."
fi

# Check that systems directory depth is three below systems
TOODEEP=$(find systems -mindepth 5)
if [[ -n $TOODEEP ]]; then
  error "Maximum depth allowed is three directories below systems."
  echo "See https://fluidattacks.com/web/es/empleos/retos-tecnicos/#estructura"
  echo "The following directories need to be fixed:"
  echo "$TOODEEP"
else
  echo "Systems directory depth check OK, continue checks..."
fi

EVIDENCE_FOLDERS_SYS=$(find systems/ -mindepth 3 -maxdepth 3 -type d)

#Check features existence for evidence folders in systems
for folder in $EVIDENCE_FOLDERS_SYS; do
  file=$folder.feature
  if [ ! -f "$file" ]; then
    error "$folder exists without $file. Evidence folders need feature files"
  fi
done
if [ -n "$ERRORS" ]; then
  echo "Features vs evidences check OK, continue checks...";
fi

#Check only png in evidence folders
for folder in $EVIDENCE_FOLDERS_SYS; do
  files=$(find "$folder" -type f)
  for file in $files; do
    file_mime=$(file --mime-type "$file" | cut -d ' ' -f 2)
    if [ "$file_mime" != "image/png" ]; then
      error "$file has invalid type. Only .png files allowed for evidence"
    fi
  done
done
if [ -n "$ERRORS" ]; then
  echo "Only png images for evidences check OK, continue checks...";
fi

exit ${ERRORS}
