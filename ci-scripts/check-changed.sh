#!/usr/bin/env bash

ERRORS=0
CHANGED=$(git diff --name-only HEAD origin/master)
TITLE=$(git log -1 --pretty=%s)
LANGS="(feature|py|cpp|c|java|cs|js|rb|vb|ml|scala|clj|d|hs|sh|php|lua|pas\
|m|r|rs|pl|pl6|go|fs|lisp|cl|f90|erl|pl|bat|bas)"

function error {
  echo -e "\\e[1;31!!m${1}\\e[0m" >&2
  ERRORS=1  # to activate set to 1. 0 test mode
}

echo "${CHANGED:-(None)}"
echo -e "\\e[1;31m^--Files modified vs origin/master.\\e[0m\\n"

# Check that solution gitlabuserlogin.ext belongs to commit.
# Skip in master and for non-solution commits.
if [[ $CI_COMMIT_REF_NAME != "master" ]] && [[ $TITLE = "solution"* ]]; then
    if ! echo "$CHANGED" | pcregrep -q "${GITLAB_USER_LOGIN}\\.[a-z0-9]*( |$)" \
    && [[ "$GITLAB_USER_LOGIN" != *"atfluid" ]]; then
      error "Solution name, likely one of the following:"
      echo "$CHANGED" | pcregrep --color "[[:alnum:]]*\\.$LANGS( |$)"
      echo "must be your Gitlab login ($GITLAB_USER_LOGIN)."
      echo "See https://fluidattacks.com/web/es\
/empleos/retos-tecnicos/#repositorio"
    else
      echo "Solution name check OK, continue checks..."
    fi
else
  echo "Rightfully skipping solution name check..."
fi

# Check that branch = gitlab user login
# If on master, don't fail pipeline, but issue a warning for local test.
if [[ $CI_COMMIT_REF_NAME = "master" ]]; then
  if [[ -z $GITLAB_USER_LOGIN ]]; then # if it's empty, running locally
    error "Don't commit to master."
    echo "See https://fluidattacks.com/web/es/empleos/retos-tecnicos/#envio"
  fi
else
  if [[ -n $GITLAB_USER_LOGIN ]]; then
    # Pipeline running from talent branch on integrator
    if [[ $CI_COMMIT_REF_NAME != "$GITLAB_USER_LOGIN" ]] &&
    [[ "$GITLAB_USER_LOGIN" != *"atfluid" ]]; then
      error "Branch name ($CI_COMMIT_REF_NAME) and \
Gitlab login ($GITLAB_USER_LOGIN) must be the same"
      echo "See https://fluidattacks.com/web/es/empleos/retos-tecnicos/#envio"
    else
        echo "Branch name OK, continue checks"
    fi
  else
    echo "Local test: make sure your branch name = your Gitlab login"
    # Local check pending $DOCKER_USER
  fi
fi

# Any file that is not .lst (or temporarily txt) in a solution commit
# is either the solution (login.xyz) or maybe an auxiliary script (helper.py).
# Check that these files are wrapped at column 80
if echo "$CHANGED" \
  | xargs pcregrep --color -nHu \
  --exclude={lst,txt,png,md,html,yaml,package-lock.json} '.{81,}' \
  2> /dev/null; then
  error "Solutions sources must be wrapped at column 80"
  echo "See https://fluidattacks.com/web/es/estilo/#fuente"
else
  echo "80 col check OK, continue checks..."
fi

# Run precommit on changed files
export PATH=$PATH:/usr/local/go/bin
if ! echo "$CHANGED" | xargs pre-commit run -v --files; then
  error "Precommit failed"
  echo "See (future:webpage docs|now:https://pre-commit.com/)"
else
  echo "Precommit passed, continue checks..."
fi

exit ${ERRORS}
#
# Linter output
# [rab@pav training]$ shellcheck check-changed.sh
# [rab@pav training]$ echo $?
# 0
# [rab@pav training]$
