#!/bin/bash

#bats tests
cd ci-scripts/bats/commit-msg-parser && \
  ./gen-bats.sh && \
  bats parse-commit.bats > /dev/null
RESULT=$?
rm bad---* ok---* parse-commit.bats
if  [[ $RESULT -ne 0 ]]; then
  echo "ERROR: bats tests for commit parser failed."
  exit 1
fi

#commitlint checks
if ! git log -1 --pretty=%B | npx commitlint; then exit 1; fi

#python commmit parser
cd ../../ && \
./parse_commit_msg.py
