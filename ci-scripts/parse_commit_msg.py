#!/usr/bin/env python

# pylint: disable= C0325, W0105, W0101, E1102, C0330

"""
-------------------------------------------------------------------------------
                       Pyparsing Commit Message Linter
-------------------------------------------------------------------------------
This script checks ONLY the last commit.
In particular, it does not check for multi-commit Merge Requests,
nor for branches that have not been rebased,
which are also a rejection reason.
A valid commit message must conform to templates/commit-msg-systems.txt
and in that case we call it a "solution-type" commit,
or start with one of the keywords
feat: fix: docs: style: refactor: test:
for internal work on the repository.

Local usage:
add your files:   $ git add xyz
commit them:      $ git commit
It helps if you have set your commit message template
to one of the provided templates.
After comitting, run this script:
$./parse_commit_msg.py
or
$python parse_commit_msg.py
If everything is OK you will get
"Solution commit message OK, continue checks..."
or
"Other commit message OK, continue checks...")
If not, you will see an error message pointing why the commit message is wrong.
In that case you can use
$git commit --amend
to fix the last commit message and re-run the parser.

It is easier to understand the grammar if read from top to bottom, starting
with the top-level parsers and working your way back to the atomic parsers.
"""

import sys
import subprocess
from pyparsing import (Literal, Word, nums, LineStart, LineEnd, Combine,
                       CharsNotIn, srange, Suppress, ParseException, oneOf)

# Parse and fail actions for atomic (lowest-level) parsers


def warnings_sitename(bad_string, loc, _expr, _err):
    """ Warnings for sitename parsers """
    raise ParseException(bad_string, loc,
                         "Sitename must follow repository conventions:\n"
                         + "See https://fluidattacks.com/web/es/empleos/"
                         + "retos-tecnicos/#101-estructura\n")


def warnings_integer(bad_string, loc, _expr, _err):
    """ Warnings for integer parsers """
    raise ParseException(bad_string, loc,
                         "Expected a non-negative integer here. \
                         Only digits.\n")


def warnings_float(bad_string, loc, _expr, _err):
    """ Warnings for float parsers """
    raise ParseException(bad_string, loc,
                         "Expected an non-negative integer or float here.\n")


def warnings_any(bad_string, loc, _expr, _err):
    """ Warnings for anything-except-parens parsers """
    raise ParseException(bad_string, loc,
                         "Anything except parens works here.\n")
    """
    Future: add more specific warnings when faced with a cryptic message.
    sys.exit(1)
    """


def warnings_separator(bad_string, loc, _expr, _err):
    """ Missing blank line warning """
    raise ParseException(bad_string, loc, "Please leave a blank line "
                         + "to separate commit title from body.\n"
                         + "See git-commit(1).\n")


def warnings_issue(bad_string, loc, _expr, _err):
    """ Missing #0 issue """
    raise ParseException(bad_string, loc, "Please add a #0 "
                         + "as explained in commit-msg-systems.txt\n")

# Atomic parsers


INTEGER = Word(nums).setName("entier").setFailAction(warnings_integer)
INTEGER.setParseAction(lambda toks: int(toks[0]))
FLOAT = Combine(Word(nums) + '.' + Word(nums))
FLOAT.setParseAction(lambda toks: float(toks[0]))
NUMBER = (FLOAT | INTEGER).setName("float").setFailAction(warnings_float)
SPACE = Word(' ', exact=1).setFailAction(warnings_sitename)

SITENAME = Word(srange("[a-z]") + nums + "-"
                ).setName("sitename").setFailAction(warnings_sitename)
ANY_CODE = CharsNotIn("()\n").setName("challcode").setFailAction(warnings_any)
COMPLEXITY = (NUMBER | ANY_CODE)
# warnings_sitename appears in separators to make message easier to understand
TITLE_SYST = (LineStart() + Suppress("sltn(sys):")
              + SPACE(" ")
              + Suppress("#0").setFailAction(warnings_issue)
              + SPACE(" ")
              + SITENAME("sitename")
              + Suppress(",").setFailAction(warnings_sitename)
              + SPACE(" ")
              + SITENAME("code") + Suppress(LineEnd())).leaveWhitespace()
# Future: integrate with title_chall via SCOPE using pyparsing.Forward()

# Parse and fail actions for mid-level parsers


def check_others(bad_string, loc, tokens):
    """ Parse action for others and discoveries row """
    ini, fin, pro = tokens
    if ini + fin != pro:
        raise ParseException(bad_string, loc,
                             "Total others not correctly computed")
    # Future: since subprocess has already been imported,
    # why not check against actual number of others in directory
    # (take directory name from site-name in parser:tokens["title"]["sitename"]
    # and in OTHERS.lst

# Helper function to construct mid-level parsers with similar structure


def int_row(label, ini, fin, oper):
    """ Returns a parser with integer row prototype.
    Good for all except scores which might admit decimals.
    "oper" refers to both "total" (addition) and "progress" (subtraction).
    """
    return (LineStart() + Suppress(label)
            + INTEGER("ini") + Suppress(ini)
            + INTEGER("fin") + Suppress(fin)
            + INTEGER("oper") + Suppress(oper))

# Mid-level parsers


OTHERS = int_row("- others:", "in,", "out,", "total.") \
    .setParseAction(check_others)

EFFORT = (LineStart() + Suppress("- effort:") + NUMBER("hours")
          + Suppress("hours during")
          + (Literal("immersion") ^ Literal("challenges"))("phase")
          + Suppress("."))

BLANK = Suppress(LineEnd()
                 ).setName("blank line").setFailAction(warnings_separator)
# setFailAction commented out above because
# since the problem string is empty, produces a RecursionError later on.

DISCOVERIES = (LineStart() + Suppress("- discovered vulnerabilities:")
               + INTEGER("by commiter") + Suppress("by me,")
               + INTEGER("already in repo") + Suppress("already in repo,")
               + INTEGER("total") + Suppress("total.") + Suppress(LineEnd())
               ).setParseAction(check_others)

TOTAL = (LineStart() + Suppress("- total estimated vulnerabilities in system:")
         + INTEGER("vulns") + Suppress(LineEnd()))

PERCENTAGE = (LineStart() + Suppress("- discovery percentage:")
              + NUMBER("computed") + Literal("%") + Suppress(LineEnd()))

# Parse action for top-level parsers


def systems_checks(bad_string, loc, tokens):
    """ For checking things that go across lines,
    like discovery percentage correctly computed """
    computed_percentage = tokens["percentage"]["computed"]
    # already checked by DISCOVERIES parse action
    computed_total_discovered = tokens["discoveries"]["total"]
    estimated_total_vulns = tokens["total estimated"]["vulns"]
    actual_percentage = computed_total_discovered*100/estimated_total_vulns
    if abs(actual_percentage - computed_percentage) > 1:
        raise ParseException(bad_string, loc,
                             "Discovery percentage not correctly computed")

# Top-level parsers


SYSTEMS_COMMIT = (TITLE_SYST("title")
                  + BLANK("blank")
                  + DISCOVERIES("discoveries")
                  + TOTAL("total estimated")
                  + PERCENTAGE("percentage")
                  + EFFORT("effort")
                  ).setName("syssol").setParseAction(systems_checks)

OTHER_COMMIT = oneOf([
  'feat',
  'perf',
  'fix',
  'revert',
  'docs',
  'style',
  'refactor',
  'test'
])
SOLUTIONS = "sltn(sys):"

CLASSIFIER = (SOLUTIONS ^ OTHER_COMMIT).setResultsName("type")

# pp.Or not used because it generates ugly exceptions
# Instead we chose to use the CLASSIFIER parser above to
# determine which of the top-level parsers to use
# and generate the appropriate warnings.


def handle_except_exit(exception, mess):
    """ Final error message. Prints line and a pointer to
    the guilty column. """
    print(exception.line)
    print(' '*(exception.col-1) + '^')
    print(exception)
    print(mess)
    sys.exit(1)


def parse_commit(message):
    """ Main function to parse a commit message """
    try:
        commit = CLASSIFIER.parseString(message)
        if commit["type"] == "sltn(sys):":
            try:
                SYSTEMS_COMMIT.parseString(message)
                print("Systems commit message OK, continue checks...")
                sys.exit(0)
            except ParseException as exc_bad_systems:
                emsg = ("\nIf this is a commit for a SYSTEM vulnerability,"
                        + "follow templates/commit-msg-systems.txt:")
                handle_except_exit(exc_bad_systems, emsg)
        else:
            print("Other commit message OK, continue checks...")
            sys.exit(0)
    except ParseException as exc_nocommit:
        emsg = ("A commit message must start with one of the allowed types:\n"
                + "sltn(sys): for solutions\n"
                + "{feat, perf, fix, revert, docs, style, refactor, test} "
                + "for other work.\n"
                + "Please review the templates in training/templates/")
        handle_except_exit(exc_nocommit, emsg)


if len(sys.argv) > 1:  # for bats testing
    with open(sys.argv[1]) as test_message:
        parse_commit(test_message.read())

else:
    try:
        CMD_OUT = subprocess.check_output('git log -1 --pretty=%B', shell=True)
        MESSAGE = CMD_OUT.decode('utf-8')
        parse_commit(MESSAGE)
    except subprocess.CalledProcessError as cmd_err:
        print("Something went wrong while retrieving the last commit:")
        print(cmd_err)
        sys.exit(1)
