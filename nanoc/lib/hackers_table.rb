# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/CyclomaticComplexity
# rubocop:disable Metrics/PerceivedComplexity
# rubocop:disable Metrics/BlockLength

def hackers_table
  users, totals = collect_info
  users_with_prcntg = add_writeups_percentage(users)
  users_sorted = users_with_prcntg.sort_by { |_, v| v['writeups'] }.reverse.to_h
  users_with_pos = add_pos_column(users_sorted)
  [users_with_pos, totals]
end

def collect_info
  users = {}
  totals = { 'writeups' => 0,
             'unique-writeups' => 0,
             'ntoes' => 0,
             'toes' => [] }

  Dir.foreach('systems') do |toe|
    next if ['.', '..'].include?(toe) || File.file?(toe)

    toe_path = 'systems/' + toe + '/'
    Dir.foreach(toe_path) do |challenge|
      next if ['.', '..'].include?(challenge) ||
              File.file?(toe_path + challenge)

      feature_path = toe_path + challenge + '/*.feature'
      others_path = toe_path + challenge + '/OTHERS.lst'
      others_exist = File.file?(others_path)
      others_empty = File.zero?(others_path)
      features = Dir.glob(feature_path)
      features.each do |feature|
        user = feature.split('/')[-1].gsub('.feature', '')
        add_hacker(user, users)
        users[user]['writeups'] += 1
        totals['writeups'] += 1
        if !others_exist || others_empty
          users[user]['unique-writeups'] += 1
          totals['unique-writeups'] += 1
        end
        unless users[user]['toes'].include?(toe)
          users[user]['toes'].push(toe)
          users[user]['ntoes'] += 1
        end
        unless totals['toes'].include?(toe)
          totals['toes'].push(toe)
          totals['ntoes'] += 1
        end
      end
    end
  end
  [users, totals]
end

def add_hacker(user, users)
  unless users.key?(user)
    user_hash = { 'pos' => -1,
                  'writeups' => 0,
                  'unique-writeups' => 0,
                  'unique-writeups-prcntg' => 0.0,
                  'ntoes' => 0,
                  'toes' => [] }
    users[user] = user_hash
    users
  end
  users
end

# Make sure to use this function after the dictionary is sorted
def add_pos_column(users)
  cont = 1
  users.keys.each do |key|
    users[key]['pos'] = cont
    cont += 1
  end
  users
end

def add_writeups_percentage(users)
  users.keys.each do |key|
    users[key]['unique-writeups-prcntg'] =
      (100.0 / users[key]['writeups'] * users[key]['unique-writeups']).round(0)
  end
  users
end
