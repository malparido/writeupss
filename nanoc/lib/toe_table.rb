# rubocop:disable Lint/MissingCopEnableDirective
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/CyclomaticComplexity
# rubocop:disable Metrics/PerceivedComplexity
# rubocop:disable Metrics/BlockLength

require 'yaml'

def toe_table
  toes_info = {}
  totals = { 'potential-vulnerabilities' => 0,
             'confirmed-vulns' => 0,
             'internal-vulns' => 0,
             'external-vulns' => 0,
             'exclusive-writeups' => 0,
             'total-internal-writeups' => 0,
             'total-external-writeups' => 0 }

  Dir.foreach('systems') do |toe|
    next if ['.', '..'].include?(toe) || File.file?(toe)

    toe_path = 'systems/' + toe + '/'
    toe_data = YAML.safe_load(File.read(toe_path + 'data.yaml'))
    confirmed_vulns = 0
    internal_vulns = 0
    external_vulns = 0
    exclusive_writeups = 0
    total_internal_writeups = 0
    total_external_writeups = 0
    Dir.foreach(toe_path) do |challenge|
      next if ['.', '..'].include?(challenge) ||
              File.file?(toe_path + challenge)

      feature_path = toe_path + challenge + '/*.feature'
      others_path = toe_path + challenge + '/OTHERS.lst'
      others_exist = File.file?(others_path)
      others_empty = File.zero?(others_path)
      features = Dir.glob(feature_path)
      is_conf_vuln = !features.empty? || (others_exist && !others_empty)

      confirmed_vulns += 1 if is_conf_vuln
      internal_vulns += 1 unless features.empty?
      external_vulns += 1 if others_exist && !others_empty
      exclusive_writeups += features.length if !others_exist || others_empty
      total_internal_writeups += features.length
      total_external_writeups += File.foreach(others_path).count if others_exist
    end

    toe_data['confirmed-vulns'] = confirmed_vulns
    toe_data['internal-vulns'] = internal_vulns
    toe_data['external-vulns'] = external_vulns
    toe_data['exclusive-writeups'] = exclusive_writeups
    toe_data['total-internal-writeups'] = total_internal_writeups
    toe_data['total-external-writeups'] = total_external_writeups

    totals['potential-vulnerabilities'] +=
      toe_data['potential-vulnerabilities'].to_i
    totals['confirmed-vulns'] += confirmed_vulns
    totals['internal-vulns'] += internal_vulns
    totals['external-vulns'] += external_vulns
    totals['exclusive-writeups'] += exclusive_writeups
    totals['total-internal-writeups'] += total_internal_writeups
    totals['total-external-writeups'] += total_external_writeups

    toes_info[toe] = toe_data
  end
  toes_info = toes_info.sort_by { |_, v| v['confirmed-vulns'] }.reverse.to_h
  [toes_info, totals]
end
