#!/bin/bash

RED='\033[0;31m'
NC='\033[0m'

cp -r /writeups writeups_tmp/
cd /writeups_tmp || exit 1
mv nanoc/* ./
bundle install -j4
nanoc
echo -e "${RED}Starting nanoc site on localhost:3000${NC}"
nanoc view
