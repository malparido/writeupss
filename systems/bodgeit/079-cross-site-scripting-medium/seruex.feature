## Version 1.4.1
## language: en

Feature:
  TOE:
    Bodgeit
  Category:
    Cross-site Scripting
  Location:
    bodgeit/search.jsp

  CWE:
    CWE-79:Cross-site Scripting
  Goal:
    Executing script and geting cookies
  Recommendation:
    Add lines of code allowing the character encoding

  Background:
  Hacker's software:
    | <Software name>     | <Version>   |
    | Parrot Security OS  | 4.2.2x64    |
    | Firefox             | 52.9.0      |
    | Bodgeit             | 1.4.0       |

  TOE information:
    Given I enter the site bodgeit/search.jsp
    And I search a item for name or price
    And the server is runing HSQLDB
    And JSP version
    And is running on Parrot OS 4.2.2x64

  Scenario: Normal use case
  Making a search of items
    Given I access the site bodgeit/search.jsp
    Then I search articles for name or price
    And I click on search
    And I see the list of the items related to my search

  Scenario: Static detection
  The var query no have a interpretation of caracteres. for this the xss can run
    When I open the file search.jsp
    Then I see the resource content the error in the line 36.
    """
    32. <jsp:include page="/header.jsp"/>
    33. <h3>Search</h3>
    34. <font size="-1">
    35. <%
    36. String query = (String) request.getParameter("q");
    37.
    38. if (request.getMethod().equals("GET") && query != null){
    39.    if (query.replaceAll("\\s", "").toLowerCase().
    indexOf("<script>alert(\"xss\")</script>") >= 0) {
    40.            conn.createStatement().execute("UPDATE Score SET status = 1
    WHERE task = 'SIMPLE_XSS'");
    41.    }
    42.
    43. %>
    """

  Scenario: Dynamic detection
  Seeing if the site can execute scripts
    Given I access the site bodgeit/search.jsp
    Then I put in the search box  "<script>alert(1)</script>"
    And the page show me pop up message (1), (view imagen1.png)
    Then the vulnerability XSS exist in the server
    Then I conclude that the server is vulnerable why not have protection XSS

  Scenario: Exploitation
  gettin cookie of user in http://localhost:8080/bodgeit/search.jsp
    Given I need add a script thats shows me the user's cookie
    Then I put int search box <script>alert(document.cookie)</script>
    And the the page show me a messaje whit the cookie for user identified
    Then I might send the cookie to other server and see it
    Then I conclude that it is possible to hijack a user's cookies on this page.

  Scenario: Remediation
  The server bodgeit has a vulnerability in the direction
    #http://localhost:8080/bodgeit/search.jsp
    Given this I advise to repair the vulnerability modifying the line 36:
    """
    32. <jsp:include page="/header.jsp"/>
    33. <h3>Search</h3>
    34. <font size="-1">
    35. <%
    36. String query = StringEscapeUtils.escapeHtml4(request.getParameter("q"));
    37.
    38. if (request.getMethod().equals("GET") && query != null){
    39.    if (query.replaceAll("\\s", "").toLowerCase().
    indexOf("<script>alert(\"xss\")</script>") >= 0) {
    40.            conn.createStatement().execute("UPDATE Score SET status = 1
    WHERE task = 'SIMPLE_XSS'");
    41.    }
    42.
    43. %>
    """
    Then I can confirm that the vulnerability can be successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.0/10 (Medium) - AV:L/AC:L/PR:N/UI:R/S:C/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.8/10 (Medium) - E:H/RL:T/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.4/10 (Low) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2018-12-23
