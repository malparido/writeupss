## Version 1.4.1
## language: en

Feature:
  TOE:
    Bodgeit
  Category:
    SQL Injection
  Location:
    bodgeit/basket.jsp

  CWE:
    CWE-89:SQL Injection
  Goal:
    Access the database through SQL injection
  Recommendation:
    Implement restrictions for multiple querys [productid, price, quantity]

  Background:
  Hacker's software:
    | <Software name>     | <Version>   |
    | Parrot Security OS  | 4.2.2x64    |
    | Firefox             | 52.9.0      |
    | Bodgeit             | 1.4.0       |
    | Nikto               | 2.1.6       |
    | Vega scanner        | 1.0         |
    | SQLmap              | 1.2.9       |

  TOE information:
    Given I enter the site bodgeit/
    And I add items to my Basket
    Then I access to my Basket
    And I see the columns (Product, Quantity, Price)
    Then I see that this can be vulnerable
    And I run a test scanner
    And I see that scanner shows a SQL injection vulnerability in querys
    |productid; price; quantity|

  Scenario: Normal use case
  Making a update of my basket
    Given I access the site
    And I add products to my basket
    And click on update my basket
    And if I want I can add other item

  Scenario: Static detection
  The input provided (post parameter) does not have the respective precaution
    When I execute vega subgraph on bodgeit/basket.jsp
    Then I see the resource content the error in the line 58, 59
    """
    <head><title>Estado HTTP 500 – Internal Server Error</title>
    <style type="text/css">h1
    {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;
    font-size:22px;} h2 {font-fami
    ly:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;
    font-size:16px;} h3 {font-family:Tahoma,Arial,
    sans-serif;color:white;
    background-color:#525D76;font-size:14px;} body {font-family:Tahoma,Arial,
    sans-serif;color:black;background-color:white;} b
    {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;}
    p {font-family:Tahoma,Arial,sans-serif;background:white;color:black;
    font-size:12px;}a {color:black;} a.name {color:black;} .line
    {height:1px;background-color:#525D76;border:none;}</style></head>
    """

  Scenario: Dynamic detection
  searching vulnerabilities with vega
    Given I am scann whit nikto the site
    And I see what nikto no find several vulnerabilitys  (OSVDB-397; OSVDB-5646)
    Then I scann whit vega subgraph the site
    And I see a vulnerability SQL injection in the querys:
    |productid; price; quantity|
    Then I can conclude that the querys of the |productid; price; quantity|
    #No have restriction of the parameter post

  Scenario: Exploitation
  http://localhost:8080/bodgeit/basket.jsp [productid=30 price=2.1-0 quanty=1]
    Given I need add the querys [productid, price, quanty] to the command SQLmap
    Then I use the next command in the console SQLmap
    #$sqlmap -u http://localhost:8080/bodgeit/basket.jsp --data
    #"productid=30-0 &price=1.00 & quantity=1"
    And I see that the SQlmap find a method Injectable (HSQLDB)
    Then I use the command:
    #$sqlmap -u http://localhost:8080/bodgeit/basket.jsp --data
    #"productid=30-0 &price=1.00 & quantity=1" --dbs
    Then I see the dbs of the server bodgeit
    Then I can conclude that SQL injection with the tool SQLmap work
    And this shows me that the server is exploitable by SQL injection.

  Scenario: Remediation
  The server bodgeit is vulnerable in the direction
    #http://localhost:8080/bodgeit/basket.jsp whit the
    #querys [productid, price, quantity]
    Given this I advise to repair the vulnerability with the following steps
    """
    The best defense against SQL injection vulnerabilities is to use
    parameterized statements.

    Sanitizing input can prevent these vulnerabilities. Variables of string
    types should be filtered for escape characters, and numeric types should be
    checked to ensure that they are valid.

    Use of stored procedures can simplify complex queries and allow for tighter
    access control settings.

    Configuring database access controls can limit the impact of exploited
    vulnerabilities. This is a mitigating strategy that can be employed in
    environments where the code is not modifiable.

    Object-relational mapping eliminates the need for SQL.
    """
    Then I can confirm that the vulnerability can be successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:L/AC:L/PR:L/UI:N/S:C/C:N/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.3/10 (Medium) - E:H/RL:T/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.2/10 (Medium) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2018-12-23
