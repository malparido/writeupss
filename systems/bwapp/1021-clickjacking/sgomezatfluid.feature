## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other Bugs
  Location:
    bwapp/clickjacking.php - Content-Security-Policy (Header missing)
  CWE:
    CWE-1021: Improper Restriction of Rendered UI Layers or Frames
  Goal:
    Make the user purchase tickets unknowingly
  Recommendation:
    Implement iframe restrictions

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | beebox                | 1.6       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bwapp

  Scenario: Normal use case
    Given I go to bwapp/clickjacking.php
    And I get a page that lets me order a number of tickets
    And it has a prepopulated ticket amount
    Then I click on Confirm
    Then I purchased 10 tickets

  Scenario: Static detection
  Host header directly sent to user
    Given I check the code at bwapp/clickjacking.php
    And I see it doesn't have any UI restrictions
    Then I know it's vulnerable to clickjacking

  Scenario: Dynamic detection
  Checking headers and UI restriction logic
    Given I intercept a GET request to the target URL with Burp
    And I see the request
    """
    GET /bWAPP/clickjacking.php HTTP/1.1
    Host: bwapp
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101
    Firefox/65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,
    */*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Connection: close
    Cookie: security_level=0; PHPSESSID=1d7748bbdf5a66d69dcbd8823443f678
    Upgrade-Insecure-Requests: 1
    """
    Then I see no X-Frame-Options or Content-Security-Policy headers
    Then I know I can clickjack the page

  Scenario: Exploitation
  Get a victim to buy tickets
    Given I know the target page is vulnerable to clickjacking
    Then I forge a page that inserts the page into an iframe
    And puts a FREE TICKETS fake button covering the 'Confirm' button
    And send it to a victim
    Then when he clicks the FREE TICKETS button
    Then ticket purchase goes through in their name

  Scenario: Remediation
  Add CSP headers
    Given I add this code at the begining of the page code
    """
    23  $headerCSP = "Content-Security-Policy:".
        "connect-src 'self' ;".
        "default-src 'self';".
        "frame-ancestors 'self' ;".
        "frame-src 'none';".
        "media-src 'self' *.example.com;".
        "object-src 'none';";
    24  header($contentSecurityPolicy);
    25  header('X-Frame-Options: SAMEORIGIN');
    """
    And I try to inject it as an iframe in my malicious page
    Then I get an error in the console
    """
    Load denied by X-Frame-Options: http://bwapp/bWAPP/clickjacking.php
    does not permit framing.
    """
    Then the vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.2/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.2/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2018-12-14