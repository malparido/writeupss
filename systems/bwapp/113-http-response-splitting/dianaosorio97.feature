## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other Bugs
  Location:
    bWAPP/http_response_splitting.php - url (header)
  CWE:
    CWE-113: Improper Neutralization of CRLF Sequences in HTTP Headers
  Goal:
    Detected a warning
  Recommendation:
    Validate the data of the headings to avoid injections

  Background:
  Hacker's software:
    | <Software name> | <Version>    |
    | Ubuntu          | 18.04.1 (x64)|
    | Mozilla Firefox | 63.0         |
    | beebox          | 1.6          |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bWAPP
    And The url is "bWAPP/http_response_splitting.php"
    Then I access the application with my broswer

  Scenario: Normal use case
    Given I access the url http://bWAPP/http_response_splitting.php
    And I see the information that the page gives me
    """
    Try to get the new line detected warning...
    Visit our blog.
    """
    Then I noticed that this page redirects me to another page
    """
    http://bWAPP/http_response_splitting.php?url=
    http://itsecgames.blogspot.com
    """

  Scenario: Static detection
  The user can insert CRLF characters in the header
    When I look at the code http_response_splitting.php
    Then I can see
    """
    43 header("Location: " . $_GET["url"]);
    """
    And I see that does not sanitise the CRLF characters
    Then I see that the application is vulnerable to HTTP response Splitting

  Scenario: Dynamic detection
    Given The request is sent by the url parameter
    Then I enter CRLF characters with the url
    """
    http:/bWAPP/http_response_splitting.php?url=
    http://itsecgames.blogspot.com%0A%0D
    """
    And I see redirects me to the page without any restriction
    Then I can conclude that does not sanitise the CRLF characters
    Then The application is vulnerable to HTTP response Splitting

  Scenario: Exploitation
  Insert special characters to generate a warning
    Given The application uses the url parameter for the redirect
    Then I want insert the follow request
    """
    HTTP/1.1 200 OK
    Content-Type: text/html
    Content-Length: 41
    Hello, this is a test
    """
    And I use an online tool to codify my request
    """
    http://yehg.net/encoding/
    """
    And I have the following result
    """
    HTTP%2F1.1%20200%20OK%0AContent-Type%3A%20text%2Fhtml%0AContent
    -Length%3A%2041%0AHello%2C%20this%20is%20a%20test
    """
    Then I send this result with the url
    """
    http://bWAPP/http_response_splitting.php?url=http:
    //itsecgames.blogspot.com%0A%0DHTTP%2F1.1%20200%20OK%0AContent
    -Type%3A%20text%2Fhtml%0AContent
    -Length%3A%2041%0AHello%2C%20this%20is%20a%20test
    """
    Then I get the output:
    """
    Warning: Header may not contain more than a single header,
    new line detected. in /var/www/bWAPP/http_response_splitting.php on line 43
    """
    Then I can conclude the warning is produced by the php mitigations in header

  Scenario: Remediation
  verify if there are CRLF characters in the user input
    Given I have patched the code by doing
    """
    43 $pattern1 = '/\%0D/';
    44 $pattern2 = '/\%0A/';
    45 $url = $_GET["url"];
    46 $url1 = urlencode($url);
    47 $check_cr = preg_match($pattern1, $url1);
    48 $check_lf = preg_match($pattern2, $url1);
    49 if ((check_cr > 0)||(check_lf > 0)) {
    50    echo "CRLF characteres were found";
    51 } else {
    52    header("Location: " . $url);
   53     }
    """
    And I tried to send the url with the same request
    Then I get:
    """
    CRLF characteres were found
    """
    Then I can confirm that the vulnerability was patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.9/10 (Medium) - E:F/RL:W/RC:R/
  Environmental: Unique and relevant attributes to a specific user environment
    5.8/10 (Medium) - CR:L/IR:M/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-12-19
