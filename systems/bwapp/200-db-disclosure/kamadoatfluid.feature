## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Page name:
    bWAPP/db/
    bWAPP/db/bwapp.sqlite
  CWE:
    CWE-200: Information Exposure
  Goal:
    Get and read the application database
  Recommendation:
    Restrict access to the database, or put it outside the server directory

  Background:
  Hacker's software:
    | <Software name>  |    <Version>    |
    |     beebox       |       1.6       |
    |    GNU bash      |      4.4.19     |
    |   VirtualBox     |       5.0       |
    |  Google Chrome   |   70.0.3538.77  |
    |     Ubuntu       |    18.04.1LTS   |

  TOE information:
    Given I'm running beebox with VirtualBox on ip "192.168.56.101"
    # https://sourceforge.net/projects/bwapp/files/bee-box/

  Scenario: Normal use case
    When I access the site
    Then I get displayed a directory on the browser

  Scenario: Static detection
  The httpd.conf file doesn't restrict access to directory listing
    When I see at the file "/etc/apache2/apache2.conf"
    And the imported "*.conf" that it includes
    And I scan all the directives
    Then I don't found anything that forbids the access to the bWAPP folders
    And I conclude I can access the dirs at "/var/www/bWAPP"
    And all its folders and files, including "/var/www/bWAPP/db"

  Scenario: Dynamic detection
  I can access the bwapp.sqlite file from my browser
    When I access "http://${bee_box_ip}/bWAPP/db/"
    Then I see the directory contents displayed on my browser
    And I can download "bwapp.sqlite" which is the database

  Scenario: Exploitation
  Reading the database
    Given I'm able to download the database file
    And I open it with an online sqlite viewer
    # http://inloop.github.io/sqlite-viewer/
    Then I get the following results
    # blog (empty)
    # heroes
    | id | login     | password       | secret                                 |
    | 1  | neo       | trinity        | Oh why didn't I took that BLACK pill?  |
    | 2  | alice     | loveZombies    | There's a cure!                        |
    | 3  | thor      | Asgard         | Oh, no... this is Earth... isn't it?   |
    | 4  | wolverine | Log@N          | What's a Magneto?                      |
    | 5  | johnny    | m3ph1st0ph3l3s | I'm the Ghost Rider!                   |
    | 6  | seline    | m00n           | It wasn't the Lycans. It was you.      |
    # movies (not important)
    # users
    | id | login  | password                                 | admin |
    | 1  | A.I.M. | 6885858486f31043e5839c735d99457f045affd0 | 1     |
    | 2  | bee    | 6885858486f31043e5839c735d99457f045affd0 | 0     |
    # fields: email, secret, activation_code, activated, reset_code aren't shown
    When I crack those SHA-1 hashes
    Then I get an authentication method
    | id | login  | password | admin |
    | 1  | A.I.M. | bug      | 1     |
    | 2  | bee    | bug      | 0     |
    When I try this credentials at "http://${bee_box_ip}/bWAPP/login.php"
    Then I confirm this are valid credentials

  Scenario Outline: Remediation
  Configure the apache service via httpd.conf
    Given I edit the file "/etc/apache2/httpd.conf"
    """
    01 ExtendedStatus On
    02 <Location /server-status>
    03  SetHandler server-status
    04  Order deny,allow
    05  Allow from all
    06 </Location>
    07
    08 # Allows WebDAV, not secure!!!
    09 Alias /webdav /var/www/bWAPP/documents
    10 <Location /webdav>
    11  DAV On
    12 </Location>
    13
    14 <Files "bwapp.sqlite">
    15     Order Allow,Deny
    16     Deny from all
    17 </Files>
    """
    And I restart the apache service
    """
    # /etc/init.d/apache2 restart
    """
    When I access "http://${bee_box_ip}/bWAPP/db/"
    Then I see the directory listed in my browser
    But without the database file
    When I access "http://${bee_box_ip}/bWAPP/db/bwapp.sqlite"
    Then I get "HTTP/1.1 403 Forbidden"

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.2/10 (High) - AV:A/AC:L/PR:N/UI:N/S:C/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.0/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.7/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-12-04
