## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Page name:
    bWAPP/information_disclosure_4.php
  CWE:
    CWE-200: Information Exposure
  Goal:
    Discover the application web framework
  Recommendation:
    Don't display information that is not necessary for the end user

  Background:
  Hacker's software:
    | <Software name> | <Version>             |
    | Ubuntu          | 18.04.1 LTS (amd64)   |
    | Google Chrome   | 70.0.3538.77 (64-bit) |
  TOE information:
    Given I am accessing the site bWAPP/information_disclosure_4.php
    And the security level is set to low

  Scenario: Normal use case
  Normal page with normal text
    When I inspect the page
    Then I see the title
    """
    Information Disclosure - Favicon
    """
    And I see a text
    """
    Some default favicons are disclosing the web framework...
    """
    And I conclude the page behaves normally

  Scenario: Static detection
  The favicon is loaded based on the security level cookie
    When I look at the souce code
    # inside of bee-box:
    #   /owaspbwa/bwapp-git/bWAPP/information_disclosure_4.php
    # non important lines has been removed
    """
    01 <?php
    -- ...
    15 $message = "";
    16 $favicon = "";
    17 switch($_COOKIE["security_level"])
    18 {
    19     case "0" :
    20         $favicon = "favicon_drupal.ico";
    21         $message = "Some default favicons are disclosing the web
       framework...";
    22         break;
    23     case "1" :
    24         $favicon = "favicon.ico";
    25         $message = "There's nothing to disclose!";
    26         break;
    27     case "2" :
    28         $favicon = "favicon.ico";
    29         $message = "There's nothing to disclose!";
    30         break;
    31     default :
    32         $favicon = "favicon_drupal.ico";
    33         $message = "Some default favicons are disclosing the web
       framework...";
    34         break;
    35 }
    36 ?>
    37 <!DOCTYPE html>
    38 <html>
    39
    40 <head>
    -- ...
    46 <link rel="shortcut icon" href="images/<?php echo $favicon ?>"
       type="image/x-icon" />
    -- ...
    53 </head>
    -- ...
    173 </html>
    """
    Then I conclude that the web framework is Drupal
    """
    https://www.drupal.org/
    """

  Scenario: Dynamic detection
  The favicon is the Drupal web framework logo
    Given I look at the page favicon [evidence](favicon.png)
    Then I recognize the logo
    And I conclude it's the Drupal Web Framework

  Scenario: Exploitation
  Knowing the web framework could allow me to exploit known vulnerabilities
    Given I know the web framework is Drupal
    Then I get reduced my vulnerabilities search space
    When I look at the current open vulnerabilities for Drupal
    """
    https://www.drupal.org/security
    """
    Then I see a list of highly and moderately critical vulnerabilities
    And I could exploit the open ones

  Scenario: Remediation
  Show the regular bWAPP favicon
    Given I have patched the code by modifying the lines
    """
    16 -- $favicon = "";
    16 ++ $favicon = "favicon.ico";
    """
    And deleting the lines 17 to 35
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.8/10 (Low) - E:U/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.2/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-11-23
