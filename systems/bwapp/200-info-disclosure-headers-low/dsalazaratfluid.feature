## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category
    Other bugs...
  Page name:
    bWAPP/information_disclosure_2.php
  CWE:
    CWE-200: Information Exposure
  Goal:
    Find out information about the server from http headers
  Recommendation:
    Hide server information from http headers

  Background:
  Hacker's software:
    | <Name>                       | <Version>          |
    | Kali GNU/Linux               | 4.18.0-kali1-amd64 |
    | Firefox Quantum              | 60.2.0             |
    | Burp Suite Community Edition | 1.7.36             |
  TOE information:
    Given I am accessing the site bWAPP/information_disclosure_2.php
    And enter a php site that displays: "Check the server headers..."
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level

  Scenario: Normal use case
  No use case
    Given I access the site
    Then I only see the message "Check the server headers..."
    And I can't do anything else

  Scenario: Static detection
  ServerTokens set to Full
    Given I check the file /etc/apache2/apache2.conf
    Then i see:
    """
    205  # ServerTokens
    206  # This directive configures what you return as the Server \
    HTTP response
    207  # Header. The default is 'Full' which sends information \
    about the OS-Type
    208  # and compiled in modules.
    209  # Set to one of:  Full | OS | Minor | Minimal | Major | Prod
    210  # where Full conveys the most information, and Prod the least.
    211  #
    212  ServerTokens Full
    """
    Then I conclude that the server is disclosing sensitive information

  Scenario: Dynamic detection
  Watching headers from burp
    Given I am listening with Burp
    And I access the site
    And set burp to catch server response
    And forward the request it caught when I tried to access the site
    Then I intercept the following response:
    """
    HTTP/1.1 200 OK
    Date: Fri, 23 Nov 2018 07:02:08 GMT
    Server: Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6 \
    PHP/5.2.4-2ubuntu5 with Suhosin-Patch mod_ssl/2.2.8 OpenSSL/0.9.8g
    X-Powered-By: I love Marvel movies :)
    ... #Unimportant lines
    """
    Then I see that information about many software versions is being disclosed
    And using such informaion for building future exploits is possible

  Scenario: Exploitation
  Searching for possible exploits
    Given I already know the versions for the server's Apache, PHP, OpenSSL
    Then I would be able to search for possible exploits
    And try to execute them
    Then I search for Apache 2.2.8 exploits
    And find this: "https://www.cvedetails.com/cve/CVE-2017-7679/"
    Then executing exploits becomes feasible

  Scenario: Remediation
  Setting ServerTokens to Prod
    Given I know that ServerTokens is set to full
    Then I set ServerTokens to Prod in /etc/apache2/apache2.conf
    """
    205  # ServerTokens
    206  # This directive configures what you return as the Server HTTP response
    207  # Header. The default is 'Full' which sends \
    information about the OS-Type
    208  # and compiled in modules.
    209  # Set to one of:  Full | OS | Minor | Minimal | Major | Prod
    210  # where Full conveys the most information, and Prod the least.
    211  #
    212  ServerTokens Prod
    """
    Then I restart the apache server with:
    """
    /etc/init.d/apache2 restart
    """
    And I use burp to watch the headers again as described in Dynamic detection
    And I get this new response:
    """
    HTTP/1.1 200 OK
    Date: Fri, 23 Nov 2018 07:42:36 GMT
    Server: Apache
    X-Powered-By: I love Marvel movies :)
    ... #Unimportant lines
    """
    Then I conclude that no sensitive information is being displayed anymore
    And the vulnerability was fixed


  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.0/10 (Medium) - AV:N/AC:H/PR:N/UI:N/S:C/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.9/10 (Low) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.1/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-11-23
