## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Page name:
    bWAPP/web.config
  CWE:
    CWE-200: Information Exposure
  Goal:
    Discover the web configuration file of the application
  Recommendation:
    Restrict access to config files

  Background:
  Hacker's software:
    | <Software name> | <Version>               |
    | XAMPP           | 17.2.12-0               |
    | Ubuntu          | 18.04.1 LTS (amd64)     |
    | Google Chrome   | 70.0.3538.77 (64-bit)   |
    | GNU bash        | version 4.4.19 (x86_64) |
  TOE information:
    Given I'm running bWAPP-latest with XAMPP on my localhost
    # https://github.com/raesene/bWAPP

  Scenario: Normal use case
  Normal page with normal text
    When I access
    """
    http://localhost/bWAPP/images/
    """
    Then I get displayed the directory on the browser
    And everything works normally

  Scenario: Static detection
  The httpd.conf file doesn't restrict access to the web.config file
    When I see at the file "/opt/lampp/apache2/httpd.conf"
    """
    01 Alias /bitnami/ "/opt/lampp/apache2/htdocs/"
    02 Alias /bitnami "/opt/lampp/apache2/htdocs"
    03
    04 <Directory "/opt/lampp/apache2/htdocs">
    05     Options Indexes FollowSymLinks
    06     AllowOverride All
    07     Order allow,deny
    08     Allow from all
    09 </Directory>
    10
    """
    Then I conclude everything in "/opt/lampp/apache2/htdocs" is accesible
    Given the bWAPP path is "/opt/lampp/apache2/htdocs/bWAPP"
    Then I conclude I have access to any file

  Scenario: Dynamic detection
  I can access the web.config file from my browser
    When I access "http://localhost/bWAPP/web.config"
    Then I see the web.config file displayed on my browser

  Scenario: Exploitation
  The web.config file allows me to learn more about the application
    Given I'm able to see the web.config file
    Then I have a new source of information about the application
    And I can use it to make more specialized attacks
    And I can target components that otherwise I didn't know they existed

  Scenario: Remediation
  Configure the apache service via httpd.conf
    Given I edit the file "/opt/lampp/apache2/httpd.conf"
    """
    01 Alias /bitnami/ "/opt/lampp/apache2/htdocs/"
    02 Alias /bitnami "/opt/lampp/apache2/htdocs"
    03
    04 <Directory "/opt/lampp/apache2/htdocs">
    05     Options Indexes FollowSymLinks
    06     AllowOverride All
    07     Order allow,deny
    08     Allow from all
    09 </Directory>
    10
    11 <Files "web.config">
    12     Order Allow,Deny
    13     Deny from all
    14 </Files>
    """
    And I restart the mysql and apache service
    When I access "http://localhost/bWAPP/web.config"
    Then I see I cannot access the file anymore
    """
    Access forbidden!
      You don't have permission to access the requested object.
      It is either read-protected or not readable by the server.

      If you think this is a server error, please contact the webmaster.

    Error 403
      localhost
        Apache/2.4.37 (Unix)
        OpenSSL/1.0.2p
        PHP/7.2.12
        mod_perl/2.0.8-dev
        Perl/v5.16.3
    """
    And I conclude the vulnerability has been patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.8/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.3/10 (Medium) - E:U/RL:U/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.5/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-11-28
