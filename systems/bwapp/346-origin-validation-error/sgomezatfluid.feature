## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Location:
    http://bwapp/bWAPP/secret-cors-2.php - Origin (header)
  CWE:
    CWE-345: Origin Validation Error
  Goal:
    Make a request to the target resource from an unauthorised location
  Recommendation:
    Validate requests with more than a CORS header

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | beebox                | 1.6       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bwapp

  Scenario: Normal use case
    Given I go to bwapp/sm_cors.php
    Then I'm greeted with the prompt
    """
    Try to steal Wolverine's secret using an AJAX request from a malicious site.
    HINT: the secret is only available to requests from intranet.itsecgames.com.
    """
    And the word "secret" links to bwapp/secret-cors-2.php
    Then I click the link
    And I get a response
    """
    This is just a normal page with no secrets :)
    """

  Scenario: Static detection
  The request procedence is only validated with a CORS header
    When I look at the code in bwapp/secret-cors-2.php
    """
    21  if(isset($_SERVER["HTTP_ORIGIN"]) and
    $_SERVER["HTTP_ORIGIN"] == "http://intranet.itsecgames.com")
    22  {
    23    header("Access-Control-Allow-Origin: http://intranet.itsecgames.com");
    24    echo "Wolverine's secret: What's a Magneto?";
    25  }
    26  else
    27  {
    28    echo "This is just a normal page with no secrets :)";
    29  }
    """
    Then I can see it only validates the Origin header before returning
    Then I conclude I can access it by spoofing such header

  Scenario: Dynamic detection
  Bruteforcing the trusted Origin
    Given I try to get the secret as a logged in user
    And I get the response
    """
    This is just a normal page with no secrets :)
    """
    And the site normally redirects you to the login page if not logged in
    And I try to access the secret after logging out
    And get the same response
    Then maybe the secret is not locked to a user but to a location with CORS
    Given I know the application is from itsecgames, which is in itsecgames.com
    Then I intercept a GET request to the target url with BurpSuite
    And send it to the Burp Intruder
    Then I work out a list of possible subdomains the app could trust
    And use the intruder to test them in the Origin header
    And mark the ones that contain
    """
    This is just a normal page with no secrets :)
    """
    Then I get an unmarked response with an Access-Control-Allow-Origin header
    Then I conclude I have hit the spot

  Scenario: Exploitation
  I can get the secret by spoofing the Origin header
    Given I intercept a GET request for the URL with BurpSuite
    And I see the request data
    """
    GET /bWAPP/secret-cors-2.php HTTP/1.1
    Host: bwapp
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101
    Firefox/65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,
    */*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Connection: close
    Upgrade-Insecure-Requests: 1
    """
    Then I get the expected response
    """
    ...

    This is just a normal page with no secrets :)
    """
    Then I paste the request on the Burp Repeater
    And change the request data to
    """
    GET /bWAPP/secret-cors-2.php HTTP/1.1
    Host: bwapp
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101
    Firefox/65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,
    */*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Connection: close
    Upgrade-Insecure-Requests: 1
    Origin: http://intranet.itsecgames.com
    """
    And I get the response
    """
    ...

    Wolverine's secret: What's a Magneto?
    """
    Then I have obtained the secret and extracted sensitive data

  Scenario: Remediation
  Validate if at least a user is logged in
    Given I have patched the code by sending a token along with the data
    Given bwapp/secret-cors-2.php gets patched as follows
    """
    21  if(isset($_SERVER["HTTP_ORIGIN"]) and
        $_SERVER["HTTP_ORIGIN"] == "http://intranet.itsecgames.com" and
        isset($_SESSION["login"]))
    22  {
    23    header("Access-Control-Allow-Origin: http://intranet.itsecgames.com");
    24    echo "Wolverine's secret: What's a Magneto?";
    25  }
    26  else
    27  {
    28    echo "This is just a normal page with no secrets :)";
    29  }
    """
    And I send a request with a spoofed Origin header
    Then I get the response
    """
    This is just a normal page with no secrets :)
    """
    Then this vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.9/10 (Medium) - E:F/RL:O/RC:C/E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.9/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2018-12-07
