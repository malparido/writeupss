## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Location:
    http://bwapp/sqli_8-2.php
  CWE:
    CWE-352: Cross-Site Request Forgery (CSRF)
  Goal:
    Make a request to the target site on behalf of an unsuspecting user
  Recommendation:
    Validate where the requests are coming from before making a transaction

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | beebox                | 1.6       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bwapp

  Scenario: Normal use case
    Given I go to bwapp/sqli_8-1.php
    And click the button to reset my secret to 'Any Bugs?'
    And go to bwapp/secret.php
    Then I get as response
    """
    Your secret: Any bugs?
    """

  Scenario: Static detection
  The request procedence isn't being validated
    When I look at the code in bwapp/sqli_8-2.php
    """
    24  $body = file_get_contents("php://input");
    25
    26  // If the security level is not MEDIUM or HIGH
    27  if($_COOKIE["security_level"] != "1"
        && $_COOKIE["security_level"] != "2")
    28  {
    29
    30  ini_set("display_errors",1);
    31
    32  $xml = simplexml_load_string($body);
    33
    34  // Debugging
    35  // print_r($xml);
    36
    37  $login = $xml->login;
    38  $secret = $xml->secret;
    39
    40  if($login && $login != "" && $secret)
    41  {
    42
    43      $sql = "UPDATE users SET secret = '" . $secret . "' WHERE login = '"
            . $login . "'";
    44
    45      // Debugging
    46      // echo $sql;
    47
    48      $recordset = $link->query($sql);
    49
    50      if(!$recordset)
    51      {
    52
    53          die("Connect Error: " . $link->error);
    54
    55      }
    56
    57      $message = $login . "'s secret has been reset!";
    58
    59  }
    """
    Then I can see it changes the user's secret
    And doesn't care where the request is coming from
    Then I conclude there is a CSRF vulnerability

  Scenario: Dynamic detection
  I can make requests on behalf of a logged in user from outside
    Given I intercept a POST request for the URL with BurpSuite
    And I see the request data
    """
    ...
    Cookie: security_level=0; PHPSESSID=1c91299fd7de2ace36aedd129d61611c
    <reset>
        <login>bee</login>
        <secret>Any bugs?</secret>
    </reset>
    """
    Then I see it doesn't pass any data to verify request procedence
    Then I paste the request on the Burp Repeater
    And change the request data to
    """
    ...
    Cookie: security_level=0; PHPSESSID=1c91299fd7de2ace36aedd129d61611c
    <reset>
        <login>bee</login>
        <secret>PWND</secret>
    </reset>
    """
    Then I go to bwapp/secret.php
    And I get the response
    """
    Your secret: PWND
    """
    And I successfully repeat the process multiple times
    Then I conclude I can make the request from anywhere and it will work


  Scenario: Exploitation
  Change the victim user's secret
    Given I create a malicious page (sgomezatfluid.html) that builds a request
    And sends it to the target URL changing the user's secret
    And upload it to http://simongomez95.github.io/csrfpoc1.html
    And I email it to a hypothetical victim
    And the victim, already logged in to bwapp, clicks on it
    Then goes to bwapp/secret.php and sees their secret has changed
    And I, the attacker, now know the victims secret

  Scenario: Remediation
  Check where the requests are coming from
    Given I have patched the code by sending a token along with the data
    Given bwapp/sqli_8-1.php gets patched as follows
    """
    23  session_start();
    24  $token= md5(uniqid());
    25  $_SESSION['token']= $token;
    26  session_write_close();
    ...
    102  xmlHttp.send("<reset><login><?php if(isset($_SESSION["login"]))
         {echo $_SESSION["login"];}?></login><secret>Any bugs?</secret><token>
         <?php echo $token?></token></reset>");
    """
    And bwapp/sqli_8-2.php gets patched as follows
    """
    41  session_start();
    42  $token = $_SESSION['token'];
    43  unset($_SESSION['token']);
    44  session_write_close();
    45  if ($token && $reqtoken==$token) {
    46      if ($login && $login != "" && $secret) {
    47
    48          $sql = "UPDATE users SET secret = '" . $secret . "'
                WHERE login = '" . $login . "'";
    49
    50          // Debugging
    51          // echo $sql;
    52
    53          $recordset = $link->query($sql);
    54
    55          if (!$recordset) {
    56
    57              die("Connect Error: " . $link->error);
    58
    59        }
    60
    61        $message = $login . "'s secret has been reset!";
    62
    63    } else {
    64
    65        $message = "An error occured!";
    66
    67      }
    68}
    """
    Then I go to my malicious page after resetting my secret
    And go to bwapp/secret.php
    Then I get as response
    """
    Your secret: Any bugs?
    """
    Then I can conclude the vulnerability is fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.1/10 (High) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/bwapp/79-improper-neutralization/
      Given I create a malicious page to send a request to the target
      And build the request so it triggers the XSS running some js code
      When the victim clicks on my link
      Then I get their secret
      And can run whatever js code I want in the victims browser

