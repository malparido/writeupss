## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Missing functional level access control
  Location:
    http://localhost/bWAPP/hostheader_1.php - host (header)
  CWE:
    CWE-444: HTTP Request Smuggling
  Goal:
    Poison the web-cache to send malicious content
  Recommendation:
    Restrict insecure proxy connections

  Background:
  Hacker's software:
    | <Software name> | <Version>               |
    | Windows         | 10.0.17134.407 (64-bit) |
    | Mozilla firefox | 63.0.3 (64-bit)         |
    | XAMPP           | 3.2.2                   |
    | Burp suite      | 1.7.36                  |
  TOE information:
    Given I'm running bWAPP-latest with XAMPP on my localhost
    And I'm accessing the bWAPP pages through my browser
    And I create a user in the directory

  Scenario: Normal use case
  Normal page with normal text
    When I access
    """
    http://localhost/bWAPP/hostheader_1.php
    """
    And everything works normally

  Scenario: Dynamic detection
  The site allows connection through proxy
    Given I investigated on the subject
    And I have teh Burp Suite on my system
    And I proceed to intercept the connection
    And I the host is visible and information can be redirected

  Scenario: Static detection
  The site show the HTTP host in the header
    Given I enter in the source code
    When I go at the header in bWAPP/hostheader_1.php
    Then I saw that the name of the host is called
    """
    <script
    src="http://<?php echo $_SERVER["HTTP_HOST"]?>/bWAPP/js/html5.js">
    </script>
    """
    Then I can poison the information by changing the name of the host
    And I conclude that access to the host name is not validated

  Scenario: Exploitation
  The Burp Suit allows to change the address of the host
    Given I intercepted the connection
    Then I proceed to poison the web-cache
    And I put the anhoter host address [evidence](intercept.png)
    And I get redirect the site successfully [evidence](newsite.png)

  Scenario Outline: Remediation
    Change the Host header for the SERVER_NAME
    Given I investigated about this vulnerability
    Then I conclude that should create a dummy vhost
    And this can catches all requests with unrecognized Host headers.
    And the vulnerability would be patched.

  Scenario: Correlations
    No correlations have been found to this date 2018-12-11