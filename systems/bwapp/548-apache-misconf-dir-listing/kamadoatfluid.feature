## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Page name:
    bWAPP/js/
    bWAPP/images/
    bWAPP/documents/
    bWAPP/password/
    bWAPP/apps/
    bWAPP/db/
  CWE:
    CWE-548: Information Exposure Through Directory Listing
  Goal:
    List folders that I, as user, shouldn't see
  Recommendation:
    Restrict access to directories through apache configuration

  Background:
  Hacker's software:
    | <Software name>  |    <Version>    |
    |    OWASPBWA      |       1.2       |
    |   VirtualBox     |       5.0       |
    |    GNU bash      |      4.4.19     |
    |  Google Chrome   |   70.0.3538.77  |
    |     Ubuntu       |    18.04.1LTS   |
  TOE information:
    Given I'm running OWASPBWA on VirtualBox on Ubuntu
    # https://www.owasp.org/index.php/OWASP_Broken_Web_Applications_Project
    And I'm accessing the bWAPP pages through my Google Chrome
    And managing the server through ssh root@bWAPP_server

  Scenario: Normal use case
  Normal page with normal text
    When I access
    """
    http://localhost/bWAPP/images/
    """
    Then I get displayed the directory on the browser
    And everything works normally

  Scenario: Static detection
  The httpd.conf file doesn't restrict access to directory listing
    When I see at the file "/etc/apache2/apache2.conf"
    And the imported "*.conf" that it includes
    And I scan all the directives
    Then I don't found anything that forbids the access to the bWAPP folders
    And I conclude I can access the dirs at "/owaspbwa/bwapp-git/bWAPP/"
    And all the folders under this path

  Scenario Outline: Dynamic detection
  I can access some dirs that are indexed on the server
    When I access <site>
    Then I get displayed the directory on the browser

    Examples:
    | <site>           |
    | bWAPP/js/        |
    | bWAPP/images/    |
    | bWAPP/documents/ |
    | bWAPP/passwords/ |
    | bWAPP/apps/      |
    | bWAPP/db/        |
    # password on latest bWAPP version
    # passwords in the bWAPP version of OWASPBWA I'm running

  Scenario: Exploitation
  The directories listed allow me to learn more about the application
    Given I'm able to see inside some directories in the server
    Then I have a new source of information about the application
    And I can use it to make more specialized attacks
    And I can target components that otherwise I didn't know they existed

  Scenario Outline: Remediation
  Configure the apache service via a .htaccess file on every folder
    Given I run the following commands
    """
    $ ssh root@${OWASP_BWA_IP}
      password: owapsbwa
    # echo "Options -Indexes" > /owaspbwa/bwapp-git/bWAPP/js/.htaccess
    # echo "Options -Indexes" > /owaspbwa/bwapp-git/bWAPP/images/.htaccess
    # echo "Options -Indexes" > /owaspbwa/bwapp-git/bWAPP/documents/.htaccess
    # echo "Options -Indexes" > /owaspbwa/bwapp-git/bWAPP/passwords/.htaccess
    # echo "Options -Indexes" > /owaspbwa/bwapp-git/bWAPP/apps/.htaccess
    # echo "Options -Indexes" > /owaspbwa/bwapp-git/bWAPP/db/.htaccess
    """
    When I access the page <site>
    Then I get <result>

    Examples:
    | <site>           | <result>             |
    | bWAPP/js/        | Error 403: Forbidden |
    | bWAPP/images/    | Error 403: Forbidden |
    | bWAPP/documents/ | Error 403: Forbidden |
    | bWAPP/passwords/ | Error 403: Forbidden |
    | bWAPP/apps/      | Error 403: Forbidden |
    | bWAPP/db/        | Error 403: Forbidden |
    # password on latest bWAPP version
    # passwords in the bWAPP version of OWASPBWA I'm running

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.9/10 (Medium) - E:U/RL:U/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.2/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-11-28
