## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Page name:
    bWAPP/images/
    bWAPP/aim.php
    bWAPP/user_new.php
  CWE:
    CWE-749: Exposed Dangerous Method or Function
  Goal:
    Discover dangerous HTTP methods supported by the application
  Recommendation:
    Disable HTTP options method

  Background:
  Hacker's software:
    | <Software name> | <Version>             |
    | XAMPP           | 17.2.12-0             |
    | Ubuntu          | 18.04.1 LTS (amd64)   |
    | Google Chrome   | 70.0.3538.77 (64-bit) |
  TOE information:
    Given I'm running bWAPP-latest with XAMPP on my localhost
    # https://github.com/raesene/bWAPP

  Scenario: Normal use case
  Normal page with normal text
    When I access
    """
    http://localhost/bWAPP/app/images/
    """
    Then I get displayed the directory on the browser
    And everything works normally

  # Scenario: Static detection
  #   it is very long and is allowed indirectly
  #   it's hard, and not clear to show it this way, it's better to go dynamic

  Scenario: Dynamic detection
  The HTTP OPTIONS Method is enabled
    When I run the following command
    """
    $ curl -X OPTIONS http://localhost/bWAPP/app/images/ -i
    """
    Then I get the following result
    """
    HTTP/1.1 200 OK
    Date: Mon, 26 Nov 2018 21:25:06 GMT
    Server: Apache/2.4.37 (Unix) OpenSSL/1.0.2p PHP/7.2.12
    mod_perl/2.0.8-dev Perl/v5.16.3
    Allow: GET,POST,OPTIONS,HEAD,TRACE
    Content-Length: 0
    Content-Type: httpd/unix-directory
    """
    When I run the following command
    """
    $ curl -X OPTIONS http://localhost/bWAPP/app/aim.php -i
    """
    Then I get the following result
    # cropped because of length
    """
    HTTP/1.1 200 OK
    Date: Mon, 26 Nov 2018 22:01:23 GMT
    Server: Apache/2.4.37 (Unix) OpenSSL/1.0.2p PHP/7.2.12
    mod_perl/2.0.8-dev Perl/v5.16.3
    X-Powered-By: PHP/7.2.12
    Transfer-Encoding: chunked
    Content-Type: text/html; charset=UTF-8
    """
    When I run the following command
    """
    $ curl -X OPTIONS http://localhost/bWAPP/app/user_new.php -i
    """
    Then I get the following result
    # cropped because of length
    """
    HTTP/1.1 200 OK
    Date: Mon, 26 Nov 2018 22:02:09 GMT
    Server: Apache/2.4.37 (Unix) OpenSSL/1.0.2p PHP/7.2.12
    mod_perl/2.0.8-dev Perl/v5.16.3
    X-Powered-By: PHP/7.2.12
    Content-Length: 3421
    Content-Type: text/html; charset=UTF-8
    """
    And I conclude I can use GET, HEAD, POST, OPTIONS and TRACE
    And I recognize OPTIONS and TRACE as non-safe methods

  Scenario: Remediation
  Configure the apache service via httpd.conf
    When I add a clause that only allows HTTP's methods GET and POST
    # httpd.conf
    """
    <Directory "/">
      Order allow,deny
      Allow from all
      <LimitExcept POST GET>
        Deny from all
      </LimitExcept>
    </Directory>
    """
    When I run the following command
    """
    $ curl -X OPTIONS http://localhost/bWAPP/app/images/ -i
    """
    Then I get the following result
    """
    HTTP/1.1 405 Not Allowed
    Server: Apache/2.4.37 (Unix) OpenSSL/1.0.2p PHP/7.2.12
    mod_perl/2.0.8-dev Perl/v5.16.3
    Date: Mon, 26 Nov 2018 22:42:55 GMT
    Content-Type: text/html; charset=UTF-8
    Content-Length: 136
    Connection: keep-alive

    <html>
    <head><title>405 Not Allowed</title></head>
    <body bgcolor="white">
    <center><h1>405 Not Allowed</h1></center>
    <hr>
    </body>
    </html>
    """
    And I conclude the vulnerability has been patched
    # the same response (405 Not Allowed) applies for the other pages too

  Scenario: Exploitation
  HTTP OPTIONS disclosures information about the target that I shouldn't see
    Given I'm able to list the HTTP Methods the server receives
    """
    Allow: GET,POST,OPTIONS,HEAD,TRACE
    """
    Then I have a describing tool that allows me to make further attacks
    # for example, XSRF via HTTP TRACE

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.8/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.3/10 (Medium) - E:U/RL:U/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.5/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-11-26
