## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Security level medium
  Page name:
    bWAPP/htmli_get.php
  CWE:
    CWE-76: Improper Neutralization of Equivalent Special Elements
  Goal:
    Perform an HTML code injection attack
  Recommendation:
    Validate application inputs against expected input models

  Background:
  Hacker's software:
    | <Name>        | <Version>                 |
    | Kali Linux    | 4.18.0-kali1-amd64        |
    | Firefox       | 61.0.1 (64-bit)           |
    | Docker        | 18.06.1-ce, build e68fc7a |
    | bWAPP         | Docker raesene/bwapp      |

  TOE information:
    Given I enter the site bWAPP/htmli_get.php
    And a page asking for first and last name is displayed
    And there are two input fields for first and lastname and a button
    And the security level is set to medium

  Scenario: Normal use case

    When I fill the fields with my first and last name
    And I press the "Go" button
    Then I get a welcome message with my first and last name

  Scenario: Static detection
  Vulnerability finding over source code
    When I look at the page source code
    Then I realize there is an input validation function named "xss_check_1"
    When I inspect the content of that function
    Then I see that the vulnerability is present in the lines 92 - 93 and 100
    And it is on the file bWAPP/functions_external.php
    """
    091 $input = str_replace("<", "&lt;", $data);
    092 $input = str_replace(">", "&gt;", $input);
    ...
    100 $input = urldecode($input);
    101 return $input;
    """
    Given the function only replaces "<" and ">" for html entities
    Then there are several ways to bypass the validation

  Scenario: Dynamic detection
    When I write <input> on the user and password
    And I press the "Go" button
    Then I get <results>
    | <input>                         | <results>                              |
    | Fluid / Attacks                 | Welcome Fluid Attacks                  |
    | <script>alert(1)</script>/hi    | Welcome <script>alert(1)</script> hi   |

  Scenario: Exploitation
    Given the welcome message shows the code injection as plain text
    And the code is not executed it may seem safe at first glance
    But the function does not validate "<" and ">" equivalents
    When I encode the <input> as URL
    And I press the "Go" button
    Then I get <result>
    | <input>                                      | <results>                 |
    | %3Ch1%3EFluid%20Attacks%3C/h1%3E  /          | evidence.png              |
    | %3Ch1%3EWe%20hack%20your%20software%3C/h1%3E |                           |

  Scenario: Remediation
  Input validation and white lists ftw!
    Given an html injection vulnerability
    And a page that asks for first and last name
    Given people names don't contain numbers or special characters
    Then I can set a white list of how inputs should look like using a regex
    And I completely change the validation function
    """
    091  $regex  = '^[A-Za-z]{3,15}$'
    092  if (preg_match( $regex , $data) ) {
    093    //Valid first name and last name
    094    return $input;
    095  }
    096  else {
    097    echo ("invalid first/last name");
    098  }
    """
    When I try to inject code in the fields
    Then The validation function will accept only 3 to 15 letters
    And the vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.2/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.0/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
  No correlations have been found to this date 2018-11-20
