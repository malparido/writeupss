## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Injection
  Location:
    http://localhost/bWAPP/iframei.php - URL (header)
  CWE:
    CWE-829: Invalidated dimentions
  Goal:
    Manipulate the parameters of the size
  Recommendation:
    Use the POST method for send parameters

  Background:
  Hacker's software:
    | <Software name> | <Version>               |
    | Windows         | 10.0.17134.407 (64-bit) |
    | Firefox         | 64.0           (64-bit) |
    | XAMPP           | 3.2.2                   |
  TOE information:
    Given I'm running bWAPP-latest with XAMPP on my localhost
    And I'm accessing the bWAPP pages through my browser
    And I create a user in the directory

  Scenario: Normal use case
  Normal page with normal text
    When I access
    """
    http://localhost/bWAPP/iframei.php
    """
    And everything works normally

  Scenario: Static detection
  The site using a GET methot
    Given I using the view source code
    Then I found the code
    """
    if(!(isset($_GET["ParamUrl"])) || !
    (isset($_GET["ParamHeight"])) || !(isset($_GET["ParamWidth"])))
    {
      header("Location: iframei.php?
      ParamUrl=robots.txt&ParamWidth=250&ParamHeight=250");

      exit;
    }
    """
    Then I saw that the params of the size can be altered for any person

  Scenario: Exploitation
  The parameters can be found in the url
    Given I localize the vulnerability
    Then I proceed to exploit it
    And I change the following values
    """
    Before
    ParamUrl=robots.txt&ParamWidth=250&ParamHeight=250");

    After
    ParamUrl=robots.txt&ParamWidth=800&ParamHeight=600");
    """
    And I change the size of the sub-window [evidence](size.png)

  Scenario Outline: Remediation
  Change the method GET for POST
    """
    This vulnerability can be solved if is
    change the method of send of parameters,
    Also it is available the redirection of data
    by url because of the same problem but that
    is not the subject of this exploit, a new php
    script must be created that receives the
    information by methodo POST and in this way
    the vulnerability would be patched
    """
  Scenario: Correlations
    No correlations have been found to this date 2018-12-19