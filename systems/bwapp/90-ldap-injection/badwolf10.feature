## Version 1.4.0
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Injection
  Page name:
    bWAPP/ldapi.php
  CWE:
    CWE-90: Improper Neutralization of Special Elements used in an LDAP Query
  Goal:
    Enter a proper malicious ldap filter input to access sensitive information
  Recommendation:
    Reinforce validation of special characters to avoid ldap query injection

  Background:
  Hacker's software:
  | <Software name>    |    <Version>         |
  | Ubuntu             | 16.04 LTS (x64)      |
  | Mozilla Firefox    | 63.0                 |
  | Visual Studio Code | 1.29.1               |
  | OpenLDAP           | 2.4.42               |
  TOE information:
    Given I'm running bee-box on Ubuntu
    And I'm accessing the bWAPP pages through my browser
    And I set up a local installation of OpenLDAP
    And I create a user in the directory

  Scenario: Normal use case
  The site allows the user to search for user information stored in ldap
    Given The access to the page bWAPP/ldap_connect.php
    Then I connect to local LDAP instans with the parameters
    """
    Login: cn=admin,dc=local,dc=bwapp
    Password: custompassword
    Server: ldap://localhost:389
    Base DN: dc=local,dc=bwapp
    """
    Then I am redirected to the search page bWAPP/ldapi.php
    And I enter a surname
    And I get the user information in a table

  Scenario: Static detection
  The page does not expose any php code to see in source code on browser

  Scenario: Dynamic detection
    Given the url http://localhost/bWAPP/ldapi.php
    Then I try searching for users using the values for ldap attributes
    And I enter my surname [evidence](ldap-sn-search.png)
    And I find the search is performed with the surname "sn" attribute
    And I find no results using other attributes
    And I deduce LDAP filter is of the form "*(sn=)*"

  Scenario: Exploitation
    Given the url "http://localhost/bWAPP/ldapi.php"
    Then I try entering my surname and close the filter with "surname)"
    And I get no returned information
    Then I try injecting another attribute "surname)(objectclass=*"
    And I get the information of the user
    Then I try "*)(objectclass=*"
    And I get the list of all users stored in LDAP including admin
    And The list can be seen in [evidence](ldap-user-list.png)

  Scenario: Remediation
  Backend code needs input validation.
    """
    A whitelist with acceptable input characters can be created in order
    to reject non-conforming inputs or transfrom them. Custom attributes
    can also be created in LDAP that cannot be guessed by attacker
    """


