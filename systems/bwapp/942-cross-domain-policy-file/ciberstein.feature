## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Security Misconfiguration
  Location:
    http://localhost/bWAPP/sm_cross_domain_policy.php - domain (header)
  CWE:
    CWE-942: Using the cross domain policy
  Goal:
    Get access to user account with other password
  Recommendation:
    Restrict the upload of files to the server

  Background:
  Hacker's software:
    | <Software name> | <Version>               |
    | Windows         | 10.0.17134.407 (64-bit) |
    | Google Chrome   | 70.0.3538.110 (64-bit)  |
    | XAMPP           | 3.2.2                   |
  TOE information:
    Given I'm running bWAPP-latest with XAMPP on my localhost
    And I'm accessing the bWAPP pages through my browser
    And I create a user in the directory

  Scenario: Normal use case
  Normal page with normal text
    When I access
    """
    http://localhost/bWAPP/sm_cross_domain_policy.php
    """
    And everything works normally

  Scenario: Static detection
  The site not validated the redirection of the information
    Given I investigated on the subject
    Then I found the following script
    """
    https://ghostbin.com/paste/5rzy2/raw
    """
    Then I adapted the script to the system
    """
    https://ghostbin.com/paste/xhhz6/raw
    """
    And I hosted a script in the folder of the site
    And I proceed to test the script

  Scenario: Exploitation
  The script hosted in the site allows to redirect information
    Given I configure the script
    Then I proceed to login
    And I put the password changed in the script
    """
    admin
    """
    And I get login successfully [evidence](log.png)

  Scenario Outline: Remediation
  Restrict the policy of domain
    """
    Validate the policy of domains to avoid
    redirecting information
    """
  Scenario: Correlations
    No correlations have been found to this date 2018-12-06