# language: en

Feature: Detect and exploit vuln HTML Injection - Reflected (GET)
  From the bWAPP application
  From the A1 - Injection category
  With Low security level

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    Given I am browsing in Firefox 57.0.4
    Given a PHP with two HTML inputs for first name and last name resp.
    And the URL is bwapp/htmli_get.php
    Given the goal is to inject HTML

  Scenario: Static detection
    When I view the page source code
    Then I see both fields must be filled and there is no input validation
    """
    echo "Welcome " . $_GET($firstname) . " " . $_GET($lastname);
    """

  Scenario Outline: Dynamic detection and explotation
    When I type some <HTML1> in "first name" input
      And <HTML2> in the "last name" inputs
    Then it becomes <rendered> in the result
    Examples:
    |         HTML1       |   HTML2   |        rendered                  |
    | <h1> big text       |   </h1>   | "big text" appears very big      |
    | <strong> text       | </strong> | "text" appears in boldface       |

# Had to comment these out due to long lines
# | """
#   Listen! <audio autoplay>  <source
#   src="https://upload.wikimedia.org/wikipedia/
#   commons/1/13/Alzheimer%27s_Disease.ogg">
#   """
#                       | </audio>  | Listen! and audio starts playing |
# | """
#   <iframe width="560" height="315" src="https://www.youtube.com/embed/
#   q2NqUvXobQc" frameborder="0" allow="autoplay; encrypted-media">
#   """
#                       | </iframe> | YouTube video loads on page      |
