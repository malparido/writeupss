# language: en

Feature: Detect and exploit vuln HTML Injection - Reflected (POST)
  From the bWAPP application
  From the A1 - Injection category
  With Medium security level

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    And I am browsing in Firefox 57.0.4
    Given a PHP site with two HTML inputs for first name and last name resp.
    And the URL is bwapp/htmli_post.php
    And the goal is to inject HTML
    And at Low security level, this vuln is pretty much the same as GET htmli

  Scenario: Static detection
    When I view the page source code for Medium security level
    Then I see that the server will change ">" and "<" to "&gt" and "&lt"
    But it doesn't check for ocurrences of "&"
    """
    echo "Welcome " . htmli($firstname) . " " .  htmli($lastname);
    function htmli($data)
      $input = str_replace("<", "&lt;", $data);
      $input = str_replace(">", "&gt;", $input);
    """

    Scenario Outline: Dynamic exploitation
    When I type some <HTML1> in "first name" input
      And " " in the "last name" input
    Then it becomes <rendered> in the result
    Examples:
    |         HTML1                 |        rendered                  |
    | <h1> big text </h1>           | "big text" appears very big      |
    | &#8707 bug &#8712 $PHP_SELF!!! | ∃ bug ∈ $PHP_SELF!!!             |
