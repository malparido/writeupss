# language: en

Feature: Detect and exploit vuln HTML Injection - Reflected (URL)
  From the bWAPP application
  From the A1 - Injection category

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    And I am browsing in Firefox 57.0.4
    Given a PHP site that shows your current URL
    And the URL is bwapp/htmli_current_url.php
    And the goal is to inject HTML

  Scenario: Dynamic detection and exploitation - Low security
    When I open the page
    Then it says "Your current URL: http://localhost/htmli_current_url.php"
    When I append some text vg "?aaa"
    Then it is reflected in the result
    But if I append some HTML vg "?<h1>edls</h1>"
    Then it is printed in the result, but with URL encoding:
    """
    http://localhost/htmli_current_url.php?%3Ch1%3Eedls%3C/h1%3E
    """
    When I intercept the GET request with burp suite proxy:
    """
    GET /htmli_current_url.php?%3Ch1%3Eedls%3C/h1%3E HTTP/1.1
    """
    And I replace the encoded characters with my original string in burp
    Then the result is rendered in the browser

  Scenario: Dynamic detection and exploitation - Medium security
    When I try the low security scenario approach
    Then the string is encoded anyway by the server
    And I see that the server will change ">" and "<" to "&gt" and "&lt"
    But it doesn't check for ocurrences of "&"
    When I append ?&#8707 bug &#8712 $PHP_SELF!!! to the URL
    Then the rendered result is
    """
    Yo...URL: http://localhost/htmli_current_url.php?∃%20bug%20∈%20$PHP_SELF!!!
    """

    Scenario: Static detection - Low and medium security
    When I look into the code
    Then we see that the URL is taken and printed as-is with no sanitization:
    """
    $url = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]
    <?php echo "<p align=\"left\">Your current URL: <i>" . $url . "</i></p>";?>
    """
    When I switch to medium security
    Then some sanitization is done, but it does not clean to the "&" character:
    """
    $url = "<script>document.write(document.URL)</script>";
    """
