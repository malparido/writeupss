# language: en

Feature: Detect and exploit vuln HTML Injection - Stored (Blog)
  From the bWAPP application
  From the A1 - Injection category

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    And I am browsing in Firefox 57.0.4
    Given a PHP site where you can add an entry to a "blog"
    And the URL is bwapp/htmli_stored.php
    And the goal is to inject HTML

  Scenario: Normal use case
    When I type some text into the field
    Then text, date, user, and a consecutive entry number get stored in DB
    And retrieved by the page and rendered in a table

  Scenario Outline: Low security injection - Dynamic detection and exploitation
    When I type <html> into the field
    Then <html> is stored in the database as-is
    And rendered as <output> in the "Entry" column
    Examples:
    | <html>                         | <output>                          |
    | <h1>edls</h1>                  | big "edls"                        |
    | <script>alert("edls")</script> | a popup that says "edls" comes up |

  Scenario: Low security - Static detection
    When I look in the code
    Then I see that the blog entry is stored in the database as-is:
    """
    $entry = htmli($_POST["entry"]);
    $owner = $_SESSION["login"];
    $sql = "INSERT INTO blog (date, entry, owner)
            VALUES (now(),'" . $entry . "','" . $owner . "')";
    """

  Scenario: Medium or high security - No vulnerability - Static detection
    When <html> is retrieved from the database
    Then it is xss_checked before rendering with PHP's htmlspecialcharacters
    """
     <tr height="40">
      <td align="center"><?php echo $row->id; ?></td>
      <td><?php echo $row->owner; ?></td>
      <td><?php echo $row->date; ?></td>
      <td><?php echo xss_check_3($row->entry); ?></td>
    </tr>
    """
    And all special characters are turned to html elements
    And the output is rendered verbatim
