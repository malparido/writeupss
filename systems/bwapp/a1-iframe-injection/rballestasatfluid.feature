# language: en

Feature: Detect and exploit vuln iFrame injection
  From the bWAPP application
  From the A1 - Injection category

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    And I am browsing in Firefox 57.0.4
    Given a PHP site where an iFrame is rendered
    And the URL is bwapp/iframei.php

  Scenario Outline: Succesful injection
    When I go the URL
    Then I'm redirected to
    """
    http://localhost/iframei.php?ParamUrl=robots.txt&ParamWidth=\
    250&ParamHeight=250
    """
    When I change the parameter ParamUrl=<new-url>;width and height appropiately
    Then the ParamUrl gets <rendered> inside the iFrame
    Examples:
    | <new-url>             | <rendered>                             |
    | bugs.txt              | The list of bugs                       |
    | iframei.php           | Page inside of page, but not recursive |
    | passwords/heroes.xml  | Heroes' user, password and secrets     |
    | dw/bwapp.sqlite       | The site's database                    |

  Scenario: Static detection
    When I look at the code
    Then I see the parameters are used to load up an iframe
    And it shows the file robots.txt by default:
    """
    <iframe src="<?php echo $_GET["ParamUrl"]?>" height="<?php
    echo \
    $_GET["ParamHeight"]?>" width="<?php echo $_GET["ParamWidth"]?>"></iframe>
    """
