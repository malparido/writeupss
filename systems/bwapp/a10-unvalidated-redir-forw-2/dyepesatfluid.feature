# language: en

Feature: Unvalidated Redirect/Forward-2
  From system bWAPP
  From the A10 - Unvalidated Redirects & Forwads
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/unvalidated_redir_fwd_2.php
  Message: Unvalidated Redirects & Forwards
  Details:
        - Click here to go back to the portal.
  Objective: Redirect the request to an unsafe url
  """

Scenario: Dangerous Redirections
  Given the initial page
  """
  # unvalidated_redir_fwd_2.php
  <div id="main">
  <h1>Unvalidated Redirects & Forwards (2)</h1>
  <p>Click <a href="unvalidated_redir_fwd_2.php?ReturnUrl=portal.php">here\
  </a> to go back to the portal.</p>
  </div>
  """
  Then I use Burp to Modify the Request
  """
  # Burp Request
  GET /bWAPP/unvalidated_redir_fwd_2.php?ReturnUrl=portal.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101...
  Referer: http://192.168.56.101/bWAPP/unvalidated_redir_fwd_2.php

  # By inspecting the request, we can see it's just redirecting
  # to urls inside the project, can't redirect it outside of the scope
  # e.g
  # Burp Request modified, to redirect to google.com
  GET /bWAPP/unvalidated_redir_fwd_2.php?ReturnUrl=www.google.com HTTP/1.1
  Host: 192.168.56.101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  ...
  ...

  # Response
  GET /bWAPP/www.google.com HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox..

  # We see there, /bWAPP/ it's mandatory to redirect inside the bWAPP project
  # Even if I modify it, it will thrown an error:
  <html><head>
  <title>400 Bad Request</title>
  </head><body>
  <h1>Bad Request</h1>
  <p>Your browser sent a request that this server could not understand.<br />
  </p>
  <hr>
  <address>Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6 PHP/5.2.4-2ubuntu5 \
  with Suhosin-Patch mod_ssl/2.2.8 OpenSSL/0.9.8g Server at 192.168.56.101 \
  Port 80</address>
  </body></html>

  # Here, we got several options, redirect the user to a part of the site
  # where we have previously injected some evil page to collect it's
  # credentials, or if it's an admin we could try and take to an unrestricted
  # are to normal users
  """
