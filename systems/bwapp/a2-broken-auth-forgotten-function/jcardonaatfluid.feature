# language: en

Feature: Forgotten Function - Implementation Flaws
  From the bWAPP system
  Of the category A2: Broken Auth. And Session Management
  With low security level
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /ba_forgotten.php
    Message: Broken Auth. - Forgotten Function
    Details:
      - Text field to enter the email
      - Button to recover the secret
    Objective: Expose all the users secret
    """

  Scenario Outline: List the secret of the users
    When I enter the email and execute the action
    Then it displays the secret in plain text
    """
    Hello Disassembly! Your secret: Change?
    """
    Given I can get the secret of any user just by knowing the email
    And that information resides in other attacks
    When I pass one by one the <email> of each user
    Then it shows the <secret> of all the users
    Examples:
      |           <email>            | <secret>  |
      | bwapp-aim@mailinator.com     | Any bugs? |
      | bwapp-bee@mailinator.com     | Other     |
      | jorgecardonavargas@gmail.com | Change?   |
