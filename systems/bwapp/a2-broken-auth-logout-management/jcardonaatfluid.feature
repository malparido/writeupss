# language: en

Feature: Logout Management - Implementation Flaws
  From the bWAPP system
  Of the category A2: Broken Auth. And Session Management
  With low security level
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /ba_logout.php
    Message: Broken Auth. - Logout Management
    Details:
      - A link to logout from the current session
    Objective: Expose logout management implementation bug
    """

  Scenario: Recover a closed session
    Given I have already logged in with my credentials
    And the following PHPSESSID token was assigned
    """
    Cookie: PHPSESSID=39abd14753e2c3fe2486266860711051
    """
    When I log out with the given link to stop the session
    Then the token hasn't change so the session isn't over
    Given I try to go to other pages after being log out it gives me an error
    But when I press previous in the browser I get back the old session
