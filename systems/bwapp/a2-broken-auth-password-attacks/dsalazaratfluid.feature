# language: en

Feature: Session Mgmt. Session ID in URL
  From system bWAPP
  From the A2 - Broken Auth. & Session Mgmt.
  With Low security level


Background:
  Given I am running Kali GNU/Linux kernel 4.18.0-kali-amd64
  And I am browsing in Firefox Quantum 60.2.0
  And I am running Burp Suite Community edition v1.7.36
  Given the following
  """
  URL: http://localhost/bWAPP/ba_pwd_attacks_1.php
  Message: Enter your credentials (bee/bug)
  Details:
  - Login form: user/password
  Objective: Check if it is possible to make password cluster attacks
  """


  Scenario: Normal use case - Successful Authentication
    When I use the credentials bee/bug
    Then I get the following success message
    """
    Successful login!
    """


  Scenario: Exceptional use case - Failed Authentication
    When I provide an invalid Login OR an invalid Password
    Then I get the following error message
    """
    Invalid credentials! Did you forgot your password?
    """


  Scenario: Dynamic exploitation
    Given the login form
    Then I use burp to intercept it
    And use a cluster bomb exploit with payloads of both users and passwords
    Then I get the bee/bug successful login by using bruteforce
    Then The successful login can be identified for its different length
    """
    | Payload 1 | Payload 2 | Length |
    | bee       | bug       | 13898  |
    | go        | bug       | 13899  |
    | god       | bug       | 13899  |
    | go        | hey       | 13899  |
    | god       | hey       | 13899  |
    | bee       | hey       | 13899  |
    | go        | bye       | 13899  |
    | god       | bye       | 13899  |
    The list goes on
    """
