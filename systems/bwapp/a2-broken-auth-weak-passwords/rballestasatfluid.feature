# language: en

Feature: Detect and exploit vuln Weak Passwords
  From the bWAPP application
  From the A2 - Broken auth & Sessiong mgmt
  With Any security level

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    Given I am browsing in Firefox 57.0.4
    Given a PHP site with inputs for user and password
    And the URL is bwapp/ba_weak_pwd.php
    Given the goal is to login

  Scenario: Static detection
    When I look in the code
    Then I see the auth check is done against constants depending on <level>
    """
    $login = "test";
    switch($_COOKIE["security_level"]){
    case "0" :
        $password = "test"; break;...}
    """

  Scenario Outline: Dynamic exploitaiton
    When I type "test" in the "Login" field
    And  I type <pswd> in the "Password" field
    Then the login is succesful
    Examples:
      | <level> | <pswd>  |
      | Low     | test    |
      | Medium  | test123 |
      | High    | Test123 |