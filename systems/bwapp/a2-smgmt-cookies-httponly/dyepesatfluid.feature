# language: en

Feature: Session Mgmt. Cookies (HTTPOnly)
  From system bWAPP
  From the A2 - Broken Auth. & Session Mgmt.
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/smgmt_cookies_httponly.php
  Message: Session Mgmt. - Cookies (HTTPOnly)
  Details:
      - Button to check current Cookies
      - Clickable js function to check Cookies
  Objective: Check the cookies, modify them
  """

Scenario: Checking Cookies & Modification
  Given the inital site
  When I click on the button Cookies
  Then I should see something like this:
  """
  # smgmt_cookies_httponly.php Request
  POST /bWAPP/smgmt_cookies_httponly.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/smgmt_cookies_httponly.php
  Cookie: security_level=0; PHPSESSID=ae688d27f993fa379b38a4380f99de4d;
  Connection: close
  Upgrade-Insecure-Requests: 1
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 12

  form=cookies

  # Response on HTML rendered

  <tr height="30">
    <td>security_level</td>
    <td>0</td>
  </tr>

  <tr height="30">
    <td>PHPSESSID</td>
    <td>ae688d27f993fa379b38a4380f99de4d</td>
  </tr>
  """
  And I click on the href holding a javascript function
  But I get the same result, the difference is that there is no new request
  Then I click again on the button to tamper the request with Burp
  And I add another Cookie
  """
  POST /bWAPP/smgmt_cookies_httponly.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Referer: http://192.168.56.101/bWAPP/smgmt_cookies_httponly.php
  Cookie: security_level=0; PHPSESSID=ae688d27f993fa379b38a4380f99de4d; \
          FOREIGN-HEADER=666-TRVE_CVLT
  Connection: close
  ...
  ...
  Content-Length: 12
  """
  Then I look at the html to see what rendered
  """
  # smgmt_cookies_httponly.html
  <tr height="30">
  <td>security_level</td>
  <td>0</td>
  </tr>

  <tr height="30">
  <td>PHPSESSID</td>
  <td>ae688d27f993fa379b38a4380f99de4d</td>
  </tr>

  <tr height="30">
  <td>FOREIGN-HEADER</td>
  <td>666-TRVE_CVLT</td>
  </tr>
  """
