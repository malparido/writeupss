# language: en

Feature: Cookies (Secure) - Implementation Flaws
  From the bWAPP system
  Of the category A2: Broken Auth. And Session Management
  With low security level
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /smgmt_cookies_secure.php
    Message: Session Mgmt. - Cookies (Secure)
    Details:
      - A button to show the current cookies
      - Table of cookies with the name value pairs
    Objective: Expose cookies implementation flaw over non-SSL channel
    """

  Scenario: Steal a session through the cookies
    Given the next cookies where assigned to my current session
    """
    Name            Value
    security_level  0
    top_security    no
    PHPSESSID       7da71f45081a80be32c6d3f2f7c4b6c5
    """
    And I have opened another browser with the user bee
    """
    Name            Value
    security_level  0
    top_security    no
    PHPSESSID       93fc50057b7a0f394682dde606985880
    """
    Given I copy the PHPSESSID cookie from bee
    And delete the cookies of my current session
    When I place the copied token with the quick console and go to the portal
    """
    document.cookie='PHPSESSID=93fc50057b7a0f394682dde606985880';
    """
    Then it redirects me to the 'Set Security Level' page
    And after selecting one, I can continue as the user bee
    And in all of the other HTTP pages I can still see the cookies
