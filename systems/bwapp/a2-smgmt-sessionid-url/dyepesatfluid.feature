# language: en

Feature: Session Mgmt. Session ID in URL
  From system bWAPP
  From the A2 - Broken Auth. & Session Mgmt.
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/smgmt_sessionid_url.php
  Message: Session Mgmt. - Session ID in URL
  Details:
      -  Session IDs should never be exposed in the URL!
  Objective: Check how session cookies are exposed
  """

Scenario: Checking Session Cookies - Low level
Leaving Session values in plain sigth, it's a bad idea in any possible
case, such thing could lead to account impersonation without much thinkering.
  Given the inital site
  When I look at the url, I see the cookie PHPSESSID in plain as parameter
  And including its value
  """
  # URL on Browser smgmt_sessionid_url.php
  http://192.168.56.101/bWAPP/smgmt_sessionid_url.php?\
  PHPSESSID=ae688d27f993fa379b38a4380f99de4d
  """
  Then I look at the request, to see how is done
  """
  GET /bWAPP/smgmt_sessionid_url.php?PHPSESSID=ae688d27f993fa379b38a4380f99de4d\
  HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Referer: http://192.168.56.101/bWAPP/smgmt_sessionid_url.php
  Cookie: security_level=0; PHPSESSID=ae688d27f993fa379b38a4380f99de4d

  # Here I see the Cookie with PHPSESSID, being used in each GET Request in
  # plain sigth.
  """

Scenario: Checking Session Cookies - High level
  Given the session disclosure on low level
  Then I look at the High level, checking what changed
  And so far, they only remove it from the URL
  But still it remains in each referer, like it can be seen here with Burp
  """
  GET /bWAPP/smgmt_sessionid_url.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/smgmt_sessionid_url.php?\
  PHPSESSID=ae688d27f993fa379b38a4380f99de4d
  Cookie: security_level=2; PHPSESSID=ae688d27f993fa379b38a4380f99de4d
  Connection: close
  Upgrade-Insecure-Requests: 1
  Cache-Control: max-age=0

  # It's quite like before, just that doesn't appear in plain sigth, but
  # still is passed not only as a cookie, but a parameter.
  """
