# language: en

Feature: Session Mgmt. Session ID in URL
  From system bWAPP
  From the A2 - Broken Auth. & Session Mgmt.
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    And I am running Burp Suite Community edition v1.7.30
    Given the following
      """
      URL: http://localhost/bWAPP/ba_pwd_attacks_1.php
      Message: Broken Auth. - Password Attacks
      Details:
      - Login form: user/password
      Objective: Check how session cookies are exposed
      """

  Scenario: Password Transmitted Over HTTP
    Given the login form
    Then I write my credentials
    And use burp to intercept the request
      """
      # ba_pwd_attacks_1.php
      POST /bWAPP/ba_pwd_attacks_1.php HTTP/1.1
      Host: 192.168.75.128
      User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
      Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
      Accept-Language: en-US,en;q=0.5
      Accept-Encoding: gzip, deflate
      Referer: http://192.168.75.128/bWAPP/ba_pwd_attacks_1.php
      Cookie: security_level=0; PHPSESSID=a7b20ec7185af9e26737048be148e744
      Connection: close
      Upgrade-Insecure-Requests: 1
      Content-Type: application/x-www-form-urlencoded
      Content-Length: 34

      login=bee&password=bug&form=submit

      # There we can see in plain the passwords
      """
