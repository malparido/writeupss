# language: en

Feature: XSS Reflected - (Custom headers)
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_custom_header.php
  Message: XSS Reflected - (Custom headers)
  Details:
      - Some web clients use custom HTTP request headers...
      - Content of our bWAPP header:
      - bWAPP marked in bold
  Objective: Send custom header in request
  """

Scenario: XSS Reflected - (Custom headers)
  Given the site within it's description
  """
  # xss_custom_header.html
  51  <div id="main">
  52    <h1>XSS - Reflected (Custom Header)</h1>
  53    <p>Some web clients use custom HTTP request headers...</p>
  54    <p>
  55    Content of our <b>bWAPP</b> header:
  56    </p>
  57  </div>
  """
  When I use Burp to check for any custom headers used
  Then I see this:
  """
  # Burp on Request
  GET /bWAPP/xss_custom_header.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0)...
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/portal.php
  Cookie: security_level=0; PHPSESSID=a9b4bbb58f31919d346dce485ddb18bc
  Connection: close
  Upgrade-Insecure-Requests: 1
  Cache-Control: max-age=0
  """
  And I see so far no rare header on request
  Then I check at the response
  """
  # Burp Response
  HTTP/1.1 200 OK
  Date: Tue, 10 Jul 2018 01:38:44 GMT
  Server: Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6
  X-Powered-By: PHP/5.2.4-2ubuntu5
  Expires: Thu, 19 Nov 1981 08:52:00 GMT
  Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0
  Pragma: no-cache
  Connection: close
  Content-Type: text/html
  Content-Length: 12818
  """
  And again, there is non custom header
  Then I look at the details given on the challenge
  And I try to send the custom header: bWAPP on the request
  """
  # Burp Request with custom header
  GET /bWAPP/xss_custom_header.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  ...
  Connection: close
  Upgrade-Insecure-Requests: 1
  Cache-Control: max-age=0
  bWAPP: custom-header=666

  bWAPP: Custom header, with random values
  """
  Then I see as response in html the content of the custom header
  """
  # HTML response
  <div id="main">
    <h1>XSS - Reflected (Custom Header)</h1>
    <p>Some web clients use custom HTTP request headers...</p>
    <p>
    Content of our <b>bWAPP</b> header:
    <i>custom-header=666</i>    </p>
  </div>
  """
