# language: en

Feature: Detect and exploit vuln XSS - Reflected (GET) vulnerability
  From the bWAPP application
  From the A3 - Cross site scripting (XSS)
  With low security level

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    Given a PHP site with inputs for name and last name
    And the URL is bWAPP/xss_get.php

  Scenario: Normal Use Case
    When  I type name1 in the "name" input
      And I type name2 in the "last name" input
      And click on "Go"
    Then the page prints "Welcome name1 name2"

  Scenario Outline: Dynamic exploitation - JS injection
    When I type "<script><javascript></script>" in either of the fields
    Then the <result> is rendered in the webpage
    And so we conclude the site is vulnerable to XSS
    Examples:
    | <javascript>           | <result>                          |
    | alert("edls")          | a popup comes up that says "edls" |
    | alert(document.cookie) | cookie details in a popup         |

  Scenario: Static detection
    When I look in the code bWAPP/xss_get.php
    Then I see there is no input validation:
    """
    $firstname = $_GET["firstname"];
    $lastname = $_GET["lastname"];
      if($firstname == "" or $lastname == ""){
        echo "<font color=\"red\">Please enter both fields...</font>";
    } else{
        echo "Welcome " . $firstname . " " . $lastname;
    }
    """
