# Version 1.3.1
# language: en

Feature:
  TOE:
    bWAPP
  Category:
    A3 - Cross-Site Scripting (XSS)
  Page name:
    bWAPP/xss_stored_2.php
  CWE:
    CWE-352: XSRF
  Goal:
    Forge a malicious request and execute it with a logged user
  Recommendation:
    Implement CSRF tokens

  Background:
  Hacker's software:
  | <Name>                       | <Version>          |
  | Kali GNU/Linux               | 4.18.0-kali1-amd64 |
  | Firefox Quantum              | 60.2.0             |
  | Burp Suite Community Edition | 1.7.36             |
  TOE information:
    Given I am accessing the site bWAPP/xss_stored_2.php
    And enter a php site that allows me to vote for a movie type
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level

  Scenario: Normal use case
  Voting for a movie
    Given I access the site
    And I see this message "Please choose your favorite movie genre"
    And can select a movie type from the following dropdown list:
    """
    - Horror
    - Action
    - Science Fiction
    """
    Then I select Science Fiction
    And click on the "Like" button
    Then I read a message saying "Thank you for making your choice!"

  Scenario: Static detection
  The code does not implement CSRF validation like CSRF tokens
    When I look at the code on /var/www/bWAPP/xss_stored_2.php
    Then I can see the vulnerability from lines 24 to 29
    And it's being caused due to the lack of a anti CSRF validations
    """
    $message = "";
    24  if(isset($_REQUEST["genre"])) {
    25      $genre = $_REQUEST["genre"];
    26      switch($_COOKIE["security_level"]) {
    27          case "0" :
    28              setcookie("movie_genre", $genre, time()+3600, "/",
                    "", false, false);
    29              break;
    ... #Unimportant lines
    85      }
    86      $message = "Thank you for making your choice!";
    87  }
    """
    Then I build a CSRF script for creating or removing blog entries

  Scenario: Dynamic detection
  Validation token missing
    Given I am listening with Burp
    And I access the site
    And I vote for the "Sicence fiction" movie type
    Then I intercept the following request:
    """
    GET /bWAPP/xss_stored_2.php?genre=sci-fi&form=like HTTP/1.1
    ... #Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=a413028fd8f00da10ad75095d401fa96; movie_genre=sci-fi
    """
    Then I can conclude that no token parameter is being required for the GET
    And I can build a CSRF script for voting for a specific movie gender

  Scenario: Exploitation
  Sending a link to a bWAPP user and make him post whatever we want
    Given I already know I can build a CSRF attack
    Then I proceed to build the following script:
    """
    <body onload="document.forms[0].submit()">
      <form method="GET" enctype="application/x-www-form-urlencoded"
      action="http://192.168.56.101/bWAPP/xss_stored_2.php"/>
        <input type="hidden" name="genre" value="sci-fi" />
        <input type="hidden" name="form" value="like" />
    </body>
    """
    And save my malicious script in "jsfiddle.net"
    And get a link for executing it: "http://jsfiddle.net/8htk2357/4/show"
    Then I send the link to a victim which in this case will be myself
    When I open the malicious link logged as bee
    Then I get instantly redirected to the bWAPP site
    And the message "Thank you for making your choice!" is already there
    Then I can conclude that the malicious link executed de script
    And redirected me to bWAPP
    And voted for me
    And I could force any logged user to vote for any genre I want

  Scenario: Remediation
  Implementing CSRF tokens
    Given I already know that xss_stored_2.php is vulnerable to CSRF attacks
    Then I add two functions to the file /var/www/bWAPP/functions_external.php:
    """
    622  function get_csrf_token() {
    623      if(isset($_SESSION['token'])) {
    624          return $_SESSION['token'];
    625      } else {
    626          $token = bin2hex(openssl_random_pseudo_bytes(16));
    627          $_SESSION['token'] = $token;
    628          return $token;
    629      }
    630  }
    631
    632  function validate_csrf_token($token) {
    633      if ($token == get_csrf_token()) {
    634          return true;
    635      } else {
    636          return false;
    637      }
    638  }
    """
    Then these functions will allow me to create and validate tokens in the blog
    Then I modify the code in /var/www/bWAPP/xss_stored_2.php
    And create an "$original_token" value on line 25
    And such value stores a token when the user visits the blog
    And try to create an "$inserted_token" from lines 26 to 28
    And such value comes from any GET request made to the blog
    And create an additional HTML input on line 154 for GET requests
    And such input creates a new token if there is none or recycles the old one
    Then I create a conditional on lines 33 to 35
    And it checks if "$original_token" and "$inserted_token" differ.
    Then if they differ, it means that the user never went through the blog
    And never obtained an "$original_token"
    Then the request is likely to be a CSRF attack
    And the GET request is invalidated
    """
    24  $message = "";
    25  $original_token = get_csrf_token();
    26  if(isset($_REQUEST["token"])) {
    27      $inserted_token = $_REQUEST["token"];
    28  }
    29  if(isset($_REQUEST["genre"])) {
    30      $genre = $_REQUEST["genre"];
    31      switch($_COOKIE["security_level"]) {
    32          case "0" :
    33              if (! validate_csrf_token($inserted_token)) {
    34                  $message =
                        "<font color=\"red\">CSRF Detected and stopped.</font>";
    35              }
    36              else {
    37                  setcookie("movie_genre", $genre, time()+3600, "/", "",
                        false, false);
    38                  $message = "Thank you for making your choice!";
    39              }
    40              break;
    ... #Unimportant lines
    95      }
    96  }
    ... #Unimportant lines
    153  <form action="<?php echo($_SERVER["SCRIPT_NAME"]); ?>" method="GET">
    154      <input type="hidden" name="token" \
             value="<?php echo get_csrf_token(); ?>"/>
    """
    Then after making all these changes
    Then I try the previous malicious script for automatically voting
    And get the following message:
    """
    CSRF Detected and stopped.
    """
    And the script fails to vote
    Then I also try to manually vote as a normal user would do
    And it works correctly
    Then I confirm the patched vulnerability and CSRF attacks are not possible

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.2/10 (Medium) - AV:N/AC:H/PR:N/UI:R/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.0/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-10-31
