#language: en

Feature: Detect and exploit vuln Insecure DOR (Change Secret)
  From the bWAPP application
  From the A4 - Insecure DOR category

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    And I am browsing in Firefox 57.0.4
    Given a PHP site wherein a user can change their "secret"
    And the URL is bwapp/insecure_direct_object_ref_1.php

  Scenario: Normal use case
    When the *currently logged-in user* types a string in the field
    Then the column "field" in their row is changed

  Scenario: Dynamic detection and exploitation
    # How does the script know who the *currently logged-in user* is?
    When I intercept the POST request with burp suite's proxy
    Then I see the request parameters go like this:
    """
    secret=myNewSuperSecret&login=bee&action=change
    """
    When I change the login parameter
    Then I can change another user's "secret" without being logged in

  Scenario: Static detection
    When I look into the PHP script code
    Then I see that for this security level tokens are not checked:
    """
    if(isset($_REQUEST["login"]) && $_REQUEST["login"])
        $login = $_REQUEST["login"];
    """
    Then we can exploit this vulnerability as previously described