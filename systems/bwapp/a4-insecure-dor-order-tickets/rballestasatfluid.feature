#language: en

Feature: Detect and exploit vuln Insecure DOR (Order Tickets)
  From the bWAPP application
  From the A4 - Insecure DOR category

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    And I am browsing in Firefox 57.0.4
    Given a PHP site wherein a user can order tickets
    And the URL is bwapp/insecure_direct_object_ref_2.php
    And an input field to type how many tickets the user wants

  Scenario: Normal use case
    When the user writes a value in the input
    Then the result of the transaction is printed in the page:
    """
    You ordered 10 movie tickets.
    Total amount charged from your account automatically: 150 EUR.
    Thank you for your order!
    """ # Tickets are 15 EUR apiece

  Scenario: Dynamic detection and exploitation
    When I look at the HTML code
    Then I see there is another hidden field:
    """
    <input name="ticket_price" value="15" type="hidden">
    """
    When I change the value to 0
    Then the amount charged is 0 as well
    When I intercept the POST request with burp suite's proxy
    Then I see the request parameters go like this:
    """
    ticket_quantity=100&ticket_price=15&action=order
    """
    And I can change the ticket price from there as well

  Scenario: Static detection and further exploitation
    When I look into the PHP code
    Then I see that the only validation done is to negative prices
    """
    $ticket_price = $_REQUEST["ticket_price"]; #if isset(), else default to 15
    $ticket_quantity = abs($_REQUEST["ticket_quantity"]);
    $total_amount = $ticket_quantity * $ticket_price;
    """