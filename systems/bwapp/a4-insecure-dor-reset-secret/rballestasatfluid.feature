#language: en

Feature: Detect and exploit vuln Insecure DOR (Reset Secret)
  From the bWAPP application
  From the A4 - Injection category

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    And I am browsing in Firefox 57.0.4
    Given a PHP site wherein a user can reset their "secret"
    And the URL is bwapp/insecure_direct_object_ref_3.php

  Scenario: Normal use case
    When the *currently logged-in user* hits the reset button
    Then the column "secret" in their row in DB should change to "Any bugs?"

  Scenario: Dynamic detection and exploitation
    # How does the script know who the *currently logged-in user* is?
    When I intercept the POST request with burp suite's proxy
    Then I see the request parameters go like this:
    """
    <reset><login>bee</login><secret>Any bugs?</secret></reset>
    """
    When I change the login or secret parameter
    Then I can change any user's "secret" without being logged in
    And I can bypass the normal use case where the reset message is fixed

  Scenario: Static detection and further exploitation
    When I look into the HTML code
    Then I see that the reset button calls a javascript function ResetSecret()
    And that function makes up the request:
    """
    xmlHttp.send("<reset><login>bee</login><secret>Any bugs?</secret></reset>");
    """
    Then we can also exploit this vulnerability by editing the JS code:
    """
    <input value="Any bugs?" onclick="anyOtherFunction();" type="button">
    """
    When I look into the PHP code
    Then I see it receives the request and UPDATES the DB
    """
    UPDATE users SET secret = '" . $secret . "' WHERE login = '" . $login . "'
    """
    And we can take advantage of no input validation to alter DB to inject SQL
    When I change the secret to be reset in the request above to
    """
    <secret>hahah! ur secret's gone!' where 'a'='a';#</secret>
    """
    Then we have deleted every user's secret