# language: en

Feature: Insecure SMB Configuration
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am running Burp Suite Community edition v1.7.30
    And I am running nmap 7.40v
    And I am running smbmap
    And I am running enum4linux v0.8.9
    Given the following in the url
      """
      Details:
      - SMB service found to be open during scan,
      do an enumeration on ports 139,445 to find out
      any useful information.
      Objective: SMB Enumeration
      """

  Scenario: SMB Enumeration
    Given a port enumeration
      """
      # Port enumeration
      139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: ITSECGAMES)
      ...
      ...
      445/tcp  open  netbios-ssn Samba smbd 3.0.28a (workgroup: ITSECGAMES)
      """
    Then I use enum4linux
    And I see the following information about the smb service
      """
      # enum4linux -a 192.168.75.128
       ===========================================
      |    Share Enumeration on 192.168.75.128    |
       ===========================================
      WARNING: The "syslog" option is deprecated

      Sharename       Type      Comment
      ---------       ----      -------
      print$          Disk      Printer Drivers
      tmp             Disk      oh noes!
      opt             Disk
      IPC$            IPC       IPC Service (bee-box server (Samba 3.0.28a))
      Xerox_Phaser_8500DN_PS:7 Printer   Xerox Phaser 8500DN PS
      Snagit_9:6      Printer   Snagit 9
      Send_To_OneNote_2010:8 Printer   Send To OneNote 2010
      PDF             Printer   PDF

      Server               Comment
      ---------            -------

      Workgroup            Master
      ---------            -------
      ITSECGAMES           BEE-BOX
      WORKGROUP            MORGUL
      """
