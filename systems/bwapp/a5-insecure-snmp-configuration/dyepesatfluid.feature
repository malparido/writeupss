# language: en

Feature: Insecure SNMP Configuration
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am running Burp Suite Community edition v1.7.30
    And I am running nmap 7.40v
    And I am running Metasploit 5.0.0-dev-888dc43
    Given the following in the url
      """
      URL: http://localhost/bWAPP/sm_snmp.php
      Message: Insecure SNMP Configuration
      Details:
      - The SNMP server has an insecure configuration!
      Objective: Enumerate existing HTTP connections and running procs.
      """

  Scenario: Insecure SNMP configuration
    The Simple Network Management Protocol (SNMP)
    is an application layer protocol, enumeration
    is oftenly used to enumerate user accounts
    passwords, groups, system names and devices
    on the targeted system.
    Given server, I first do a port enumeration
      """

      """
    And after I checked snmp port is avaliable
    Then I use metasploit to do the snmp enumeration
      """
      msf5 > use auxiliary/scanner/snmp/snmp_enum
      msf5 auxiliary(scanner/snmp/snmp_enum) > show options

      Module options (auxiliary/scanner/snmp/snmp_enum):

        Name       Current Setting  Required  Description
        ----       ---------------  --------  -----------
        COMMUNITY  public           yes       SNMP Community String
        RETRIES    1                yes       SNMP Retries
        RHOSTS                      yes       The target address range
        RPORT      161              yes       The target port (UDP)
        THREADS    1                yes       The number of concurrent threads
        TIMEOUT    1                yes       SNMP Timeout
        VERSION    1                yes       SNMP Version <1/2c>

      msf5 auxiliary(scanner/snmp/snmp_enum) > set rhosts 192.168.75.128
      rhosts => 192.168.75.128
      """
    And I see the system information
      """
      # [*] System information
      Host IP                       : 192.168.75.128
      Hostname                      : bee-box
      Description                   : Linux bee-box 2.6.24-16-generic
      Contact                       : Your master bee
      Location                      : Every bee needs a home!
      Uptime snmp                   : 04:50:31.15
      Uptime system                 : 04:50:06.23
      System date                   : 2018-8-1 20:37:55.0
      """
    And network information
      """
      [*] Network information:

      IP forwarding enabled         : no
      Default TTL                   : 64
      TCP segments received         : 12159
      TCP segments sent             : 10126
      TCP segments retrans          : 0
      Input datagrams               : 13339
      Delivered datagrams           : 13330
      Output datagrams              : 10392

      [*] Network interfaces:

      Interface                     : [ up ] lo
      Id                            : 1
      Mac Address                   : :::::

      Interface                     : [ up ] eth0
      Id                            : 2
      Mac Address                   : 08:00:27:ca:72:3f
      """
    Then I look at running procs
      """
      [*] Processes:

      Id                  Status              Name                Path
      1                   runnable            init                /sbin/init
      2                   runnable            kthreadd            kthreadd
      3                   runnable            migration/0         migration/0
      4                   runnable            ksoftirqd/0         ksoftirqd/0
      5                   runnable            watchdog/0          watchdog/0
      6                   runnable            events/0            events/0
      7                   runnable            khelper             khelper
      """
