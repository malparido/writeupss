# language: en

Feature: Man-in-the-Middle Attack (HTTP)
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    And I am running Burp Suite Community edition v1.7.30
    Given the following in the url
      """
      URL: http://localhost/bWAPP/sm_mitm_1.php
      Message: Man-in-the-Middle Attack (HTTP)
      Details:
      - Enter your credentials
      - Login/Password
      Objective: Perform a Man-in-the-Middle attack
      """

  Scenario: Man-in-the-Middle Attack over HTTP
    Given the login form
      """
      # sm_mitm_1.php
      <form action="/bWAPP/sm_mitm_1.php" method="POST">

      <p><label for="login">Login:</label><br />
      <input type="text" id="login" name="login" size="20" /></p>

      <p><label for="password">Password:</label><br />
      <input type="password" id="password" name="password" size="20" /></p>

      <button type="submit" name="form" value="submit">Login</button>

      </form>
      """
    Then I input access credentials
      """
      # Access credentials:
      # login: bee
      # password: bug
      """
    And I use Burpsuite to intercept the Request
      """
      # Burp Request
      POST /bWAPP/sm_mitm_1.php HTTP/1.1
      Host: 192.168.75.128
      User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
      Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
      Accept-Language: en-US,en;q=0.5
      Accept-Encoding: gzip, deflate
      Referer: http://192.168.75.128/bWAPP/sm_mitm_1.php
      Cookie: security_level=0; PHPSESSID=7a98d15b45370254d78400c0262e57b6
      Connection: close
      Upgrade-Insecure-Requests: 1
      Content-Type: application/x-www-form-urlencoded
      Content-Length: 34

      login=bee&password=bug&form=submit

      # In this HTTP request, the target is the TCP connection
      # between server and host.
      # 1. By using Burp, the connection is splitted into 2 new connections
      # one between client and Burp (Attacker) and server and Burp
      # which allows the attacker to see/modify/tamper/eavesdrop
      # the initial Request and check server response
      """
    Then server sends its response
      """
      # Burp response
      HTTP/1.1 200 OK
      Date: Thu, 02 Aug 2018 01:45:07 GMT
      Server: Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6 PHP/5.2.4-2ubuntu5
      X-Powered-By: PHP/5.2.4-2ubuntu5
      Expires: Thu, 19 Nov 1981 08:52:00 GMT
      Cache-Control: no-store, no-cache, must-revalidate, post-check=0
      Pragma: no-cache
      Connection: close
      Content-Type: text/html
      Content-Length: 13416
      """
