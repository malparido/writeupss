# language: en

Feature: Information Disclosure
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    Given the following in the url
      """
      URL: http://localhost/bWAPP/phpinfo.php
      Message: -
      Details:
      - phpinfo file disponible for anyone
      Objective: Check php info of the server
      """

  Scenario: Information Disclosure
    Given the file phpinfo file
    Then I open it on browser
    And I can see the following information
      """
      # phpinfo.php
      # System information plus build date
      # Which migth give the attacker an specific version to search for any
      # vulnerabilities
      System   Linux bee-box 2.6.24-16-generic #1 SMP Thu Apr 10 13:23:42 UTC
      2008 i686
      Build Date   Feb 27 2008 20:27:58
      Server API   Apache 2.0 Handler
      """
    And I can see PHP core information
      """
      # phpinfo.php PHP Core
      allow_call_time_pass_reference  On  On
      allow_url_fopen  On  On
      allow_url_include  On  On
      """
    And I can see if it using any module for databases or ssl
      """
      # Openssl
      OpenSSL support   enabled
      OpenSSL Version   OpenSSL 0.9.8g 19 Oct 2007

      # MySql
      PDO Driver for MySQL, client library version  5.0.96

      # MySqli
      Client API library version   5.0.96
      Client API header version   5.0.51a
      MYSQLI_SOCKET   /var/run/mysqld/mysqld.sock
      """
