# language: en

Feature: Backup Source Code Detected
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    Given the following in the url
      """
      URL: http://localhost/bWAPP/portal.bak
      Message: -
      Details:
      - File can be downloaded without any restriction
      Objective: Check the backup content
      """

  Scenario: Backup Source Code Detected
    According to the CWE-530 (Exposure of Backup File to
    an Unathorized Control Sphere), is a misconfiguration
    of a file stored in a directory accesible to outsiders.
    It migth not be that critical, but it can reveal
    useful information for an attacker.
    Given the file portal.bak file
    Then I download it
    And I see it's content
      """
      # Displaying Content of backup file
      # skorn@Morgul ~/Downloads> more portal.bak
      <p><i>Which bug do you want to hack today? :)</i></p>
      <form action="<?php echo($_SERVER["SCRIPT_NAME"]);?>" method="POST">
      <select name="bug" size="9" id="select_portal">
      <?php
      // Lists the options from the array 'bugs' (bugs.txt)
      foreach ($bugs as $key => $value)
      {
      $bug = explode(",", trim($value));
      // Debugging
      // echo "key: " . $key;
      // echo " value: " . $bug[0];
      // echo " filename: " . $bug[1] . "<br />";
      echo "<option value='$key'>$bug[0]</option>";
      }
      ?>
      </select>
      <br />
      <button type="submit" name="form" value="submit">Hack</button>
      </form>
      """
