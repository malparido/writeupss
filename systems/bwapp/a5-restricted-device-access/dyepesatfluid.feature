# language: en

Feature: Restricted Device Access
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Google Chrome Version 68.0.3440.75 (Official Build) (64-bit)
    And I am using Tamper Chrome extension
    And I am using Chrome UA Spoofer extension
    Given the following in the url
      """
      URL: http://localhost/bWAPP/restrict_device_access.php
      Message: Restricted Device Access
      Details:
      - Only some authorized devices have access to the content of this page.
      Objective: This is not a smartphone or a tablet computer (Apple/Android)!
      """

  Scenario: Restricting access through header
    Given the restriction on the site
      """
      # restrict_device_access.php
      This is not a smartphone or a tablet computer (Apple/Android)!
      """
    Then I use the UA Spooder extension to change the host headers
    And I choose IOS - Iphone6
      """
      User-Agent: Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X)
      AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e
      Safari/8536.25
      """
    Then I can access it!
      """
      This is a smartphone or a tablet computer!
      """
