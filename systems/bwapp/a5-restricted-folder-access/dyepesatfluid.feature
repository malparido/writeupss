# language: en

Feature: Restricted Folder Access
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    And I am running Burp Suite Community edition v1.7.30
    Given the following in the url
      """
      URL: http://localhost/bWAPP/restrict_folder_access.php
      Message: Restricted Device Access
      Details:
      - Only authorized users have access to the documents folder.
      - Log off and try to access files in this directory...
      Objective: Access the "restricted" files
      """

  Scenario: Unrestricted Files
    Given the "restriction" on the folder
    Then the archives in there are listed
      """
      # restrict_folder_access.php
      Terminator_Salvation.pdf
      The_Cabin_in_the_Woods.pdf
      bWAPP_intro.pdf
      The_Amazing_Spider-Man.pdf
      Iron_Maiden.pdf
      trve-exploit.sh
      The_Dark_Knight_Rises.pdf
      The_Incredible_Hulk.pdf
      """
    Then I access one of the files
    And I look at the headers of the request
      """
      # Burp Request
      GET /bWAPP/restrict_folder_access.php HTTP/1.1
      Host: 192.168.75.128
      User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
      Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
      Accept-Language: en-US,en;q=0.5
      Accept-Encoding: gzip, deflate
      Referer: http://192.168.75.128/bWAPP/portal.php
      Cookie: security_level=0; PHPSESSID=cf24fa284e05b5cb69ed93815bbac9aa
      Connection: close
      Upgrade-Insecure-Requests: 1
      Cache-Control: max-age=0

      # So far, the only header that will make the request to be
      # "accepted" would be the Cookies: PHPSESSID
      # Let's see if this is being validated
      """
    Then I logout
    And I try to access one specific file
    But knowing I'm not logged in
    And the session cookies aren't there
      """
      # File to access
      Terminator_Salvation.pdf
      """
    Then I try to access, while checking the request
      """
      # http://192.168.75.128/bWAPP/documents/Terminator_Salvation.pdf
      GET /bWAPP/documents/Terminator_Salvation.pdf HTTP/1.1
      Host: 192.168.75.128
      User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
      Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
      Accept-Language: en-US,en;q=0.5
      Accept-Encoding: gzip, deflate
      If-Modified-Since: Sun, 02 Nov 2014 22:52:04 GMT
      If-None-Match: "ce103-71065-506e814298d00"
      Cache-Control: max-age=0
      """
    And I see I can access the resource without being authenticated
