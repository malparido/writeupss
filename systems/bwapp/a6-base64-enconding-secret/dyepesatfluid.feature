# language: en

Feature: Base64 Enconding(Secret)
  From system bWAPP
  From the A6 - Sensitive Data Exposure
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/insecure_crypt_storage_3.php
  Message: Base64 Encoding(Secret)
  Details:
        - Your secret has been stored as an encrypted cookie!
        - HINT: try to decrypt it...
  Objective: Look for the secret cookie and decrypt it
  """

Scenario: Secret cookies weak ciphering
  Given the form with it's details
  When I check the Burp request/response, I see this:
  """
  # Burp request insecure_crypt_storage_3.php
  GET /bWAPP/insecure_crypt_storage_3.php HTTP/1.1
  Host: 192.168.56.101
  ...
  ...
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Cookie: security_level=0; PHPSESSID=a7629a18732ddad1988f3b83453760c7
  ...
  ...

  # On request, the cookie doesn't seem to be there, what about in response?

  # Burp Response
  HTTP/1.1 200 OK
  Date: Wed, 11 Jul 2018 08:48:56 GMT
  X-Powered-By: PHP/5.2.4-2ubuntu5
  Expires: Thu, 19 Nov 1981 08:52:00 GMT
  Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0
  Pragma: no-cache
  Set-Cookie: secret=YXNkc2Fk; expires=Wed, 11-Jul-2018 09:48:56 GMT; path=/
  ...
  ...

  # On there I see the Set-Cookie Header, with the secret cookie
  # on base64.
  secret=YXNkc2Fk;
  """
  Then I put it on a file to decode it
  """
  # Decoding the Base64 text
  # skhorn@Morgul ~/D/f/t/s/bwapp> touch secret.txt
  # skhorn@Morgul ~/D/f/t/s/bwapp> echo "YXNkc2Fk" > secret.txt
  #
  # skhorn@Morgul ~/D/f/t/s/bwapp> cat secret.txt | base64 -d
  # asdsad
  #
  # As strange as it seems, that's the secret key, it was changed for testing
  # on previous challenges
  """
