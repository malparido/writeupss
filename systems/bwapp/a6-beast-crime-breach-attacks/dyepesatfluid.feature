# language: en

Feature: BEAST/CRIME/BREACH
  From system bWAPP
  From the A6 - Sensitive Data Exposure
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am running Burp Suite Community edition v1.7.30
    And I am running O-Saft 18.07.78
    Given the following in the url
      """
      URL: http://localhost/bWAPP/insuff_transp_layer_protect_3.php
      Message: BEAST/CRIME/BREACH Attacks
      Details:
      - The Lighttpd web server is vulnerable to the BEAST,
        CRIME and BREACH attacks.
      - Configure TLS 1.1 or above, disable SSL and HTTP compression!
      - HINT: test the SSL connection on port 9443 with the O-Saft tool...
      Objective: Findout if the lighttpd 9443 port is vulnerable.
      """

  Scenario: BEAST/CRIME/BREACH Attacks
    BEAST (Browser Exploit Against SSL/TLS)
    CRIME (Compression Ratio Info-Leak Made Easy)
    BREACH (Browser Reconnaissance and Exfiltration via Adaptive Compression
    of Hypertext), all three attacks relies on SSL implementations,
    which involves weak cryptographic algorithms,  compression attacks
    that allowed the ability to recover selected parts of the
    traffic
    Given the server, the I modify the lighttpd config to disable SSL
    And disable HTTP Compression
      """
      # /etc/lighttpd/conf-enabled/10-ssl.conf
      ##
      ## Documentation: /usr/share/doc/lighttpd-doc/ssl.txt
      ##      http://www.lighttpd.net/documentation/ssl.html

      #### SSL engine
      $SERVER["socket"] == ":9443" {
            ssl.engine                  = "enable"
            ssl.pemfile                 = "/etc/ssl/certs/bee-box.pem"
            ssl.use-sslv2               = "enable"
            ssl.use-sslv3               = "disable"
            ssl.use-compression         = "disable"
      }
      """
    Then I do port/service enumeration
      """
      # Port enumeration: 9443
      skorn@Morgul ~/Downloads> sudo nmap -sS -T4 -sV -sC 192.168.75.128 -p 9443
      Nmap scan report for 192.168.75.128
      Host is up (0.000091s latency).

      PORT     STATE SERVICE  VERSION
      9443/tcp open  ssl/http lighttpd 1.4.19
        |_http-server-header: lighttpd/1.4.19
        |_http-title: Site doesn't have a title (text/html).
        | ssl-cert: Subject: commonName=bee-box.bwapp.local/organizationName=MME
      stateOrProvinceName=Flanders/countryName=BE
        | Not valid before: 2013-04-14T18:11:32
        |_Not valid after:  2018-04-13T18:11:32
        |_ssl-date: 2018-08-08T03:11:03+00:00; -9h41m38s from scanner time.
        | sslv2:
        |   SSLv2 supported
        |   ciphers:
        |     SSL2_RC2_128_CBC_WITH_MD5
        |     SSL2_DES_192_EDE3_CBC_WITH_MD5
        |     SSL2_DES_64_CBC_WITH_MD5
        |     SSL2_RC2_128_CBC_EXPORT40_WITH_MD5
        |     SSL2_RC4_128_WITH_MD5
        |_    SSL2_RC4_128_EXPORT40_WITH_MD5
      MAC Address: 08:00:27:CA:72:3F (Oracle VirtualBox virtual NIC)

      Host script results:
        |_clock-skew: mean: -9h41m38s, deviation: 0s, median: -9h41m38s
      """
    Then I use the O-saft tool to check that port vulnerabilities
    And this challenge is not to exploit
    But to enumerate and find out if it is vulnerable
      """
      # The output of the tool is quite extensive
      # And even checks ciphers strength per protocol.
      # sudo ./o-saft-docker +check --v --host 192.168.75.128 --port 9443
      # Here we go:
      Target does not support SSLv2:        no ( )
      Target does not support SSLv3:        no ( )

      Connection is safe against BREACH attack:  no (<<NOT YET IMPLEMENTED>>)
      Connection is safe against CRIME attack:  no
      Connection is safe against BEAST attack (any cipher):  no
      ( SSLv2:RC2-CBC-MD5 SSLv2:DES-CBC3-MD5 SSLv2:DES-CBC-MD5
      SSLv2:EXP-RC2-CBC-MD5 SSLv2:EXP-RC2-CBC-MD5 .... )

      # It shows an extensive list of all the ciphers used by this
      # service/implementation which are vulnerable to the BEAST attack
      #
      # This Attacks are quite hard to do, plus they have certain
      # specific requirements to work, back in 2014 or before, there
      # were a slightly chance to make them work or exploit, even in the
      # wild, now the chances are lesser than that
      """
