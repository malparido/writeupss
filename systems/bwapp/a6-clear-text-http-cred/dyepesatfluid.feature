# language: en

Feature: Clear Text HTTP (Credentials)
  From system bWAPP
  From the A6 - Sensitive Data Exposure
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/insuff_transp_layer_protect_1.php
  Message: Clear text HTTP (Credentials)
  Details:
        - Login form
        - Asking for login and password
  Objective: Retrieve password passed in clear
  """

Scenario: Sniffing credentials in plain over HTTP
It seems to be general usability to send credentials over a network in
plain text, although, if it is done, there is one condition that should be met
before this, the channel should be secure, that means, sending it via HTTPS,
HTTP protocol communication, encrypted using SSL(Secure Socket Layers)/TLS
(Transport Layer Security).
  Given the form with it's details
  When I fill the login and password and send the request to check it on Burp
  Then I should see in plain both the login and password
  """
  # Burp Request
  POST /bWAPP/insuff_transp_layer_protect_1.php HTTP/1.1
  Host: 192.168.56.101
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.56.101/bWAPP/insuff_transp_layer_protect_1.php
  Cookie: security_level=0; PHPSESSID=a7629a18732ddad1988f3b83453760c7
  Connection: close
  Upgrade-Insecure-Requests: 1
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 34

  login=bee&password=bug&form=submit
  """
