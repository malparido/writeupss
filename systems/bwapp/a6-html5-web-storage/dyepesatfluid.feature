# language: en

Feature: HTML5 Web Storage (Secret)
  From system bWAPP
  From the A6 - Sensitive Data Exposure
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/insecure_crypt_storage_1.php
  Message: HTML5 Web Storage
  Details:
        - Your login name and secret have been stored as HTML5 web storage!
        - HINT: try to grab it using XSS...
  Objective: Get login name and secret from web storage
  """

Scenario: Getting data from local storage
In HTML5 web applications can store data within the user's browser.
Before it, data had to be sent via cookies and procesed in each request
to server, although local storage is more secure, that depends mostly
on the programmer. Let's take a look why
  Given the site, I check to the source code
  """
  # insecure_crypt_storage_1.php
  <script>
  if(typeof(Storage) !== "undefined")
  {
    localStorage.login = "bee";
    localStorage.secret = "<script>document.write(document.cookie);</script>";
  }
  else
  {
    alert("Sorry, your browser does not support web storage...");
  }
  </script>
  <title>bWAPP - Sensitive Data Exposure</title>
  # We got here the source code, the pure html source from that site
  # Interesting here, is that the script handling the local storage
  # is at plain sigth, even revealing us the variables used.

  # What would happen it we send the request back to the server
  # with some modifications? Like:
  <script>
  if(typeof(Storage) !== "undefined")
  {
    localStorage.login = "bee";
    localStorage.secret = "<script>document.write(document.cookie);</script>";
    document.write(localStorage.login);
    document.write(localStorage.secret);

  # This would likely end with a response displaying in top of the site:
  bee
  security_level=0; PHPSESSID=5dddafb8a79dfc4102c0954db948ef9d
  # Well, the cookies appear there, because in a previous challenge
  # the secret was modified with an XSS.
  """
