# language: en

Feature: Clear Credentials over HTTP
  From system bWAPP
  From the A5 - Security Misconfiguration
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    And I am running Burp Suite Community edition v1.7.30
    Given the following in the url
      """
      URL: http://localhost/bWAPP/sm_mitm_1.php
      Message: Man-in-the-Middle Attack (HTTP)
      Details:
      - Enter your credentials
      - Login/Password
      Objective: Clear Credentials over HTTP
      """

  Scenario: Clear credentials over HTTP
    Given the login form
      """
      # sm_mitm_1.php
      <form action="/bWAPP/sm_mitm_1.php" method="POST">

      <p><label for="login">Login:</label><br />
      <input type="text" id="login" name="login" size="20" /></p>

      <p><label for="password">Password:</label><br />
      <input type="password" id="password" name="password" size="20" /></p>

      <button type="submit" name="form" value="submit">Login</button>

      </form>
      """
    Then I input access credentials
      """
      # Access credentials:
      # login: bee
      # password: bug
      """
    And I use Burpsuite to intercept the Request
      """
      # Burp Request
      POST /bWAPP/sm_mitm_1.php HTTP/1.1
      Host: 192.168.75.128
      User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
      Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
      Accept-Language: en-US,en;q=0.5
      Accept-Encoding: gzip, deflate
      Referer: http://192.168.75.128/bWAPP/sm_mitm_1.php
      Cookie: security_level=0; PHPSESSID=7a98d15b45370254d78400c0262e57b6
      Connection: close
      Upgrade-Insecure-Requests: 1
      Content-Type: application/x-www-form-urlencoded
      Content-Length: 34

      login=bee&password=bug&form=submit

      # Here we can see clear credentials over an HTTP request.
      """
