# language: en

Feature: BEAST/CRIME/BREACH
  From system bWAPP
  From the A6 - Sensitive Data Exposure
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am running Burp Suite Community edition v1.7.30
    And I am running O-Saft 18.07.78
    Given the following in the url
      """
      URL: http://localhost/bWAPP/insuff_transp_layer_protect_4.php
      Message: Poodle Attack
      Details:
      - The web server is vulnerable to the POODLE attack.
      - If disabling SSLv3 is not an option, then consider implementing
      TLS_FALLBACK_SCSV.
      - More info: Exploiting The SSL 3.0 Fallback
        https://www.openssl.org/~bodo/ssl-poodle.pdf
      """

  Scenario: Poodle Attack
    The POODLE (Padding Oracle On Downgraded Legacy Encryption SSL 3.0)
    is a vulnerability that attacks the Handshake implementation
    on SSL/TLS, it works by downgrading the version used by one lower
    as there was no restriction on the server to not to fallback.
    So two machines to be able to communicate over an encrypted channel
    they must speak the same "language", the same version of the
    protocol is a requirement, but if one is using SSLv3 and the
    other TLSv1, the mayor version might fallback till SSLv3,
    to initiate communication.
    This fallback attacks weak cryptographic algorithms used in SSLv3.
    Given the server, the I modify the lighttpd config to disable SSL
      """
      # /etc/lighttpd/conf-enabled/10-ssl.conf
      ##
      ## Documentation: /usr/share/doc/lighttpd-doc/ssl.txt
      ##      http://www.lighttpd.net/documentation/ssl.html

      #### SSL engine
      $SERVER["socket"] == ":9443" {
            ssl.engine                  = "enable"
            ssl.pemfile                 = "/etc/ssl/certs/bee-box.pem"
            ssl.use-sslv2               = "enable"
            ssl.use-sslv3               = "disable"
      }
      """
    Then I do port/service enumeration
      """
      # Port enumeration: 9443
      skorn@Morgul ~/Downloads> sudo nmap -sS -T4 -sV -sC 192.168.75.128 -p 9443
      Nmap scan report for 192.168.75.128
      Host is up (0.000091s latency).

      PORT     STATE SERVICE  VERSION
      9443/tcp open  ssl/http lighttpd 1.4.19
        |_http-server-header: lighttpd/1.4.19
        |_http-title: Site doesn't have a title (text/html).
        | ssl-cert: Subject: commonName=bee-box.bwapp.local/organizationName=MME
      stateOrProvinceName=Flanders/countryName=BE
        | Not valid before: 2013-04-14T18:11:32
        |_Not valid after:  2018-04-13T18:11:32
        |_ssl-date: 2018-08-08T03:11:03+00:00; -9h41m38s from scanner time.
        | sslv2:
        |   SSLv2 supported
        |   ciphers:
        |     SSL2_RC2_128_CBC_WITH_MD5
        |     SSL2_DES_192_EDE3_CBC_WITH_MD5
        |     SSL2_DES_64_CBC_WITH_MD5
        |     SSL2_RC2_128_CBC_EXPORT40_WITH_MD5
        |     SSL2_RC4_128_WITH_MD5
        |_    SSL2_RC4_128_EXPORT40_WITH_MD5
      MAC Address: 08:00:27:CA:72:3F (Oracle VirtualBox virtual NIC)

      Host script results:
        |_clock-skew: mean: -9h41m38s, deviation: 0s, median: -9h41m38s
      """
    Then I use the O-saft tool to check that port vulnerabilities
    And this challenge is not to exploit
    But to enumerate and find out if it is vulnerable
      """
      # The output of the tool is quite extensive
      # And even checks ciphers strength per protocol.
      # sudo ./o-saft-docker +check --v --host 192.168.75.128 --port 9443
      # Here we go:
      Target does not support SSLv2:        no ( )
      Target does not support SSLv3:        no ( )

      Connection is safe against POODLE attack:  no (SSLv3)
      #
      # The attack work requires a Man-in-the-Middle attack
      # between client and server, it was likely to work
      # on chrome v16 back in 2014, today is quite hard
      # due to the browser requirement.
      """
