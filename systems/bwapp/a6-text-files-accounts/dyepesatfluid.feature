# language: en

Feature: Text Files Accounts
  From system bWAPP
  From the A6 - Sensitive Data Exposure
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/insecure_crypt_storage_3.php
  Message: Text Files Accounts
  Details:
        - Input login/password form
        - Insert button
        - 1 File to be downloaded
        -  1 File to be deleted
  Objective: Get users passwords
  """

Scenario: Getting password (low security)
  Given the website
  When I input an username with it's password
  """
  # Burp Request insecure_crypt_storage_2.php
  # Username: new
  # Password: account
  POST /bWAPP/insecure_crypt_storage_2.php HTTP/1.1
  Host: 192.168.56.101
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8

  username=new&password=account&insert=Insert

  # Then at response
  The account was added!
  # Plus the download link of the file passwords/accounts.txt
  # Content:
  HTTP/1.1 200 OK
  Date: Sat, 21 Jul 2018 16:35:45 GMT
  Last-Modified: Sat, 21 Jul 2018 16:35:12 GMT
  ETag: "ce15c-62-57184ff09b000"
  Accept-Ranges: bytes
  Content-Length: 98
  Connection: close
  Content-Type: text/plain

  'bee', 'bug'
  'hola', 'ola'
  'bee', '6885858486f31043e5839c735d99457f045affd0'
  'new', 'account'

  # This is an obvious case of sensitive data exposure, we can see
  # in plain text the passwords of all users, well except for the one
  # that is hashed
  """

Scenario: Getting password (medium security)
  Given the website as before
  Then I change the security to medium
  When I insert new user into accounts
  Then the password should be hashed, source code tells us that
  """
  # insecure_crypt_storage_2.php
  $username = xss_check_3($username);
  $password = sha1($password, false);
  $line = "'" . $username . "', '" . $password . "'" . "\r\n";
  break;

  # Hash: sha1.
  #
  # Let's use that account mentioned before:
  'bee', '6885858486f31043e5839c735d99457f045affd0'

  # Let's try to find a collision for that hash using this:
  # http://md5decrypt.net/en/Sha1/#answer
  # Found it in 2.07s
  # 6885858486f31043e5839c735d99457f045affd0 : bug
  # Pretty secure, isn't?
  """
