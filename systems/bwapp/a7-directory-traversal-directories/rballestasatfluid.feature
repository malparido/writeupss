#language: en

Feature: Detect and exploit vuln Directory Traversal - Directories
  From the bWAPP application
  From the A7 - Missing functional level access controls category

  Background:
    Given I am running Manjaro GNU/Linux kernel 4.9.77-1-MANJARO
    And I am browsing in Firefox 57.0.4
    Given a PHP site where some pdf files are listed and linked to
    And the URL is bwapp/directory_traversal_2.php

  Scenario Outline: Dynamic detection and exploitation
    When I look at the URL
    Then I see there is a GET parameter ?directory=documents
    When I change the value to ?directory=<dir>
    Then the <files> in that folder are listed and can be downloaded
    Examples:
    """
    | <dir> | <files> |
    | . | All pages in the server root |
    | .. | All files in the server (/) |
    | passwords | Heroes' passwords in XML |
    | admin | DB, passwords and other server settings |

  Scenario: Static detection and further exploitation
    When I look into the PHP code
    Then I see that we can any file except .htaccess will be listed
    """
    if($line != "." && $line != ".." && $line != ".htaccess")
    echo "<a href=\"".$dir."/".$line."\"target=\"_blank\">".$line."</a><br />";
    """
