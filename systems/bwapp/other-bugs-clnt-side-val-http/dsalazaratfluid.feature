# language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Page name:
    bWAPP/cs_validation.php
    bWAPP/ldap_connect.php
    bWAPP/sqli_3.php
    bWAPP/sqli_8-2.php
    bWAPP/sqli_16.php
    bWAPP/xmli_1.php
    bWAPP/ba_insecure_login.php
    bWAPP/ba_pwd_attacks_1.php
    bWAPP/ba_weak_pwd.php
    bWAPP/xss_login.php
    bWAPP/insecure_direct_object_ref_1.php
    bWAPP/insecure_direct_object_ref_3.php
    bWAPP/csrf_1.php
    bWAPP/cs_validation.php
    bWAPP/insecure_iframe.php
  CWE:
    CWE-319: Password Transmitted over HTTP
    CWE-598: Password Transmitted over Query String
  Goal:
    Capture unencoded passwords trasmitted over a network
  Recommendation:
    Use HTTPS

  Background:
  Hacker's software:
    | <Name>                       | <Version>          |
    | Kali GNU/Linux               | 4.18.0-kali1-amd64 |
    | Firefox Quantum              | 60.2.0             |
    | Wireshark                    | 2.6.4              |
    | Burp Suite Community Edition | 1.7.36             |
  TOE information:
    Given I am accessing bWAPP/cs_validation.php
    And I get to a site where I can change my password
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level

  Scenario: Normal use case
  Changing bee's password
    Given I am on the site
    And I fill the site fields as following:
    """
    Current password="bug"
    New password="bug2"
    Re-type new password="bug2"
    """
    And click on the "change" button
    Then I get a message saying "The password has been changed!"

  Scenario: Static detection
  The site doesn't implement HTTPS
    When I look at the Apache httpd configuration file
    And it is in /etc/lighttpd/lighttpd.conf
    Then I can see that the file does not reference any SSL certificate
    Then I can conclude that the site doesn't implement HTTPS

  Scenario: Dynamic detection
  The site uses HTTP protocol
    Given I am on the site
    And I am intercepting packages with Burp
    And I fill the site fields as following
    """
    Current password="bug2"
    New password="bug"
    Re-type new password="bug"
    """
    And click on the "change" button
    Then I catch the following package:
    """
    POST /bWAPP/cs_validation.php HTTP/1.1
    Host: 192.168.56.101
    ... #Unimportant lines
    Cookie: security_level=0; \
    SESScb1b7e2ce310cad2d8cf9c0927abc1d2= \
    nuYBcVz3p8gIpZlEwnO8B7SeeDJbAf0L50PIyrft27o; \
    PHPSESSID=fdec82f66cd2571388bee3db6b8ef890
    ... #Unimportant lines
    password_curr=bug2&password_new=bug&password_conf=bug&action=change
    """
    Then I forward the package to change my password again
    Then by looking at the package's first line I know the site uses HTTP/1.1
    And by looking at the package's last line I know the fields are plain text

  Scenario: Exploitation
  Sniffing the network for credentials
    Given I know I can see logins and passwords from the HTTP packages
    Then I proceed to set up an environment to sniff packages:
    Then I go to Virtualbox
    And Open settings for the VM that is running bWAPP
    And Under the Network tab
    Then I select the adapter "Adapter 2 / Host-only Adapter"
    And under "Advanced", I set "Allow all" for "Promiscuous mode"
    And get an evidence: [Evidence](promiscuous-mode.png)
    Then I clone the bWAPP VM
    And end up with a setup of three machines with name <machinename>
    And each of them with one <interface> and one <ip>
    | <machinename>   | <interface> | <ip>           |
    | My PC (sniffer) | vboxnet0    | 192.168.56.1   |
    | bWAPP1 (host)   | vboxnet0    | 192.168.56.101 |
    | bWAPP2 (client) | vboxnet0    | 192.168.56.102 |
    Then I finished setting up the testing environment for sniffing packages
    Then I open Wireshark
    And I click on the "Capture" menu
    And I click on "Options"
    And I select vboxnet0
    And I check "Promiscuous" checkbox
    And I click on Start as shown here: [Evidence](wireshark-promiscuous.png)
    Then I go to bWAPP2
    And open Firefox
    And access bWAPP1: "http://192.168.56.101/bWAPP/portal.php"
    And login as bee
    And go to the password-changing site
    And this is the link: "http://192.168.56.101/bWAPP/cs_validation.php"
    And I fill the site fields as following
    """
    Current password="bug"
    New password="bug2"
    Re-type new password="bug2"
    """
    And click on the Change button
    Then I go to Wireshark on My PC and check if I actually caught the package
    And I can confirm that the package was sucessfully caught
    And the old and new passwords stolen
    And get an evidence: [Evidence](exploitation.png)
    And that it seems like the request was created from My PC to bWAPP1
    But this is caused because My PC works as the router in this network

  Scenario: Maintaining access
  Stealing user account
    Given I already know bug's current password
    And the site to change it
    Then I can change the password again and hijack the bug account
    Then I can conclude that maintaining access to the system is feasible

  Scenario Outline: Extraction
  Looking for bee's secret
    Given I already know bee's login information
    Then I can access the site bWAPP/secret_html.php
    And obtain bee's secret
    And access all the site pages that would be forbidden
    Then I can conclude that information extraction is feasible

  Scenario: Remediation
  Getting SSL certificate and configuring HTTPS
    Given I do not have a SSL certificate at hand
    Then I can't reproduce the fix and prove my attack vector wouldn't work
    But if I hypothetically got a SSL certificate
    And configured HTTPS  on every referenced site on Feature - Page Name
    Then I would Theoretically be able to hide user and password fields
    And no account stealing through sniffing would be possible

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.8/10 (High) - AV:A/AC:H/PR:N/UI:R/S:C/C:H/I:L/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.3/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-10-25
