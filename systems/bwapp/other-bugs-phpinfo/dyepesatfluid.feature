# language: en

Feature: Base64 Enconding(Secret)
  From system bWAPP
  From the A6 - Sensitive Data Exposure
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/information_disclosure_1.php
  Message: Information Disclosure - phpinfo
  Details:
        - phpinfo file direct access without restriction
  Objective: Check the file and see what information leaked
  """

Scenario: Information Disclosure phpfile info
phpinfo file migth not be a vulnerability per se, but the information along
migth pose a threat to the server, by exposing specific php version, extra
modules, database engine used, etc.
  Given the phpinfo
  When I look at it I can see the following:
  """
  # phpinfo
  #
  # By looking at it I already know it's using Mysql as database
  # /etc/php5/apache2/conf.d/mysql.ini,
  # /etc/php5/apache2/conf.d/mysqli.ini,
  # /etc/php5/apache2/conf.d/pdo_mysql.ini,
  #
  # I know too, it's using the ldap module
  # /etc/php5/apache2/conf.d/ldap.ini,
  #
  # Plus the sqlite module
  # /etc/php5/apache2/conf.d/pdo_sqlite.ini,
  # /etc/php5/apache2/conf.d/sqlite.ini
  #
  # Then, I got information on the apache2 service running
  # Like which version is using, an attacker could search for specific attacks
  # if it is vulnerable
  #
  # Apache Version  Apache/2.2.8 (Ubuntu) DAV/2 mod_fastcgi/2.4.6
  # PHP/5.2.4-2ubuntu5 with Suhosin-Patch mod_ssl/2.2.8 OpenSSL/0.9.8g
  # Server Administrator  webmaster@localhost
  # Hostname:Port  bee-box:80
  # User/Group  www-data(33)/33
  # Max Requests  Per Child: 0 - Keep Alive: on - Max Per Connection: 100
  # Timeouts  Connection: 300 - Keep-Alive: 15
  # Virtual Server  Yes
  # Server Root  /etc/apache2
  #
  # I know too, all the modules used by apache
  # Loaded Modules  core mod_log_config mod_logio prefork http_core mod_so
  # mod_actions mod_alias mod_auth_basic mod_authn_file mod_authz_default
  # mod_authz_groupfile mod_authz_host mod_authz_user mod_autoindex mod_cgi
  # mod_cgid mod_dav mod_dav_fs mod_dir mod_env mod_fastcgi mod_headers
  # mod_include mod_mime mod_negotiation mod_php5 mod_rewrite mod_setenvif
  # mod_ssl mod_status
  #
  # More data that could be seen within this info:
  #
  # -  HTTP Headers Information
  # - FTP enabled
  # - What kind of hashes are available
  # - If Json is enabled, which version
  # - If ldap is enabled, which version
  #
  # Again, as said before, this migth not be a vulnerability, but it could
  # be the source to find particular vulnerabilities by knowing the
  # configuration of the systems and some of its services
  """
