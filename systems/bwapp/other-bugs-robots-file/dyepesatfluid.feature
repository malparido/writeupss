# language: en

Feature: Information Disclosure - Robots.txt
  From system bWAPP
  From the Others bugs
  With Low security level

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given the following in the url
  """
  URL: http://localhost/bWAPP/information_disclosure_3.php
  Message: Information Disclosure - Robots File
  Details:
        - Robots File information
  Objective: Determine why the Robots file migth disclose information
  """

Scenario: Robots crawling
This file is intended to prevent web 'robots' aka as crawlers, from
visiting certain directories in a web site for maintenance or indexing
purposes. A malicious user may also be able to use the contents of this
file to learn of sensitive documents or directories along the
web server.
  Given the robots.txt
  """
  # Contents of robots.txt:

  User-agent: GoodBot
  Disallow:

  User-agent: BadBot
  Disallow: /

  User-agent: *
  Disallow: /admin/
  Disallow: /documents/
  Disallow: /images/
  Disallow: /passwords/
  """
  Then I can learn from this, that some passwords are at my reach
  """
  # Content of passwords directory:
  #  heroes.xml  02-Nov-2014 23:52  1.2K
  #  web.config.bak  02-Nov-2014 23:52  7.4K
  #  wp-config.bak  02-Nov-2014 23:52
  #
  # Content of heroes.xml
  # Seems to be the passwords for the challenge:
  # http://192.168.56.101/bWAPP/sqli_16.php
  #
  # <hero>
  # <id>1</id>
  # <login>neo</login>
  # <password>trinity</password>
  # <secret>Oh why didn't I took that BLACK pill?</secret>
  # <movie>The Matrix</movie>
  # <genre>action sci-fi</genre>
  # </hero>
  # <hero>
  # <id>2</id>
  # <login>alice</login>
  # <password>loveZombies</password>
  # <secret>There's a cure!</secret>
  # <movie>Resident Evil</movie>
  # <genre>action horror sci-fi</genre>
  # </hero>
  #
  # Content of web.config.bak:
  # Some sort of connection details on a windows service
  # <!--
  #   The <authentication> section enables configuration
  #    of the security authentication mode used by
  #    ASP.NET to identify an incoming user.
  #  -->
  #
  # <connectionStrings>
  # <add name="bWAPPConnectionString" connectionString="Data
  # Source=bee-box;Initial Catalog=bWAPP;Persist Security Info=True;User
  #     ID=wolverine;Password=Log@N"/>
  #
  # Content of wp-config.bak
  # Mysql credentials
  #
  # <?php
  # // ** MySQL settings ** //
  # define('DB_NAME', 'bWAPP');    // The name of the database
  # define('DB_USER', 'thor');     // Your MySQL username
  # define('DB_PASSWORD', 'Asgard'); // ...and password
  # define('DB_HOST', 'localhost');    // 99% chance you won't need to change
  # this value
  # define('DB_CHARSET', 'utf8');
  # define('DB_COLLATE', '');
  """
