# language: en

Feature: Forgot Password
  From the DVNA system
  Of the category A2: Broken Authentication and Session Management
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Versión 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /forgotpw
    Message: Reset Password
    Details:
      - Text field to enter the login name
      - Submit botton to perform the action
    Objective: Change the password of a user
    """

  Scenario: Exploit the reset password functionality
    Given that I have access to the source code
    And the file dvna/core/authHandler.js contain the auth and session scheme
    When I look out the script I found the function that resets the password
    """
    49 if (req.query.token == md5(req.query.login)) {
    50       res.render('resetpw', {
    51         login: req.query.login,
    52         token: req.query.token
    53       })
    54     }
    """
    Then I know that both login and token are sent through the request
    And the token has to be the same as the login encrypted in a md5 hash
    Given a sample reset link that can be found in the app
    """
    /resetpw?login=user&token=ee11cbb19052e40b07aac0ca060c23ee
    """
    When I make a request with my username and a generated token
    """
    /resetpw?login=disassembly&token=14bb80ce28b0b7ac9e291e2532c15ee1
    """
    Then I get access to the resource that let change my password
