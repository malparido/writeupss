# Version 1.4.0
# language: en

Feature:
  TOE:
    DVWA
  Category:
    Other, easter eggs, bugs
  Page name:
    DVWA/vulnerabilities/view_source.php
  CWE:
    CWE-23: Relative Path Traversal
  Goal:
    Display sensitive files by doing a relative path traversal
  Recommendation:
    Limit the user input to show only what is in a secure base folder

  Background:
  Hacker's software:
    | <Name>        | <Version>                 |
    | Ubuntu        | 18.04.1 LTS (amd64)       |
    | Google Chrome | 70.0.3538.77 (64-bit)     |
    | Docker        | 18.06.1-ce, build e68fc7a |
  TOE information:
    Given I'm running DVWA on a Docker container on port 32768

  Scenario Outline: Normal use case
    When I access DVWA/vulnerabilities/view_source.php<query>
    Then I get <results>

    Examples:
    | <query>                      | <results>              |
    | ?id=brute&security=low       | [Evidence](image1.png) |
    | ?id=csp&security=medium      | [Evidence](image2.png) |
    | ?id=sqli&security=impossible | [Evidence](image3.png) |

  Scenario: Static detection
  The PHP code doesn't sanitize the input on the query
    When I look at the page source code
    Then I see that the vulnerability is present on line 53
    # DVWA/vulnerabilities/view_source.php
    """
    01 <?php
    11 $id       = $_GET[ 'id' ];
    12 $security = $_GET[ 'security' ];
    13
    53 $source = @file_get_contents( DVWA_WEB_PAGE_TO_ROOT .
       "vulnerabilities/{$id}/source/{$security}.php" );
    54 $source = str_replace( array( '$html .=' ), array( 'echo' ), $source );
    55
    71 $page[ 'body' ] .= "
    72 <div class=\"body_padded\">
    73   <h1>{$vuln} Source</h1>
    74
    75   <h2>vulnerabilities/{$id}/source/{$security}.php</h2>
    76   <div id=\"code\">
    77   <table width='100%' bgcolor='white' style=\"border:2px #C0C0C0 solid\">
    78       <tr>
    79 <td><div id=\"code\">" . highlight_string( $source, true ) . "</div></td>
    80       </tr>
    81     </table>
    82   </div>
    83   {$js_html}
    84   <br /> <br />
    85
    86   <form>
    87    <input type=\"button\" value=\"Compare All Levels\" onclick=\"window.l
       ocation.href='view_source_all.php?id=$id'\">
    88   </form>
    89 </div>\n";
    90
    91 dvwaSourceHtmlEcho( $page );
    92
    93 ?>
    """
    Given the the variables $id and $security are not beeing sanitized
    Then I conclude that a relative path traversal attack can be performed

  Scenario Outline: Dynamic detection
  The page allows relative path traversing
    When I access DVWA/vulnerabilities/view_source.php<query>
    Then I get <results>

    Examples:
    | <query>                           | <results>              |
    | ?id=xss_s&security=../../../index | [Evidence](image4.png) |

  Scenario Outline: Exploitation
  Traversing the path and displaying config files
    When I access DVWA/vulnerabilities/view_source.php<query>
    Then I get <results>

    Examples:
    | <query>                                         | <results>              |
    | [Evidence](image5.png)                          | [Evidence](image6.png) |
    | ?id=csrf&security=../../../config/config.inc    | [Evidence](image7.png) |

  Scenario Outline: Remediation
  The PHP code can be fixed by sanitizing user input against
    Given I compare the user supplied path against my desired source path
    And I replace lines 53 and 54 with
    """
    53 $basepath = DVWA_WEB_PAGE_TO_ROOT . "vulnerabilities/{$id}/source/";
    54 $realBase = realpath($basepath);
    55
    56 $userpath = DVWA_WEB_PAGE_TO_ROOT . "vulnerabilities/{$id}/source/{$secur
       ity}.php";
    57 $realUserPath = realpath($userpath);
    58
    59 if ($realUserPath === false || strpos($realUserPath, $realBase) !== 0) {
    50   $source = "RELATIVE PATH TRAVERSAL DETECTED: " . $realBase . " vs " . $
       realUserPath;
    61 } else {
    62   $source = @file_get_contents( DVWA_WEB_PAGE_TO_ROOT . "vulnerabilities/
       {$id}/source/{$security}.php" );
    63 }
    64
    65 $source = str_replace( array( '$html .=' ), array( 'echo' ), $source );
    """
    When I access DVWA/vulnerabilities/view_source.php<query>
    Then I get <results>
    And the vulnerability is patched

    Examples:
    | <query>                                        | <results>               |
    | [Evidence](image5.png)                         | [Evidence](image8.png)  |
    | ?id=csrf&security=../../../config/config.inc   | [Evidence](image9.png)  |
    | ?id=csp&security=low                           | [Evidence](image10.png) |

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.2/10 (High) - E:H/RL:O/RC:C/
    Environmental: Unique and relevant attributes to a specific user environment
      5.5/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date <2018-11-08>