# Version 1.4.0
# language: en

Feature:
  TOE:
    DVWA
  Category:
    Security level high
  Page name:
    DVWA/vulnerabilities/weak_id
  CWE:
    CWE-326: Inadequate Encryption Strength
  Goal:
    Predict the sesion's IDs and describe a possible exploit scenario
  Recommendation:
    Use hashing algorithms and methods suggested by NIST

  Background:
  Hacker's software:
    | <Name>        | <Version>                 |
    | Ubuntu        | 18.10 (amd64)             |
    | Google Chrome | 70.0.3538.77 (64-bit)     |
    | Docker        | 18.06.1-ce, build e68fc7a |
  TOE information:
    Given I enter the site DVWA/vulnerabilities/weak_id
    And a PHP site is displayed
    And the site contains a button that allows me to generate a cookie
    And the cookie contains a ficticious session ID

  Scenario: Normal use case
    Given I am at the page DVWA/vulnerabilities/weak_id
    And I click on the generate button
    Then a new cookie is written with the following values
    | Name                 | dvwaSession                      |
    | Content              | c4ca4238a0b923820dcc509a6f75849b |
    | Domain               | localhost                        |
    | Path                 | /vulnerabilities/weak_id         |
    | Send for             | Any kind of connection           |
    | Created              | 07/Nov/2018 07:03:20 GMT-5       |
    | Expires              | 07/Nov/2018 08:03:20 GMT-5       |

  Scenario: Static detection
  The PHP code makes use of a sequential md5 checksum to generate the session ID
    When I look at the page source code
    Then the vulnerability is present from line 05 to 11
    And it is on on the file DVWA/vulnerabilities/weak_id/source/high.php
    """
    01 <?php
    02
    03 $html = "";
    04
    05 if ($_SERVER['REQUEST_METHOD'] == "POST") {
    06     if (!isset ($_SESSION['last_session_id_high'])) {
    07         $_SESSION['last_session_id_high'] = 0;
    08     }
    09     $_SESSION['last_session_id_high']++;
    10     $cookie_value = md5($_SESSION['last_session_id_high']);
    11     setcookie("dvwaSession", $cookie_value, time()+3600,
       "/vulnerabilities/weak_id/", $_SERVER['HTTP_HOST'], false, false);
    12 }
    13
    14 ?>
    """
    Given the cookie content is created from the hash of a sequential number
    Then I can predict the session IDs of other users

  Scenario Outline: Dynamic detection
  The site generates the session IDs from a sequential md5 checksum
    When I do <action>
    Then I get <session_ID_value>
    When I reverse <session_ID_value> with the tool
    """
    https://www.md5online.es/descifrar-md5.html
    """
    Then I get <reversed_md5>
    And I can see that the pattern is to hash an incremental counter

    Examples:
    | <action>         | <session_ID_value>               | <reversed_md5> |
    | Press the button | c4ca4238a0b923820dcc509a6f75849b | 1              |
    | Press the button | c81e728d9d4c2f636f067f89cc14862c | 2              |
    | Press the button | eccbc87e4b5ce2fe28308fd9f2a7baf3 | 3              |
    | Press the button | a87ff679a2f3e71d9181a67b7542122c | 4              |
    | Press the button | e4da3b7fbbce2345d7772b0674a318d5 | 5              |

  Scenario Outline: Exploitation
  Gaining access as any of the last hour logged users
  # The objective in DVWA is to only predict the session IDs
  # It's not exploitable since the written cookie is just a toy (It's not real)
  # But if it were the real session ID (PHPSESSID), I'd do the following
    When I log-in to my account
    Then I get a session ID for my session
    # let's assume it is:
    """
    d010396ca8abf6ead8cacc2c2f2f26c7
    """
    Given I reverse my session ID with the md5 reversing tool
    """
    https://www.md5online.es/descifrar-md5.html
    """
    Then I get the current value for the counter
    """
    1542
    """
    Given I create a list of sequentially decremental md5 hashes
    | counter | md5 hash                         |
    | 1542    | d010396ca8abf6ead8cacc2c2f2f26c7 |
    | 1541    | 1373b284bc381890049e92d324f56de0 |
    | 1540    | cda72177eba360ff16b7f836e2754370 |
    | 1539    | 17e23e50bedc63b4095e3d8204ce063b |
    And the HTML code of the website for a logged user looks like this
    # This code is hypotetical, but usually a website contains the username
    # of the logged user inside its content, lets name this template
    # logged_user.html
    """
    <!-- logged_user.html -->
    <html>
      <head>
      </head>
      <body>
        username: kamadoatfluid
        <br><br>
        ... website content ...
      </body>
    </html>
    """
    And the HTML code of the website for a not logged user looks like this
    # This code is hypotetical, but usually for a not logged user a website
    # redirects to the login page, lets name it not_logged_user.html
    """
    <!-- not_logged_user.html -->
    <html>
      <head>
      </head>
      <body>
        Log-in or sign-up to see this page!
        <br><br>
        ... Log in/sign up form ...
      </body>
    </html>
    """
    And I create a <cookie> file
    And I execute <bash_command>
    Then I get an HTML file that matches the pattern <pattern>
    And I see that cookie1.txt belongs to a logged user (myself)
    And I see that cookie4.txt belongs to a logged user (another user)
    And I can use this cookies to authenticate myself as that user

    Examples:
    | <cookie>               | <bash_command>         | <pattern>            |
    | [Evidence](image1.png) | [Evidence](image2.png) | logged_user.html     |
    | [Evidence](image3.png) | [Evidence](image4.png) | not_logged_user.html |
    | [Evidence](image5.png) | [Evidence](image6.png) | not_logged_user.html |
    | [Evidence](image7.png) | [Evidence](image8.png) | logged_user.html     |

  Scenario Outline: Remediation
  The PHP code can be fixed by using a more secure way to generate session IDs
    When I use a current secure hash algorithm like SHA256 and enough salt
    Then the session IDs are secure against this attack vector
    """
    01  <?php
    02
    03  $html = "";
    04
    05  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    06    $cookie_value = hash('sha256', random_bytes(16) . time());
    07    setcookie("dvwaSession", $cookie_value, time()+3600,
           "/vulnerabilities/weak_id/", $_SERVER['HTTP_HOST'], true, true);
    08  }
    09  ?>
    """
    When I press the button
    Then I get <session_ID_value>
    Given I can't reverse a salted sha256 hash in less than one hour
    Then the system is patched against this kind of attack vector

    Examples:
    | <session_ID_value>                                               |
    | b0253f9ff96ba31882fe609ee7745d474cdf59648d49fa7ec60e345752432768 |
    | 08e53647bc89ae9da304ede4f52a3fb387a380e3ebd824ee8add0802c624cc14 |
    | c03c294430adce9443eda51f0f0771f8a29960ff9dcf2feabc6099fd6bf5a53e |
    | 19fa2e1a174451a334f16388a9810399e9e0b51c9e5555ddfa4bae4c8cd00526 |

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N/
    Temporal: Attributes that measure the exploit's popularity and fixability
      4.6/10 (Medium) - E:U/RL:O/RC:C/
    Environmental: Unique and relevant attributes to a specific user environment
      4.0/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date <2018-11-07>
