# Version 1.3.1
# language: en

Feature:
  TOE:
    DVWA
  Category:
    Security level low
  Page name:
    DVWA/vulnerabilities/csp/
  CWE:
    CWE-657: Violation of Secure Design Principles
  Goal:
    Bypass the Content Security Policy (CSP) and execute javascript code
  Recommendation:
    Enforce the contenct security policy to only accept self scripting

  Background:
  Hacker's software:
    | <Name>        | <Version>                 |
    | Ubuntu        | 18.10 (amd64)             |
    | Google Chrome | 70.0.3538.77 (64-bit)     |
    | Docker        | 18.06.1-ce, build e68fc7a |
  TOE information:
    Given I enter the site DVWA/vulnerabilities/csp/
    And a PHP site is displayed
    And the site contains a text field that allows me to enter a url
    And the site contains a include button
    And it allows me to include as a script what is pointed to by the url

  Scenario: Normal use case
    When I leave the text field blank
    And I press the include button
    Then the website reloads
    And everything stays visually the same

  Scenario: Static detection
  The Content Security Policy allows to include javascript from external sources
    When I look at the page source code
    Then I see that the vulnerability is present from line 03 to 05
    And it is on the file DVWA/vulnerabilities/csp/source/low.php
    """
    01 <?php
    02
    03 $headerCSP = "Content-Security-Policy: script-src 'self'
       https://pastebin.com  example.com code.jquery.com
       https://ssl.google-analytics.com ;"; // allows js from self,
       pastebin.com, jquery and google analytics.
    04
    05 header($headerCSP);
    """
    Given in the Content Security Policy,
    Then the directive <directive> has assigned the values <site>
    | <directive>      | <site>                           |
    | script-src       | 'self'                           |
    | script-src       | https://pastebin.com             |
    | script-src       | example.com                      |
    | script-src       | code.jquery.com                  |
    | script-src       | https://ssl.google-analytics.com |
    Then the Content Security Policy allows to include
    And run internal and external scripts from the previous listed sources

  Scenario: Dynamic detection
  The site allows to include javascript code from some external providers
    When I write <input> on the textfield
    And I press the include button
    Then I get <results>
    | <input>                             | <results>                          |
    | https://hastebin.com/raw/ziludodace | Refused to load the script 'https: |
    |                                     | //hastebin.com/raw/ogijatulux' bec |
    |                                     | ause it violates the following Con |
    |                                     | tent Security Policy directive: "s |
    |                                     | cript-src 'self' https://pastebin. |
    |                                     | com  example.com code.jquery.com h |
    |                                     | ttps://ssl.google-analytics.com ". |
    |                                     | [Evidence](image1.png)             |
    | https://pastebin.com/raw/9Gef9GZu   | pop-up telling 'kamadoatfluid!'    |
    |                                     | [Evidence](image2.png)             |
    # code at https://hastebin.com/raw/ziludodace and
    # code at https://pastebin.com/raw/9Gef9GZu based on Jamieson Becker @ Quora
    #   https://www.quora.com/What-is-script-alert-1-script
    Then Content Security Policy blocks javascript resources from hastebin
    And Content Security Policy allows javascript resources from pastebin

  Scenario: Exploitation
  Executing javascript code from an external resource at my will
    Given I create a paste for a javascript code at https://pastebin.com
    And I enter the <url> from the paste on the text field
    When I click on the include button
    Then I get <results>
    | <url>                             | <results>                            |
    | https://pastebin.com/raw/9Gef9GZu | A pop-up appears telling             |
    |                                   | 'kamadoatfluid'                      |
    |                                   | [Evidence](image2.png)               |
    | https://pastebin.com/raw/jWFKzyW2 | The website rotates ~180 degrees and |
    |                                   | replaces all images with images of   |
    |                                   | Justin Beaver                        |
    |                                   | [Evidence](image3.png)               |
    # code at https://pastebin.com/raw/9Gef9GZu based on Jamieson Becker @ Quora
    #   https://www.quora.com/What-is-script-alert-1-script
    # code at https://pastebin.com/raw/jWFKzyW2 by monkeyshine @ github
    #   https://github.com/codebox/monkeyshine/blob/master/monkeyshine.js

  Scenario: Remediation
  The PHP code can be fixed by enforcing the Content Security Policy
    Given I modify DVWA/vulnerabilities/csp/source/low.php
    And enforce the allowed CSP in the header of the website by modifying

    """
    01 <?php
    02
    03 $headerCSP = "Content-Security-Policy: script-src 'self';";
    04
    05 header($headerCSP);
    """
    Then the CSP settings allows only loading resources from the same origin
    # This not only prevent this attack vector, but also reduces some XSS risks
    # when running on modern browsers
    When I enter the url from the paste as <input> on the text field
    And I click the include button
    Then I get <results>
    | <input>                             | <results>                          |
    | https://pastebin.com/raw/9Gef9GZu   | Refused to load the script 'https: |
    |                                     | //pastebin.com/raw/9Gef9GZu' becau |
    |                                     | se it violates the following Conte |
    |                                     | nt Security Policy directive: "scr |
    |                                     | ipt-src 'self'".                   |
    |                                     | [Evidence](image4.png)             |
    | https://pastebin.com/raw/jWFKzyW2   | Refused to load the script 'https: |
    |                                     | //pastebin.com/raw/jWFKzyW2' becau |
    |                                     | se it violates the following Conte |
    |                                     | nt Security Policy directive: "scr |
    |                                     | ipt-src 'self'".                   |
    |                                     | [Evidence](image5.png)             |
    # code at https://pastebin.com/raw/9Gef9GZu based on Jamieson Becker @ Quora
    #   https://www.quora.com/What-is-script-alert-1-script
    # code at https://pastebin.com/raw/jWFKzyW2 by monkeyshine @ github
    #   https://github.com/codebox/monkeyshine/blob/master/monkeyshine.js
    And the vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      4.4/10 (Medium) - AV:L/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:L
    Temporal: Attributes that measure the exploit's popularity and fixability
      3.9/10 (Low) - E:U/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      2.8/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date <2018-10-30>
