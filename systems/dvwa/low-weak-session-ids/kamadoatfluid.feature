# Version 1.3
# language: en

Feature:
  TOE:
    DVWA
  Category:
    Security level low
  Page name:
    Weak Session IDs
  CWE:
    CWE-315: Cleartext Storage of Sensitive Information in a Cookie
  Goal:
    Predict the sessions IDs and gain access as any user
  Recommendation:
    Use hashing algorithms and methods suggested by NIST

  Background:
  Hacker's software:
    | Name          | Version                   |
    | Ubuntu        | 16.04.5 LTS (amd64)       |
    | Google Chrome | 70.0.3538.67 (64-bit)     |
    | Docker        | 18.06.1-ce, build e68fc7a |
  TOE information:
    Given a PHP site with a button that allows me to generate a new session ID
    Then my objective is to predict the session IDs and gain access as any user

  Scenario: Normal use case
    Given I am at the page DVWA/vulnerabilities/weak_id
    And I click on the "Generate" button
    Then a new cookie is written with the following values
    """
    localhost false /vulnerabilities/weak_id false 1540406011 dvwaSession 1
    """

  Scenario: Static detection
  The PHP code makes use of an incremental session ID generation algorithm
    When I look at the page source code
    Then I see the code that constructs the session ID:
    """
    01  <?php
    02  $html = "";
    03  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    04    if (!isset ($_SESSION['last_session_id'])) {
    05      $_SESSION['last_session_id'] = 0;
    06    }
    07    $_SESSION['last_session_id']++;
    08    $cookie_value = $_SESSION['last_session_id'];
    09    setcookie("dvwaSession", $cookie_value);
    10  }
    11  ?>
    """
    When I click on the "Generate" button
    Then I Generate the current session ID
    And by sucessively substracting one to the current session ID
    Then I can get the session IDs of past users

  Scenario: Dynamic detection
  The website generates a secuential session ID value as I press the button
    When I click on the "Generate" button ten times
    Then I Generate ten session IDs
    And the last session ID is the current session ID
    """
    +------------+
    | Session ID |
    +------------+
    |     1      |
    |     2      |
    |     3      |
    |     4      |
    |     5      |
    |     6      |
    |     7      |
    |     8      |
    |     9      |
    |     10     |
    +------------+
    """
    And the pattern seems to be incremental by one
    When I sucessively substract one to my current session ID
    Then I get the session IDs of past users

  Scenario: Exploitation
  Gaining access as a user and getting pages as this logged user
  #  There is not much use in DVWA because the user session ID is limited
  #  to the domain /vulnerabilities/weak_id
  #  But in a real case where the domain is less restricticted, as in a
  #  social network, you can do many things with the IDs you got
  #  - Download snapshots of any page that the logged user can see
  #  - Download snapshots of the profile page from the logged user,
  #    extract the username, and create a database of {UserName, SessionID}
  #  - Log-in and do phishing as any user in the databse you created
    When I create a cookie with the following values
    """
    localhost false / false 1540419010 dvwaSession 1
    """
    Then I can download html snapshots as a logged user with a bash script
    # <port> is the port where I'm running DVWA
    """
    wget --load-cookies cookie.txt 'localhost:<port>/vulnerabilities/weak_id/'
    """
  #  Then I can scan the profile page to extract the username for that session
  #  ID and build a table with the session IDs for all existing users
  #  And do any phishing I want

  Scenario: Remediation
  The PHP code can be fixed by using a more secure way to generate session IDs
    When I use a current secure hash algorithm like SHA512 and a bit of salt
    Then the session IDs are not correlated one with another
    """
    1  <?php
    2
    3  $html = "";
    4
    5  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    6    $cookie_value = hash(sha512, random_bytes(10) . time());
    7    setcookie("dvwaSession", $cookie_value, time()+3600,
           "/vulnerabilities/weak_id/", $_SERVER['HTTP_HOST'], true, true);
    8  }
    9  ?>
    """
    And a brute force approach becomes infeasible (as of today's technology)
    And a pattern based approach is impossible (unless sha512 were invertible)
    # consider using random functions that are cryptografically secure as salt
    # http://php.net/manual/es/function.random-bytes.php

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.5/10 (High) - E:H/RL:U/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      8.6/10 (Medium) - R:L/IR:H/AR:L/MAV:N/MAC:L/MPR:N/MUI:R/MS:U/MC:H/MI:H

  Scenario: Correlations
    No correlations have been found to this date <2018-10-24>
