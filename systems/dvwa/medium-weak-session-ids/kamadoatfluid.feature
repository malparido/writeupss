# Version 1.3.1
# language: en

Feature:
  TOE:
    DVWA
  Category:
    Security level medium
  Page name:
    DVWA/vulnerabilities/weak_id
  CWE:
    CWE-6: J2EE Misconfiguration: Insufficient Session-ID Length
    CWE-315: Cleartext Storage of Sensitive Information in a Cookie
  Goal:
    Predict the session's IDs and gain access as a logged user
  Recommendation:
    Use hashing algorithms and methods suggested by NIST

  Background:
  Hacker's software:
    | <Name>        | <Version>                 |
    | Ubuntu        | 18.10 (amd64)             |
    | Google Chrome | 70.0.3538.77 (64-bit)     |
    | Docker        | 18.06.1-ce, build e68fc7a |
  TOE information:
    Given I enter the site DVWA/vulnerabilities/weak_id
    And a PHP site is displayed
    And the site contains a button that allows me to generate a cookie
    And the cookie contains a ficticious session ID

  Scenario: Normal use case
    Given I am at the page DVWA/vulnerabilities/weak_id
    And I click on the "Generate" button at Oct 26, 2018 at 7:54:20 AM (GMT-5)
    Then a new cookie is written with the following values
    | Name                 | dvwaSession                    |
    | Content              | 1540558460                     |
    | Domain               | localhost                      |
    | Path                 | /vulnerabilities/weak_id       |
    | Send for             | Any kind of connection         |
    | Accessible to script | Yes                            |
    | Created              | 26/Oct/2018 07:54:20 GMT-5     |
    | Expires              | When the browsing session ends |

  Scenario: Static detection
  The PHP code makes use of current UNIX time as session ID generation algorithm
    When I look at the page source code
    Then I see that the vulnerability is present from line 06 to 07:
    """
    01  <?php
    02
    03  $html = "";
    04
    05  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    06      $cookie_value = time();
    07      setcookie("dvwaSession", $cookie_value);
    08  }
    09  ?>
    """
    Given the session ID is computed from the PHP function <time()>
    And I press the button at <current_time>
    Then <content> is written as the session ID in the cookie
    | <current_time>            | <time>     | <content>  |
    | 26/Oct/2018 7:54:20 GMT-5 | 1540558460 | 1540558460 |
    Then I conclude that the session ID is exactly the cookie creation time
    And it is in units of UNIX epoch time

  Scenario: Dynamic detection
  In the session ID cookie, the content value is related to the creation time
    When I press the button at <current_time>
    Then I get a cookie for the session ID with <content> and <created> values
    | <current_time>            | <content>  | <created>                 |
    | 26/Oct/2018 7:54:20 GMT-5 | 1540558460 | 26/Oct/2018 7:54:20 GMT-5 |
    | 26/Oct/2018 7:54:21 GMT-5 | 1540558461 | 26/Oct/2018 7:54:21 GMT-5 |
    | 26/Oct/2018 7:54:22 GMT-5 | 1540558462 | 26/Oct/2018 7:54:22 GMT-5 |
    Then It's easily seen that <current_time> is equivalent to <created>
    And <content> is the UNIX epoch time of <created>
    Then session ID generation algorithm makes use of the UNIX epoch time
    And creates the session ID

  Scenario: Exploitation
  Gaining access as any of the last hour logged users for which the session
  is still active
  # The objective in DVWA is to only predict the session ID
  # It's not exploitable since the written cookie is just a toy (It's not real)
  # But if it were the real session ID (PHPSESSID), I'd do the following
    Given I want to try three possible session IDs
    And I want to find if they belong to a user
    And <x> is a unix epoch time ranging in the last hour
    | <x>           |
    | 1540558463    |
    | 1540558528    |
    | 1540558715    |
    And the HTML code of the website for a logged user looks like this
    # This code is hypotetical, but usually a website contains the username
    # of the logged user inside its content, lets name it
    # logged_user_template.html
    """
    <!-- logged_user_template.html -->
    <html>
      <head>
      </head>
      <body>
        username: kamadoatfluid
        <br><br>
        ... website content ...
      </body>
    </html>
    """
    And the HTML code of the website for a not logged user looks like this
    # This code is hypotetical, but usually for a not logged user a website
    # redirects to the login page, lets name it not_logged_user.html
    """
    <!-- not_logged_user.html -->
    <html>
      <head>
      </head>
      <body>
        Log-in or sign-up to see this page!
        <br><br>
        ... Log in/sign up form ...
      </body>
    </html>
    """
    And I create a <cookie> file with <cookie_name>
    | <cookie_name> | <cookie>                                                 |
    | cookie1.txt   | localhost false /vulnerabilities/weak_id false           |
    |               | 0 dvwaSession 1540558463                                 |
    | cookie2.txt   | localhost false /vulnerabilities/weak_id false           |
    |               | 0 dvwaSession 1540558528                                 |
    | cookie3.txt   | localhost false /vulnerabilities/weak_id false           |
    |               | 0 dvwaSession 1540558715                                 |
    And I execute <bash_command>
    Then I get <result>
    | <bash_command>                              | <result>                   |
    | $ wget --load-cookies cookie1.txt \         | not_logged_user.html       |
    | > 'localhost:32768/vulnerabilities/weak_id/ |                            |
    | $ wget --load-cookies cookie2.txt \         | not_logged_user.html       |
    | > 'localhost:32768/vulnerabilities/weak_id/ |                            |
    | $ wget --load-cookies cookie3.txt \         | logged_user_template.html  |
    | > 'localhost:32768/vulnerabilities/weak_id/ |                            |
    Then I see that cookie3.txt belongs to a logged user
    And I can use this cookie to authenticate myself as that user

  Scenario: Remediation
  The PHP code can be fixed by using a more secure way to generate session IDs
    When I use a current secure hash algorithm like SHA512 and a bit of salt
    Then the session IDs are not correlated one with another
    """
    01  <?php
    02
    03  $html = "";
    04
    05  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    06    $cookie_value = hash('sha512', random_bytes(32) . time());
    07    setcookie("dvwaSession", $cookie_value, 0,
           "/vulnerabilities/weak_id/", $_SERVER['HTTP_HOST'], true, true);
    08  }
    09  ?>
    """
    When I execute the previous code at UNIX epoch time <time>
    Then I get the following <session ID>
    # The value of SHA512 has been split in chunks of 16 bytes per line
    | <time>     | <session ID>                     |
    | 1540581282 | a5bec573a994bff570ec77415986c138 |
    |            | 61d5cf5dcc855d76e0c3acbc1febbdd9 |
    |            | 7089319353ed0dc5e673bac635c68120 |
    |            | 82b6a104c8891fef7edf33c924910df6 |
    | 1540581283 | a44b2ca9193c801f7d278e4c44dbe554 |
    |            | e36cfbd02a35b16f2a5093e245321c04 |
    |            | 64657e013f322ef66429ba64ee5f16f6 |
    |            | 0e52f9050615d9d6396b2e562d729e76 |
    | 1540581284 | b29090353454d730cdeb39bbeac94ed7 |
    |            | be7642368a0404a970b5f71f313b0d4d |
    |            | 4661b89873ca7ee80dc3763c30161660 |
    |            | c3d1fae6ff10c3f7a1ceb139ee6221eb |
    Given SHA512 is non-biyective and non-invertible in feasible time
    And PHP's random_bytes function is cryptografically secure
    # http://php.net/manual/en/function.random-bytes.php
    And the salt used is more than half the output from the hashing function
    Then the system is patched against this kind of attack vector
    And <time> and <session ID> are no correlated anymore
    And the system is also patched against best-known cracking techniques
    # Moreover, to crack hash('sha512', random_bytes(x) . time())
    # You need to perform in average k*(2^(4*x)) sha512 computations
    # where k is a positive integer, therefore making the effort worthless

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      4.6/10 (Medium) - E:U/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      4.0/10 (Medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date <2018-10-26>
