# language: en

Feature: AngularJS Interpolation - Cookie value that is fed into $parse.
  From Firing Range System

  Background:
    Given I'm running Windows 7 Pro
    And also using Firefox version 43.0.1
    And the property "browser.urlbar.filter.javascript=false"
    Given the following
    """
    Message: Cookie value that is fed into $parse.
    Details:
      - Injection into $parse via a cookie value.
    """

    Scenario: AngularJS Interpolation - Form value that is fed into $parse.
    Given the initial site
    Given I inspect the page source code
    """
    <!DOCTYPE html>
    <title>Angular Cookie Parse</title>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.js">
    </script>
    <script>
      /**
       * Returns the value of the first cookie with the given name.
       */
      function lookupCookie(name) {
        var parts = document.cookie.split(/\s*;\s*/);
        var nameEq = name + '=';
        for (var i = 0; i < parts.length; i++) {
          if (parts[i].indexOf(nameEq) == 0) {
            return parts[i].substr(nameEq.length);
          }
        }
      }

      // Pre-seed the cookie, if it has not been set yet.
      var irrelevantName = 'irrelevantAngularCookie';
      if (!lookupCookie(irrelevantName)) {
        document.cookie = irrelevantName + '="42"';
      }

      angular.module('test', [])
          .controller('VulnerableController', ['$parse', function($parse) {
            $parse(lookupCookie(irrelevantName))({});
          }]);
    </script>
    <div ng-app="test" ng-controller="VulnerableController"></div>
    """
    When I see the URL i noticed that it is 1.6.0
    And  I replace write into the cookie "irrelevantAngularCookie"
    """
    constructor.constructor('alert(1)')()
    """
    And refresh the page
    Then An alert is executed with the browser
    Then The vulnerability is verified