# language: en

Feature: AngularJS - Custom start and end without surrounding symbols
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/angular/angular_body_alt_symbols/1.4.0?q=test
    Message: Interpolation symbols, by default {{}}, are replaced with [[]]
    Details:
      - AngularJS custom start and end symbols Page
      - Page in blank
    Objective: Demostrated the Clickjacking vulnearbility on the page.
    """

    Scenario: Testing the vulnerabiity on the page
    Given the following source code
    """
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.js">
    </script>
    <script>
      angular.module('ng').config(function($interpolateProvider) {
          $interpolateProvider.
          startSymbol('[[').
          endSymbol(']]');
      });
    </script>
    <div>
      [[test]]
    </div>
    """
    When I noticed the value of 'q' parameter was passed to the DIV
    And replaced with the following on the URL
    """
    q=this%20a%20new%20parameter%20[[5%2B5]]
    """
    Then once the site was reloaded on the top of the page appeared a text
    """
    this a new parameter 10
    """
    And that means the site taking the 'q' as an AngularJS expression
    And the source code was modified as following
    """
    <div>
      this a new parameter [[5+5]]
    </div>
    """
