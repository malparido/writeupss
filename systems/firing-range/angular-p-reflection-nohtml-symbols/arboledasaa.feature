# language: en

Feature: Parameter reflection on body with symbols from Firing Range System

  Background:
    Given I'm running Windows 7 Pro
    And also using Firefox version 43.0.1
    And the property "browser.urlbar.filter.javascript=false"
    Given the following
    """
    URL:
    Message: Parameter reflection into body, no HTML entity symbol escaping,
             with custom symbols
    Details:
      - The parameter is reflected into the page,
        with alternate interpolation symbols properly escaped with backslashes.
        However, the equivalent HTML entities are not escaped,
        so AngularJS can still execute the payload.
    """

  Scenario: AngularJS Interpolation - parameter reflection on get
    Given the initial site
    Given I see there is a parameter "q" in the URL
    When I send "test"
    Then the body contains "test"
    When I send "{{2*2}}"
    Then it returns "\{\{2*2\}\}"
    Then it behaves as described
    When I send 'hola%26%2391%3B%26%2391%3B2*2%26%2393%3B%26%2393%3Bmundo'
    Then "hola4mundo" is shown
    Then it was with '"&#91;" ([)'
    And '"&#93;" (])'
    Then the vulnerability is confirmed
