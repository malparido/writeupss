# language: en

Feature: AngularJS Interpolation - parameter reflection on body
  From Firing Range System

  Background:
    Given I'm running Windows 7 Pro
    And also using Firefox version 43.0.1
    And the property "browser.urlbar.filter.javascript=false"
    Given the following
    """
    Message: Parameter reflection into body, no HTML entity symbol escaping
    Details:
      - The parameter is reflected into the page,
        with the default interpolation symbols
        properly escaped with backslashes.
        However, the equivalent HTML entities are not escaped,
        so AngularJS can still execute the payload.
    """

    Scenario: AngularJS Interpolation - parameter reflection on get
    Given the initial site
    Given I see there is a parameter "q" in the URL
    When I send "test"
    Then the body contains "test"
    When I send "{{2*2}}"
    Then it returns "\{\{2*2\}\}"
    Then it behaves as described
    When I send:
    """
    &#123;&#123;2*2&#125;&#125;
    """
    Then it returns nothing
    When I send:
    """
    hola#123;#123;2*2&#125;#125;mundo
    """
    Then it returns "hola"
    Then is escaping and never sees "mundo"
    When I send:
    """
    &#91;&#91;2*2&#93;&#93;
    """
    Then nothing is shown
    When I send:
    """
    &lbrace;&lbrace;2*2&&rbrace;&rbrace;
    """
    Then nothing is shown
    When I send:
    """
    &lcub;&lcub;2*2&rcub;&rcub;
    """
    Then nothing is shown
    When I send:
    """
    &#x0007B;&#x0007B;2*2&#x0007D;&#x0007D;
    """
    Then nothing is shown
    When I send:
    """
    %26%23123%3B%26%23123%3B2*2%26%23125%3B%26%23125%3B
    """
    Then "4" is shown
    Then the encoding is the issue
    Then the vulnerability is confirmed