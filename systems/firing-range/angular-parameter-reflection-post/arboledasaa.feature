# language: en

Feature: AngularJS Interpolation - parameter reflection on post
  From Firing Range System

  Background:
    Given I'm running Windows 7 Pro
    And also using Firefox version 43.0.1
    And the property "browser.urlbar.filter.javascript=false"
    Given the following
    """
    URL:
https://public-firing-range.appspot.com/angular/angular_body_raw_post/1.6.0
    Message: POST parameter reflection into body, no symbol escaping
    Details:
      - The POST parameter is reflected into the page as-is;
        no server-side filtering is performed apart from '<' and '>'.
    """

    Scenario: AngularJS Interpolation - parameter reflection on post
    Given the initial site
    Given I see there is a text-box and a button
    When I send "test"
    Then the body contains "test"
    When I send {{2*2}}
    Then it returns 4
    Then the server executed the code
    Then the vulnerability is confirmed