# language: en

Feature: AngularJS Interpolation - Storage value that is fed into $parse.
  From Firing Range System

  Background:
    Given I'm running Windows 7 Pro
    And also using Firefox version 43.0.1
    And the property "browser.urlbar.filter.javascript=false"
    Given the following
    """
    Message: Storage value that is fed into $parse.
    Details:
      - Injection into $parse via a localStorage value.
    """

    Scenario: AngularJS Interpolation - Storage value that is fed into $parse.
    Given the initial site
    Given I inspect the page source code
    """
    <script>
      var irrelevantKey = 'irrelevant-key';
      if (!localStorage[irrelevantKey]) {
        localStorage[irrelevantKey] = '42';
      }
      angular.module('test', [])
          .controller('VulnerableController', ['$parse', function($parse) {
            $parse(localStorage.getItem(irrelevantKey))({});
          }]);
    </script>
    """
    When I see the URL i noticed that it is 1.6.0
    And  I replace write into the localStorage variable "irrelevantKey"
    """
    constructor.constructor('alert(1)')()
    """
    And refresh the page
    Then An alert is executed with the browser
    Then The vulnerability is verified