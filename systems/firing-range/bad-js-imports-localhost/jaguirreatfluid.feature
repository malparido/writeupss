# language: en

Feature: JavaScript import from bad sources - localhost
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/remoteinclude/parameter/script?q=
    http://127.0.0.2/localhost_import.js
    Message: These vulnerabilities import JavaScript from bad sources that are
    not necessarily owned by the page owner.
    Details:
      - Script inclusions from locahost Page
      - Page in blank and two files loaded.
    Objective: Demostrated the vulnearbility of load files from a local ip.
    """

    Scenario: load a js File from the localhost
    Given I checked the source code an found
    """
    <html>
      <head><title>Remote include - URL seeded from query parameter</title>
      </head>
      <body>
        <script src="http://127.0.0.2/localhost_import.js"></script>
      </body>
    </html>
    """
    And saw the js file is called from a loopback ip 127.0.0.2
    When I start a server running MAMP on port 80:90
    And add a file to the localhost server running with MAMP named 'attack.js'
    """
    alert('I\'m a diferent js');
    """
    Then open the page url of firing range with the vulnerability
    And changed the 'q' parameter to other js on a loopback ip by the following
    """
    q=http://127.0.0.1/attack.js
    """
    And after I reloaded the page with the new parameter the alert popup
