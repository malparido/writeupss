# language: en

Feature: CORS tests - Allows insecure scheme
  From Firing Range System

  Background:
    Given I'm running Windows 7 pro
    And also using Google Chrome version 69.0.3497.100
    Given the following
    """
    Message: CORS tests - Allows insecure scheme
    Details:
        - If an HTTP origin can perform authenticated requests
          to an HTTPS origin, the purpose of SSL/TLS encryption
          in terms of web security is defeated.
    """

  Scenario: Allows insecure scheme
    Given the initial site
    Then I inspect the page source code
    """
    <script>
      var xhr = new XMLHttpRequest();
      xhr.open('POST',
 'https://public-firing-range.appspot.com/cors/alloworigin/allowInsecureScheme',
        true);
      xhr.send();
    </script>
    """
    And I see that it is making a post requests
    Given I change the URL to a HTTP
    And start recording the web traffic
    Then I see that the response contains the header:
    """
    access-control-allow-credentials: true
    """
    Then I conclude that it allow credentials throw HTTP
    Then The vulnerability is verified