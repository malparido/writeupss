# language: en

Feature: CORS tests - Allowing Null Origin
  From Firing Range System

  Background:
    Given I'm running Windows 10 Enterprise 1803 (17134.228)
    And also using Burp Suite Community Edition v1.7.36
    And also using Firefox version 61.0.2
    Given the following
    """
    URL: http://localhost:8080/cors/alloworigin/allowNullOrigin
    Message: CORS tests - Allowing Null Origin
    Details:
        - The null origin is permitted
        - Its is equivalent to allowing every origin
    Objective: Perform a CORS tests with Allowing Null Origin
    """
  Scenario: Allowing Null Origin
    Given the inital site
    Then I inspect the page source code
    """
    <script>
      var xhr = new XMLHttpRequest();
      xhr.open('POST',
      'http://localhost:8080/cors/alloworigin/allowNullOrigin', true);
      xhr.send();
    </script>
    """
    And CORS request does not exist
    """
    Request:
      POST /cors/alloworigin/dynamicAllowOrigin HTTP/1.1
      Host: localhost:8080
      User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0)...
      Accept: */*
      Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
      Accept-Encoding: gzip, deflate
      Referer: http://localhost:8080/cors/alloworigin/allowNullOrigin
      ...
    Response:
      HTTP/1.1 200 OK
      Cache-Control: no-cache, no-store, must-revalidate
      Pragma: no-cache
      Expires: Thu, 01 Jan 1970 00:00:00 GMT
      Content-Type: text/html; charset=utf-8
      Server: Development/1.0
      Date: Fri, 14 Sep 2018 15:02:08 GMT
      Content-Length: 7
      Connection: close
    """
    Then I edit the request to public-firing-range.appspot.com
    """
    URL = https://public-firing-range.appspot.com
    Request:
      POST URL/cors/alloworigin/allowNullOrigin HTTP/1.1
      Host: public-firing-range.appspot.com
      User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0)...
      Accept: */*
      Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
      Accept-Encoding: gzip, deflate
      Referer: http://localhost:8080/cors/alloworigin/allowNullOrigin
      Origin: http://localhost:8080
      ...
    Response:
      HTTP/1.1 200 OK
      Access-Control-Allow-Origin: null
      Access-Control-Allow-Credentials: true
      Cache-Control: no-cache, no-store, must-revalidate
      Pragma: no-cache
      Expires: Thu, 01 Jan 1970 00:00:00 GMT
      Content-Type: text/html; charset=utf-8
      Server: Development/1.0
      Date: Fri, 14 Sep 2018 15:03:40 GMT
      Content-Length: 7
      Connection: close
    """
    Then I edit the request with other Origin Header
    """
    URL = https://public-firing-range.appspot.com
    Request:
      POST URL/cors/alloworigin/allowNullOrigin HTTP/1.1
      Host: public-firing-range.appspot.com
      User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0)...
      Accept: */*
      Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
      Accept-Encoding: gzip, deflate
      Referer: http://localhost:8080/cors/alloworigin/allowNullOrigin
      Origin: Any Origin
      ...
    Response:
      HTTP/1.1 200 OK
      Access-Control-Allow-Origin: null
      Access-Control-Allow-Credentials: true
      Cache-Control: no-cache, no-store, must-revalidate
      Pragma: no-cache
      Expires: Thu, 01 Jan 1970 00:00:00 GMT
      Content-Type: text/html; charset=utf-8
      Server: Development/1.0
      Date: Fri, 14 Sep 2018 15:03:40 GMT
      Content-Length: 7
      Connection: close
    """
    Then I conclude that this test case will always have a null origin
    Then The vulnerability is verified