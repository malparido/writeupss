# language: en

Feature: DOM XSS - Event triggering - eval
  From Firing Range System

  Background:
    Given I'm running Windows 7
    And also using Google Chrome version 69.0.3497.100

    Scenario: static detection
    Given the following
    """
    Message: Event triggering - eval
    Details:
    - This class of XSS is only triggered after an event is fired
    """
    Given I inspect the page source code
    """
    <body>
        <script>
          function deferredPayload() {
      // Sync trigger.
    eval(payload);

    // Async trigger.
    function trigger(payload) {
      eval(payload);
    };
    }// Trigger XSS by waiting for typing events
    // watch for change on an input field.

    var input = document.createElement('input');
    input.setAttribute('type', 'text');

    var payload = '';
    function xssIt(e) {
      payload = e.target.value;
      deferredPayload();
    }

    input.addEventListener('keyup', xssIt);
    input.addEventListener('change', xssIt);

    document.body.appendChild(input);// Sync trigger.
    eval(payload);

    // Async trigger.
    function trigger(payload) {
      eval(payload);
    };
        </script><input type="text">

    </body>
    """

    Scenario: DOM XSS - Event triggering - eval
    Given i type "alert(1)" in the textbox
    Then alert is shown
    Then The vulnerability is verified