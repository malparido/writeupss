# language: en

Feature: DOM XSS - Event triggering - innerHTML
  From Firing Range System

  Background:
    Given I'm running Windows 7
    And also using Google Chrome version 69.0.3497.100

    Scenario: static detection
    Given the following
    """
    Message: Event triggering - innerHTML
    Details:
    - This class of XSS is only triggered after an event is fired
    """
    Given I inspect the page source code
    """
    <script>
          function deferredPayload() {
      var div = document.createElement('div');
    document.documentElement.appendChild(div);

    // Sync trigger.
    div.innerHTML = payload;

    // Async trigger.
    function trigger(payload) {
      div.innerHTML = payload;
    };

    }var form = document.createElement('form');

    var input = document.createElement('input');
    input.setAttribute('id', 'userInput');

    var submit = document.createElement('input');
    submit.setAttribute('type', 'submit');

    form.setAttribute('action', '#');
    form.appendChild(input);
    form.appendChild(submit);

    var payload = '';
    form.onsubmit = function() {
      payload = document.getElementById('userInput').value;
      deferredPayload();
      return false;
    };

    document.body.appendChild(form);var div = document.createElement('div');
    document.documentElement.appendChild(div);

    // Sync trigger.
    div.innerHTML = payload;

    // Async trigger.
    function trigger(payload) {
      div.innerHTML = payload;
    };

    </script>
    """

    Scenario: DOM XSS - Event triggering - innerHTML
    Given i type in the textbox:
    """
    <button onClick=alert(1)>Try me</button>
    """
    And press submit
    And press the button "Try me"
    Then alert is shown
    Then The vulnerability is verified