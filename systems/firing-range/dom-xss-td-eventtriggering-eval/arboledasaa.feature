# language: en

Feature: DOM XSS - Event triggering - eval
  From Firing Range System

  Background:
    Given I'm running Windows 7
    And also using Google Chrome version 69.0.3497.100

    Scenario: static detection
    Given the following
    """
    Message: Event triggering - eval
    Details:
    - This class of XSS is only triggered after an event is fired
    """
    Given I inspect the page source code
    """
    <body>
        <script>
          function deferredPayload() {
      // Sync trigger.
    eval(payload);

    // Async trigger.
    function trigger(payload) {
      eval(payload);
    };
    }var form = document.createElement('form');

    var input = document.createElement('input');
    input.setAttribute('id', 'userInput');

    var submit = document.createElement('input');
    submit.setAttribute('type', 'submit');

    form.setAttribute('action', '#');
    form.appendChild(input);
    form.appendChild(submit);

    var payload = '';
    form.onsubmit = function() {
      payload = document.getElementById('userInput').value;
      deferredPayload();
      return false;
    };

    document.body.appendChild(form);// Sync trigger.
    eval(payload);

    // Async trigger.
    function trigger(payload) {
      eval(payload);
    };
    </script>
    <form action="#"><input id="userInput"><input type="submit"></form>

    </body>
    """

    Scenario: DOM XSS - Event triggering - eval
    Given i type "alert(1)" in the textbox an press submit
    Then alert is shown
    Then The vulnerability is verified