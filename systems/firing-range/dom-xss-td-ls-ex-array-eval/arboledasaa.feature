# language: en

Feature: dom xss, dom-xss-td-ls-ex-array-eval
  From Firing Range System

  Background:
    Given I'm running Windows 10 Pro 1703 (15063.0)
    And also using Firefox version 62.0
    Given the following
    """
    URL: https://public-firing-range.appspot.com
    URN: /dom/toxicdom/external/localStorage/array/eval
    """
  Scenario: Dynamic Header Generation
    Given the inital site
    And I see that the page contains the following code:
    """
<script type="text/javascript"
src="/dom/toxicdomscripts/localStorage/array/eval"></script>
    """
    And it contains
    """
    if (!localStorage['badValue']) {
  localStorage['badValue'] = Math.random();
}
var payload = localStorage['badValue'];

setTimeout(function() {
  trigger(localStorage['badValue']); // Using the async trigger.

// Clean up the localStorage to avoid causing side-effect on subsequent tests.
  localStorage.clear();
}, 10);
// Sync trigger.
eval(payload);

// Async trigger.
function trigger(payload) {
  eval(payload);
};
    """
    Given I type F12, go to "Application", "Local Storage", and type:
    """
    Key: badValue
    Value: alert(1)
    """
    And refresh "Local Storge"
    And refresh the page
    Then a 2 pop up's are shown
    Then the vulnerability is verified