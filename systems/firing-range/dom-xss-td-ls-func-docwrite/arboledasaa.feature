# language: en

Feature: dom xss, toxicdom-localstorage-func-documentWrite
  From Firing Range System

  Background:
    Given I'm running Windows 10 Pro 1703 (15063.0)
    And also using Firefox version 62.0
    Given the following
    """
    URL: https://public-firing-range.appspot.com
    URN: /dom/toxicdom/localStorage/function/documentWrite
    """
  Scenario: Dynamic Header Generation
    Given the inital site
    Given I type F12, go to "Application", "Local Storage", and type:
    """
    Key: badValue
    Value: <script>alert(1);</script>
    """
    And refresh "Local Storge"
    And refresh the page
    Then a 2 pop up's are shown
    Then the vulnerability is verified