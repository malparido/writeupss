# language: en

Feature: dom xss, toxicdom-localstorage-func-innerHtml
  From Firing Range System

  Background:
    Given I'm running Windows 10 Pro 1703 (15063.0)
    And also using Firefox version 62.0
    Given the following
    """
    URL: https://public-firing-range.appspot.com
    URN: /dom/toxicdom/localStorage/function/innerHtml
    """
  Scenario: Dynamic Header Generation
    Given the inital site
    Then I see that the page contains the following code:
    """
<script>
      if (!localStorage.getItem('badValue')) {
  localStorage.setItem('badValue', Math.random());
}

var payload = localStorage.getItem('badValue'); // Using sync trigger.

setTimeout(function() {
  trigger(localStorage.getItem('badValue')); // Using the async trigger.

 // Clean up the localStorage to avoid causing side-effect on subsequent tests.
  localStorage.clear();
}, 10);
var div = document.createElement('div');
document.documentElement.appendChild(div);

// Sync trigger.
div.innerHTML = payload;

// Async trigger.
function trigger(payload) {
  div.innerHTML = payload;
};

    </script>
    """
    Given I type F12, go to "Application", "Local Storage", and type:
    """
    Key: badValue
    Value: <script>alert(1);</script>
    """
    And refresh "Local Storge"
    And refresh the page
    Then a 2 pop up's are shown
    Then the vulnerability is verified