# language: en

Feature: firing-range, dom-xss-td-ss-ex-array-eval
  From Firing Range System

  Background:
    Given I'm running Ubuntu 16 LTS
    And also using Firefox version 62.0

  Scenario: Static detection
    When i visit the page
    Then the page contains:
    """
    <script type="text/javascript"
      src="/dom/toxicdomscripts/sessionStorage/array/eval"></script>
    """
    And the referenced script contains:
    """
    if (!sessionStorage['badValue']) {
      sessionStorage['badValue'] = Math.random();
    }

    var payload = sessionStorage['badValue'];

    setTimeout(function() {
      trigger(sessionStorage['badValue']); // Using the async trigger.

      sessionStorage.clear();
    }, 10);
    // Sync trigger.
    eval(payload);

    // Async trigger.
    function trigger(payload) {
      eval(payload);
    };
    """

  Scenario: Exploitation
    Given the inital page
    When I create a sesion storage key and value:
    """
    key: badValue
    value: alert(1)
    """
    And I refresh the page
    Then a pop up is shown
    Then The vulnerability is verified
