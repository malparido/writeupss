# language: en

Feature: firing-range, dom-xss-td-ss-ex-funct-docwrite
  From Firing Range System

  Background:
    Given I'm running Ubuntu 16 LTS
    And also using Firefox version 62.0

  Scenario: Static detection
    When i visit the page
    Then the page contains:
    """
    <script type="text/javascript"
      src="/dom/toxicdomscripts/sessionStorage/function/documentWrite">
    </script>
    """
    And the refered script contains:
    """
    <script>
      if (!sessionStorage.getItem('badValue')) {
        sessionStorage.setItem('badValue', Math.random());
      }

      var payload = sessionStorage.getItem('badValue'); // Using sync trigger.

      setTimeout(function() {
        trigger(sessionStorage.getItem('badValue'));
      // Clean up the sessionStorage to avoid side-effects on subsequent tests.
        sessionStorage.clear();
      }, 10);
      // Sync trigger.
      document.write(payload);

      // Async trigger.
      function trigger(payload) {
        document.write(payload);
      }
    </script>
    """

  Scenario: Exploitation
    Given the inital page
    When I create a sesion storage key and value:
    """
    key: badValue
    value: <button onClick=alert(1)>Try me</button>
    """
    And I paste the link of the page again
    And press enter
    And I push the button
    Then a pop up is shown
    Then The vulnerability is verified
