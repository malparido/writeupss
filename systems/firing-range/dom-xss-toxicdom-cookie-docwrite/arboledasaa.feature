# language: en

Feature: dom xss, toxicdom-cookie-documentWrite
  From Firing Range System

  Background:
    Given I'm running Windows 10 Pro 1703 (15063.0)
    And also using Firefox version 62.0
    Given the following
    """
    URL: https://public-firing-range.appspot.com
    URN: /dom/toxicdom/document/cookie_set/documentWrite
    """
  Scenario: Dynamic Header Generation
    Given the inital site
    And see that it expects a cookie "badValue"
    Then I edit the cookie value to
    """
    <script>alert(1)</script>
    """
    Then a pop up is shown
    Then The vulnerability is verified