# language: en

Feature: dom xss, toxicdom-cookie-innerhtml
  From Firing Range System

  Background:
    Given I'm running Windows 10 Pro 1703 (15063.0)
    And also using Firefox version 62.0
    Given the following
    """
    URL: https://public-firing-range.appspot.com
    URN: /dom/toxicdom/document/cookie_set/innerHtml
    """
  Scenario: Dynamic Header Generation
    Given the inital site
    Then I inspect the page source code
    """

     /*
     * Return the value of the first cookie with the given name.
     */
    function lookupCookie(name) {
      var parts = document.cookie.split(/\s*;\s*/);
      var nameEq = name + '=';
      for (var i = 0; i < parts.length; i++) {
        if (parts[i].indexOf(nameEq) == 0) {
          return parts[i].substr(nameEq.length);
        }
      }
    }

    // Pre-seed the cookie, if it has not been set yet.
    if (!lookupCookie('badValue')) {
      document.cookie = 'badValue="a"';
    }

    var payload = lookupCookie('badValue');

    setTimeout(function() {
      trigger(lookupCookie('badValue'));
    }, 10);
    var div = document.createElement('div');
    document.documentElement.appendChild(div);

    // Sync trigger.
    div.innerHTML = payload;

    // Async trigger.
    function trigger(payload) {
      div.innerHTML = payload;
    };
    """
    And see that it expects a cookie "badValue"
    Then I edit the cookie value to
    """
    <button onclick="alert(1)">Try it</button>
    """
    Then a button is added
    Then if i click it, a pop up is shown
    Then The vulnerability is verified