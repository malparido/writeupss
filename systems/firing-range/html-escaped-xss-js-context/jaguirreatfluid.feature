# language: en

Feature: Javascript unquoted assignment - HTML escaped
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/escape/serverside/escapeHtml/js_assignment?q=a
    Message: Assigns the parameter value to an unquoted js assignment, i.e.
    var foo = %q;
    Details:
      - html javascript escaped page
      - Page in blank
    Objective: Perform an xss attack on the page
    """

    Scenario: XSS attempt
    Given the description that the parameter is executed on javascript
    And I looked at the following source code
    """
    <html>
    <body>
      <script>var foo=a;</script>
    </body>
    </html>
    """
    And noticed that the 'q' parameter on URL is replaced inside the script
    When I replace the url 'q' parameter by the following
    """
    q=document.write(%27This%20is%20an%20XSS%20attack%27)
    """
    And reloaded the page with the new parameter
    Then the page was reloaded with the text 'This is an XSS attack' on the top
