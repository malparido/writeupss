# language: en

Feature: Mixed Content
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URL: https://public-firing-range.appspot.com/mixedcontent/index.html
    Details:
      - Mixed content page
      - Message: This page contains a script tag which sources a
        script over HTTP. This creates a mixed content vulnerability
        when the page itself is opened via HTTPS.
    Objective: Prove an mixed content vulnerability on the page
    """

    Scenario: Mixed content attempt
    Given the hint on the message ogf the page
    When I check the source code on the page given and found the following
    """
    <script
    src="http://public-firing-range.appspot.com/mixedcontent/script.js">
    </script>
    """
    And found an error on the browser console
    Then a message comunicating the insecure content appears
    """
    index.html:1 Mixed Content: The page at
    'https://public-firing-range.appspot.com/mixedcontent/index.html' was
    loaded over HTTPS, but requested an insecure script
    'http://public-firing-range.appspot.com/mixedcontent/script.js'.
    This request has been blocked; the content must be served over HTTPS.
    """
    And shows that the script is loaded via HTTP
