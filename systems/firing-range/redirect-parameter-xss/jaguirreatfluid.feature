# language: en

Feature: Redirect XSS - Redirect Parameter
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/redirect/index.html
    Details:
      - Redirect XSS page
      - Three links with names:
        - Redirect - Parameter
        - Redirect - Parameter - Blocks javascript
        - Redirect - Meta
    Objective: Perform a redirect attack on the page
    """

    Scenario: Redirect XSS attempt
    Given I selected the 'Redirect - Parameter' or any other option
    And the page was reloaded to the home page
    And I looked up the following source code
    """
    <body>
      <ul>
        <li><a href="/redirect/parameter?url=/">Redirect - Parameter</a></li>
        ...
      </ul>
    </body>
    """
    When I replace the url parameter by 'https://www.google.com'
    And reloaded the page with the new parameter
    Then the page was redirected to the google home page and displayed
