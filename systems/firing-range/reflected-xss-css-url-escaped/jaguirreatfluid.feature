# language: en

Feature: URL - CSS - URL escaped
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/escape/serverside/encodeUrl/css_import?q=a
    Message: Assigns the parameter to the SRC property of a STYLE tag in
    the HEAD
    Details:
      - CSS URL escaped page
    Objective: Perform an xss attack on the page
    """

    Scenario: Reflected XSS attempt
    Given the description that the parameter is redirected to a page
    And I looked at the following source code
    """
    <html>
      <head>
        <link rel="stylesheet" type="text/css" href="a" />
      </head>
      <body>
      </body>
    </html>
    """
    When I replace the url 'q' parameter by the following
    """
    q=a”/><script></script>
    """
    And reloaded the page with the new parameter
    Then the page was reloaded and all the page was in blank
    And there was displayed an error on the console
    """
    Failed to load resource: the server responded with a status of 400
    (Bad Request)
    """
    And there was a new css file with the exact name
    But there is no change in the page because it encode the special characters

    Scenario: css injection file attempt
    When I create a css file and upload it on transfer.sh with the following
    """
    body {
      background: url(https://news.bitcoin.com/wp-content/uploads/2018/07/
        korean-government-criticized.png);
      background-repeat: repeat;
      background-origin: content-box;
    }
    """
    And replace the url 'q' parameter by the following using an URL encoder
    """
    q=https://transfer.sh/161gx/untitled.css
    """
    Then the page was reloaded and all the page was in blank
    And after a few seconds on the page was loaded an image
    And there was a new css file with the untitled.css name
    And an image was added to the documents of the page
