# language: en

Feature: Remote Inclusion - Object Raw - parameter
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/remoteinclude/parameter/object_raw?q=
    https://google.com
    Message: Tests in this page allow an attacker to control the URL of an
    active content being imported in the page.
    Details:
      - Remote Inclusion - Object Raw Page
      - Page in blank
    Objective: Show the Remote inclusion vulnearbility on the page.
    """

    Scenario: Remote inclusion attempt
    Given I am running MAMP to run a web page on port 80:81
    When I create a new web page with the following code
    """
    <html>
    <head>
    </head>
    <body>
      <style>
        p {
          position: absolute;
          left: 20px;
          top: 100px;
        }
        button {
          position: absolute;
          left: 20px;
          top: 20px;
        }
      </style>
      <button>You will be reach!</button>
      <p>CLICK THE BUTTON TO GAIN MONEY</p>
    </body>
    </html>
    """
    And I replace the url 'q' parameter by the following
    """
    q=http://localhost:8081/untitled.html
    """
    And reloaded the page with the new parameter
    Then the page was reloaded with the view of my created page in the top
    And a new file on the name 'untitled.html' appeared
