# language: en

Feature: Tag Based XSS - Pure Tags DIV
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/tags/div?q=a
    Message: Allows any DIV tag
    Details:
      - div tag Based Page
      - Message on the top of the page with content 'Invalid input, no tags'
    Objective: Perform a redirect of the page with a tag based xss
    """

    Scenario: div XSS first attempt
    Given the description said we need to do a redirect with the div tag
    And I found that on the source code on the body there is div tag
    When I replace the url 'q' parameter by the following using an URL encoder
    """
    %3Cdiv+id%3D%22redirect%22+onclick%3D%22window.location.href%3D%27http%3A
    %2F%2Fwww.google.com%27%22%3E%3Ca%3Eredirected+link%3C%2Fa%3E%3C%2Fdiv%3E
    """
    And using the 'window.location.ref' attributes and 'a' tag passed a div tag
    Then the page was reloaded with a text message 'redirected link'
    And once I clicked on the text the page was redirected to google home page
