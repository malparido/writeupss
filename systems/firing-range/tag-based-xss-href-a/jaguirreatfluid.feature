# language: en

Feature: Tag Based XSS - Attributes restricted HREF - a
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/tags/a/href?q=a
    Message: The following vectors will only allow a certain attribute for a
    tag. The first value is the tag being allowed, the second the specific
    attribute.
    Details:
      - HREF - A tag Based Page
      - Message on the top of the page with content 'Invalid input, no tags'
    Objective: Perform a redirect of the page with a tag based xss
    """

    Scenario: href - a XSS first attempt
    Given the description said we need to do a redirect with the href attribute
    When I replace the url 'q' parameter by the following using an URL encoder
    """
    %3Ca%20href=%27http://www.google.com%27%3Eclick%20here%3C/a%3E
    """
    And using the 'href' attributes passed an 'a' tag
    Then the page was reloaded with a text message 'click here'
    And once I clicked on the text the page was redirected to google home page
    And on the source code was annexed
    """
    <body>
      <a href="http://www.google.com">click here</a>
    </body>
    """
