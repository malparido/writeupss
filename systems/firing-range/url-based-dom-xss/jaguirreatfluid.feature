# language: en

Feature: URL Based DOM XSS - hash input.formaction
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed Firiring Range locally
    Given the following scenario
    """
    URL: http://localhost:8080/urldom/location/hash/input.formaction#
    //example.org
    Details:
      - button.formaction page
      - Submit button
    Objective: Prove a flash injection vulneranbility on the page
    """

    Scenario: Submited form attempt
    Given the hint on the message of the page gave me
    When I looked at the source code at the attribute of the form
    """
    <form>
    <input formaction type="submit" value="submit!">
    </form>
    """
    And found that depends on the url passed so I changed to
    """
    http://localhost:8080/urldom/location/hash/input.formaction#
    http://www.google.com
    """
    And click submit to send the form
    Then the page was redirected to the google domain
    And shows that the form can be submited to a any other URL
