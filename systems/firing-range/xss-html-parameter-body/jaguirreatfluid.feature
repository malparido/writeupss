# language: en

Feature: Reflected XSS HTML - Parameter Body
  From the Firing Range system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And installed firing-range locally
    Given the following scenario
    """
    URL: http://localhost:8080/reflected/parameter/body?q=a
    Message: The parameter is echoed within the main BODY tag
    Details:
      - Reflected XSS HTML Parameter-Body page
      - The page shows an 'a' on the top
    Objective: Perform an reflected xss on the body of the HTML page
    """
    Scenario: HTML injection attempt
    Given the following HTML code for the page
    """
    <html>
      <body>
        a
      </body>
    </html>
    """
    When I see the URL noticed that 'q' parameter is the content of the body
    And  I replace the 'q' parameter by the following code in html
    """
    q=<h1>this%20change%20the%20body</h1>
    """
    Then the page was reloaded with a message 'this change the body' in bold
    And the code was added to the html code of the page
    """
    <html>
      <body>
        <h1>this change the body</h1>
      </body>
    </html>
    """
