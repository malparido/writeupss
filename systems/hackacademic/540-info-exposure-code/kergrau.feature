## Version 1.4.1
## language: en

Feature:
  TOE:
    hackademic
  Location:
    http://127.0.0.1/hackademic/challenges/ch002/index.php
  CWE:
    CWE-540: Information Exposure Through Source Code
  Goal:
    Find password of this site.
  Recommendation:
    Remove visible code to user which contains the password.

  Background:
  Hacker's Software:
    | <Software name> |     <Version>     |
    | Ubuntu          | 16.04.1 (64-bits) |
    | XAMPP           | 7.2.0             |
    | Mozilla Firefox | 63.0.3 (64-bits)  |
    | Hackademic      | 0.9               |
  TOE information:
  Given I'm running Hackademic on XAMPP
    And I access to challenge page through firefox
    Then I access to challenge 2 to find password

  Scenario: Normal Case
  The site allows enter a password and validate it.
    Given I go to http://127.0.0.1/hackademic/challenges/ch002/index.php
    And I see the page [evidence](normal-use-case.png)

  Scenario: Static Detection
  The password is reveals in the source code.
    When I open inspector code I could see a javascript code in the HTML
    """
    81 <input name="Button1" type="button" value="Enter"
       style="width: 95px" onclick="GetPassInfo()" /></fieldset><br />

    94 function GetPassInfo(){
    95 var madhouuuuuuuseeee = "givesacountinatoap lary"
    98 var d = madhouuuuuuuseeee.charAt(3);

    101 var f = madhouuuuuuuseeee.charAt(5);
    102 var j = madhouuuuuuuseeee.charAt(9);
        var h = madhouuuuuuuseeee.charAt(6);
    103 var x = madhouuuuuuuseeee.charAt(21);
    104 var s = madhouuuuuuuseeee.charAt(17);
        var l = madhouuuuuuuseeee.charAt(11);
    105 var k = madhouuuuuuuseeee.charAt(10);
    106 var t = madhouuuuuuuseeee.charAt(18);
    107 var o = madhouuuuuuuseeee.charAt(13);
    108 var q = madhouuuuuuuseeee.charAt(15);
    109 var i = madhouuuuuuuseeee.charAt(7);
    110 var y = madhouuuuuuuseeee.charAt(22);

    116 var Wrong = (d+""+j+""+k+""+d+""+x+""+t+""+o+""+t+""+h+""+i+""+l+""
                     +j+""+t+""+k+""+i+""+t+""+s+""+q+""+f+""+y)

    118 /*if (document.forms[0].Password1.value == Wrong)
    119 location.href="index.php?Result=" + Wrong;
    120 */
    121 location.href="index.php?Result=" + document.forms[0].Password1.value;
    122 }
    """
    Then I conclude in this code in line 81 calls a function {GetPassInfo()}
    And this in the line 116 reveals the password by chars.

  Scenario: Dynamic Detection
  I can see the password in the source code.
    Given the challenge page I opened the inspector code
    Then I analyzed the code and I saw a script {GetPassInfo()}
    And this function contains a puzzle if you solve it yo have the password
    Then this script pass password to a PHP function to validate it.
    Then I conclude that the problem is I can see function hence the password
    When I inspect source code.

  Scenario: Exploitation
  Solve the puzzle.
    Given the puzzle {(see static detection)}
    Then I proceed to extract chars of variable
    And put in the order of Wrong variable (see static detection).
    Then I conclude I solved the puzzle then I got the password

  Scenario: Remediation
  Passing directly password to PHP code
    Given I have patched the code by sending password directly to PHP code
    And I erased the javascript function {GetPassInfo()}
    Then the password is not visible to user.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    2.5/10 (Low) - AV:L/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.8/10 (Medium) - E:F/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.8/10 (Medium) - CR:X/IR:X/AR:X/MAV:L/MAC:X/MPR:N/MUI:N
                      /MS:U/MC:L/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2018-12-10