# language: en

Feature: Stored Cross-Site Scripting
  From the Hackazon system
  Of the category A7: Cross-Site Scripting
  As the registered user disassembly

  Background:
    Given I'm running Ubuntu 16.04 LTS
    And using Docker version 18.06.1-ce, build e68fc7a
    And also using Firefox 62
    Given the following scenario
    Given i'm admin
    """
    URN: /admin/faq
    Message: Frequently Asked Questions
    Details:
      - Form with fields to enter question and answer
      - A button to submit the question that is going to be listed
    Objective: Verify the XSS PoC
    """

  Scenario: Implement a XSS attack
    Given the initial page
    When I create a new FAQ with
    """
    Question: <button onClick=alert(1)>Try me</button>
    Answer: <button onClick=alert(1)>Try me</button>
    """
    And I submit it
    Then a new FAQ is created
    When a user visits the FAQ page
    Then the new FAQ is shown with 2 buttons
    When the user click them
    Then script is excecuted
