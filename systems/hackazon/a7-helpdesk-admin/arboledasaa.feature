# language: en

Feature: Stored Cross-Site Scripting
  From the Hackazon system
  Of the category A7: Cross-Site Scripting

  Background:
    Given I'm running Ubuntu 16.04
    And using Docker version 18.06.1-ce, build e68fc7a
    And also using Firefox 62.0 (64-bit)
    Given the following scenario
    """
    URN: /admin/enquiry/edit/3
    Message: Enquiry
    Field: Message
    """

  Scenario: Send a malicious script to a customer
    Given I'm autenticated as admin
    When I reply to a Enquiry with
    """
    <script>alert(1)</script>
    """
    And the user opens the page:
    """
    URN: helpdesk/#enquiry/3
    """
    Then it takes a while to load
    Then i conclude that it executed
    But the browser blocked it

  Scenario: Send a malicious script to a customer with social engineering
    Given I'm autenticated as admin
    And the user trust me, as i'm admin
    When I reply to a Enquiry with
    """
    <button onClick="alert(1)">Try me</button>
    """
    And the user opens the page:
    """
    URN: helpdesk/#enquiry/3
    """
    And the user clicks the button
    Then an alert is shown
    Then the vulnerability is confirmed
