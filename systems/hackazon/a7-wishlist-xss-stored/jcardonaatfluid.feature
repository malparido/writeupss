# language: en

Feature: Cross-Site Scripting - Stored
  From the Hackazon system
  Of the category A7: Cross-Site Scripting
  As the registered user dissasembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /wishlist/new
    Message: Add Wish List
    Details:
      - Form to create a new wish list with the name and type fields
      - A button to submit the data that redirects to the new list
    Objective: Perform a first approach to a real XSS attack
    """

  Scenario: Inject an XSS vector in the name field
    Given I found that the website is prone to XSS through text fields
    And this PoC is intended to expose the possibility of an attack
    When I create a new wish list and look out the source code of the web page
    """
    <a href="/wishlist/view/16">New Wish List</a>
    """
    Then it shows me that maybe the name field is not being properly sanitized
    When I use the following attack vector to reveal the user session cookies
    """
    # Change the current name to
    </a><script>alert(document.cookie)</script><a>
    """
    Then pop up the PHPSESSID token and other irrelevant cookies of the user
    """
    PHPSESSID=8cdc33fs1gjj96qc3qorru3fa0;
    visited_products=%2C3%2C64%2C101%2C21%2C1%2C;
    security_level=0
    """
    Given it's pretty obvious that the name appears to be a little bit strange
    And just by deleting the wish list, the XSS disappears it's not very useful
