# language: en
Feature: Lack of Binary Protections
  From the hackazon mobile apk
  From the OWASP 2014-M10 Security Misconfiguration category

  Background:
   Given I'm running https://www.ostorlab.co Mobile Application Security Scanner
   Given the i download the hackazon mobile apk from
   """
   https://github.com/rapid7/hackazon/raw/master/web/app/hackazon.apk
   """
  Scenario: Vulnerability discovery
   Given I see the scan result
   Then I found the apk was compiled without obfuscation
   Then the application could be reverse engineered
