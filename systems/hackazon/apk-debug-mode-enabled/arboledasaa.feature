# language: en
Feature: Using Components with Known Vulnerabilities
  From the hackazon mobile apk
  From the OWASP A6:2017 Security Misconfiguration category
  From the CERT DRD10-J Do not relase apps that are debuggable category

  Background:
   Given I'm running https://www.ostorlab.co Mobile Application Security Scanner
   Given the i download the hackazon mobile apk from
   """
   https://github.com/rapid7/hackazon/raw/master/web/app/hackazon.apk
   """
  Scenario: Vulnerability discovery
   Given I see the scan result
   Then I found the apk was compiled with "Debug mode" enabled
