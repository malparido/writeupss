# language: en
Feature: Application Error diclosure
  From the hackazon system
  From the HTTP fingerprinting category

  Background:
   Given I'm running OWASP ZAP 2.7.0
   Given the following scenario
   """
   URL: http://192.168.168.1.56
   URN: /twitter
   """
  Scenario: Vulnerability discovery
   Given I scan for vulnerabilites
   And The scanner sends this header
   """
   GET http://192.168.1.56/twitter HTTP/1.1
   User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; rv:39.0)
   Gecko/20100101 Firefox/39.0
   Pragma: no-cache
   Cache-Control: no-cache
   Content-Length: 0
   Referer: http://192.168.1.52/sitemap.xml
   Cookie: PHPSESSID=53updtf0ru84v1a51ggfe305i5;
   visited_products=
   %2C101%2C16%2C81%2C74%2C64%2C61%2C51%2C178%2C95%2C93%2C163%2C148%2C
   Host: 192.168.1.56
   """
   Then it shows the following header
   """
   HTTP/1.1 200 OK
   Date: Thu, 30 Aug 2018 19:50:40 GMT
   Server: Apache/2.4.7 (Ubuntu)
   X-Powered-By: PHP/5.5.9-1ubuntu4.24
   Expires: Thu, 19 Nov 1981 08:52:00 GMT
   Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0
   Pragma: no-cache
   Vary: Accept-Encoding
   Content-Length: 178
   """
   But it shouldn't, in particular it runs on Apache/2.4.7 (Ubuntu)
