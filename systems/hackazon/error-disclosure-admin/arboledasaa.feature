# language: en
Feature: Application Error diclosure
  From the hackazon system
  From the A6:2017 - Security Misconfiguration category
  From the OWASP Improper Error Handling category

  Background:
   Given I'm running Ubuntu 16.04 LTS
   Given I'm running Docker 18.06.1-ce, build e68fc7a
   Given I'm running hackzon from docker
   """
   https://github.com/mdeous/docker-hackazon
   """

  Scenario: Vulnerability discovery
   When i visit the URL at LINK.lst
   Then the following is shown
   """
   Fatal error: Call to undefined method
   App\Admin\Controller\Product::getRoleOptions()
   in /var/www/hackazon/classes/App/Admin/Controller/Product.php
   on line 184
   """
   Then the application structure is disclosed to the admin
   Then the vulnerability is confirmed
