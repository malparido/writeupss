# language: en
Feature: External redirect admin
  From the hackazon system

  Background:
    Given I'm running Firefox 62 (64 bit)
    Given I also running Docker version 18.06.1-ce
    Given I also running ZAP version 2.7.0

  Scenario: Vulnerability discovery
    Given The site is running from docker
    Given I need to know vulnerables endpoints
    When I run ZAP to discover that endpoints
    When I configure the login script for the admin account
    Then ZAP discover the endpoints
    """
    URN:/admin/user/login
    Query string: return_url
    """
    Then it test the query string with pasting a remote url
    """
    return_url=http%3A%2F%2Fwww.google.com%2F
    """
    And finds that it is redirected to google

  Scenario: Manual validation
    When I visit the url in the LINK.lst file
    When I autenticate as admin
    Then I'm redirected to google.com
    Then the vuln is validated

