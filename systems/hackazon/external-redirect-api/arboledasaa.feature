# language: en
Feature: External redirect api
  From the hackazon system
  Background:
    Given I also running Docker version 18.06.1-ce
    Given I also running ZAP version 2.7.0

  Scenario: Vulnerability discovery
    Given i'm logged in the api
    Given i test the "api/product?page=" URN
    When I see that it's contents are echoed in the HTTP Header: "LINK"
    Then I craft the following argument:
    """
    page=1%3E%3B%20rel%3D%22hack%22%2C%3Chttp%3A%2F%2Fwww.google.com.co
    """
    Then it returns in the LINKS
    """
    Link: </api/product?page=1>;
    rel="hack",<http://www.google.com.co>;
    rel="current",</api/product?page=1>;
    rel="first",</api/product?page=1>;
    rel="last"
    """
    Then it returns the link as a valid rel
    Then a api customer may be trick into an arbitrary url
    Then the vuln is confirmed
