# language: en
Feature: External redirect
  From the hackazon system
  Background:
    Given I'm running Firefox 61.0.2 (64 bit)
    Given I also running Docker version 18.06.0-ce
    Given I also running ZAP version 2.7.0

  Scenario: Vulnerability discovery
    Given I need to know vulnerables endpoints
    Then I run ZAP to discover that endpoints
    Then ZAP discover the endpoints
    """
    URL:http://localhost/user/login?return_url=http%3A%2F%2Fwww.google.com%2F
    """
    Then an user login is redirected to the given url
    And this happens even when the url is outside of the top-level domain
