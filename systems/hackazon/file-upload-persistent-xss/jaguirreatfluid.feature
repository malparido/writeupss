# language: en

Feature: File Upload Persistent XSS
  From the hackazon system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URL: http://localhost/account/profile/edit
    Message: File Upload Persistent XSS
    Details:
      - Edit profile page
      - First and last name and phone number text fields
      - Select avatar image button
    Objective: Perform a XSS Insection to the site with a upload file
    """
    Scenario: Local File injection attempt
    When I see the profile page and the user account image on the souce code
    And I realize that is a document with format png
    And I tried the file for a txt file
    Then The page is reloaded with a question mark insted of an image
    And In the profile main page a question mark appears
    But it can not be open by cicking on the image

    Scenario: Persistent XSS attempt
    When I change the image to a HTML file with the code
    """
    <html>
    <script>
      alert(New Persistent XSS);
    </script>
    </html>
    """
    And I select open the image in a new Tab
    Then The a new page is open with a pop up with message "New Persistent XSS"

    Scenario: Persistent XSS cookie stealing
    When I upload insted of an image a HTML file with the code
    """
    <html>
    <script>
      alert(document.cookie);
    </script>
    </html>
    """
    And I select open the image in a new Tab
    Then A new page is open with a pop up with message
    """
    visited_products=%2C202%2C203%2C176%2C83%2C184%2C78%2C157%2C75%2C209%2C188
    %2C189%2C41%2C197%2C64%2C;
    PHPSESSID=0ebd34d506b35d00113b59acb1e36286
    """
    And gives ID's information of the sesion user and products visited
