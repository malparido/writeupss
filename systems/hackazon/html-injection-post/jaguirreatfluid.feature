# language: en

Feature: HTML Injection - Reflected (POST)
  From the hackazon system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URL: http://hackazon.webscantest.com/faq
    Message: HTML injection - Reflected POST
    Details:
      - FAQ page
      - Text field to enter the email
      - Text field to enter the comment (question)
      - Submit) button
    Objective: Perform HTML to the web page
    """
    Scenario: HTML code behavior
    When I look for the inputs "email" and "question" on source code
    And field the field with normal information email and text
    Then I see that the email field has the validation to be in email format
    But the on the question field the only validation is not to be empty
    And the question content is published on the page

    Scenario: HTML injection attempt
    When I type a text with email format on "email" input
    And  I type "<button>HTML error</button>" on "question" input
    Then In the page was published a clickable button with the tag "HTML error"
