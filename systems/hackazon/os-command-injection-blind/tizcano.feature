# language: en
Feature: os-command-injection-blind
  From the hackazon system

  Background:
    Given I'm running Firefox 61.0.2 (64 bit)
    Given I also running Docker version 18.06.0-ce
    Given I also running ZAP version 2.7.0

  Scenario: Vulnerability discovery
    Given I know some vulnerables endpoints
    Given I know the system run on Apache/2.4.7 (Ubuntu)
    Then I make a os command injection attack
    """
    URL: http://localhost/account/documents?page=;sleep%2010s
    """
    Then I check the developer tools to check the time of the request
    """
    Normal Waiting: 38ms
    Attack Waiting: 10047 ms
    """
    Then I check the system is vulnerable to os command injection based on:
    """
    ; and | separators
    """

