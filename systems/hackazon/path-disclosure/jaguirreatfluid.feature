# language: en

Feature: Full Path Disclosure
  From the hackazon system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URL: http://localhost/account/documents?page=delivery.html
    Message: Full Path Disclosure
    Details:
      - Delivery document page
      - Text field to enter the email
      - Text field to enter the comment (question)
      - Submit) button
    Objective: Found a FPD on the web page
    """
    Scenario: Empty array attempt
    When I look for a page on hackazon with a file uploaded
    And found that a method to make a FPD to cause an error
    Then I rewrite the URL adding the braces as following
    """
    URL: http://localhost/account/documents?page[]=delivery.html
    """
    And the page was reloaded with the next error
    """
    Uncaught Error: Call to a member function raw() on array in
    /Applications/home/hackazon/hackazon-master/classes/App/Controller
    /Account.php:97 Stack trace: 0 /Applications/home/hackazon/hackazon-
    master/classes/App/Core/BaseController.php(333):
    App\Controller\Account->action_documents()... {main} thrown in
    /Applications/home/hackazon/hackazon-master/classes/App/Controller/
    Account.php on line 97
    """
    And showing the full path and other configurations of the file.

