# language: en
Feature: Path Traversal
  From the hackazon system

  Background:
    Given I'm running Firefox 61.0.2 (64 bit)
    Given I also running Docker version 18.06.0-ce
    Given I also running ZAP version 2.7.0

  Scenario: Vulnerability discovery
    Given The site is running from Apache/2.4.7 (Ubuntu)
    Given I need to know vulnerables endpoints
    Then I run ZAP to discover that endpoints
    Then ZAP discover the endpoints
    """
    URL:http://localhost/account/documents?page=../../../../../../../etc/passwd
    """
    Then I open the account controller
    And see the action_documents controller don't check the command to execute
    And run
    """
    cat ../../../../../../../etc/passwd
    """
