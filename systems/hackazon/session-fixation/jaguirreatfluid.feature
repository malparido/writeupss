# language: en

Feature: Session fixation
  From the hackazon system

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 18.06.0-ce, build 26399
    And also using Safari Version 11.1.2 (13605.3.8)
    And using Google Chrome Version 68.0.3440.106
    Given the following scenario
    """
    URL: http://hackazon.webscantest.com/
    Message: Session fixation
    Details:
      - Login Page with:
        - Text field to enter the Username
        - Text field to enter the Password
        - Sing In button
      - FAQ page with:
        - Text field to enter the email
        - Text field to enter the comment (question)
        - Submit button
    Objective: Perform a session fixation to the web page
    """
    Scenario: session ID behavior
    When I enter to see the cookies before login
    And notice that the sesion id is the php default one 'PHPSESSID'
    And there is the value of the products search in 'view_products'
    But once I login with the Username and Password
    And the page of the authenticated user and cookies view are reloaded
    Then the 'PHPSESSID' doesn't change its value
    And mantains its value throught all the pages view inlcuding after log out
    But the 'view_products' value changes everytime the user see a new product

    Scenario: Session Fixation first attempt
    When I try to enter a xss on the principal page fixing the URL as:
    """
    localhost/<script>document.cookie=”PHPSESSID=abcd”;</script>
    """
    And  I reload the page to apply the changes
    Then a Message error on the page is displayed
    """
    Error: 404 Not Found Please try to change your request.
    """
    And I close and reopen the browser to verity if the cookie was fixed
    But A new session ID was set after loaded the page.

    Scenario: Session Fixation second attempt
    When I try to enter a xss on the FAQ on the question field by:
    """
    <script> document.cookie='PHPSESSID=123456'</script>
    """
    And the comment is submited and shown on FAQ page
    And I close and open the FAQ page on a chrome browser
    And open the cookies view to check the session id
    Then I notice that the 'PHPSESSID' has the value '123456'
    And after I login with the Username and Password the value stays still
    And everytime a user visit FAQ page the session id will be fixed
