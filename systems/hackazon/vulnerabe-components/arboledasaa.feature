# language: en
Feature: Using Components with Known Vulnerabilities
  From the hackazon system
  From the OWASP A9:2017 - Using Components with Known Vulnerabilities category

  Background:
   Given I'm running OWASP ZAP 2.7.0
   Given the following scenario
   """
   URL: http://hackazon.webscantest.com/
   """
  Scenario: Vulnerability discovery
   Given I see the body response
   Then I found there is "jquery-1.10.2.js"
   Then I search for the known bugs of this version
   """
   https://bugs.jquery.com/query?status=closed&version=1.10.2
   &col=id&col=summary&col=status&col=owner&col=type&col=priority&col=milestone
   &order=priority
   """
   And found over 331 bugs
