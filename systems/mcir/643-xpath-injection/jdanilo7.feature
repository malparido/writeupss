## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR: Magical Code Injection Rainbow
  Category:
    XPath Injection
  Location:
    MCIR/xmlmao/xpath_challenges/challenge0.php - Injection String (field)

  CWE:
    CWE-643: XPath Injection
  Goal:
    Retrieve all the user names
  Recommendation:
    Escape the input

  Background:
  Hacker's software:
    |     <Software name>      |    <Version>    |
    |          OWASP           |       1.6       |
    |   VMWare Workstation 15  |     15.0.2      |
    |     Firefox Quantum      |    60.2.0esr    |
    |        Kali Linux        |      4.18       |
  TOE information:
    Given I'm running OWASP on VMWare on Kali Linux
    And I'm accessing the MCIR pages through my Firefox browser
    And I enter a php page that allows me to query a user using XPath

  Scenario: Normal use case
  The site allows the user to enter a user name and get an XML object with it
    Given I access the page MCIR/xmlmao/xpath_challenges/challenge0.php
    And I enter the string "jsmiley"
    And I click the Inject! button
    Then I get an XML object containing the entered user name
    And I can see [evidence](normal-use-case.png)

  Scenario: Static Detection
  The php code does not validate the input before forming the XPath query
    When I look at the source code
    And I check the code that prepares the XML request
    """
    45  $node_path = $display_node_path = '/xmlfile/users';
    46  $node_name = $display_node_name = '/user';
    47  $condition = $display_condition = "username='jsmiley'";
    48  $sub_node = $display_sub_node = '/username';
    ...
    60  switch ($_REQUEST['location']){
    ...
    73    case 'condition_string':
    74      $condition = "username='" . $_REQUEST['inject_string'] . "'";
    75      break;
    ...
    87  $query = $node_path . $node_name . "[". $condition . "]" . $sub_node;
    ...
    91  $xml = simplexml_load_file('data.xml');
    ...
    105  ini_set('display_errors', 1);
    106  $results = $xml->xpath($query);
    """
    Then I notice there is no validation on the input for the XPath query

  Scenario: Dynamic detection
  The input of the Inject! field is parsed as part of the XPath query
    Given I enter "jsmiley'" in the text field
    And I click the Inject! button
    Then I get the error message that can be seen in [evidence](xml-error.png)
    And I conclude that my input was parsed as part of a query
    And the application is vulnerable to XPath injection

  Scenario: Exploitation
  Retrieving all user names
    Given I enter "' or ''='" in the input field
    And I click the Inject! button
    Then I get a result array with objects containing all the user names
    And it can be seen in [evidence](names-list.png)

  Scenario: Remediation
  The php code can be fixed by the input before forming the XPath query
    Given I have patched the code escaping the user input
    """
    87  $condition = htmlspecialchars($condition, ENT_QUOTES);
    88  $query = $node_path . $node_name . "[". $condition . "]" . $sub_node;
    """
    Then I can prevent the injection of the code shown above
    Given If I re-run my exploit by entering ""' or ''='" as input
    And clicking the Inject! button
    Then I get an error message indicating that my input could not be parsed
    And it can be seen in [evidence](empty-array.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.2/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.8/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.8/10 (High) - CR:M/IR:M

  Scenario: Correlations
    No correlations have been found to this date 2018-12-18
