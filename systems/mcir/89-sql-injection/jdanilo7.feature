## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR: Magical Code Injection Rainbow
  Category:
    SQL Injection
  Location:
    MCIR/sqlol/challenges/challenge0.php - Injection String (field)

  CWE:
    CWE-89: SQL Injection
  Goal:
    Retrieve all user names from the database
  Recommendation:
    Escape the query string

  Background:
  Hacker's software:
    |     <Software name>      |    <Version>    |
    |          OWASP           |       1.6       |
    |   VMWare Workstation 15  |     15.0.2      |
    |     Firefox Quantum      |    60.2.0esr    |
    |        Kali Linux        |      4.18       |
  TOE information:
    Given I'm running OWASP on VMWare on Kali Linux
    And I'm accessing the MCIR pages through my Firefox browser
    And I enter a php page that allows me to query a user

  Scenario: Normal use case
  The site allows the user to enter a user name and retrieve that record
    Given I access the page MCIR/sqlol/challenges/challenge0.php
    And I enter the user name "Peter Weiner"
    And I click the Inject! button
    Then I get a success message showing an array with the user name I entered
    And I can see [evidence](normal-use-case.png)

  Scenario: Static Detection
  The php code does not validate the input before querying the database
    When I look at the source code
    And I check the code that prepares the statement for the where clause
    And I notice that it is split in two files. The first one has this code:
    """
    54    $display_column_name = $column_name = 'username';
    55    $display_table_name = $table_name = 'users';
    56    $display_where_clause = $where_clause = 'WHERE isadmin = 0';
    57    $display_group_by_clause = $group_by_clause = 'GROUP BY username';
    58    $display_order_by_clause = $order_by_clause =
          'ORDER BY username ASC';
    59    $display_having_clause = $having_clause = 'HAVING 1 = 1';
    60
    61    switch ($_REQUEST['location']){
    ...
    70        case 'where_string':
    71            $where_clause = "WHERE username = '"
                  . $_REQUEST['inject_string'] . "'";
    72            break;
    ...
    92    $query = "SELECT $column_name FROM $table_name $where_clause
          $group_by_clause $order_by_clause ";
    """
    And the second one has this code:
    """
    18    $db_conn = NewADOConnection($dsn);
    ...
    24    $db_conn->SetFetchMode(ADODB_FETCH_ASSOC);
    25    $results = $db_conn->Execute($query);
    """
    Then I notice there is no validation on the input value for $where_clause

  Scenario: Dynamic detection
  The input of the Inject! field is parsed as part of a query
    Given I enter "Peter Weiner'" in the text field
    And I click the Inject! button
    Then I get the error message that can be seen in [evidence](sql-error.png)
    And I conclude that my input was parsed as part of a query
    And the application is vulnerable to sql injection

  Scenario: Exploitation
  Retrieving all the users in the database
    Given I enter "' or 'a'='a" in the input text field
    And I click the Inject! button
    Then I get a list containing all the users in the database
    And it can be seen in [evidence](users-list.png)

  Scenario: Remediation
  The php code can be fixed by escaping the input string
    Given I have patched the php code by moving the formation of the query
    And putting it in the second file, and escaping the input string
    Then I can prevent the injection of the code shown above
    """
    18  $db_conn = NewADOConnection($dsn);
    ...
    23  $input_val = mysql_real_escape_string($_REQUEST['inject_string']);
    24  $where_clause = "WHERE username = '$input_val'";
    25  $query = "SELECT $column_name FROM $table_name $where_clause
    26           $group_by_clause $order_by_clause ";"
    27  $db_conn->SetFetchMode(ADODB_FETCH_ASSOC);
    28    $results = $db_conn->Execute($query);
    """
    Then If I re-run my exploit by entering "' or 'a'='a" as input
    And clicking the Inject! button
    Then I get an empty response since there were no matching records
    And it can be seen in [evidence](empty-list.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.1/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.7/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.4/10 (Medium) - CR:H/IR:H

  Scenario: Correlations
    No correlations have been found to this date 2018-12-09
