## Version 1.4.1
## language: en

Feature:
  TOE:
    MCIR: Magical Code Injection Rainbow
  Category:
    XML Injection
  Location:
    MCIR/xmlmao/xmli_challenges/challenge0.php - Injection String (field)

  CWE:
    CWE-91: XML Injection
  Goal:
    Add an addtional data record
  Recommendation:
    Use an XML schema to validate the input

  Background:
  Hacker's software:
    |     <Software name>      |    <Version>    |
    |          OWASP           |       1.6       |
    |   VMWare Workstation 15  |     15.0.2      |
    |     Firefox Quantum      |    60.2.0esr    |
    |        Kali Linux        |      4.18       |
  TOE information:
    Given I'm running OWASP on VMWare on Kali Linux
    And I'm accessing the MCIR pages through my Firefox browser
    And I enter a php page that allows me to input data through an XML service

  Scenario: Normal use case
  The site allows the user to enter a data string and store that value
    Given I access the page MCIR/xmlmao/xmli_challenges/challenge0.php
    And I enter the string "value1"
    And I click the Inject! button
    Then I get a result message showing the value that I entered
    And I can see [evidence](normal-use-case.png)

  Scenario: Static Detection
  The php code does not validate the input before processing the XML request
    When I look at the source code
    And I check the code that prepares the XML request
    """
    44  <?php
    45  $xmldata = '<?xml version="1.0" encoding="ISO-8859-1"?>
    46  <!DOCTYPE xmlfile [
    47  <!ENTITY author "Inject4" > ]>
    48  <xmlfile>
    49   <hooray attrib="Inject2">
    50    <ilovepie>Inject1</ilovepie>
    51   </hooray>
    52   <data>
    53    <![CDATA[Inject3]]>
    54   </data>
    55  </xmlfile>
    56  ';
    ...
    67    switch($_REQUEST['location']){
    ...
    76        case 'cdatavalue':
    77        $xmldata = str_replace('Inject3', $_REQUEST['inject_string'],
              $xmldata);
    78        break;
    ...
    115  ini_set('display_errors', 1);
    116  $xml =simplexml_load_string($xmldata,'SimpleXMLElement',$xmloptions);
    """
    Then I notice there is no validation on the input value for the data tag

  Scenario: Dynamic detection
  The input of the Inject! field is parsed as part of the XML request
    Given I enter "value1]]>" in the text field
    And I click the Inject! button
    Then I get the error message that can be seen in [evidence](xml-error.png)
    And I conclude that my input was parsed as part of a request
    And the application is vulnerable to XML injection

  Scenario: Exploitation
  Storing more than one data value
    Given I enter "value1]]></data><data> <![CDATA[hackval" in the input field
    And I click the Inject! button
    Then I get a result message showing the two values that I entered
    And it can be seen in [evidence](values-list.png)

  Scenario: Remediation
  The php code can be fixed by using an XML schema to validate the data
    Given I have created an XML schema
    """
    1  <?xml version="1.0"?>
    2  <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    3  <xs:element name="xmlfile">
    4    <xs:complexType>
    5      <xs:sequence>
    6        <xs:element name="hooray">
    7          <xs:complexType>
    8            <xs:sequence>
    9              <xs:element name="ilovepie" type="xs:string"/>
    10           </xs:sequence>
    11            <xs:attribute name="attrib" type="xs:string"/>
    12          </xs:complexType>
    13       </xs:element>
    14       <xs:element name="data" type="xs:string"/>
    15     </xs:sequence>
    16   </xs:complexType>
    17  </xs:element>
    18  </xs:schema>
    """
    And I have patched the code using the schema to validate the XML string
    """
    115  ini_set('display_errors', 1);
    116  $xml =simplexml_load_string($xmldata,'SimpleXMLElement',$xmloptions);
    117  $dom = dom_import_simplexml($xml);
    118  if (!$dom->schemaValidate(schema.xsd)) {
    119    trigger_error("Invalid input.");
    120  }
    """
    Then I can prevent the injection of the code shown above
    Given If I re-run my exploit
    And enter "value1]]></data><data> <![CDATA[hackval" as input
    And click the Inject! button
    Then I get an error message because the XML string didn't match the format
    And it can be seen in [evidence](xml-validated.png)

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.2/10 (High) - CR:M/IR:M

  Scenario: Correlations
    No correlations have been found to this date 2018-12-13
