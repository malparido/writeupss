## Version 1.4.0
## language: en

Feature:
  TOE:
    Mutillidae 2.1.19
  Page name:
    A3 - Broken Authentication and Session Management - Login
  CWE:
    CWE-639: Authorization Bypass through User-Controlled Key
  Goal:
    Gain access to the site as a user different from myself
  Recommendation:
    Cypher cookies in some way. Validate data in each request.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows       | 10            |
    | Google Chrome | 69.0.3497.100 |
  TOE information:
    Given I am accessing the site "http://mutillidae/index.php?page=login.php"
    And enter a php site that allows me to login to the site
    And the server is running PHP version 5.2.4
    And Apache version 2.2.8
    And is running on Metasploitable 2 with kernel 2.6.24

  Scenario: Normal use case
  Accesing the website as normal
    Given I access "http://mutillidae/index.php?page=login.php"
    And I enter my credentials for the site
    Then I'm logged in and can use the site as myself

  Scenario: Static detection
    When I look at the code for index.php
    Then I see it's taking the uid directly from the cookies in plaintext
    """
    292 if (isset($_COOKIE['uid'])){
    293
    294  try{
    295    $lQueryResult =
                $SQLQueryHandler->getUserAccountByID($_COOKIE['uid']);
    296
    297      if ($lQueryResult->num_rows > 0) {
    298        $row = $lQueryResult->fetch_object();
    299        $_SESSION['loggedin'] = 'True';
    300        $_SESSION['uid'] = $row->cid;
    301        $_SESSION['logged_in_user'] = $row->username;
    302        $_SESSION['logged_in_usersignature'] = $row->mysignature;
    303        $_SESSION['is_admin'] = $row->is_admin;
    304        header('Logged-In-User: '.$_SESSION['logged_in_user'], true);
    305      }// end if ($result->num_rows > 0)
    306
    307    } catch (Exception $e) {
    308       echo $CustomErrorHandler->FormatError($e, $lQueryString);
    309   }// end try
    """
    Then I conclude it will log you in with whatever uid you give

  Scenario: Dynamic detection
  No data validation
    Given I go to """http://mutillidae/index.php?page=login.php"""
    And open Chrome developer tools to see what cookies the site is using
    And I see a field named """uid""" with just a number
    Then I guess it's the current user id
    And I try changing it's value and refreshing the page
    And I see my user info in the header change to another user's
    And I go to other pages confirming I can use the site as this other user
    Then I conclude I can spoof the uid and be any user I want

  Scenario: Exploitation
  Get admin rights to the site when I shouldn't be able to
    Given I know I can make myself any user by changing the uid field in cookies
    Then I try different uids to see if I can find an interesting user
    And when I change the value to 1
    Then I find myself logged in as admin
    And I can perform whatever actions I want on the site as admin

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    10/10 (High) - E:H/RL:X/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.4/10 (High) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:L

  Scenario: Correlations
    systems/multilidae/a2-xss-persistent-useragent
      Given I obtain some user's cookies using a XSS attack
      And I use that cookie data in my own browser
      Then I can login as that specific user and spoof their identity
