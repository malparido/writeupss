## Version 1.4.1
## language: en

Feature:
  TOE:
    WackoPicko
  Category:
    Cookie Without 'HttpOnly' Flag
  Location:
   admin/index.php?page=login

  CWE:
    CWE-1004: Sensitive Cookie Without 'HttpOnly' Flag
  Goal:
    Get cookies with wireshark
  Recommendation:
    set the HttpOnly flag to true

  Background:
  Hacker's software:
    | <Software name>     | <Version>   |
    | Firefox             | 64.0        |
    | WackoPicko          | 1.4.0       |
    | wireshark           | 2.4.2       |

  TOE information:
    Given I enter the site http://localhost:8080/
    And I can buy, upload and share images on the site
    And the server is runing MySQL version >=5
    And PHP version 5.5.9
    And apache version 2.4.7
    And is running on Docker image (adamdoupe/wackopicko) on parrot OS 4.2.2x64

  Scenario: Normal use case
  Buying and uploading images
    Given I access the site "http://localhost:8080/users/login.php"
    Then I enter or register with my name
    And I can upload images
    And I see images on recent images
    And I can comment images and send comment

  Scenario: Static detection
  The cookie of the user and admin login "admin/index.php?page=login"
  or "http://localhost:8080/users/login.php" no has protection.
  httpOnly flag is not set on file php.ini
    When I open the file php.ini on "/etc/php5/cli/php.ini"
    Then I see the "session.cookie_httponly" is not set
    """
    1400 ; Whether or not to add the httpOnly flag to the cookie, which makes it
    inaccessible to browser scripting languages such as JavaScript.
    1401 ; http://php.net/session.cookie-httponly
    1402 session.cookie_httponly =

    1404 ; Handler used to serialize data.php is the standard serializer of PHP.
    1405 ; http://php.net/session.serialize-handler
    1406 session.serialize_handler = php
    """

  Scenario: Dynamic detection
  logging in with my user on the site
    Given I access the site "http://localhost:8080/admin/index.php?page=login"
    #or "http://localhost:8080/users/login.php"
    Then I see that site can be vulnerable because the login box may have
    #Vulnerables cookies
    And I see too that the page URL is http and not https
    Then the insecure cookies can exist
    And I conclude that the listen to cookies can be posible.

  Scenario: Exploitation
  getting the cookie taking advantage the HttpOnly flag
    Given I open wireshark
    Then I select my network card (localhost)
    And I go to the page "http://localhost:8080/admin/index.php?page=login"
    Then my wireshark showme the cookie on http. see imagen.png
    Then I conclude that the get cookies is possible in local mode.

  Scenario: Remediation
  The server WackoPicko has session cookie without HttpOnlyflag in the direction
  "http://localhost:8080/admin/index.php?page=login" or
  "http://localhost:8080/users/login.php"
    Given this I advise to repair the vulnerability modifying the line 1402:
    """
    1400 ; Whether or not to add the httpOnly flag to the cookie, which makes it
    inaccessible to browser scripting languages such as JavaScript.
    1401 ; http://php.net/session.cookie-httponly
    1402 session.cookie_httponly = 1

    1404 ; Handler used to serialize data.php is the standard serializer of PHP.
    1405 ; http://php.net/session.serialize-handler
    1406 session.serialize_handler = php
    """
    Then I can confirm that the vulnerability can be successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.3/10 (Low) - AV:L/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.1/10 (Low) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    2.4/10 (Low) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2018-12-31
