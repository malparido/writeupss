Name:   WackoPicko
Tech:   PHP
Site:   https://www.aldeid.com/wiki/WackoPicko
Repo:   https://github.com/adamdoupe/WackoPicko/
Author: adamdoupe
From:   https://www.owasp.org/index.php/OWASP_Vulnerable_Web_Applications_Directory_Project#tab=Off-Line_apps
