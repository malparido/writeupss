## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Location:
    http://localhost/WebGoat/start.mvc#lesson/AuthBypass.lesson/1
    - secQuestion0 (Field)
  CWE:
    CWE-287: Improper Authentication
  Goal:
    Bypass a questions authentication
  Recommendation:
    Ensure the request parameters usage for authentication

  Background:
  Hacker's software:
    | <Software name>    |    <Version>         |
    | Windows            | 10.0.16299 (x64)     |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |
    | Burp Suite CE      | 1.7.36               |
  TOE information:
    Given I'm running WebGoat standalone jar application
    And using java jre1.8.0_181 to run it
    """
    java -jar webgoat-server-8.0.0.M21.jar --server.port=80
    """
    And url "http://localhost/WebGoat/start.mvc#lesson/AuthBypass.lesson/1"
    And I'm accessing the WebGoat site through my browser

  Scenario: Normal use case
  The site allows the user to reset password by question validation
    Given The access to the websit url
    Then I fill two validation questions
    """
    What is the name of your favorite teacher?
    What is the name of the street you grew up on?
    """
    And I do not know the correct answers
    And I click the submit button
    Then the website shows the message
    """
    Not quite, please try again.
    """

  Scenario: Static detection
  Inspect request parameters
    Given website access through browser
    Then I inspect the available code using the browser
    And I check the parameters in the form
    """
    <p>What is the name of your favorite teacher?</p>
    <input name="secQuestion0" value="" type="TEXT">
    <p>What is the name of the street you grew up on?</p>
    <input name="secQuestion1" value="" type="TEXT"><br>
    """
    And I see the validation parameters "secQuestion0" and "secQuestion1"

  Scenario: Dynamic detection
  Intercept request parameters
    Given the website url
    Then I use Burp Suite CE along Firefox to intercept for normal use case
    And I see the parameters posted for the 1st click step
    """
    secQuestion0=&secQuestion1=&jsEnabled=1
    &verifyMethod=SEC_QUESTIONS&userId=12309746
    """

  Scenario: Exploitation
  Tamper the questions parameters
    Given the website url
    Then I enter security questions and clic submit
    And I use Burp to intercept the POST request parameters
    """
    secQuestion0=&secQuestion1=&jsEnabled=1
    &verifyMethod=SEC_QUESTIONS&userId=12309746
    """
    Then I modify the questions parameters
    """
    secQuestion2=&secQuestion3=&jsEnabled=1
    &verifyMethod=SEC_QUESTIONS&userId=12309746
    """
    Then I Forward the modified request
    And I get the questions bypassed [evidence](verification-skip.png)
    And I can now change the password

  Scenario: Remediation
  Ensure the parameters "secQuestion0" and "secQuestion1" are mandatory for
  the verification process

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:L/AC:L/PR:H/UI:R/S:U/C:H/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.5/10 (Medium) - E:U/RL:X/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    5.2/10 (Medium) - CR:H/IR:L/AR:L

  Scenario: Correlations
  No correlations have been found to this date 2018-12-18
