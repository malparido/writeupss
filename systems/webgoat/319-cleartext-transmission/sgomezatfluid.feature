## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    insecure Communication
  Location:
    /WebGoat/start.mvc - username, password (Fields)
  CWE:
    CWE-319: Cleartext Transmission of Sensitive Information
  Goal:
    Sniff victim's credentials
  Recommendation:
    Always encrypt sensitive data

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Wireshark             | 2.6.5     |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to WebGoat/start.mvc#lesson/InsecureLogin.lesson/1
    Then I get a button that logs in as another user
    And a form to submit the users credentials

  Scenario: Dynamic detection
  Detecting http communication
    Given I go to WebGoat/start.mvc#lesson/InsecureLogin.lesson/1
    And submit my credentials
    Then I notice the whole process is going through http
    Then I know all the data is traveling in plaintext

  Scenario: Exploitation
  Sniff another user's credentials
    Given I credentials are being sent in plaintext
    Then I sniff the network with Wireshark
    And wait until the victim logs in to the site
    Then I capture the http packet with his creds (evidence)[wireshark.png]
    Then I can login as the victim

  Scenario: Remediation
  Implement HTTPS
    Given I set the server up to only accept encrypted connections via HTTPS
    Then all the data is encrypted
    And an attacker can't sniff credentials anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.8/10 (Medium) - AV:A/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.3/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-02