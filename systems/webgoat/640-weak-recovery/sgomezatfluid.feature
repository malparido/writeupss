## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Password Reset
  Location:
    WebGoat/start.mvc#lesson/PasswordReset.lesson/3 - securityQuestion (field)
  CWE:
    CWE-640: Weak Password Recovery Mechanism for Forgotten Password
  Goal:
    Recover another user's password
  Recommendation:
    Don't use a fixed security question, throttle recovery tries

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    Given I am running WebGoat on http://localhost:8000

  Scenario: Normal use case
    Given I go to WebGoat/start.mvc#lesson/PasswordReset.lesson/3
    Then I see a password recovery form
    Then I input my username and secret question answer
    Then I recover my password

  Scenario: Static detection
  Only question and valid username disclosure
    Given I see the code in "/password-reset/resources/html/PasswordReset.html"
    """
    115 <form>
    116     <div class="form-group">
    117         <label>Your username</label>
    118         <input name="username" class="form-control" placeholder=
                "Username"
    119         type="text"/>
    120     </div>
    121     <div class="form-group">
    122         <label>What is your favorite color?</label>
    123         <input class="form-control" placeholder="Answer security
                question"
    124         type="text"
    125                 name="securityQuestion"/>
    126     </div>
    127     <div class="form-group">
    128         <button type="submit" class="btn btn-primary btn-block">
    129         Submit</button>
    130     </div>
    131 </form>
    """
    Then I notice there's only one security question option
    Then I check the code in "/password-reset/plugin/QuestionsAssignment.java"
    """
    47  String validAnswer = COLORS.get(username.toLowerCase());
    48  if (validAnswer == null) {
    49      return trackProgress(failed().feedback("password-questions-unknown-
              user").feedbackArgs(username).build());
    50  } else if (validAnswer.equals(securityQuestion)) {
    51      return trackProgress(success().build());
    52  }
    53  return trackProgress(failed().build());
    """
    And I notice it gives feedback about if the user exists or not

  Scenario: Dynamic detection
  Listing users
    Given I am at the password recovery form
    Then I try to recover a password with the username "user"
    But I get this response
    """
    User username is not a valid user
    """
    Then I try it with my own username and a bad answer and get
    """
    Sorry the solution is not correct, please try again.
    """
    Then I know I can list valid users by trying different usernames
    Then I try different bad answers to see if they get throttled or blocked
    But they don't, so I know I can bruteforce questions
    Then I try common names until I get the same message as with my name
    Then I know I have a valid user and can bruteforce the security question

  Scenario: Exploitation
  Reset another users password
    Given I have a valid username, and know I can bruteforce sec questions
    And the secret question is "Favorite Color"
    Then I try different colors
    And I find the answer is "purple"
    Then I have reset the victim's password

  Scenario: Remediation
  No fixed secutiry question
    Given the system is patched to don't give me an only question
    """
    115 <form>
    116   <div class="form-group">
    117       <label>Your username</label>
    118       <input name="username" class="form-control" placeholder="Username"
              type="text"/>
    119   </div>
    120   <div class="form-group">
    121       <label>Security Question</label>
    122       <select name="securityQuestion">
    123           <option value="car">What was your first car brand?</option>
    124           <option value="mother">What was your mothers maiden name?
                  </option>
    125           <option value="music">what is your favorite music genre?
                  </option>
    126           <option value="pet">What was your pets first name?</option>
    127       </select>
    128   </div>
    129   <div class="form-group">
    130       <label>Secret answer</label>
    131       <input class="form-control" placeholder="Answer security question"
              type="text"
    132               name="securityAnswer"/>
    133   </div>
    134   <div class="form-group">
    135       <button type="submit" class="btn btn-primary btn-block"> Submit
              </button>
    136   </div>
    137 </form>
    """
    And doesn't give me feedback on whether or not the user Im resetting exists
    """
    47  String validAnswer = COLORS.get(username.toLowerCase());
    48  if (validAnswer.equals(securityQuestion)) {
    49      return trackProgress(success().build());
    50  }
    51  return trackProgress(failed().build());
    """
    Then bruteforcing the question is way harder

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:H/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.2/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.9/10 (Medium) - CR:L/IR:H/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2018-12-28