## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Cross Site Scripting
  Location:
    WebGoat/start.mvc#lesson/CrossSiteScripting.lesson/12 - commentInput (Field)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Goal:
    Execute a stored XSS attack
  Recommendation:
    Sanitize user input before rendering page

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to WebGoat/start.mvc#lesson/CrossSiteScripting.lesson/12
    And I get a comment box where I can submit comments and see previous ones

  Scenario: Static detection
  No input sanitizing
    Given I check the code at
    """
    webgoat-lessons/cross-site-scripting/src/main/java/org/owasp/webgoat/plugin/
    StoredXssComments.java
    """
    And I see
    """
    92  Comment comment = parseJson(commentStr);
    93
    94  EvictingQueue<Comment> comments = userComments.getOrDefault(webSession.
    getUserName(), EvictingQueue.create(100));
    95  comment.setDateTime(DateTime.now().toString(fmt));
    96  comment.setUser(webSession.getUserName());
    97
    98  comments.add(comment);
    99  userComments.put(webSession.getUserName(), comments);
    """
    Then I notice it stores the raw user input
    Then I check the code that shows the comments
    """
    78  List<Comment> allComments = Lists.newArrayList();
    79  Collection<Comment> newComments = userComments.get(webSession.
    getUserName());
    80  allComments.addAll(comments);
    81  if (newComments != null) {
    82      allComments.addAll(newComments);
    83  }
    84  Collections.reverse(allComments);
    85  return allComments;
    """
    And notice it directly outputs whatever is stored

  Scenario: Dynamic detection
  Detecting stored XSS
    Given I submit a comment injecting JS code
    """
    <script>alert(1)</script>
    """
    Then I reload the comments page and get an alert
    Then I know the page is vulnerable to XSS

  Scenario: Exploitation
  Execute the challenge JS function
    Given I know the target is vulnerable to stored XSS
    And the challenge in "WebGoat/start.mvc#lesson/CrossSiteScripting.lesson/12"
    """
    See the comments below.

    Add a comment with a javascript payload. Again …​ you want to call the
    webgoat.customjs.phoneHome function.

    As an attacker (offensive security), keep in mind that most apps are not
    going to have such a straight-forwardly named compromise. Also, you may have
    to find a way to load your own javascript dynamically to fully achieve goals
    of exfiltrating data.
    """
    Then I submit a comment that calls the challenge function
    """
    <script>webgoat.customjs.phoneHome()</script>
    """
    Then I reload the page, open the browser console and get the challenge code
    Then I have successfully exploited the page

  Scenario: Remediation
  Sanitize user input
    Given I patch the code to sanitize user input before storing it
    """
    92  Comment comment = parseJson(commentStr);
    93
    94  EvictingQueue<Comment> comments = userComments.getOrDefault(webSession.
    getUserName(), EvictingQueue.create(100));
    95  comment.setDateTime(DateTime.now().toString(fmt));
    96  comment.setUser(webSession.getUserName());
    97  comment = Jsoup.clean(comment, Whitelist.basic());
    98  comments.add(comment);
    99  userComments.put(webSession.getUserName(), comments);
    """
    Then I submit a malicious comment
    Then it gets encoded and the script doesn't run

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.9/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-02