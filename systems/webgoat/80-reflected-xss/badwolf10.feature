## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Location:
    http://localhost/WebGoat/start.mvc#lesson/CrossSiteScripting.lesson/6
    - field1 (Field)
  CWE:
    CWE-80: Improper Neutralization of Script-Related HTML Tags in a Web Page
  Goal:
    Find vulnerable field to reflected XSS
  Recommendation:
    Sanitize input from html or javascript characters

  Background:
  Hacker's software:
    | <Software name>    |    <Version>         |
    | Windows            | 10.0.16299 (x64)     |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |
  TOE information:
    Given I'm running WebGoat standalone jar application
    And using java jre1.8.0_181 to run it
    """
    java -jar webgoat-server-8.0.0.M21.jar --server.port=80
    """
    And the url
    """
    http://localhost/WebGoat/start.mvc#lesson/CrossSiteScripting.lesson/6
    """
    And I'm accessing the WebGoat site through my browser

  Scenario: Normal use case
  The site allows the user to update a shopping cart with relevant information
    Given The access to the websit url
    Then I fill the quantity fields form and creditcard fields
    And I click the update cart
    Then the website returns the computed price and credit card information

  Scenario: Static detection
  Inspect reflected code
    Given The backend code at
    """
    webgoat-lessons/cross-site-scripting/src/main/java/org/owasp/webgoat
    /plugin/
    CrossSiteScriptingLesson5a.java
    """
    Then I check the piece of the code
    """
    65 StringBuffer cart = new StringBuffer();
    66 cart.append("Thank you for shopping at WebGoat. <br />You're support is
    appreciated<hr />");
    67 cart.append("<p>We have chaged credit card:" + field1 + "<br />");
    68 cart.append(   "                             ------------------- <br />
    ");
    69 cart.append( "
    """
    And I see the parameter "field1" is returned in the response at line 67

  Scenario: Dynamic detection
  Intercept request parameters
    Given the website url
    Then I fill all field of the form
    And I click update card button
    And I get the credit card field content returned
    And I deduce that field is vulnerable

  Scenario: Exploitation
  Inject script in vulnerable field
    Given the website url
    And the detected vulnerable field of the credit card number
    Then I inject the payload
    """
    <script>alert(document.cookie)</script>
    """
    And I see cookie information of the site [evidence](xss-reflected.png)

  Scenario: Remediation
  Sanitize "field1" input
    Given the backend source code
    Then use apache utils to escape HTML malicious tags in "field1"
    """
    import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;
    """
    And sanitize it before returning
    """
    field1 = escapeHtml4(field1);
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.8/10 (Medium) - AV:L/AC:L/PR:L/UI:R/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:U/RL:W/RC:R
  Environmental: Unique and relevant attributes to a specific user environment
    4.1/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-02
