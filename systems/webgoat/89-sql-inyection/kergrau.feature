## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Location:
    http://127.0.0.1:8080/WebGoat/start.mvc#lesson/SqlInjection.lesson/6
  CWE:
    CWE-89: SQL Injection
  Goal:
    Retrieve all the users from the users table.
  Recommendation:
    Parameterize queries

  Background:
  Hacker's software:
    | <Software name>    |    <Version>         |
    | Ubuntu             | 16.04 (64-bits)      |
    | Mozilla Firefox    | 64.0 (64-bits)       |
    | WebGoat            | 8.0                  |
  TOE information:
    Given I'm running WebGoat standalone jar application
    """
    java -jar webgoat-server-8.0.0.M21.jar
    """
    And I'm accessing the WebGoat site through my browser

  Scenario: Normal use case
  The site returns data associate a name.
    Given a textbox you put in there a name
    And then click in "Get Account Info" button
    Then the site returns all data associate to name of the textbox.

  Scenario: Dynamic detection
  I tried a SQL injection attack
    Given the statement of lesson
    Then I see a SQL query [evidence](dynamic-detec.png)
    Then I knew the vulnerability

  Scenario: Exploitation
  Retrieve all user's data
    When I knew the vulnerability is "SQL Injection"
    Then I pass to textbox the follow string "kerg' or'1'='1"
    Then site returns me all user's data [evidence](returns-data.png)

  Scenario: Remediation
  Parameterize queries
    Given the current query
    """
    "select * from users where LAST_NAME = ‘" + userName + "'";
    """
    And a way of mitigate the SQL injection is parametized query.
    Then the parameterized query would be as follows
    """
    String query = "SELECT * FROM users WHERE last_name = ?";
    PreparedStatement statement = connection.prepareStatement(query);
    statement.setString(1, accountName);
    ResultSet results = statement.executeQuery();
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.3/10 (Medium) - AV:N/AC:H/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.5/10 (Medium) - E:P/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.2/10 (Medium) - CR:H/IR:H/AR:H

  Scenario: Correlations
    No correlations have been found to this date 2018-12-19