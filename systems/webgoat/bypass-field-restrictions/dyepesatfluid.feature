# language: en

Feature: Bypass field restrictions from WebGoat Project

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am running WebGoat 8.0 in Docker container webgoat/webgoat-8.0:
  """
  openjdk:8
  """
  And I am using Firefox 52.7.3 (64-bit)
  And I am runnin Burp Suite Community edition v1.7.30
  Given a Java web site with Salary Manager as title
  """
  URL: WebGoat/start.mvc#lesson/BypassRestrictions.lesson/1
  Message: Field restrictions
  Objective 1: Send a request that bypasses restrictions of all four of these \
  fields
  Evidence: Four input elements
            - Select field with two possible values
            - Radio button with two possible values
            - Checkbox: value either on or off
            - Input restricted to max 5 characters
  """

Scenario Outline: Information gathering
  """
  The user has a to much control over the front-end of a web
  Application. Sometimes HTML code can be altered, sometimes scripts.
  It is why there should be input validation on server-side
  """
  Given a form element containing what was specified before
  And where each of this has the following with its correspondant value

  """
  # Select field with two possible values
  <select name="select">
     <option value="option1">Option 1</option>
     <option value="option2">Option 2</option>
  </select>

  # Radio button with two possible values
  <div>Radio button with two possible values</div>
  ...
  <input name="radio" value="option1" checked="checked" type="radio">
  ...
  <input name="radio" value="option2" type="radio">

  # Checkbox: value either on or off
  <div>Checkbox: value either on or off</div>
  <input name="checkbox" checked="checked" type="checkbox">

  # Input restricted to max 5 characters
  <input value="12345" name="shortInput" maxlength="5" type="text">
  """
  When I submit the data as it is
  And using BurpSuite as proxy to intercept the request
  Then I should see the request like this:
  """
  POST /WebGoat/BypassRestrictions/FieldRestrictions HTTP/1.1
  Host: 192.168.1.116:8080
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 \
  Firefox/52.0
  Accept: */*
  Accept-Language: en-US,en;q=0.5
  Accept-Encoding: gzip, deflate
  Referer: http://192.168.1.116:8080/WebGoat/start.mvc
  Content-Type: application/x-www-form-urlencoded; charset=UTF-8
  X-Requested-With: XMLHttpRequest
  Content-Length: 57
  Cookie: JSESSIONID=F469C6C600FF763A47E3A0DCBDDC86CF
  Connection: close

  select=option1&radio=option1&checkbox=on&shortInput=12345
  """

Scenario: Bypassing controls
  """
  If client-side data is not validated at the server-side
  Tampering data would allow to bypass controls by sending
  Arbitrary data
  """
  When I modify the values of the input on the intercepted request

  # Original sent data
  | Type | Element    | Value     |
  | Body | select     | option1   |
  | Body | radio      | option1   |
  | Body | checkbox   | on        |
  | Body | shortInput | 12345     |

  And use values that were not expected

  # Tampered data
  | Type | Element    | Value     |
  | Body | select     | option13  |
  | Body | radio      | option13  |
  | Body | checkbox   | true      |
  | Body | shortInput | "!"#$SAD" |

  And use the tampered data in burp

  """
  POST /WebGoat/BypassRestrictions/FieldRestrictions HTTP/1.1
  Host: 192.168.1.116:8080
  ...
  select=option13&radio=option13&checkbox=true&shortInput="!"#$SAD"
  """
  And as data was not validated in server-side
  Then I should validate the challenge
  """
  RESPONSE
  HTTP/1.1 200
  X-Content-Type-Options: nosniff
  X-XSS-Protection: 1; mode=block
  X-Frame-Options: DENY
  X-Application-Context: application:8080
  Content-Type: application/json;charset=UTF-8
  Date: Wed, 04 Apr 2018 18:08:30 GMT
  Connection: close
  Content-Length: 132

  {
    "lessonCompleted" : true,
    "feedback" : "Congratulations. \
    You have successfully completed the assignment.",
    "output" : null
  }
  """
