# language: en

Feature: Client side filtering
  From WebGoat Project
  From Client side
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am running WebGoat 8.0 in Docker container webgoat/webgoat-8.0:
  """
  openjdk:8
  """
  And I am using Firefox 52.7.3 (64-bit)
  Given a Java web site with Salary Manager as title
  """
  URL: WebGoat/start.mvc#lesson/ClientSideFiltering.lesson/1
  Message: Client side filtering
  Objective 1: What is Neville Bartholomew's salary?
  Evidence: Information displayed whan an employee is chosen
  from a drop down menu
  """
  Given I am logged in as an employee
  And should not have access to CEO information

Scenario Outline: Flaw discovery
Whenever data is send to the client, it is a good practice to send
only information which they are supposed to have access to.
  Given a drop down menu with employee names

  # Actual drop down items shown:
  |   | Choose Employee |
  | 1 | Larry Stooge    |
  | 2 | Curly Stooge    |
  | 3 | Eric Walker     |
  | 4 | Tom Cat         |
  | 5 | Jerry Mouse     |
  | 6 | David Giambi    |
  | 7 | Bruce McGuirre  |
  | 8 | Sean Livingston |
  | 9 | Joanne McDougal |

  When I click in each item, the following information is displayed:

  # Employee information table
  | User ID | First Name | Last Name | SSN         | Salary |
  | 103     | Curly      | Stooge    | 961-08-0047 | 5000   |

  Then I should seek if extra information is being sent
  And take advantage of that flaw

Scenario: Exploit extra information
An extra amount of information is being returned by the server
information, that not everyone should have access to.
  When I inspect the DOM  with Firefox Developer Tools
  And I look the code related with the drop down menu

  """
  # Drop down menu
  <select id="UserSelect" onfocus="fetchUserData()" ...>

  And I see there is a related tag:

  # Table element containing employee records
  <table style="display: none" id="hiddenEmployeeRecords" ...>

  When I look it's structure:

  # Records table html structure
  <tbody>
  <tr>
    <td>UserID</td>
    <td>First Name</td>
    <td>Last Name</td>
    <td>SSN</td>
    <td>Salary</td>
  </tr>

  And I see there are actually more elements
  And more elements that the ones that appear in the selector

  # Table elements
  <tr id="101" <="" tr="">
  <tr id="102" <="" tr="">
  <tr id="103" <="" tr="">
  <tr id="104" <="" tr="">
  <tr id="105" <="" tr="">
  <tr id="106" <="" tr="">
  <tr id="107" <="" tr="">
  <tr id="108" <="" tr="">
  <tr id="109" <="" tr="">

  # Extra elements not shown on selector
  <tr id="110" <="" tr="">
  <tr id="111" <="" tr="">
  <tr id="112" <="" tr="">
  """

  When I check the each of the extra elements
  Then I see the target information
  """
  # Target information revealed
  <tr id="112" <="" tr="">
    <td>112</td>
    <td>Neville</td>
    <td>Bartholomew</td>
    <td>111-111-1111</td>
    <td>450000</td>
  </tr>
  """
  And I use his salary value to solve the challenge
