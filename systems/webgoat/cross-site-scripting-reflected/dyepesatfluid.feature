# language: en

Feature: Cross-Site Scripting(XSS)
  From WebGoat Project
  From Cross Site Scripting category
  With my username Skhorn
 
Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am running WebGoat 8.0 in Docker container webgoat/webgoat-8.0:
  """
    JVM with openjdk:8
  """
  And I am using Firefox 52.7.3 (64-bit)
  Given a Java the first challenge on Reflected XSS
  """
  URL: WebGoat/start.mvc#lesson/CrossSiteScripting.lesson/6
  Message: Try it! Reflected XSS
  Objective: Perform a Reflect XSS on user input
  Evidence: Shopping Cart table with multiple user input
  """
  
Scenario Outline: Flaw detection
User input not sanitized can lead to non intended behavior
  Given an specific payload to inject
  """
  "<script>alert('my javascript here')</script>"
  """
  When I submit the payload onto an <input> form which susceptible to XSS
  Then I take advantage of input not sanitized on server side
  And I get this:
  """
  well done, but alerts aren't very impressive are they? Please continue.
  """