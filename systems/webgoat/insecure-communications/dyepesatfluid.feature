# language: en

Feature: Insecure Communications
  From WebGoat Project
  From Cross Site Scripting category
  With my username Skhorn
 
Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am running WebGoat 8.0 in Docker container webgoat/webgoat-8.0:
  """
    JVM with openjdk:8
  """
  And I am using Firefox 52.7.3 (64-bit)
  And I am using BurpSuit Community Edition v1.7.30
  Given a Java web site with a login form
  """
  URL: /WebGoat/start.mvc#lesson/InsecureLogin.lesson/1
  Message: Insecure Login
  Objective: Retrieve login credentials
  Evidence: Login form
  """
  
Scenario Outline: Flaw detection
Login credentials sent via request 
  Given a login form
  When I fireup Burp
  And configure the proxy to incerpt requests or responses
  When I press the login button
  And I check the intercepted request on Burp
  Then I should see in plain text the login credentials