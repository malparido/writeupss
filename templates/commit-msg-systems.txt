################################################################################
#### Commit template for solutions to challenges in training/systems only ######
#### Version 1.1.2 #############################################################
################################################################################
#
# Fill the fields in angle brackets <> as indicated in these comments.
# These comments will not appear in the final commit message.
# <site-name> and <vuln-code> must conform to repository naming conventions
# example: sltn(sys): bwapp, A9-buffer-overflow-local
# The line after the commit title and the final line MUST be blank,
# but there must not be a blank line at the beggining.
#
####### begin template #######
sltn(sys): #0 <system-name>, <vulnerability name>

- discovered vulnerabilities: X by me, Y already in repo, X+Y total.
- total estimated vulnerabilities in system:   N
- discovery percentage:                        D%
- effort: X hours during <challenges/immersion>.

######## end template ########
#
# The folder containing your solution must be named as <vulnerability name>
# X is the ACCUMULATED number of vulnerabilites in this system discovered by you
# Y is the ACCUMULATED number of vulnerabilites already reported in the repo,
# including those previously reported by you
# N is the estimated number of vulnerabilities in the system. This number
# could be in the official documentation of the system or a site like VulnHub.
# D is (X+Y)*100/N, with at most two decimals.
# Effort is the number of hours you worked on the vulnerability,
# not necessarily in one sitting. For example if you dedicated two hours today
# and later 20 minutes, and three more hours the day after, that's
# 5 hours and 20 minutes = 5.3 hours.
# Effort report must cover also the time required
# for searching existing solutions (OTHERS),
# as well as linting and compiling time before sending the MR.
# If the MR gets rejected, additional time used on fixing it has to be added
#
# Full example:
# sltn(sys): #0 gruyere, elevation-of-privilege
#
# - discovered vulnerabilities: 2 by me, 30 already in repo, 32 total.
# - total estimated vulnerabilities in system:   110
# - discovery percentage:                       29.1%
# - effort: 6.5 hours during challenges.
#
# Next time you find a vulnerability in the same system,
# you will have 3 by me, 32 already, and so on.
#
# To use this template automatically every time you commit, run
# $ git config --local commit.template templates/commit-msg-systems.txt
#
# Remember to make ONE commit for each challenge solution.
# This commit must also include the appropriate number of OTHERS solutions,
# which is currently 10.
# Then make ONE Merge Request with that ONE commit for ONE challenge,
# but ONLY after the pipeline has succeeded.
# See https://fluidattacks.com/web/es/empleos/retos-tecnicos/#envio.
