#!/bin/bash

#shellcheck disable=SC2154
#shellcheck disable=SC2086
#shellcheck disable=SC2044

print_help() {
  cat << EOF

This command replaces the name of feature files and evidence folders made by
users whose username changed.

It takes the following inputs:
./replace.sh [path-to-writeups-repo] [old-username] [new-username]

Example:
./replace.sh /home/dsalazar/writeups podany270895 dsalazaratfluid

Output:
All feature files and evidences with name podany270895 will be renamed to
dsalazaratfluid
EOF
}

if [[ -z $2 || -z $3 ]]; then
  echo "Error: You must provide old and new usernames"
  print_help
  exit 1
fi

if [[ ! -d $1 ]]; then
  echo "Error: Invalid repo directory. Please provide path to writeups repo."
  print_help
  exit 1
fi

cd "$1" || exit 1
for feature in $(find ./systems -name "$2"".feature"); do
  mv $feature ${feature/$2/$3}
done
for evidence in $(find ./systems -type d -name "$2"); do
  mv $evidence ${evidence/$2/$3}
done
